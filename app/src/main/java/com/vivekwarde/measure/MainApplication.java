package com.vivekwarde.measure;

import android.app.Application;
import android.preference.PreferenceManager;
import android.support.v7.mediarouter.BuildConfig;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.SoundUtility;

public class MainApplication extends Application {
    public static final String ADMOB_TEST_DEVICE_IDS = "test";
    public static final String BANNER_AD_UNIT_ID = "ca-app-pub-7271677424286350/9310908626";
    public static final String GOOGLE_ANALYTICS_TRACKING_ID = "UA-54940125-35";
    public static final String INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-7271677424286350/1787641821";
    public static final String TAG = "MultiMeasures";
    public static final int mToolsNavigatedNumToShowAd = 1;
    public static boolean mIsAltimeterUnlocked = false;
    public static boolean mIsBarometerUnlocked = false;
    public static boolean mIsRemoveAds = false;
    public static String mLicenseKey = null;
    public static int mToolsNavigatedCount = 0;
    private static GoogleAnalytics mAnalytics = null;
    private static Tracker mTracker;

    static {
        mIsRemoveAds = false;
        mIsAltimeterUnlocked = false;
        mIsBarometerUnlocked = false;
        mToolsNavigatedCount = mToolsNavigatedNumToShowAd;
        mLicenseKey = BuildConfig.FLAVOR;
    }

    public static GoogleAnalytics getAnalytics() {
        return mAnalytics;
    }

    public static Tracker getTracker() {
        return mTracker;
    }

    public void onCreate() {
        super.onCreate();
        initSounds();
        initGoogleAnalytics();
        mIsRemoveAds = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_IS_PREMIUM, false);
        mIsAltimeterUnlocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_IS_ALTIMETER_UNLOCKED, false);
        mIsBarometerUnlocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_IS_BAROMETER_UNLOCKED, false);
    }

    private void initSounds() {
        SoundUtility.getInstance().initSounds(getApplicationContext());
        SoundUtility.getInstance().loadAllSounds();
    }

    private void initGoogleAnalytics() {
        mLicenseKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv4JMsgjR+OfTwoUMDSPY2HQnw1PSKKJNf9YfUahDM5NoF7jRPgnoy2o0+AyNRbXOHWic4WuynezGsCDIGrIe0njVgUE+9HRzuTdFSMrS9gIOkopNNBP59zitXUOvhuXeUoM+QA4Y1n9+UqgG0kfCRhIjEi9tfAPHFTTWzEX4jVQQLDOduPKq6StmJDYchUHlIynkMtomynKNIPKWcMDpe3RzRsSbZXGBA5NYA/rpVT4liRWbpPSbfeyDMMYiFwKPMDq+GDZQdmvgRiTjFmrxaH22ezMzV6gOOCAjwIbrBFMXvCRX0t61KXI1jEID7ZhTOcoan10DLovKv52+wHwbbwIDAQAB";
//        mLicenseKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo9gEyyeOoHAk6Oo" + "dvxyE7UBLIbY+bI08KDJDvXCPJwUVLSgkpEKeQI5KErIDaXKp8IonhcZED" + "kyI5bN5eRNyp/JVhm7szvbkn6XFzGMeXRvA/QBMe+Uu+HTjbAY75gO8B+p" + "Rg1vjc4DAafgHRCMo9Avahsdn34Gb4PRzuq8KVN7FAGLC20TSnuY2/lIOW" + "CrohlJl8OXinQQG5/CYZ9UDa7NPpfo5IvQpg7aTs/ucesLWHgXN8Bt5p9g" + "AHn5bAPD20OOJlLjH0S+T4VwgaiQQBVAyZy6kfv2N4xvwyGbb1Jozr7wF8" + "q72bMIj0tI1WQFSkYZLnnIoYhSG3nxiwSPbAQIDAQAB" + "test";
        mAnalytics = GoogleAnalytics.getInstance(this);
        mTracker = mAnalytics.newTracker(GOOGLE_ANALYTICS_TRACKING_ID);
        mTracker.enableExceptionReporting(true);
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.enableAutoActivityTracking(true);
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public void onTerminate() {
        SoundUtility.getInstance().cleanup();
        super.onTerminate();
    }
}
