package com.vivekwarde.measure.altimeter;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.search.SearchAuth.StatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;

import java.util.Random;

public class AltimeterActivity extends BaseActivity implements OnClickListener, LocationListener, SensorEventListener {
    public static final int MENU_BUTTON_ID = 1;
    public static final int SETTINGS_BUTTON_ID = 2;
    public static final int UPGRADE_BUTTON_ID = 0;
    protected static final int ALTITUDE_TEXT_ID = 12;
    protected static final int ALTITUDE_UNIT_TEXT_ID = 13;
    protected static final int ALTITUDE_VARIANCE_TEXT_ID = 14;
    protected static final int BASE_IMAGE_ID = 3;
    protected static final int FACE_IMAGE_ID = 4;
    protected static final int FACE_SCALER_IMAGE_ID = 6;
    protected static final int HAND_IMAGE_ID = 7;
    protected static final int LABEL_LATITUDE_TEXT_ID = 15;
    protected static final int LABEL_LONGITUDE_TEXT_ID = 17;
    protected static final int LED_SCREEN_LEFT_IMAGE_ID = 10;
    protected static final int LED_SCREEN_RIGHT_IMAGE_ID = 11;
    protected static final int OUTPUT_LATITUDE_TEXT_ID = 16;
    protected static final int OUTPUT_LONGITUDE_TEXT_ID = 18;
    protected static final int RING_IMAGE_ID = 5;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BTW_UPDATES = 60000;
    private static final String[] PERMISSION_LOCATION;
    private static final int REQUEST_LOCATION = 0;

    static {
        String[] strArr = new String[MENU_BUTTON_ID];
        strArr[REQUEST_LOCATION] = "android.permission.ACCESS_FINE_LOCATION";
        PERMISSION_LOCATION = strArr;
    }

    protected Runnable mFakePressureSensorTask;
    protected Handler mFakePressureSensorTimerHandler;
    protected LocationManager mLocationManager;
    private TextView mAltitudeText;
    private TextView mAltitudeUnitText;
    private SPScale9ImageView mBaseImage;
    private float mCurHandAngle;
    private Location mCurLocation;
    private float mCurScalerAngle;
    private int mCurrentScaler;
    private ImageView mFaceImage;
    private ImageView mFaceScalerImage;
    private TextView mGetModeText;
    private ImageView mHandImage;
    private boolean mIsFakeSensor;
    private TextView mLabelLatitudeText;
    private TextView mLabelLongitudeText;
    private ImageView mLedScreenLeftImage;
    private ImageView mLedScreenRightImage;
    private SPImageButton mMenuButton;
    private TextView mOutputLatitudeText;
    private TextView mOutputLongitudeText;
    private Sensor mPressureSensor;
    private ImageView mRingImage;
    private SensorManager mSensorManager;
    private SPImageButton mSettingsButton;

    public AltimeterActivity() {
        this.mMenuButton = null;
        this.mSettingsButton = null;
        this.mBaseImage = null;
        this.mFaceImage = null;
        this.mRingImage = null;
        this.mFaceScalerImage = null;
        this.mHandImage = null;
        this.mLedScreenLeftImage = null;
        this.mLedScreenRightImage = null;
        this.mAltitudeText = null;
        this.mAltitudeUnitText = null;
        this.mGetModeText = null;
        this.mCurLocation = null;
        this.mIsFakeSensor = false;
        this.mCurrentScaler = MENU_BUTTON_ID;
        this.mCurHandAngle = 0.0f;
        this.mCurScalerAngle = 0.0f;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(MENU_BUTTON_ID);
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        initSensor();
        initLocationService();
        createUi();
        this.mIsFakeSensor = false;
        if (this.mIsFakeSensor) {
            this.mFakePressureSensorTimerHandler = new Handler();
            this.mFakePressureSensorTask = new Runnable() {
                public void run() {
                    AltimeterActivity.this.updateAltitude((float) new Random().nextInt(StatusCodes.AUTH_DISABLED));
                    AltimeterActivity.this.mFakePressureSensorTimerHandler.postDelayed(this, 2500);
                }
            };
        }
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    @SuppressLint({"NewApi"})
    private void createUi() {
        this.mMainLayout = new RelativeLayout(this);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < OUTPUT_LATITUDE_TEXT_ID) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                AltimeterActivity.this.arrangeLayout();
                if (VERSION.SDK_INT >= AltimeterActivity.OUTPUT_LATITUDE_TEXT_ID) {
                    AltimeterActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    AltimeterActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(BASE_IMAGE_ID, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mMainLayout.addView(this.mUiLayout);
        this.mBaseImage = new SPScale9ImageView(this);
        this.mBaseImage.setId(BASE_IMAGE_ID);
        this.mBaseImage.setBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap());
        this.mUiLayout.addView(this.mBaseImage);
        this.mFaceScalerImage = new ImageView(this);
        this.mFaceScalerImage.setId(FACE_SCALER_IMAGE_ID);
        this.mFaceScalerImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.altimeter_face_scaler)).getBitmap());
        this.mUiLayout.addView(this.mFaceScalerImage);
        this.mFaceImage = new ImageView(this);
        this.mFaceImage.setId(FACE_IMAGE_ID);
        this.mFaceImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.altimeter_face_markings)).getBitmap());
        this.mUiLayout.addView(this.mFaceImage);
        this.mHandImage = new ImageView(this);
        this.mHandImage.setId(HAND_IMAGE_ID);
        this.mHandImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.altimeter_hand)).getBitmap());
        this.mUiLayout.addView(this.mHandImage);
        this.mRingImage = new ImageView(this);
        this.mRingImage.setId(RING_IMAGE_ID);
        this.mRingImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.altimeter_rim)).getBitmap());
        this.mRingImage.setScaleType(ScaleType.CENTER);
        this.mUiLayout.addView(this.mRingImage);
        int ledScreenWidth = (mScreenWidth - (((int) getResources().getDimension(R.dimen.ALTIMETER_HORZ_MARGIN_BTW_LEFT_LED_SCREEN_AND_LEFT_BASE)) * BASE_IMAGE_ID)) / SETTINGS_BUTTON_ID;
        this.mLedScreenLeftImage = new ImageView(this);
        this.mLedScreenLeftImage.setId(LED_SCREEN_LEFT_IMAGE_ID);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        Bitmap scale9bitmap = ImageUtility.scale9Bitmap(bitmap, ledScreenWidth, (bitmap.getHeight() - 2) * SETTINGS_BUTTON_ID);
        this.mLedScreenLeftImage.setImageBitmap(scale9bitmap);
        this.mUiLayout.addView(this.mLedScreenLeftImage);
        this.mLedScreenRightImage = new ImageView(this);
        this.mLedScreenRightImage.setId(LED_SCREEN_RIGHT_IMAGE_ID);
        this.mLedScreenRightImage.setImageBitmap(scale9bitmap);
        this.mUiLayout.addView(this.mLedScreenRightImage);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mAltitudeText = new TextView(this);
        this.mAltitudeText.setId(ALTITUDE_TEXT_ID);
        this.mAltitudeText.setTypeface(face);
        this.mAltitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.ALTIMETER_OUTPUT_CARDINAL_FONT_SIZE));
        this.mAltitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mAltitudeText.setPaintFlags(this.mAltitudeText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mAltitudeText);
        this.mAltitudeText.setText("N/A");
        this.mAltitudeUnitText = new TextView(this);
        this.mAltitudeUnitText.setId(ALTITUDE_UNIT_TEXT_ID);
        this.mAltitudeUnitText.setTypeface(face);
        this.mAltitudeUnitText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.ALTIMETER_OUTPUT_CARDINAL_UNIT_FONT_SIZE));
        this.mAltitudeUnitText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mAltitudeUnitText.setPaintFlags(this.mAltitudeUnitText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mAltitudeUnitText);
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_ALTIMETER_UNIT_KEY, "0").equals("0")) {
            this.mAltitudeUnitText.setText("m");
        } else {
            this.mAltitudeUnitText.setText("ft");
        }
        this.mGetModeText = new TextView(this);
        this.mGetModeText.setId(ALTITUDE_UNIT_TEXT_ID);
        this.mGetModeText.setTypeface(face);
        this.mGetModeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.ALTIMETER_OUTPUT_NOMINAL_FONT_SIZE));
        this.mGetModeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mGetModeText.setPaintFlags(this.mGetModeText.getPaintFlags() | Barcode.ITF);
        this.mGetModeText.setGravity(MENU_BUTTON_ID);
        this.mUiLayout.addView(this.mGetModeText);
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY, "0").equals("0")) {
            this.mGetModeText.setText(getResources().getString(R.string.IDS_PRESSURE_SENSOR));
        } else {
            this.mGetModeText.setText(getResources().getString(R.string.IDS_GPS_AND_WIFI));
        }
        this.mLabelLatitudeText = new TextView(this);
        this.mLabelLatitudeText.setId(ALTITUDE_VARIANCE_TEXT_ID);
        this.mLabelLatitudeText.setTypeface(face);
        this.mLabelLatitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.ALTIMETER_LABEL_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mLabelLatitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelLatitudeText.setPaintFlags(this.mLabelLatitudeText.getPaintFlags() | Barcode.ITF);
        ViewHelper.setAlpha(this.mLabelLatitudeText, 0.5f);
        this.mUiLayout.addView(this.mLabelLatitudeText);
        this.mLabelLatitudeText.setText(getResources().getString(R.string.IDS_LATITUDE));
        this.mOutputLatitudeText = new TextView(this);
        this.mOutputLatitudeText.setId(OUTPUT_LATITUDE_TEXT_ID);
        this.mOutputLatitudeText.setTypeface(face);
        this.mOutputLatitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.ALTIMETER_OUTPUT_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mOutputLatitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputLatitudeText.setPaintFlags(this.mOutputLatitudeText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputLatitudeText);
        this.mOutputLatitudeText.setText("N/A");
        this.mLabelLongitudeText = new TextView(this);
        this.mLabelLongitudeText.setId(LABEL_LONGITUDE_TEXT_ID);
        this.mLabelLongitudeText.setTypeface(face);
        this.mLabelLongitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.ALTIMETER_LABEL_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mLabelLongitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelLongitudeText.setPaintFlags(this.mLabelLongitudeText.getPaintFlags() | Barcode.ITF);
        ViewHelper.setAlpha(this.mLabelLongitudeText, 0.5f);
        this.mUiLayout.addView(this.mLabelLongitudeText);
        this.mLabelLongitudeText.setText(getResources().getString(R.string.IDS_LONGITUDE));
        this.mOutputLongitudeText = new TextView(this);
        this.mOutputLongitudeText.setId(OUTPUT_LONGITUDE_TEXT_ID);
        this.mOutputLongitudeText.setTypeface(face);
        this.mOutputLongitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.ALTIMETER_OUTPUT_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mOutputLongitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputLongitudeText.setPaintFlags(this.mOutputLongitudeText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputLongitudeText);
        this.mOutputLongitudeText.setText("N/A");
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(MENU_BUTTON_ID);
        this.mMenuButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap());
        this.mMenuButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mMenuButton);
        this.mSettingsButton = new SPImageButton(this);
        this.mSettingsButton.setId(SETTINGS_BUTTON_ID);
        this.mSettingsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mSettingsButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mSettingsButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(REQUEST_LOCATION);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? View.GONE : View.VISIBLE);
        this.mUiLayout.addView(this.mNoAdsButton);
    }

    private void arrangeLayout() {
        int i = REQUEST_LOCATION;
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(LED_SCREEN_LEFT_IMAGE_ID);
        params.addRule(LED_SCREEN_RIGHT_IMAGE_ID);
        params.topMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / SETTINGS_BUTTON_ID;
        this.mMenuButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(FACE_SCALER_IMAGE_ID, this.mMenuButton.getId());
        params.addRule(REQUEST_LOCATION, this.mMenuButton.getId());
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / SETTINGS_BUTTON_ID;
        this.mSettingsButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(LED_SCREEN_LEFT_IMAGE_ID);
        params.addRule(9);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), REQUEST_LOCATION);
        this.mNoAdsButton.setLayoutParams(params);
        int headerBarHeight = this.mMenuButton.getHeight() + (((int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN)) * SETTINGS_BUTTON_ID);
        params = new LayoutParams(-1, -1);
        params.topMargin = headerBarHeight;
        this.mBaseImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(ALTITUDE_VARIANCE_TEXT_ID);
        params.addRule(8, this.mBaseImage.getId());
        int i2 = mScreenHeight;
        if (this.mBannerAdView != null) {
            i = this.mBannerAdView.getHeight();
        }
        params.bottomMargin = (((((i2 - i) - this.mRingImage.getHeight()) - headerBarHeight) - this.mLedScreenLeftImage.getHeight()) - ((int) getResources().getDimension(R.dimen.ALTIMETER_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE))) / SETTINGS_BUTTON_ID;
        this.mRingImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mRingImage.getId());
        params.leftMargin = (this.mRingImage.getWidth() - this.mFaceScalerImage.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(FACE_SCALER_IMAGE_ID, this.mRingImage.getId());
        params.topMargin = (this.mRingImage.getHeight() - this.mFaceScalerImage.getHeight()) / SETTINGS_BUTTON_ID;
        this.mFaceScalerImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mRingImage.getId());
        params.leftMargin = (this.mRingImage.getWidth() - this.mFaceImage.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(FACE_SCALER_IMAGE_ID, this.mRingImage.getId());
        params.topMargin = (this.mRingImage.getHeight() - this.mFaceImage.getHeight()) / SETTINGS_BUTTON_ID;
        this.mFaceImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mRingImage.getId());
        params.leftMargin = (this.mRingImage.getWidth() - this.mHandImage.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(FACE_SCALER_IMAGE_ID, this.mRingImage.getId());
        params.topMargin = (this.mRingImage.getHeight() - this.mHandImage.getHeight()) / SETTINGS_BUTTON_ID;
        this.mHandImage.setLayoutParams(params);
        int ledscreenLeftAndRightMargin = (mScreenWidth - (this.mLedScreenLeftImage.getWidth() * SETTINGS_BUTTON_ID)) / BASE_IMAGE_ID;
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mBaseImage.getId());
        params.leftMargin = ledscreenLeftAndRightMargin;
        params.addRule(FACE_SCALER_IMAGE_ID, this.mBaseImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.ALTIMETER_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE);
        this.mLedScreenLeftImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HAND_IMAGE_ID, this.mBaseImage.getId());
        params.rightMargin = ledscreenLeftAndRightMargin;
        params.addRule(FACE_SCALER_IMAGE_ID, this.mBaseImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.ALTIMETER_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE);
        this.mLedScreenRightImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.leftMargin = (((this.mLedScreenLeftImage.getWidth() - this.mAltitudeText.getWidth()) - ((int) getResources().getDimension(R.dimen.ALTIMETER_HORZ_MARGIN_BTW_RIGHT_OUTPUT_CARDINAL_AND_LEFT_ITS_UNIT))) - this.mAltitudeUnitText.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(FACE_SCALER_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.topMargin = (((this.mLedScreenLeftImage.getHeight() - this.mAltitudeText.getHeight()) - ((int) getResources().getDimension(R.dimen.ALTIMETER_VERT_MARGIN_BTW_BOTTOM_OUTPUT_CARDINAL_AND_TOP_OUTPUT_NOMINAL))) - this.mAltitudeUnitText.getHeight()) / SETTINGS_BUTTON_ID;
        this.mAltitudeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(MENU_BUTTON_ID, this.mAltitudeText.getId());
        params.addRule(FACE_IMAGE_ID, this.mAltitudeText.getId());
        params.leftMargin = (int) getResources().getDimension(R.dimen.ALTIMETER_HORZ_MARGIN_BTW_RIGHT_OUTPUT_CARDINAL_AND_LEFT_ITS_UNIT);
        this.mAltitudeUnitText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.addRule(HAND_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.addRule(8, this.mLedScreenLeftImage.getId());
        params.bottomMargin = (((this.mLedScreenLeftImage.getHeight() - this.mAltitudeText.getHeight()) - ((int) getResources().getDimension(R.dimen.ALTIMETER_VERT_MARGIN_BTW_BOTTOM_OUTPUT_CARDINAL_AND_TOP_OUTPUT_NOMINAL))) - this.mGetModeText.getHeight()) / SETTINGS_BUTTON_ID;
        this.mGetModeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HAND_IMAGE_ID, this.mLedScreenRightImage.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.ALTIMETER_HORZ_MARGIN_BTW_RIGHT_LABEL_LATITUDE_AND_RIGHT_LED_SCREEN);
        params.addRule(FACE_SCALER_IMAGE_ID, this.mLedScreenRightImage.getId());
        params.topMargin = (((this.mLedScreenRightImage.getHeight() - (this.mLabelLatitudeText.getHeight() * SETTINGS_BUTTON_ID)) - ((int) getResources().getDimension(R.dimen.ALTIMETER_VERT_MARGIN_BTW_BOTTOM_OUTPUT_LATITUDE_AND_TOP_LABEL_LONGITUDE))) - (this.mOutputLatitudeText.getHeight() * SETTINGS_BUTTON_ID)) / SETTINGS_BUTTON_ID;
        this.mLabelLatitudeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HAND_IMAGE_ID, this.mLabelLatitudeText.getId());
        params.addRule(BASE_IMAGE_ID, this.mLabelLatitudeText.getId());
        this.mOutputLatitudeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HAND_IMAGE_ID, this.mOutputLatitudeText.getId());
        params.addRule(BASE_IMAGE_ID, this.mOutputLatitudeText.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.ALTIMETER_VERT_MARGIN_BTW_BOTTOM_OUTPUT_LATITUDE_AND_TOP_LABEL_LONGITUDE);
        this.mLabelLongitudeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HAND_IMAGE_ID, this.mLabelLongitudeText.getId());
        params.addRule(BASE_IMAGE_ID, this.mLabelLongitudeText.getId());
        this.mOutputLongitudeText.setLayoutParams(params);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
        this.mSensorManager.unregisterListener(this);
        if (this.mIsFakeSensor) {
            this.mFakePressureSensorTimerHandler.removeCallbacks(this.mFakePressureSensorTask);
        }
    }

    protected void onResume() {
        super.onResume();
        String getMethod = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY, "0");
        if (getMethod.equals("0")) {
            this.mSensorManager.registerListener(this, this.mPressureSensor, BASE_IMAGE_ID);
        }
        startLocationUpdates();
        if (this.mIsFakeSensor) {
            this.mFakePressureSensorTimerHandler.removeCallbacks(this.mFakePressureSensorTask);
            this.mFakePressureSensorTimerHandler.postDelayed(this.mFakePressureSensorTask, 2500);
        }
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_ALTIMETER_UNIT_KEY, "0").equals("0")) {
            this.mAltitudeUnitText.setText("m");
        } else {
            this.mAltitudeUnitText.setText("ft");
        }
        if (getMethod.equals("0")) {
            this.mGetModeText.setText("Pressure Sensor");
        } else {
            this.mGetModeText.setText("GPS");
        }
    }

    public void onClick(View source) {
        if (source.getId() == MENU_BUTTON_ID) {
            onButtonMenu();
        } else if (source.getId() == SETTINGS_BUTTON_ID) {
            onButtonSettings();
        } else if (source.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    void initSensor() {
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mPressureSensor = this.mSensorManager.getDefaultSensor(FACE_SCALER_IMAGE_ID);
        if (this.mPressureSensor == null) {
            Toast.makeText(this, "You device does not have pressure sensor!", MENU_BUTTON_ID).show();
        }
    }

    void initLocationService() {
        if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
            requestLocationPermission();
        } else {
            initLocManager();
        }
    }

    private void requestLocationPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.ACCESS_FINE_LOCATION")) {
            ActivityCompat.requestPermissions(this, PERMISSION_LOCATION, REQUEST_LOCATION);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != 0) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else if (grantResults.length == MENU_BUTTON_ID && grantResults[REQUEST_LOCATION] == 0) {
            initLocManager();
        }
    }

    public void initLocManager() {
        this.mLocationManager = (LocationManager) getSystemService("location");
        if (!this.mLocationManager.isProviderEnabled("network") && !this.mLocationManager.isProviderEnabled("gps")) {
            Toast.makeText(this, "No location services", MENU_BUTTON_ID).show();
        } else if (VERSION.SDK_INT < 23) {
            try {
                this.mLocationManager.requestLocationUpdates("network", 0, 0.0f, this);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            try {
                this.mLocationManager.requestLocationUpdates("gps", 0, 0.0f, this);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            }
        } else if (checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            try {
                this.mLocationManager.requestLocationUpdates("network", 0, 0.0f, this);
            } catch (IllegalArgumentException e22) {
                e22.printStackTrace();
            }
            try {
                this.mLocationManager.requestLocationUpdates("gps", 0, 0.0f, this);
            } catch (IllegalArgumentException e222) {
                e222.printStackTrace();
            }
        }
    }

    public void onLocationChanged(Location location) {
        this.mCurLocation = location;
        this.mOutputLatitudeText.setText(Location.convert(this.mCurLocation.getLatitude(), SETTINGS_BUTTON_ID));
        this.mOutputLongitudeText.setText(Location.convert(this.mCurLocation.getLongitude(), SETTINGS_BUTTON_ID));
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY, "0").equals("1")) {
            updateAltitude((float) this.mCurLocation.getAltitude());
        }
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void launchLocationSettings() {
        Builder alertDialog = new Builder(this);
        alertDialog.setTitle(getString(R.string.IDS_INFORMATION));
        alertDialog.setMessage(getString(R.string.IDS_LOCATION_SERVICES_ASKING));
        alertDialog.setPositiveButton(getString(R.string.IDS_SETTINGS), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AltimeterActivity.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        });
        alertDialog.setNegativeButton(getString(R.string.IDS_CANCEL), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        MiscUtility.setDialogFontSize(this, alertDialog.show());
    }

    private void startLocationUpdates() {
        if (this.mLocationManager != null) {
            boolean isGPSEnabled = this.mLocationManager.isProviderEnabled("gps");
            boolean isNetworkEnabled = this.mLocationManager.isProviderEnabled("network");
            if (isGPSEnabled || isNetworkEnabled) {
                if (isNetworkEnabled) {
                    this.mLocationManager.requestLocationUpdates("network", MIN_TIME_BTW_UPDATES, 10.0f, this);
                    if (this.mLocationManager != null) {
                        try {
                            this.mCurLocation = this.mLocationManager.getLastKnownLocation("network");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (isGPSEnabled && this.mCurLocation == null) {
                    this.mLocationManager.requestLocationUpdates("gps", MIN_TIME_BTW_UPDATES, 10.0f, this);
                    if (this.mLocationManager != null) {
                        try {
                            this.mCurLocation = this.mLocationManager.getLastKnownLocation("gps");
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                    return;
                }
                return;
            }
            launchLocationSettings();
        }
    }

    private void stopLocationUpdates() {
        if (this.mLocationManager != null) {
            this.mLocationManager.removeUpdates(this);
        }
    }

    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public final void onSensorChanged(SensorEvent event) {
        if (FACE_SCALER_IMAGE_ID == event.sensor.getType()) {
            float altitude = SensorManager.getAltitude(1013.25f, event.values[REQUEST_LOCATION]);
            if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY, "0").equals("0")) {
                updateAltitude(altitude);
            }
        }
    }

    private void updateAltitude(float newAltitudeInMeter) {
        float newAltitude = newAltitudeInMeter;
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_ALTIMETER_UNIT_KEY, "0").equals("1")) {
            newAltitude = (float) (((double) newAltitude) * 3.28084d);
        }
        if (this.mAltitudeText != null) {
            this.mAltitudeText.setText(BuildConfig.FLAVOR + ((int) newAltitude));
        }
        int scaler = MENU_BUTTON_ID;
        float scalerAngle = 0.0f;
        int altitude = (int) Math.abs(newAltitude);
        if (altitude < LED_SCREEN_LEFT_IMAGE_ID) {
            scaler = MENU_BUTTON_ID;
            scalerAngle = 18.72f * 4.0f;
        } else if (altitude < 100) {
            scaler = LED_SCREEN_LEFT_IMAGE_ID;
            scalerAngle = 18.72f * 3.0f;
        } else if (altitude < GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE) {
            scaler = 100;
            scalerAngle = 18.72f * 2.0f;
        } else if (altitude < StatusCodes.AUTH_DISABLED) {
            scaler = GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE;
            scalerAngle = 18.72f;
        } else if (altitude < 100000) {
            scaler = StatusCodes.AUTH_DISABLED;
            scalerAngle = 0.0f;
        } else if (altitude < 1000000) {
            scaler = 100000;
            scalerAngle = 0.0f;
        }
        if (scaler != this.mCurrentScaler) {
            this.mCurrentScaler = scaler;
            RotateAnimation rotateScalerAnim = new RotateAnimation(this.mCurScalerAngle, scalerAngle, MENU_BUTTON_ID, 0.5f, MENU_BUTTON_ID, 0.5f);
            rotateScalerAnim.setDuration(200);
            rotateScalerAnim.setFillAfter(true);
            if (this.mFaceScalerImage != null) {
                this.mFaceScalerImage.startAnimation(rotateScalerAnim);
            }
            this.mCurScalerAngle = scalerAngle;
        }
        float angle = (float) Math.IEEEremainder((double) ((Math.abs(newAltitude) / ((float) this.mCurrentScaler)) * 36.0f), 360.0d);
        RotateAnimation rotateAnim = new RotateAnimation(this.mCurHandAngle, angle, MENU_BUTTON_ID, 0.5f, MENU_BUTTON_ID, 0.5f);
        rotateAnim.setDuration(200);
        rotateAnim.setFillAfter(true);
        if (this.mHandImage != null) {
            this.mHandImage.startAnimation(rotateAnim);
        }
        this.mCurHandAngle = angle;
    }
}
