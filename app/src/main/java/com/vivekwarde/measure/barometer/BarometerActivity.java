package com.vivekwarde.measure.barometer;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.vision.barcode.Barcode;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.utilities.ImageUtility;

import java.util.Locale;
import java.util.Random;

public class BarometerActivity extends BaseActivity implements OnClickListener, SensorEventListener {
    public static final int MENU_BUTTON_ID = 1;
    public static final int SETTINGS_BUTTON_ID = 2;
    public static final int UPGRADE_BUTTON_ID = 0;
    protected static final int BAROMETER_TEXT_ID = 12;
    protected static final int BAROMETER_UNIT_TEXT_ID = 13;
    protected static final int BASE_IMAGE_ID = 3;
    protected static final int FACE_IMAGE_ID = 4;
    protected static final int HAND_IMAGE_ID = 7;
    protected static final int LED_SCREEN_IMAGE_ID = 10;
    protected static final int RING_IMAGE_ID = 5;
    protected Runnable mFakePressureSensorTask;
    protected Handler mFakePressureSensorTimerHandler;
    private TextView mBarometerText;
    private TextView mBarometerUnitText;
    private SPScale9ImageView mBaseImage;
    private float mCurHandAngle;
    private ImageView mFaceImage;
    private ImageView mHandImage;
    private boolean mIsFakeSensor;
    private ImageView mLedScreenImage;
    private SPImageButton mMenuButton;
    private Sensor mPressureSensor;
    private ImageView mRingImage;
    private SensorManager mSensorManager;
    private SPImageButton mSettingsButton;

    public BarometerActivity() {
        this.mMenuButton = null;
        this.mSettingsButton = null;
        this.mBaseImage = null;
        this.mFaceImage = null;
        this.mRingImage = null;
        this.mHandImage = null;
        this.mLedScreenImage = null;
        this.mBarometerText = null;
        this.mBarometerUnitText = null;
        this.mIsFakeSensor = false;
        this.mCurHandAngle = 0.0f;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(MENU_BUTTON_ID);
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        initSensor();
        createUi();
        this.mIsFakeSensor = false;
        if (this.mIsFakeSensor) {
            this.mFakePressureSensorTimerHandler = new Handler();
            this.mFakePressureSensorTask = new Runnable() {
                public void run() {
                    BarometerActivity.this.updatePressure((new Random().nextFloat() * BitmapDescriptorFactory.HUE_GREEN) + 960.0f);
                    BarometerActivity.this.mFakePressureSensorTimerHandler.postDelayed(this, 2500);
                }
            };
        }
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    @SuppressLint({"NewApi"})
    private void createUi() {
        int i;
        this.mMainLayout = new RelativeLayout(this);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                BarometerActivity.this.arrangeLayout();
                if (VERSION.SDK_INT >= 16) {
                    BarometerActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    BarometerActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(BASE_IMAGE_ID, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mMainLayout.addView(this.mUiLayout);
        this.mBaseImage = new SPScale9ImageView(this);
        this.mBaseImage.setId(BASE_IMAGE_ID);
        this.mBaseImage.setBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap());
        this.mUiLayout.addView(this.mBaseImage);
        this.mFaceImage = new ImageView(this);
        this.mFaceImage.setId(FACE_IMAGE_ID);
        this.mFaceImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.barometer_face)).getBitmap());
        this.mUiLayout.addView(this.mFaceImage);
        this.mHandImage = new ImageView(this);
        this.mHandImage.setId(HAND_IMAGE_ID);
        this.mHandImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.barometer_hand)).getBitmap());
        this.mUiLayout.addView(this.mHandImage);
        this.mRingImage = new ImageView(this);
        this.mRingImage.setId(RING_IMAGE_ID);
        this.mRingImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.barometer_ring)).getBitmap());
        this.mRingImage.setScaleType(ScaleType.CENTER);
        this.mUiLayout.addView(this.mRingImage);
        int ledScreenWidth = mScreenWidth - (((int) getResources().getDimension(R.dimen.BAROMETER_HORZ_MARGIN_BTW_EDGE_LED_SCREEN_AND_EDGE_BASE)) * SETTINGS_BUTTON_ID);
        this.mLedScreenImage = new ImageView(this);
        this.mLedScreenImage.setId(LED_SCREEN_IMAGE_ID);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        this.mLedScreenImage.setImageBitmap(ImageUtility.scale9Bitmap(bitmap, ledScreenWidth, (bitmap.getHeight() - 2) * SETTINGS_BUTTON_ID));
        this.mUiLayout.addView(this.mLedScreenImage);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mBarometerText = new TextView(this);
        this.mBarometerText.setId(BAROMETER_TEXT_ID);
        this.mBarometerText.setTypeface(face);
        this.mBarometerText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.BAROMETER_PRESSURE_VALUE_FONT_SIZE));
        this.mBarometerText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mBarometerText.setPaintFlags(this.mBarometerText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mBarometerText);
        this.mBarometerText.setText("N/A");
        this.mBarometerUnitText = new TextView(this);
        this.mBarometerUnitText.setId(BAROMETER_UNIT_TEXT_ID);
        this.mBarometerUnitText.setTypeface(face);
        this.mBarometerUnitText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.BAROMETER_PRESSURE_UNIT_FONT_SIZE));
        this.mBarometerUnitText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mBarometerUnitText.setPaintFlags(this.mBarometerUnitText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mBarometerUnitText);
        this.mBarometerUnitText.setText("hPa/\nmbar");
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(MENU_BUTTON_ID);
        this.mMenuButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap());
        this.mMenuButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mMenuButton);
        this.mSettingsButton = new SPImageButton(this);
        this.mSettingsButton.setId(SETTINGS_BUTTON_ID);
        this.mSettingsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mSettingsButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mSettingsButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        SPImageButton sPImageButton = this.mNoAdsButton;
        if (MainApplication.mIsRemoveAds) {
            i = 8;
        } else {
            i = 0;
        }
        sPImageButton.setVisibility(i);
        this.mUiLayout.addView(this.mNoAdsButton);
    }

    private void arrangeLayout() {
        int i = 0;
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(LED_SCREEN_IMAGE_ID);
        params.addRule(11);
        params.topMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / SETTINGS_BUTTON_ID;
        this.mMenuButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mMenuButton.getId());
        params.addRule(0, this.mMenuButton.getId());
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / SETTINGS_BUTTON_ID;
        this.mSettingsButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(LED_SCREEN_IMAGE_ID);
        params.addRule(9);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), 0);
        this.mNoAdsButton.setLayoutParams(params);
        int headerBarHeight = this.mMenuButton.getHeight() + (((int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN)) * SETTINGS_BUTTON_ID);
        params = new LayoutParams(-1, -1);
        params.topMargin = headerBarHeight;
        this.mBaseImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(8, this.mBaseImage.getId());
        int i2 = mScreenHeight;
        if (this.mBannerAdView != null) {
            i = this.mBannerAdView.getHeight();
        }
        params.bottomMargin = (((((i2 - i) - this.mRingImage.getHeight()) - headerBarHeight) - this.mLedScreenImage.getHeight()) - ((int) getResources().getDimension(R.dimen.BAROMETER_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE))) / SETTINGS_BUTTON_ID;
        this.mRingImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mRingImage.getId());
        params.leftMargin = (this.mRingImage.getWidth() - this.mFaceImage.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(6, this.mRingImage.getId());
        params.topMargin = (this.mRingImage.getHeight() - this.mFaceImage.getHeight()) / SETTINGS_BUTTON_ID;
        this.mFaceImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(RING_IMAGE_ID, this.mRingImage.getId());
        params.leftMargin = (this.mRingImage.getWidth() - this.mHandImage.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(6, this.mRingImage.getId());
        params.topMargin = (this.mRingImage.getHeight() - this.mHandImage.getHeight()) / SETTINGS_BUTTON_ID;
        this.mHandImage.setLayoutParams(params);
        int ledscreenLeftAndRightMargin = (mScreenWidth - this.mLedScreenImage.getWidth()) / SETTINGS_BUTTON_ID;
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(6, this.mBaseImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.BAROMETER_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE);
        this.mLedScreenImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HAND_IMAGE_ID, this.mBaseImage.getId());
        params.rightMargin = ((this.mBarometerText.getWidth() + this.mBarometerUnitText.getWidth()) + ((int) getResources().getDimension(R.dimen.BAROMETER_HORZ_MARGIN_BTW_LEFT_LABEL_VALUE_AND_LEFT_UNIT))) + ((int) getResources().getDimension(R.dimen.BAROMETER_HORZ_MARGIN_BTW_RIGHT_LABEL_UNIT_AND_RIGHT_LED_SCREEN));
        params.addRule(6, this.mLedScreenImage.getId());
        params.topMargin = (this.mLedScreenImage.getHeight() - this.mBarometerText.getHeight()) / SETTINGS_BUTTON_ID;
        this.mBarometerText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HAND_IMAGE_ID, this.mLedScreenImage.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.BAROMETER_HORZ_MARGIN_BTW_RIGHT_LABEL_UNIT_AND_RIGHT_LED_SCREEN);
        params.addRule(6, this.mBarometerText.getId());
        params.topMargin = (this.mBarometerText.getHeight() - this.mBarometerUnitText.getHeight()) / SETTINGS_BUTTON_ID;
        this.mBarometerUnitText.setLayoutParams(params);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        this.mSensorManager.unregisterListener(this);
        if (this.mIsFakeSensor) {
            this.mFakePressureSensorTimerHandler.removeCallbacks(this.mFakePressureSensorTask);
        }
    }

    protected void onResume() {
        super.onResume();
        if (this.mIsFakeSensor) {
            this.mFakePressureSensorTimerHandler.removeCallbacks(this.mFakePressureSensorTask);
            this.mFakePressureSensorTimerHandler.postDelayed(this.mFakePressureSensorTask, 2500);
        }
        this.mSensorManager.registerListener(this, this.mPressureSensor, BASE_IMAGE_ID);
    }

    public void onClick(View source) {
        if (source.getId() == MENU_BUTTON_ID) {
            onButtonMenu();
        } else if (source.getId() == SETTINGS_BUTTON_ID) {
            onButtonSettings();
        } else if (source.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    void initSensor() {
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mPressureSensor = this.mSensorManager.getDefaultSensor(6);
        if (this.mPressureSensor == null) {
            Toast.makeText(this, "You device does not have pressure sensor!", MENU_BUTTON_ID).show();
        }
    }

    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public final void onSensorChanged(SensorEvent event) {
        if (6 == event.sensor.getType()) {
            updatePressure(event.values[0]);
        }
    }

    private void updatePressure(float newPressureValue) {
        Object[] objArr = new Object[MENU_BUTTON_ID];
        objArr[0] = Float.valueOf(newPressureValue);
        String sPressureValue = String.format(Locale.US, "%.2f", objArr);
        if (this.mBarometerText != null) {
            this.mBarometerText.setText(sPressureValue);
        }
        float pressureValue = newPressureValue;
        if (pressureValue < 960.0f) {
            pressureValue = 960.0f;
        } else if (pressureValue > 1080.0f) {
            pressureValue = 1080.0f;
        }
        float angle = (pressureValue - 1020.0f) * 2.0f;
        RotateAnimation rotateAnim = new RotateAnimation(this.mCurHandAngle, angle, MENU_BUTTON_ID, 0.5f, MENU_BUTTON_ID, 0.5f);
        rotateAnim.setDuration(200);
        rotateAnim.setFillAfter(true);
        if (this.mHandImage != null) {
            this.mHandImage.startAnimation(rotateAnim);
        }
        this.mCurHandAngle = angle;
    }
}
