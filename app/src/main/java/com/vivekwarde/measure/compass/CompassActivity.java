package com.vivekwarde.measure.compass;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;
import java.util.Locale;

public class CompassActivity extends BaseActivity implements OnClickListener, SensorEventListener, LocationListener {
    protected static final int BASE_HEADING_IMAGE_ID = 4;
    protected static final int BASE_IMAGE_ID = 3;
    protected static final int FACE_IMAGE_ID = 7;
    private static final int HEADING_ARROW_BLINKING_INTERVAL = 1000;
    protected static final int HEADING_ARROW_DIM_IMAGE_ID = 5;
    protected static final int HEADING_ARROW_GLOW_IMAGE_ID = 6;
    protected static final int LABEL_LATITUDE_TEXT_ID = 15;
    protected static final int LABEL_LONGITUDE_TEXT_ID = 17;
    protected static final int LED_SCREEN_LEFT_IMAGE_ID = 10;
    protected static final int LED_SCREEN_RIGHT_IMAGE_ID = 11;
    protected static final int MAP_BUTTON_ID = 8;
    public static final int MENU_BUTTON_ID = 1;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BTW_UPDATES = 60000;
    protected static final int OUTPUT_CARDINAL_TEXT_ID = 12;
    protected static final int OUTPUT_CARDINAL_UNIT_TEXT_ID = 13;
    protected static final int OUTPUT_LATITUDE_TEXT_ID = 16;
    protected static final int OUTPUT_LONGITUDE_TEXT_ID = 18;
    protected static final int OUTPUT_NOMINAL_TEXT_ID = 14;
    private static final String[] PERMISSION_LOCATION;
    private static final int REQUEST_LOCATION = 0;
    protected static final int RING_IMAGE_ID = 9;
    public static final int SETTINGS_BUTTON_ID = 2;
    public static final int UPGRADE_BUTTON_ID = 0;
    private float[] mAccelerometerData;
    private ImageView mBaseHeadingImage;
    private SPScale9ImageView mBaseImage;
    private Location mCurLocation;
    private float mCurRotation;
    private ImageView mFaceImage;
    private ImageView mHeadingArrowDimImage;
    private ImageView mHeadingArrowGlowImage;
    protected Handler mHeadingArrowTimerHandler;
    protected Runnable mHeadingArrowTimerTask;
    private TextView mLabelLatitudeText;
    private TextView mLabelLongitudeText;
    private ImageView mLedScreenLeftImage;
    private ImageView mLedScreenRightImage;
    protected LocationManager mLocationManager;
    private float[] mMagneticData;
    private SPImageButton mMapButton;
    private SPImageButton mMenuButton;
    private TextView mOutputCardinalText;
    private TextView mOutputCardinalUnitText;
    private TextView mOutputLatitudeText;
    private TextView mOutputLongitudeText;
    private TextView mOutputNominalText;
    private ImageView mRingImage;
    private SensorManager mSensorManager;
    private SPImageButton mSettingsButton;

    public CompassActivity() {
        this.mMenuButton = null;
        this.mSettingsButton = null;
        this.mBaseImage = null;
        this.mBaseHeadingImage = null;
        this.mHeadingArrowDimImage = null;
        this.mHeadingArrowGlowImage = null;
        this.mFaceImage = null;
        this.mMapButton = null;
        this.mRingImage = null;
        this.mLedScreenLeftImage = null;
        this.mLedScreenRightImage = null;
        this.mCurRotation = 0.0f;
        this.mCurLocation = null;
    }

    static {
        String[] strArr = new String[MENU_BUTTON_ID];
        strArr[REQUEST_LOCATION] = "android.permission.ACCESS_FINE_LOCATION";
        PERMISSION_LOCATION = strArr;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(MENU_BUTTON_ID);
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
            requestLocationPermission();
        } else {
            initLocManager();
        }
        this.mHeadingArrowTimerHandler = new Handler();
        this.mHeadingArrowTimerTask = new Runnable() {
            public void run() {
                AlphaAnimation alphaAnim;
                if (ViewHelper.getAlpha(CompassActivity.this.mHeadingArrowGlowImage) > 0.5f) {
                    alphaAnim = new AlphaAnimation(ViewHelper.getAlpha(CompassActivity.this.mHeadingArrowGlowImage), 0.0f);
                } else {
                    alphaAnim = new AlphaAnimation(ViewHelper.getAlpha(CompassActivity.this.mHeadingArrowGlowImage), TextTrackStyle.DEFAULT_FONT_SCALE);
                }
                alphaAnim.setDuration(1000);
                CompassActivity.this.mHeadingArrowGlowImage.startAnimation(alphaAnim);
                CompassActivity.this.mHeadingArrowTimerHandler.postDelayed(this, 1000);
            }
        };
        createUi();
        initSensor();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    void initSensor() {
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        if (this.mSensorManager.getDefaultSensor(MENU_BUTTON_ID) == null || this.mSensorManager.getDefaultSensor(MENU_BUTTON_ID) == null) {
            Toast.makeText(this, "You device does not have sensors for using compass", MENU_BUTTON_ID).show();
        }
    }

    private void requestLocationPermission() {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.ACCESS_FINE_LOCATION")) {
            ActivityCompat.requestPermissions(this, PERMISSION_LOCATION, REQUEST_LOCATION);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != 0) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else if (grantResults.length == MENU_BUTTON_ID && grantResults[REQUEST_LOCATION] == 0) {
            initLocManager();
        }
    }

    public void initLocManager() {
        this.mLocationManager = (LocationManager) getSystemService("location");
        if (!this.mLocationManager.isProviderEnabled("network") && !this.mLocationManager.isProviderEnabled("gps")) {
            Toast.makeText(this, "No location services", MENU_BUTTON_ID).show();
        } else if (VERSION.SDK_INT < 23) {
            try {
                this.mLocationManager.requestLocationUpdates("network", 0, 0.0f, this);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            try {
                this.mLocationManager.requestLocationUpdates("gps", 0, 0.0f, this);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            }
        } else if (checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            try {
                this.mLocationManager.requestLocationUpdates("network", 0, 0.0f, this);
            } catch (IllegalArgumentException e22) {
                e22.printStackTrace();
            }
            try {
                this.mLocationManager.requestLocationUpdates("gps", 0, 0.0f, this);
            } catch (IllegalArgumentException e222) {
                e222.printStackTrace();
            }
        }
    }

    @SuppressLint({"NewApi"})
    private void createUi() {
        int i;
        this.mMainLayout = new RelativeLayout(this);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < OUTPUT_LATITUDE_TEXT_ID) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                CompassActivity.this.arrangeLayout();
                if (VERSION.SDK_INT >= CompassActivity.OUTPUT_LATITUDE_TEXT_ID) {
                    CompassActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    CompassActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(BASE_IMAGE_ID, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mMainLayout.addView(this.mUiLayout);
        this.mBaseImage = new SPScale9ImageView(this);
        this.mBaseImage.setId(BASE_IMAGE_ID);
        this.mBaseImage.setBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap());
        this.mUiLayout.addView(this.mBaseImage);
        this.mBaseHeadingImage = new ImageView(this);
        this.mBaseHeadingImage.setId(BASE_HEADING_IMAGE_ID);
        this.mBaseHeadingImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.compass_base_heading)).getBitmap());
        this.mUiLayout.addView(this.mBaseHeadingImage);
        this.mHeadingArrowDimImage = new ImageView(this);
        this.mHeadingArrowDimImage.setId(HEADING_ARROW_DIM_IMAGE_ID);
        this.mHeadingArrowDimImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.compass_heading_arrow_dim)).getBitmap());
        this.mUiLayout.addView(this.mHeadingArrowDimImage);
        this.mHeadingArrowGlowImage = new ImageView(this);
        this.mHeadingArrowGlowImage.setId(HEADING_ARROW_GLOW_IMAGE_ID);
        this.mHeadingArrowGlowImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.compass_heading_arrow_glow)).getBitmap());
        this.mUiLayout.addView(this.mHeadingArrowGlowImage);
        this.mFaceImage = new ImageView(this);
        this.mFaceImage.setId(FACE_IMAGE_ID);
        this.mFaceImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.compass_face)).getBitmap());
        this.mUiLayout.addView(this.mFaceImage);
        this.mMapButton = new SPImageButton(this);
        this.mMapButton.setId(MAP_BUTTON_ID);
        this.mMapButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.compass_button_map)).getBitmap());
        this.mMapButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mMapButton);
        this.mRingImage = new ImageView(this);
        this.mRingImage.setId(RING_IMAGE_ID);
        this.mRingImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.compass_ring)).getBitmap());
        this.mRingImage.setScaleType(ScaleType.CENTER);
        this.mUiLayout.addView(this.mRingImage);
        this.mLedScreenLeftImage = new ImageView(this);
        this.mLedScreenLeftImage.setId(LED_SCREEN_LEFT_IMAGE_ID);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        Bitmap scale9bitmap = ImageUtility.scale9Bitmap(bitmap, (int) (((float) bitmap.getWidth()) + (20.0f * getResources().getDisplayMetrics().density)), (bitmap.getHeight() - 2) * SETTINGS_BUTTON_ID);
        this.mLedScreenLeftImage.setImageBitmap(scale9bitmap);
        this.mUiLayout.addView(this.mLedScreenLeftImage);
        this.mLedScreenRightImage = new ImageView(this);
        this.mLedScreenRightImage.setId(LED_SCREEN_RIGHT_IMAGE_ID);
        this.mLedScreenRightImage.setImageBitmap(scale9bitmap);
        this.mUiLayout.addView(this.mLedScreenRightImage);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mOutputCardinalText = new TextView(this);
        this.mOutputCardinalText.setId(OUTPUT_CARDINAL_TEXT_ID);
        this.mOutputCardinalText.setTypeface(face);
        this.mOutputCardinalText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.COMPASS_OUTPUT_CARDINAL_FONT_SIZE));
        this.mOutputCardinalText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputCardinalText.setPaintFlags(this.mOutputCardinalText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputCardinalText);
        this.mOutputCardinalText.setText("N/A");
        this.mOutputCardinalUnitText = new TextView(this);
        this.mOutputCardinalUnitText.setId(OUTPUT_CARDINAL_UNIT_TEXT_ID);
        this.mOutputCardinalUnitText.setTypeface(face);
        this.mOutputCardinalUnitText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.COMPASS_OUTPUT_CARDINAL_UNIT_FONT_SIZE));
        this.mOutputCardinalUnitText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputCardinalUnitText.setPaintFlags(this.mOutputCardinalUnitText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputCardinalUnitText);
        this.mOutputCardinalUnitText.setText("~");
        this.mOutputNominalText = new TextView(this);
        this.mOutputNominalText.setId(OUTPUT_NOMINAL_TEXT_ID);
        this.mOutputNominalText.setTypeface(face);
        this.mOutputNominalText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.COMPASS_OUTPUT_NOMINAL_FONT_SIZE));
        this.mOutputNominalText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputNominalText.setPaintFlags(this.mOutputCardinalUnitText.getPaintFlags() | Barcode.ITF);
        this.mOutputNominalText.setGravity(MENU_BUTTON_ID);
        this.mUiLayout.addView(this.mOutputNominalText);
        this.mOutputNominalText.setText("N/A");
        this.mLabelLatitudeText = new TextView(this);
        this.mLabelLatitudeText.setId(LABEL_LATITUDE_TEXT_ID);
        this.mLabelLatitudeText.setTypeface(face);
        this.mLabelLatitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.COMPASS_LABEL_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mLabelLatitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelLatitudeText.setPaintFlags(this.mLabelLatitudeText.getPaintFlags() | Barcode.ITF);
        ViewHelper.setAlpha(this.mLabelLatitudeText, 0.5f);
        this.mUiLayout.addView(this.mLabelLatitudeText);
        this.mLabelLatitudeText.setText(getResources().getString(R.string.IDS_LATITUDE));
        this.mOutputLatitudeText = new TextView(this);
        this.mOutputLatitudeText.setId(OUTPUT_LATITUDE_TEXT_ID);
        this.mOutputLatitudeText.setTypeface(face);
        this.mOutputLatitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.COMPASS_OUTPUT_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mOutputLatitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputLatitudeText.setPaintFlags(this.mOutputLatitudeText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputLatitudeText);
        this.mOutputLatitudeText.setText("N/A");
        this.mLabelLongitudeText = new TextView(this);
        this.mLabelLongitudeText.setId(LABEL_LONGITUDE_TEXT_ID);
        this.mLabelLongitudeText.setTypeface(face);
        this.mLabelLongitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.COMPASS_LABEL_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mLabelLongitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelLongitudeText.setPaintFlags(this.mLabelLongitudeText.getPaintFlags() | Barcode.ITF);
        ViewHelper.setAlpha(this.mLabelLongitudeText, 0.5f);
        this.mUiLayout.addView(this.mLabelLongitudeText);
        this.mLabelLongitudeText.setText(getResources().getString(R.string.IDS_LONGITUDE));
        this.mOutputLongitudeText = new TextView(this);
        this.mOutputLongitudeText.setId(OUTPUT_LONGITUDE_TEXT_ID);
        this.mOutputLongitudeText.setTypeface(face);
        this.mOutputLongitudeText.setTextSize(MENU_BUTTON_ID, getResources().getDimension(R.dimen.COMPASS_OUTPUT_LATTITUDE_AND_LONGITUDE_FONT_SIZE));
        this.mOutputLongitudeText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputLongitudeText.setPaintFlags(this.mOutputLongitudeText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputLongitudeText);
        this.mOutputLongitudeText.setText("N/A");
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(MENU_BUTTON_ID);
        this.mMenuButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap());
        this.mMenuButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mMenuButton);
        this.mSettingsButton = new SPImageButton(this);
        this.mSettingsButton.setId(SETTINGS_BUTTON_ID);
        this.mSettingsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mSettingsButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mSettingsButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(REQUEST_LOCATION);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        SPImageButton sPImageButton = this.mNoAdsButton;
        if (MainApplication.mIsRemoveAds) {
            i = MAP_BUTTON_ID;
        } else {
            i = REQUEST_LOCATION;
        }
        sPImageButton.setVisibility(i);
        this.mUiLayout.addView(this.mNoAdsButton);
        updateCompassRotation(32.0f);
    }

    private void arrangeLayout() {
        int i = REQUEST_LOCATION;
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(LED_SCREEN_LEFT_IMAGE_ID);
        params.addRule(LED_SCREEN_RIGHT_IMAGE_ID);
        params.topMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / SETTINGS_BUTTON_ID;
        this.mMenuButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mMenuButton.getId());
        params.addRule(REQUEST_LOCATION, this.mMenuButton.getId());
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / SETTINGS_BUTTON_ID;
        this.mSettingsButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(LED_SCREEN_LEFT_IMAGE_ID);
        params.addRule(RING_IMAGE_ID);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), REQUEST_LOCATION);
        this.mNoAdsButton.setLayoutParams(params);
        int headerBarHeight = this.mMenuButton.getHeight() + (((int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN)) * SETTINGS_BUTTON_ID);
        params = new LayoutParams(-1, -1);
        params.topMargin = headerBarHeight;
        this.mBaseImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(OUTPUT_NOMINAL_TEXT_ID);
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mBaseImage.getId());
        params.topMargin = (-this.mBaseHeadingImage.getHeight()) / SETTINGS_BUTTON_ID;
        this.mBaseHeadingImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(OUTPUT_NOMINAL_TEXT_ID);
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mBaseHeadingImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_TOP_HEADING_ARROW_AND_TOP_BASE_HEADING);
        this.mHeadingArrowDimImage.setLayoutParams(params);
        this.mHeadingArrowGlowImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(OUTPUT_NOMINAL_TEXT_ID);
        params.addRule(MAP_BUTTON_ID, this.mBaseImage.getId());
        int i2 = mScreenHeight;
        if (this.mBannerAdView != null) {
            i = this.mBannerAdView.getHeight();
        }
        params.bottomMargin = (((((i2 - i) - this.mRingImage.getHeight()) - headerBarHeight) - this.mLedScreenLeftImage.getHeight()) - ((int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE))) / SETTINGS_BUTTON_ID;
        this.mRingImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HEADING_ARROW_DIM_IMAGE_ID, this.mRingImage.getId());
        params.leftMargin = (this.mRingImage.getWidth() - this.mFaceImage.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mRingImage.getId());
        params.topMargin = (this.mRingImage.getHeight() - this.mFaceImage.getHeight()) / SETTINGS_BUTTON_ID;
        this.mFaceImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HEADING_ARROW_DIM_IMAGE_ID, this.mFaceImage.getId());
        params.leftMargin = (this.mFaceImage.getWidth() - this.mMapButton.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mFaceImage.getId());
        params.topMargin = (this.mFaceImage.getHeight() - this.mMapButton.getHeight()) / SETTINGS_BUTTON_ID;
        this.mMapButton.setLayoutParams(params);
        int ledscreenLeftAndRightMargin = (mScreenWidth - (this.mLedScreenLeftImage.getWidth() * SETTINGS_BUTTON_ID)) / BASE_HEADING_IMAGE_ID;
        params = new LayoutParams(-2, -2);
        params.addRule(HEADING_ARROW_DIM_IMAGE_ID, this.mBaseImage.getId());
        params.leftMargin = ledscreenLeftAndRightMargin;
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mBaseImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE);
        this.mLedScreenLeftImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(FACE_IMAGE_ID, this.mBaseImage.getId());
        params.rightMargin = ledscreenLeftAndRightMargin;
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mBaseImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_TOP_LED_SCREEN_AND_TOP_BASE);
        this.mLedScreenRightImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HEADING_ARROW_DIM_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.leftMargin = (((this.mLedScreenLeftImage.getWidth() - this.mOutputCardinalText.getWidth()) - ((int) getResources().getDimension(R.dimen.COMPASS_HORZ_MARGIN_BTW_RIGHT_OUTPUT_CARDINAL_AND_LEFT_ITS_UNIT))) - this.mOutputCardinalUnitText.getWidth()) / SETTINGS_BUTTON_ID;
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.topMargin = (((this.mLedScreenLeftImage.getHeight() - this.mOutputCardinalText.getHeight()) - ((int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_BOTTOM_OUTPUT_CARDINAL_AND_TOP_OUTPUT_NOMINAL))) - this.mOutputNominalText.getHeight()) / SETTINGS_BUTTON_ID;
        this.mOutputCardinalText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(MENU_BUTTON_ID, this.mOutputCardinalText.getId());
        params.addRule(BASE_HEADING_IMAGE_ID, this.mOutputCardinalText.getId());
        params.leftMargin = (int) getResources().getDimension(R.dimen.COMPASS_HORZ_MARGIN_BTW_RIGHT_OUTPUT_CARDINAL_AND_LEFT_ITS_UNIT);
        this.mOutputCardinalUnitText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(HEADING_ARROW_DIM_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.addRule(FACE_IMAGE_ID, this.mLedScreenLeftImage.getId());
        params.addRule(MAP_BUTTON_ID, this.mLedScreenLeftImage.getId());
        params.bottomMargin = (((this.mLedScreenLeftImage.getHeight() - this.mOutputCardinalText.getHeight()) - ((int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_BOTTOM_OUTPUT_CARDINAL_AND_TOP_OUTPUT_NOMINAL))) - this.mOutputNominalText.getHeight()) / SETTINGS_BUTTON_ID;
        this.mOutputNominalText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(FACE_IMAGE_ID, this.mLedScreenRightImage.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.COMPASS_HORZ_MARGIN_BTW_RIGHT_LABEL_LATITUDE_AND_RIGHT_LED_SCREEN);
        params.addRule(HEADING_ARROW_GLOW_IMAGE_ID, this.mLedScreenRightImage.getId());
        params.topMargin = (((this.mLedScreenRightImage.getHeight() - (this.mLabelLatitudeText.getHeight() * SETTINGS_BUTTON_ID)) - ((int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_BOTTOM_OUTPUT_LATITUDE_AND_TOP_LABEL_LONGITUDE))) - (this.mOutputLatitudeText.getHeight() * SETTINGS_BUTTON_ID)) / SETTINGS_BUTTON_ID;
        this.mLabelLatitudeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(FACE_IMAGE_ID, this.mLabelLatitudeText.getId());
        params.addRule(BASE_IMAGE_ID, this.mLabelLatitudeText.getId());
        this.mOutputLatitudeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(FACE_IMAGE_ID, this.mOutputLatitudeText.getId());
        params.addRule(BASE_IMAGE_ID, this.mOutputLatitudeText.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.COMPASS_VERT_MARGIN_BTW_BOTTOM_OUTPUT_LATITUDE_AND_TOP_LABEL_LONGITUDE);
        this.mLabelLongitudeText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(FACE_IMAGE_ID, this.mLabelLongitudeText.getId());
        params.addRule(BASE_IMAGE_ID, this.mLabelLongitudeText.getId());
        this.mOutputLongitudeText.setLayoutParams(params);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
        this.mSensorManager.unregisterListener(this);
        this.mHeadingArrowTimerHandler.removeCallbacks(this.mHeadingArrowTimerTask);
    }

    protected void onResume() {
        super.onResume();
        startLocationUpdates();
        if (this.mSensorManager.getDefaultSensor(MENU_BUTTON_ID) != null) {
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(MENU_BUTTON_ID), BASE_IMAGE_ID);
        }
        if (this.mSensorManager.getDefaultSensor(SETTINGS_BUTTON_ID) != null) {
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(SETTINGS_BUTTON_ID), BASE_IMAGE_ID);
        }
        this.mHeadingArrowTimerHandler.removeCallbacks(this.mHeadingArrowTimerTask);
        this.mHeadingArrowTimerHandler.postDelayed(this.mHeadingArrowTimerTask, 1000);
    }

    public void onClick(View source) {
        if (source.getId() == MENU_BUTTON_ID) {
            onButtonMenu();
        } else if (source.getId() == SETTINGS_BUTTON_ID) {
            onButtonSettings();
        } else if (source.getId() == MAP_BUTTON_ID) {
            onButtonMap();
        } else if (source.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    private void onButtonMap() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(BASE_HEADING_IMAGE_ID, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mCurLocation == null) {
            launchLocationSettings();
            return;
        }
        Object[] objArr = new Object[SETTINGS_BUTTON_ID];
        objArr[REQUEST_LOCATION] = Double.valueOf(this.mCurLocation.getLatitude());
        objArr[MENU_BUTTON_ID] = Double.valueOf(this.mCurLocation.getLongitude());
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?&daddr=%f,%f", objArr);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
            } catch (ActivityNotFoundException e2) {
                Toast.makeText(this, "Please install a maps application", MENU_BUTTON_ID).show();
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == MENU_BUTTON_ID) {
            this.mAccelerometerData = event.values;
        }
        if (event.sensor.getType() == SETTINGS_BUTTON_ID) {
            this.mMagneticData = event.values;
        }
        if (this.mAccelerometerData != null && this.mMagneticData != null) {
            float[] rotationMatrix = new float[RING_IMAGE_ID];
            if (SensorManager.getRotationMatrix(rotationMatrix, new float[RING_IMAGE_ID], this.mAccelerometerData, this.mMagneticData)) {
                float[] orientation = new float[BASE_IMAGE_ID];
                SensorManager.getOrientation(rotationMatrix, orientation);
                float newAzimuth = (float) Math.toDegrees((double) orientation[REQUEST_LOCATION]);
                if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_COMPASS_HEADING_KEY, "1").equals("0") && this.mCurLocation != null) {
                    newAzimuth += new GeomagneticField(Double.valueOf(this.mCurLocation.getLatitude()).floatValue(), Double.valueOf(this.mCurLocation.getLongitude()).floatValue(), Double.valueOf(this.mCurLocation.getAltitude()).floatValue(), System.currentTimeMillis()).getDeclination();
                }
                updateCompassRotation(newAzimuth);
            }
        }
    }

    @SuppressLint({"DefaultLocale"})
    private void updateCompassRotation(float azimuth) {
        float rotation = azimuth;
        if (rotation < 0.0f) {
            rotation += 360.0f;
        }
        Object[] objArr = new Object[MENU_BUTTON_ID];
        objArr[REQUEST_LOCATION] = Integer.valueOf((int) rotation);
        this.mOutputCardinalText.setText(String.format(Locale.getDefault(), "%d", objArr));
        String headingText = getResources().getString(R.string.IDS_NORTH);
        if ((360.0f >= rotation && ((double) rotation) >= 337.5d) || (0.0f <= rotation && ((double) rotation) <= 22.5d)) {
            headingText = getResources().getString(R.string.IDS_NORTH);
        } else if (((double) rotation) > 22.5d && ((double) rotation) < 67.5d) {
            headingText = getResources().getString(R.string.IDS_NORTH_EAST);
        } else if (((double) rotation) >= 67.5d && ((double) rotation) <= 112.5d) {
            headingText = getResources().getString(R.string.IDS_EAST);
        } else if (((double) rotation) > 112.5d && ((double) rotation) < 157.5d) {
            headingText = getResources().getString(R.string.IDS_SOUTH_EAST);
        } else if (((double) rotation) >= 157.5d && ((double) rotation) <= 202.5d) {
            headingText = getResources().getString(R.string.IDS_SOUTH);
        } else if (((double) rotation) > 202.5d && ((double) rotation) < 247.5d) {
            headingText = getResources().getString(R.string.IDS_SOUTH_WEST);
        } else if (((double) rotation) >= 247.5d && ((double) rotation) <= 292.5d) {
            headingText = getResources().getString(R.string.IDS_WEST);
        } else if (((double) rotation) <= 292.5d || ((double) rotation) >= 337.5d) {
            headingText = "?";
        } else {
            headingText = getResources().getString(R.string.IDS_NORTH_WEST);
        }
        this.mOutputNominalText.setText(headingText);
        RotateAnimation rotateAnim = new RotateAnimation(this.mCurRotation, -rotation, MENU_BUTTON_ID, 0.5f, MENU_BUTTON_ID, 0.5f);
        rotateAnim.setDuration(100);
        rotateAnim.setFillAfter(true);
        this.mFaceImage.startAnimation(rotateAnim);
        this.mCurRotation = -rotation;
    }

    public void onLocationChanged(Location location) {
        this.mCurLocation = location;
        this.mOutputLatitudeText.setText(Location.convert(this.mCurLocation.getLatitude(), SETTINGS_BUTTON_ID));
        this.mOutputLongitudeText.setText(Location.convert(this.mCurLocation.getLongitude(), SETTINGS_BUTTON_ID));
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void launchLocationSettings() {
        Builder alertDialog = new Builder(this);
        alertDialog.setTitle(getString(R.string.IDS_INFORMATION));
        alertDialog.setMessage(getString(R.string.IDS_LOCATION_SERVICES_ASKING));
        alertDialog.setPositiveButton(getString(R.string.IDS_SETTINGS), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CompassActivity.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        });
        alertDialog.setNegativeButton(getString(R.string.IDS_CANCEL), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        MiscUtility.setDialogFontSize(this, alertDialog.show());
    }

    private void startLocationUpdates() {
        if (this.mLocationManager != null) {
            boolean isGPSEnabled = this.mLocationManager.isProviderEnabled("gps");
            boolean isNetworkEnabled = this.mLocationManager.isProviderEnabled("network");
            if (isGPSEnabled || isNetworkEnabled) {
                if (isNetworkEnabled) {
                    this.mLocationManager.requestLocationUpdates("network", MIN_TIME_BTW_UPDATES, 10.0f, this);
                    if (this.mLocationManager != null) {
                        try {
                            this.mCurLocation = this.mLocationManager.getLastKnownLocation("network");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (isGPSEnabled && this.mCurLocation == null) {
                    this.mLocationManager.requestLocationUpdates("gps", MIN_TIME_BTW_UPDATES, 10.0f, this);
                    if (this.mLocationManager != null) {
                        try {
                            this.mCurLocation = this.mLocationManager.getLastKnownLocation("gps");
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                    return;
                }
                return;
            }
            launchLocationSettings();
        }
    }

    private void stopLocationUpdates() {
        if (this.mLocationManager != null) {
            this.mLocationManager.removeUpdates(this);
        }
    }
}
