package com.vivekwarde.measure.custom_controls;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;

import com.google.android.gms.cast.TextTrackStyle;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.inapp_purchase.IabHelper;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.ArrayList;

public class ActionView extends RelativeLayout implements OnClickListener, OnTouchListener {
    private static final int BORDER_VIEW_ID = 999;
    private static final int THIS_ID = 1000;
    protected BorderView mBorderView;
    protected OnActionViewEventListener mListener;
    Point mArrowPosition;
    int mContentHeight;
    ArrayList<String> mTitleList;
    int mWidth;
    private int mLeftMargin;
    private int mRightMargin;

    public ActionView(Context context) {
        super(context);
        this.mContentHeight = 0;
        this.mListener = null;
        this.mArrowPosition = null;
        this.mTitleList = null;
        this.mBorderView = null;
        this.mWidth = 0;
        this.mLeftMargin = 0;
        this.mRightMargin = 0;
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.DIALOG_BACKGROUND_COLOR));
        setOnTouchListener(this);
        this.mWidth = getResources().getDisplayMetrics().widthPixels;
    }

    public void construct(int leftMargin, int rightMargin, Point point, ArrayList<String> titleList, int contentHeight) {
        setId(titleList.size() + THIS_ID);
        this.mLeftMargin = leftMargin;
        this.mRightMargin = rightMargin;
        this.mArrowPosition = point;
        this.mContentHeight = contentHeight;
        this.mTitleList = titleList;
        initSubviews();
    }

    protected void initSubviews() {
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.button_control_ninepatch)).getBitmap();
        int margin = (int) getResources().getDimension(R.dimen.ACTION_VIEW_MARGIN);
        this.mBorderView = new BorderView(getContext());
        this.mBorderView.construct(2, (double) (((float) (this.mArrowPosition.x - this.mLeftMargin)) / (((((float) this.mWidth) - ((float) this.mLeftMargin)) - ((float) this.mRightMargin)) - ((float) this.mBorderView.getCornerCapSize()))));
        this.mBorderView.setId(BORDER_VIEW_ID);
        LayoutParams params = new LayoutParams(-1, -2);
        params.addRule(15);
        params.addRule(10);
        params.setMargins(this.mLeftMargin, this.mArrowPosition.y, this.mRightMargin, 0);
        this.mBorderView.setPadding(0, margin, 0, margin);
        this.mBorderView.setLayoutParams(params);
        this.mBorderView.setClickable(true);
        addView(this.mBorderView);
        float fontSize = getResources().getDimension(R.dimen.ACTION_VIEW_FONT_SIZE);
        int space = (int) getResources().getDimension(R.dimen.METRONOME_LOAD_SAVE_SPACE_BTW_BUTTONS_AND_CONTENT);
        bitmap = ImageUtility.scale9Bitmap(bitmap, ((this.mWidth - this.mLeftMargin) - this.mRightMargin) - (margin * 2), bitmap.getHeight() - 2);
        for (int i = 0; i < this.mTitleList.size(); i++) {
            SPButton btn = new SPButton(getContext());
            btn.setId(i + 1);
            btn.setOnClickListener(this);
            btn.setBackgroundAutoFlipBitmap(bitmap);
            params = new LayoutParams(-2, -2);
            if (i == 0) {
                params.addRule(10);
                params.setMargins(0, 0, 0, 0);
            } else {
                params.addRule(3, i);
                params.setMargins(0, space, 0, 0);
            }
            params.addRule(14);
            btn.setLayoutParams(params);
            btn.setTextSize(1, fontSize);
            btn.setText((CharSequence) this.mTitleList.get(i));
            int[][] r14 = new int[3][];
            r14[0] = new int[]{16842919};
            r14[1] = new int[]{-16842910};
            r14[2] = new int[0];
            btn.setTextColor(new ColorStateList(r14, new int[]{ContextCompat.getColor(getContext(), R.color.CONTROL_BUTTON_PRESSED_STATE_COLOR), ContextCompat.getColor(getContext(), R.color.CONTROL_BUTTON_PRESSED_STATE_COLOR), ContextCompat.getColor(getContext(), R.color.CONTROL_BUTTON_NORMAL_STATE_COLOR)}));
            btn.setShadowLayer(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE, ViewCompat.MEASURED_STATE_MASK);
            this.mBorderView.addView(btn);
        }
    }

    public void setOnActionViewEventListener(OnActionViewEventListener listener) {
        this.mListener = listener;
    }

    public void onClick(View v) {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mListener != null) {
            int id;
            if (v.equals(this)) {
                id = (v.getId() + IabHelper.IABHELPER_ERROR_BASE) - 1;
            } else {
                id = v.getId() - 1;
            }
            this.mListener.onActionViewEvent(this, id);
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!v.equals(this)) {
            return false;
        }
        if (event.getAction() == 0) {
            onClick(this);
        }
        return true;
    }

    public interface OnActionViewEventListener {
        void onActionViewEvent(ActionView actionView, int i);
    }
}
