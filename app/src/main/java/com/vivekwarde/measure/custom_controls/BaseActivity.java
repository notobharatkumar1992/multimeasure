package com.vivekwarde.measure.custom_controls;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders.EventBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.dialogs.RemoveAdsDialogFragment;
import com.vivekwarde.measure.dialogs.RemoveAdsDialogFragment.NoticeDialogListener;
import com.vivekwarde.measure.inapp_purchase.IabBroadcastReceiver;
import com.vivekwarde.measure.inapp_purchase.IabBroadcastReceiver.IabBroadcastListener;
import com.vivekwarde.measure.inapp_purchase.IabHelper;
import com.vivekwarde.measure.inapp_purchase.IabHelper.OnIabPurchaseFinishedListener;
import com.vivekwarde.measure.inapp_purchase.IabHelper.OnIabSetupFinishedListener;
import com.vivekwarde.measure.inapp_purchase.IabHelper.QueryInventoryFinishedListener;
import com.vivekwarde.measure.inapp_purchase.IabResult;
import com.vivekwarde.measure.inapp_purchase.Inventory;
import com.vivekwarde.measure.inapp_purchase.Purchase;
import com.vivekwarde.measure.settings.SettingsActivity;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.ArrayList;
import java.util.Arrays;

public class BaseActivity extends Activity implements IabBroadcastListener, NoticeDialogListener {
    public static final int AD_VIEW_ID = 999;
    public static final String IN_APP_REMOVE_ADS_SKU = "com.vivek." + "premium";
    public static final int IN_APP_REQUEST_CODE = 10001;
    protected static int mScreenHeight;
    protected static int mScreenWidth;

    static {
        mScreenWidth = 0;
        mScreenHeight = 0;
    }

    protected AdView mBannerAdView;
    protected RelativeLayout mMainLayout;
    protected SPImageButton mNoAdsButton;
    protected RelativeLayout mUiLayout;
    IabBroadcastReceiver mBroadcastReceiver;
    private QueryInventoryFinishedListener mGotInventoryListener;
    private IabHelper mInAppHelper;
    private InterstitialAd mInterstitialAdView;
    private OnIabPurchaseFinishedListener mPurchaseFinishedListener;

    public BaseActivity() {
        this.mBroadcastReceiver = null;
        this.mInAppHelper = null;
        this.mBannerAdView = null;
        this.mInterstitialAdView = null;
        this.mMainLayout = null;
        this.mUiLayout = null;
        this.mNoAdsButton = null;
        this.mGotInventoryListener = new QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                Log.d(MainApplication.TAG, "Query inventory finished.");
                if (BaseActivity.this.mInAppHelper != null) {
                    if (result.isFailure()) {
                        BaseActivity.this.complain("Failed to query inventory: " + result);
                        return;
                    }
                    Log.d(MainApplication.TAG, "Query inventory was successful.");
                    Purchase removeAdsPurchase = inventory.getPurchase(BaseActivity.IN_APP_REMOVE_ADS_SKU);
                    boolean z = removeAdsPurchase != null && BaseActivity.this.verifyDeveloperPayload(removeAdsPurchase);
                    MainApplication.mIsRemoveAds = z;
                    Log.d(MainApplication.TAG, "User is " + (MainApplication.mIsRemoveAds ? "PREMIUM" : "NOT PREMIUM"));
                    BaseActivity.this.updateUi();
                    Log.d(MainApplication.TAG, "Initial inventory query finished; enabling main UI.");
                }
            }
        };
        this.mPurchaseFinishedListener = new OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                Log.d(MainApplication.TAG, "Purchase finished: " + result + ", purchase: " + purchase);
                if (BaseActivity.this.mInAppHelper != null && !result.isFailure()) {
                    if (BaseActivity.this.verifyDeveloperPayload(purchase)) {
                        Log.d(MainApplication.TAG, "Purchase successful.");
                        if (purchase.getSku().equals(BaseActivity.IN_APP_REMOVE_ADS_SKU)) {
                            Log.d(MainApplication.TAG, "Purchase is remove ads upgrade. Congratulating user.");
                            BaseActivity.this.alert(BaseActivity.this.getString(R.string.IDS_THANK_YOU_FOR_UPGRADING_TO_PREMIUM));
                            MainApplication.mIsRemoveAds = true;
                            BaseActivity.this.savePurchasesLocally();
                            BaseActivity.this.updateUi();
                            return;
                        }
                        return;
                    }
                    BaseActivity.this.complain("Error purchasing. Authenticity verification failed.");
                }
            }
        };
    }

    protected void initInAppPurchase() {
        Log.d(MainApplication.TAG, "Creating IAB helper.");
        this.mInAppHelper = new IabHelper(this, MainApplication.mLicenseKey);
        this.mInAppHelper.enableDebugLogging(true);
        Log.d(MainApplication.TAG, "Starting setup.");
        this.mInAppHelper.startSetup(new OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(MainApplication.TAG, "Setup finished.");
                if (!result.isSuccess()) {
                    BaseActivity.this.complain("Problem setting up in-app billing: " + result);
                } else if (BaseActivity.this.mInAppHelper != null) {
                    BaseActivity.this.mBroadcastReceiver = new IabBroadcastReceiver(BaseActivity.this);
                    BaseActivity.this.registerReceiver(BaseActivity.this.mBroadcastReceiver, new IntentFilter(IabBroadcastReceiver.ACTION));
                    Log.d(MainApplication.TAG, "Setup successful. Querying inventory.");
                    BaseActivity.this.mInAppHelper.queryInventoryAsync(BaseActivity.this.mGotInventoryListener);
                }
            }
        });
    }

    public void receivedBroadcast() {
        Log.d(MainApplication.TAG, "Received broadcast notification. Querying inventory.");
        this.mInAppHelper.queryInventoryAsync(this.mGotInventoryListener);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(MainApplication.TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (this.mInAppHelper != null) {
            if (this.mInAppHelper.handleActivityResult(requestCode, resultCode, data)) {
                Log.d(MainApplication.TAG, "onActivityResult handled by IABUtil.");
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    void complain(String message) {
        Log.e(MainApplication.TAG, "**** Multi Measures Error: " + message);
        alert("Error: " + message);
    }

    private void alert(String message) {
        Builder bld = new Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(MainApplication.TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    protected void updateUi() {
        int i = 0;
        if (PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_GENERAL_LAST_USED_TOOL_KEY, 0) != 7) {
            if (MainApplication.mIsRemoveAds) {
                hideBannerAd();
            } else {
                showBannerAd();
            }
            SPImageButton sPImageButton = this.mNoAdsButton;
            if (MainApplication.mIsRemoveAds) {
                i = 8;
            }
            sPImageButton.setVisibility(i);
        }
    }

    private void savePurchasesLocally() {
        Editor editor = getPreferences(0).edit();
        editor.putBoolean(SettingsKey.SETTINGS_IS_PREMIUM, MainApplication.mIsRemoveAds);
        editor.apply();
    }

    private boolean verifyDeveloperPayload(Purchase p) {
        return true;
    }

    protected void onRemoveAdsButton() {
        new RemoveAdsDialogFragment().show(getFragmentManager(), "RemoveAdsDialogFragment");
        Log.d(MainApplication.TAG, "Upgrade button clicked; launching purchase flow for upgrade.");
        MainApplication.getTracker().send(new EventBuilder().setCategory("Button").setAction("Open").setLabel("Upgrade").build());
    }

    public void onDialogRemoveAdsClickOk(DialogFragment dialog) {
        this.mInAppHelper.launchPurchaseFlow(this, IN_APP_REMOVE_ADS_SKU, IN_APP_REQUEST_CODE, this.mPurchaseFinishedListener, BuildConfig.FLAVOR);
    }

    public void onDialogRemoveAdsClickCancel(DialogFragment dialog) {
    }

    protected void createBannerAdView() {
        if (!MainApplication.mIsRemoveAds) {
            this.mBannerAdView = new AdView(this);
            this.mBannerAdView.setId(AD_VIEW_ID);
            this.mBannerAdView.setAdSize(AdSize.SMART_BANNER);
            this.mBannerAdView.setAdUnitId(MainApplication.BANNER_AD_UNIT_ID);
            LayoutParams params = new LayoutParams(-2, -2);
            params.addRule(10);
            this.mBannerAdView.setLayoutParams(params);
            AdRequest.Builder builder = new AdRequest.Builder();
            builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            ArrayList aList = new ArrayList(Arrays.asList(MainApplication.ADMOB_TEST_DEVICE_IDS.split(",")));
            for (int i = 0; i < aList.size(); i++) {
                builder.addTestDevice(aList.get(i).toString());
            }
            this.mBannerAdView.loadAd(builder.build());
            this.mBannerAdView.setAdListener(new AdListener() {
                public void onAdLoaded() {
                    BaseActivity.this.showBannerAd();
                }

                public void onAdFailedToLoad(int errorcode) {
                    BaseActivity.this.hideBannerAd();
                }
            });
        }
    }

    protected void showBannerAd() {
        if (this.mBannerAdView != null && this.mBannerAdView.getParent() == null && !MainApplication.mIsRemoveAds) {
            this.mMainLayout.addView(this.mBannerAdView);
            this.mUiLayout.getLayoutParams().height = mScreenHeight - this.mBannerAdView.getHeight();
            this.mUiLayout.requestLayout();
        }
    }

    protected void hideBannerAd() {
        if (this.mBannerAdView != null) {
            this.mMainLayout.removeView(this.mBannerAdView);
            this.mUiLayout.getLayoutParams().height = mScreenHeight;
            this.mUiLayout.requestLayout();
        }
    }

    protected void resumeAds() {
        if (this.mBannerAdView != null) {
            this.mBannerAdView.resume();
        }
    }

    protected void pauseAds() {
        if (this.mBannerAdView != null) {
            this.mBannerAdView.pause();
        }
    }

    public void createAndLoadInterstitialAdView(Activity curActivity) {
        if (!MainApplication.mIsRemoveAds) {
            this.mInterstitialAdView = new InterstitialAd(curActivity);
            this.mInterstitialAdView.setAdUnitId(MainApplication.INTERSTITIAL_AD_UNIT_ID);
            requestNewInterstitialAd();
            this.mInterstitialAdView.setAdListener(new AnonymousClass5(curActivity));
        }
    }

    public void requestNewInterstitialAd() {
        if (this.mInterstitialAdView != null) {
            AdRequest.Builder builder = new AdRequest.Builder();
            builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            ArrayList aList = new ArrayList(Arrays.asList(MainApplication.ADMOB_TEST_DEVICE_IDS.split(",")));
            for (int i = 0; i < aList.size(); i++) {
                builder.addTestDevice(aList.get(i).toString());
            }
            this.mInterstitialAdView.loadAd(builder.build());
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_GENERAL_LAST_USED_TOOL_KEY, 0) != 7) {
            createBannerAdView();
        }
        createAndLoadInterstitialAdView(this);
        initInAppPurchase();
    }

    protected void onDestroy() {
        if (this.mBannerAdView != null) {
            this.mBannerAdView.destroy();
        }
        if (this.mBroadcastReceiver != null) {
            unregisterReceiver(this.mBroadcastReceiver);
        }
        Log.d(MainApplication.TAG, "Destroying inapp helper.");
        if (this.mInAppHelper != null) {
            this.mInAppHelper.dispose();
            this.mInAppHelper = null;
        }
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        pauseAds();
    }

    protected void onResume() {
        super.onResume();
        resumeAds();
    }

    protected void onButtonMenu() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (MainApplication.mToolsNavigatedCount >= 1) {
            MainApplication.mToolsNavigatedCount = 1;
            if (MainApplication.mIsRemoveAds || !this.mInterstitialAdView.isLoaded()) {
                finish();
                return;
            } else {
                this.mInterstitialAdView.show();
                return;
            }
        }
        MainApplication.mToolsNavigatedCount++;
        finish();
    }

    protected void onButtonSettings() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
    }

    /* renamed from: com.vivekwarde.measure.custom_controls.BaseActivity.5 */
    class AnonymousClass5 extends AdListener {
        final /* synthetic */ Activity val$curActivity;

        AnonymousClass5(Activity activity) {
            this.val$curActivity = activity;
        }

        public void onAdLoaded() {
        }

        public void onAdFailedToLoad(int errorcode) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    BaseActivity.this.requestNewInterstitialAd();
                }
            }, 500);
        }

        public void onAdClosed() {
            this.val$curActivity.finish();
        }
    }
}
