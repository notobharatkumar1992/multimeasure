package com.vivekwarde.measure.custom_controls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.widget.RelativeLayout;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.utilities.MiscUtility;

public class BorderView extends RelativeLayout {
    public static final int ANCHOR_TYPE_BOTTOM = 4;
    public static final int ANCHOR_TYPE_LEFT = 1;
    public static final int ANCHOR_TYPE_NONE = 0;
    public static final int ANCHOR_TYPE_RIGHT = 3;
    public static final int ANCHOR_TYPE_TOP = 2;
    protected double mAnchorPos;
    protected int mAnchorType;
    protected Rect mDstRect;
    protected Bitmap mFrameBitmap;
    protected Rect mSrcRect;
    private int[] mCapPoints;

    public BorderView(Context context) {
        super(context);
        this.mFrameBitmap = null;
        this.mSrcRect = new Rect();
        this.mDstRect = new Rect();
        this.mCapPoints = null;
        setWillNotDraw(false);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.tooltip_background)).getBitmap();
        this.mCapPoints = new int[ANCHOR_TYPE_BOTTOM];
        int ptId = ANCHOR_TYPE_NONE;
        for (int i = ANCHOR_TYPE_LEFT; i < bitmap.getWidth(); i += ANCHOR_TYPE_LEFT) {
            int pxColor = bitmap.getPixel(i, ANCHOR_TYPE_NONE);
            if (ptId == 0 && pxColor != 0) {
                this.mCapPoints[ptId] = i - 1;
                ptId = ANCHOR_TYPE_LEFT;
            }
            if (ptId == ANCHOR_TYPE_LEFT && pxColor == 0) {
                this.mCapPoints[ptId] = i - 1;
                ptId = ANCHOR_TYPE_TOP;
            }
            if (ptId == ANCHOR_TYPE_TOP && pxColor != 0) {
                this.mCapPoints[ptId] = i - 1;
                ptId = ANCHOR_TYPE_RIGHT;
            }
            if (ptId == ANCHOR_TYPE_RIGHT && pxColor == 0) {
                this.mCapPoints[ptId] = i - 1;
                break;
            }
        }
        this.mFrameBitmap = Bitmap.createBitmap(bitmap, ANCHOR_TYPE_LEFT, ANCHOR_TYPE_LEFT, bitmap.getWidth() - 2, bitmap.getHeight() - 2);
    }

    public void construct(int anchortype, double anchorpos) {
        this.mAnchorType = anchortype;
        this.mAnchorPos = anchorpos;
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        if (this.mFrameBitmap != null && ((LayoutParams) getLayoutParams()) != null) {
            int apos;
            int cornerCap = this.mCapPoints[ANCHOR_TYPE_NONE];
            int middleLen = this.mCapPoints[ANCHOR_TYPE_LEFT] - this.mCapPoints[ANCHOR_TYPE_NONE];
            int xArrow = this.mCapPoints[ANCHOR_TYPE_LEFT];
            int arrowLen = this.mCapPoints[ANCHOR_TYPE_TOP] - this.mCapPoints[ANCHOR_TYPE_LEFT];
            int w = getWidth();
            int h = getHeight();
            int ww = this.mFrameBitmap.getWidth();
            int hh = this.mFrameBitmap.getHeight();
            int wa = (w - (cornerCap * ANCHOR_TYPE_TOP)) - arrowLen;
            int ha = (h - (cornerCap * ANCHOR_TYPE_TOP)) - arrowLen;
            MiscUtility.setRect(this.mDstRect, ANCHOR_TYPE_NONE, ANCHOR_TYPE_NONE, cornerCap, cornerCap);
            MiscUtility.setRect(this.mSrcRect, ANCHOR_TYPE_NONE, ANCHOR_TYPE_NONE, cornerCap, cornerCap);
            canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            MiscUtility.setRect(this.mDstRect, w - cornerCap, ANCHOR_TYPE_NONE, cornerCap, cornerCap);
            MiscUtility.setRect(this.mSrcRect, ww - cornerCap, ANCHOR_TYPE_NONE, cornerCap, cornerCap);
            canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            MiscUtility.setRect(this.mDstRect, ANCHOR_TYPE_NONE, h - cornerCap, cornerCap, cornerCap);
            MiscUtility.setRect(this.mSrcRect, ANCHOR_TYPE_NONE, hh - cornerCap, cornerCap, cornerCap);
            canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            MiscUtility.setRect(this.mDstRect, w - cornerCap, h - cornerCap, cornerCap, cornerCap);
            MiscUtility.setRect(this.mSrcRect, ww - cornerCap, hh - cornerCap, cornerCap, cornerCap);
            canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            int r0 = this.mAnchorType;
            if (r0 != ANCHOR_TYPE_TOP) {
                MiscUtility.setRect(this.mDstRect, cornerCap, ANCHOR_TYPE_NONE, w - (cornerCap * ANCHOR_TYPE_TOP), cornerCap);
                MiscUtility.setRect(this.mSrcRect, cornerCap, ANCHOR_TYPE_NONE, middleLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            } else {
                apos = (int) Math.round(this.mAnchorPos * ((double) wa));
                MiscUtility.setRect(this.mDstRect, cornerCap + apos, ANCHOR_TYPE_NONE, arrowLen, cornerCap);
                MiscUtility.setRect(this.mSrcRect, xArrow, ANCHOR_TYPE_NONE, arrowLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, cornerCap, ANCHOR_TYPE_NONE, apos, cornerCap);
                MiscUtility.setRect(this.mSrcRect, cornerCap, ANCHOR_TYPE_NONE, middleLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, (cornerCap + apos) + arrowLen, ANCHOR_TYPE_NONE, wa - apos, cornerCap);
                MiscUtility.setRect(this.mSrcRect, cornerCap, ANCHOR_TYPE_NONE, middleLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            }
            r0 = this.mAnchorType;
            if (r0 != ANCHOR_TYPE_BOTTOM) {
                MiscUtility.setRect(this.mDstRect, cornerCap, h - cornerCap, w - (cornerCap * ANCHOR_TYPE_TOP), cornerCap);
                MiscUtility.setRect(this.mSrcRect, cornerCap, hh - cornerCap, middleLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            } else {
                apos = (int) Math.round(this.mAnchorPos * ((double) wa));
                MiscUtility.setRect(this.mDstRect, cornerCap + apos, h - cornerCap, arrowLen, cornerCap);
                MiscUtility.setRect(this.mSrcRect, xArrow, hh - cornerCap, arrowLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, cornerCap, h - cornerCap, apos, cornerCap);
                MiscUtility.setRect(this.mSrcRect, cornerCap, hh - cornerCap, middleLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, (cornerCap + apos) + arrowLen, h - cornerCap, wa - apos, cornerCap);
                MiscUtility.setRect(this.mSrcRect, cornerCap, hh - cornerCap, middleLen, cornerCap);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            }
            r0 = this.mAnchorType;
            if (r0 != ANCHOR_TYPE_LEFT) {
                MiscUtility.setRect(this.mDstRect, ANCHOR_TYPE_NONE, cornerCap, cornerCap, h - (cornerCap * ANCHOR_TYPE_TOP));
                MiscUtility.setRect(this.mSrcRect, ANCHOR_TYPE_NONE, cornerCap, cornerCap, middleLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            } else {
                apos = (int) Math.round(this.mAnchorPos * ((double) ha));
                MiscUtility.setRect(this.mDstRect, ANCHOR_TYPE_NONE, cornerCap + apos, cornerCap, arrowLen);
                MiscUtility.setRect(this.mSrcRect, ANCHOR_TYPE_NONE, xArrow, cornerCap, arrowLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, ANCHOR_TYPE_NONE, cornerCap, cornerCap, apos);
                MiscUtility.setRect(this.mSrcRect, ANCHOR_TYPE_NONE, cornerCap, cornerCap, middleLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, ANCHOR_TYPE_NONE, (cornerCap + apos) + arrowLen, cornerCap, ha - apos);
                MiscUtility.setRect(this.mSrcRect, ANCHOR_TYPE_NONE, cornerCap, cornerCap, middleLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            }
            r0 = this.mAnchorType;
            if (r0 != ANCHOR_TYPE_RIGHT) {
                MiscUtility.setRect(this.mDstRect, w - cornerCap, cornerCap, cornerCap, h - (cornerCap * ANCHOR_TYPE_TOP));
                MiscUtility.setRect(this.mSrcRect, ww - cornerCap, cornerCap, cornerCap, middleLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            } else {
                apos = (int) Math.round(this.mAnchorPos * ((double) ha));
                MiscUtility.setRect(this.mDstRect, w - cornerCap, cornerCap + apos, cornerCap, arrowLen);
                MiscUtility.setRect(this.mSrcRect, ww - cornerCap, xArrow, cornerCap, arrowLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, w - cornerCap, cornerCap, cornerCap, apos);
                MiscUtility.setRect(this.mSrcRect, ww - cornerCap, cornerCap, cornerCap, middleLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
                MiscUtility.setRect(this.mDstRect, w - cornerCap, (cornerCap + apos) + arrowLen, cornerCap, ha - apos);
                MiscUtility.setRect(this.mSrcRect, ww - cornerCap, cornerCap, cornerCap, middleLen);
                canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            }
            MiscUtility.setRect(this.mDstRect, cornerCap, cornerCap, w - (cornerCap * ANCHOR_TYPE_TOP), h - (cornerCap * ANCHOR_TYPE_TOP));
            MiscUtility.setRect(this.mSrcRect, cornerCap, cornerCap, cornerCap, cornerCap);
            canvas.drawBitmap(this.mFrameBitmap, this.mSrcRect, this.mDstRect, null);
            setDrawingCacheEnabled(true);
        }
    }

    public int getCornerCapSize() {
        return this.mCapPoints[ANCHOR_TYPE_NONE];
    }

    public int getArrowCapSize() {
        return this.mCapPoints[ANCHOR_TYPE_TOP] - this.mCapPoints[ANCHOR_TYPE_LEFT];
    }

    public int getAnchorType() {
        return this.mAnchorType;
    }

    public double getAnchorPos() {
        return this.mAnchorPos;
    }
}
