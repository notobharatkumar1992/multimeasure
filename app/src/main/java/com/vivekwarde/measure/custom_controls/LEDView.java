package com.vivekwarde.measure.custom_controls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;

public class LEDView extends RelativeLayout {
    protected ImageView[] mColonView;
    protected ImageView[][] mLEDView;
    Bitmap[] mColonBitmap;
    Bitmap[] mDigit;
    Bitmap mDigit8BacklitBitmap;
    Bitmap[] mDotBitmap;
    boolean mIsLapView;
    int mLEDType;
    int mOverlapWidth;
    ImageView[][] r0;

    @Deprecated
    public LEDView(Context context) {
        super(context);
        r0 = new ImageView[4][];
        r0[0] = new ImageView[]{null, null};
        r0[1] = new ImageView[]{null, null};
        r0[2] = new ImageView[]{null, null};
        r0[3] = new ImageView[]{null, null};
        this.mLEDView = r0;
        this.mColonView = new ImageView[]{null, null, null};
        this.mDigit = new Bitmap[]{null, null, null, null, null, null, null, null, null, null};
        this.mLEDType = 0;
        this.mIsLapView = false;
        this.mDigit8BacklitBitmap = null;
        this.mColonBitmap = new Bitmap[]{null, null};
        this.mDotBitmap = new Bitmap[]{null, null};
        this.mOverlapWidth = 0;
    }

    public LEDView(Context context, int ledType) {
        super(context);
        r0 = new ImageView[4][];
        r0[0] = new ImageView[]{null, null};
        r0[1] = new ImageView[]{null, null};
        r0[2] = new ImageView[]{null, null};
        r0[3] = new ImageView[]{null, null};
        this.mLEDView = r0;
        this.mColonView = new ImageView[]{null, null, null};
        this.mDigit = new Bitmap[]{null, null, null, null, null, null, null, null, null, null};
        this.mLEDType = 0;
        this.mIsLapView = false;
        this.mDigit8BacklitBitmap = null;
        this.mColonBitmap = new Bitmap[]{null, null};
        this.mDotBitmap = new Bitmap[]{null, null};
        this.mOverlapWidth = 0;
        setBackgroundColor(0);
        this.mLEDType = ledType;
        this.mOverlapWidth = (int) getResources().getDimension(this.mLEDType == 0 ? R.dimen.STOPWATCH_LED_OVERLAP_SIZE : R.dimen.TIMER_LED_OVERLAP_SIZE);
        initImages();
        initSubviews();
    }

    void initImages() {
        Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(this.mLEDType == 0 ? R.drawable.stopwatch_digits : R.drawable.timer_digits)).getBitmap();
        int w = bitmap.getWidth() / (this.mLEDType == 0 ? 15 : 13);
        int h = bitmap.getHeight();
        for (int i = 0; i < 10; i++) {
            this.mDigit[i] = Bitmap.createBitmap(bitmap, i * w, 0, w, h);
        }
        this.mDigit8BacklitBitmap = Bitmap.createBitmap(bitmap, w * 10, 0, w, h);
        if (this.mLEDType == 0) {
            this.mColonBitmap[0] = Bitmap.createBitmap(bitmap, w * 11, 0, w, h);
            this.mColonBitmap[1] = Bitmap.createBitmap(bitmap, w * 12, 0, w, h);
            this.mDotBitmap[0] = Bitmap.createBitmap(bitmap, w * 13, 0, w, h);
            this.mDotBitmap[1] = Bitmap.createBitmap(bitmap, w * 14, 0, w, h);
            return;
        }
        Bitmap[] bitmapArr = this.mColonBitmap;
        Bitmap[] bitmapArr2 = this.mColonBitmap;
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, w * 11, 0, w, h);
        bitmapArr2[1] = createBitmap;
        bitmapArr[0] = createBitmap;
        bitmapArr = this.mDotBitmap;
        bitmapArr2 = this.mDotBitmap;
        createBitmap = Bitmap.createBitmap(bitmap, w * 12, 0, w, h);
        bitmapArr2[1] = createBitmap;
        bitmapArr[0] = createBitmap;
    }

    protected void initSubviews() {
        int id;
        int i;
        int id2 = 113;
        int i2 = 0;
        while (i2 < 4) {
            int j = 0;
            id = id2;
            while (j < 2) {
                int l = (int) (((((double) (this.mDigit8BacklitBitmap.getWidth() * 2)) - (SeismometerGraphView.MAX_ACCELERATION * ((double) this.mOverlapWidth))) * ((double) i2)) + ((double) ((this.mDigit8BacklitBitmap.getWidth() - this.mOverlapWidth) * j)));
                this.mLEDView[i2][j] = new ImageView(getContext());
                id2 = id + 1;
                this.mLEDView[i2][j].setId(id);
                this.mLEDView[i2][j].setImageBitmap(this.mDigit[0]);
                LayoutParams params = new LayoutParams(-2, -2);
                params.addRule(15);
                params.setMargins(l, 0, 0, 0);
                this.mLEDView[i2][j].setLayoutParams(params);
                addView(this.mLEDView[i2][j]);
                j++;
                id = id2;
            }
            i2++;
            id2 = id;
        }
        Bitmap[] bitmap = new Bitmap[3];
        bitmap[0] = this.mColonBitmap[this.mIsLapView ? 1 : 0];
        Bitmap[] bitmapArr = this.mColonBitmap;
        if (this.mIsLapView) {
            i = 1;
        } else {
            i = 0;
        }
        bitmap[1] = bitmapArr[i];
        bitmapArr = this.mDotBitmap;
        if (this.mIsLapView) {
            i = 1;
        } else {
            i = 0;
        }
        bitmap[2] = bitmapArr[i];
        i2 = 0;
        id = id2;
        while (i2 < 3) {
            this.mColonView[i2] = new ImageView(getContext());
            id2 = id + 1;
            this.mColonView[i2].setId(id);
            this.mColonView[i2].setImageBitmap(bitmap[i2]);
            LayoutParams params = new LayoutParams(-2, -2);
            params.addRule(15);
            params.addRule(5, this.mLEDView[i2][1].getId());
            params.addRule(7, this.mLEDView[i2 + 1][0].getId());
            this.mColonView[i2].setLayoutParams(params);
            addView(this.mColonView[i2]);
            i2++;
            id = id2;
        }
    }

    public void setTime(double seconds) {
        if (seconds > 359999.0d) {
            seconds = 359999.0d;
        }
        if (seconds < 0.0d) {
            seconds = 0.0d;
        }
        int hours = (int) ((seconds / 60.0d) / 60.0d);
        int mins = (int) ((seconds / 60.0d) - ((double) (hours * 60)));
        int secs = (int) ((seconds - ((double) ((hours * 60) * 60))) - ((double) (mins * 60)));
        int sec = (int) ((seconds - Math.floor(seconds)) * 100.0d);
        this.mLEDView[0][0].setImageBitmap(this.mDigit[hours / 10]);
        this.mLEDView[0][1].setImageBitmap(this.mDigit[hours % 10]);
        this.mLEDView[1][0].setImageBitmap(this.mDigit[mins / 10]);
        this.mLEDView[1][1].setImageBitmap(this.mDigit[mins % 10]);
        this.mLEDView[2][0].setImageBitmap(this.mDigit[secs / 10]);
        this.mLEDView[2][1].setImageBitmap(this.mDigit[secs % 10]);
        this.mLEDView[3][0].setImageBitmap(this.mDigit[sec / 10]);
        this.mLEDView[3][1].setImageBitmap(this.mDigit[sec % 10]);
    }

    public void turnOn() {
        int i;
        for (i = 0; i < 4; i++) {
            for (int j = 0; j < 2; j++) {
                this.mLEDView[i][j].setVisibility(0);
            }
        }
        for (i = 0; i < 3; i++) {
            this.mColonView[i].setVisibility(0);
        }
        this.mIsLapView = false;
    }

    public void turnOff() {
        int i;
        for (i = 0; i < 4; i++) {
            for (int j = 0; j < 2; j++) {
                this.mLEDView[i][j].setVisibility(4);
            }
        }
        for (i = 0; i < 3; i++) {
            this.mColonView[i].setVisibility(4);
        }
        this.mIsLapView = true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mDigit8BacklitBitmap == null && this.mDigit[0] == null) {
            Log.i(getClass().getName(), "Digit bitmap load failed");
            return;
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 2; j++) {
                canvas.drawBitmap(this.mDigit8BacklitBitmap, (float) ((int) (((((double) (this.mDigit8BacklitBitmap.getWidth() * 2)) - (SeismometerGraphView.MAX_ACCELERATION * ((double) this.mOverlapWidth))) * ((double) i)) + ((double) ((this.mDigit8BacklitBitmap.getWidth() - this.mOverlapWidth) * j)))), (float) ((getHeight() - this.mDigit8BacklitBitmap.getHeight()) / 2), null);
            }
        }
        setDrawingCacheEnabled(true);
    }

    protected class LEDIndex {
        public static final int FS = 3;
        public static final int FS1 = 0;
        public static final int FS2 = 1;
        public static final int H = 0;
        public static final int H1 = 0;
        public static final int H2 = 1;
        public static final int M = 1;
        public static final int M1 = 0;
        public static final int M2 = 1;
        public static final int S = 2;
        public static final int S1 = 0;
        public static final int S2 = 1;

        protected LEDIndex() {
        }
    }

    public class LEDType {
        public static final int LED_TYPE_STOPWATCH = 0;
        public static final int LED_TYPE_TIMER = 1;
    }
}
