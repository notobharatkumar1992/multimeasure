package com.vivekwarde.measure.custom_controls;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

public class LineView extends View {
    private Paint mPaint;
    private Path mPath;

    public LineView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        this.mPaint = new Paint();
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeWidth(metrics.density + 0.5f);
        this.mPaint.setColor(ViewCompat.MEASURED_STATE_MASK);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setPathEffect(new DashPathEffect(new float[]{20.0f, 15.0f}, 0.0f));
        this.mPath = new Path();
    }

    protected void onDraw(Canvas canvas) {
        this.mPath.reset();
        int measuredHeight = getHeight();
        int measuredWidth = getWidth();
        if (measuredHeight <= measuredWidth) {
            this.mPath.moveTo((float) measuredWidth, (float) (measuredHeight / 2));
            this.mPath.lineTo(0.0f, (float) (measuredHeight / 2));
            canvas.drawPath(this.mPath, this.mPaint);
        } else {
            this.mPath.moveTo((float) (measuredWidth / 2), (float) measuredHeight);
            this.mPath.lineTo((float) (measuredWidth / 2), 0.0f);
            canvas.drawPath(this.mPath, this.mPaint);
        }
        setDrawingCacheEnabled(true);
    }
}
