package com.vivekwarde.measure.custom_controls;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;
import com.vivekwarde.measure.utilities.SPTimer;

public class MyImageView extends ImageView implements Runnable {
    private SPTimer mAnimationTimer;
    private Bitmap[] mBitmaps;
    private boolean mIsHighlighted;

    public MyImageView(Context context) {
        super(context);
        this.mBitmaps = new Bitmap[]{null, null};
        this.mIsHighlighted = false;
    }

    public void setBitmaps(Bitmap normalBitmap, Bitmap highlightedBitmap) {
        int i = 1;
        this.mBitmaps[0] = normalBitmap;
        this.mBitmaps[1] = highlightedBitmap;
        Bitmap[] bitmapArr = this.mBitmaps;
        if (!this.mIsHighlighted) {
            i = 0;
        }
        setImageBitmap(bitmapArr[i]);
    }

    public void setHighlighted(boolean highlighted) {
        this.mIsHighlighted = highlighted;
        setImageBitmap(this.mBitmaps[this.mIsHighlighted ? 1 : 0]);
    }

    public void setHidden(boolean hidden) {
        setVisibility(hidden ? 4 : 0);
    }

    public void startAnimation(int duration) {
        if (duration <= 0) {
            duration = 100;
        }
        if (this.mAnimationTimer == null) {
            this.mAnimationTimer = new SPTimer(new Handler(), this, duration, false);
        }
        if (!this.mAnimationTimer.isEnabled()) {
            this.mAnimationTimer.start();
        }
    }

    public void stopAnimation() {
        if (this.mAnimationTimer != null) {
            this.mAnimationTimer.stop();
        }
        setHighlighted(false);
    }

    public void run() {
        boolean z;
        int i = 1;
        if (this.mIsHighlighted) {
            z = false;
        } else {
            z = true;
        }
        this.mIsHighlighted = z;
        Bitmap[] bitmapArr = this.mBitmaps;
        if (!this.mIsHighlighted) {
            i = 0;
        }
        setImageBitmap(bitmapArr[i]);
    }
}
