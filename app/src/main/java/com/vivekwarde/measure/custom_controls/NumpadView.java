package com.vivekwarde.measure.custom_controls;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;

import com.google.android.gms.cast.TextTrackStyle;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

public class NumpadView extends RelativeLayout implements OnClickListener, OnTouchListener {
    public static final int NUMPAD_MODE_METRONOME = 0;
    public static final int NUMPAD_MODE_PROTRACTOR = 1;
    public static final int NUMPAD_MODE_RULER = 2;
    protected int mBitmapHeight;
    protected int mBitmapWidth;
    Rect mClientFrame;
    NumpadViewEventListener mListener;
    int mNumpadMode;
    Paint mPaint;

    @Deprecated
    public NumpadView(Context context) {
        super(context);
        this.mBitmapWidth = NUMPAD_MODE_METRONOME;
        this.mBitmapHeight = NUMPAD_MODE_METRONOME;
        this.mPaint = null;
        this.mListener = null;
        this.mNumpadMode = NUMPAD_MODE_PROTRACTOR;
        this.mClientFrame = null;
    }

    public NumpadView(Context context, int numpadMode, int width, int height, int topMargin, int bottomMargin) {
        super(context);
        this.mBitmapWidth = NUMPAD_MODE_METRONOME;
        this.mBitmapHeight = NUMPAD_MODE_METRONOME;
        this.mPaint = null;
        this.mListener = null;
        this.mNumpadMode = NUMPAD_MODE_PROTRACTOR;
        this.mClientFrame = null;
        setWillNotDraw(false);
        setBackgroundColor(NUMPAD_MODE_METRONOME);
        this.mNumpadMode = numpadMode;
        this.mClientFrame = new Rect(NUMPAD_MODE_METRONOME, topMargin, width, height - bottomMargin);
        this.mPaint = new Paint();
        this.mPaint.setARGB(SeismometerGraphView.HISTORY_SIZE_PER_PAGE, NUMPAD_MODE_METRONOME, NUMPAD_MODE_METRONOME, NUMPAD_MODE_METRONOME);
        initSubviews();
        setOnTouchListener(this);
    }

    public void setListener(NumpadViewEventListener listener) {
        this.mListener = listener;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void initSubviews() {
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.button_keyboard_ninepatch)).getBitmap();
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Eurostile LT Medium.ttf");
        this.mBitmapWidth = bitmap.getWidth() - 2;
        this.mBitmapHeight = bitmap.getHeight() - 2;
        int[] iArr = new int[NUMPAD_MODE_RULER];
        iArr = new int[]{5, 4};
        int i = NUMPAD_MODE_RULER;
        int[] colCount = new int[]{3, 4};
        int index = this.mNumpadMode == 0 ? NUMPAD_MODE_METRONOME : NUMPAD_MODE_PROTRACTOR;
        String[][] arrTitle = new String[NUMPAD_MODE_RULER][];
        arrTitle[NUMPAD_MODE_METRONOME] = new String[]{"1", "2", "C", "3", "4", BuildConfig.FLAVOR, "5", "6", "Done", "7", "8", BuildConfig.FLAVOR, "9", "0", BuildConfig.FLAVOR, BuildConfig.FLAVOR};
        arrTitle[NUMPAD_MODE_PROTRACTOR] = new String[]{"1", "2", "3", "C", "4", "5", "6", BuildConfig.FLAVOR, "7", "8", "9", "Done", "0", BuildConfig.FLAVOR, ".", BuildConfig.FLAVOR};
        int nVSpace = (this.mClientFrame.height() - (this.mBitmapHeight * iArr[index])) / (iArr[index] + NUMPAD_MODE_PROTRACTOR);
        int nHSpace = nVSpace;
        int nLeft = (this.mClientFrame.width() - ((this.mBitmapWidth * colCount[index]) + ((colCount[index] - 1) * nHSpace))) / NUMPAD_MODE_RULER;
        int nTop = this.mClientFrame.top + nVSpace;
        Bitmap[] stateBitmap = new Bitmap[]{null, null, null};
        for (int col = NUMPAD_MODE_METRONOME; col < colCount[index]; col += NUMPAD_MODE_PROTRACTOR) {
            for (int row = NUMPAD_MODE_METRONOME; row < iArr[index]; row += NUMPAD_MODE_PROTRACTOR) {
                String text = arrTitle[index][(colCount[index] * row) + col];
                if (text != BuildConfig.FLAVOR) {
                    int w;
                    int h;
                    Bitmap setBitmap;
                    SPButton btn;
                    int[][] iArr2;
                    int[] iArr3;
                    int[] iArr4;
                    LayoutParams layoutParams;
                    int x = ((this.mBitmapWidth * col) + nLeft) + (col * nHSpace);
                    int y = ((this.mBitmapHeight * row) + nTop) + (row * nVSpace);
                    if (text.compareTo("C") != 0) {
                        if (text.compareTo("Done") != 0) {
                            if (text.compareTo("0") == 0) {
                                w = (this.mBitmapWidth * NUMPAD_MODE_RULER) + nHSpace;
                                h = this.mBitmapHeight;
                                if (stateBitmap[NUMPAD_MODE_PROTRACTOR] == null) {
                                    stateBitmap[NUMPAD_MODE_PROTRACTOR] = ImageUtility.scale9Bitmap(bitmap, w, h);
                                }
                                setBitmap = stateBitmap[NUMPAD_MODE_PROTRACTOR];
                            } else {
                                w = this.mBitmapWidth;
                                h = this.mBitmapHeight;
                                if (stateBitmap[NUMPAD_MODE_RULER] == null) {
                                    stateBitmap[NUMPAD_MODE_RULER] = ImageUtility.scale9Bitmap(bitmap, w, h);
                                }
                                setBitmap = stateBitmap[NUMPAD_MODE_RULER];
                            }
                            btn = new SPButton(getContext());
                            btn.setBackgroundAutoTransparentBitmap(setBitmap);
                            btn.setId((col * 10) + row);
                            btn.setTypeface(font);
                            btn.setText(text);
                            iArr2 = new int[NUMPAD_MODE_RULER][];
                            iArr3 = new int[NUMPAD_MODE_PROTRACTOR];
                            iArr3[NUMPAD_MODE_METRONOME] = 16842919;
                            iArr2[NUMPAD_MODE_METRONOME] = iArr3;
                            iArr2[NUMPAD_MODE_PROTRACTOR] = new int[NUMPAD_MODE_METRONOME];
                            iArr4 = new int[NUMPAD_MODE_RULER];
                            iArr4[NUMPAD_MODE_METRONOME] = Color.argb(100, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK);
                            iArr4[NUMPAD_MODE_PROTRACTOR] = Color.argb(SeismometerGraphView.HISTORY_SIZE_PER_PAGE, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK);
                            btn.setTextColor(new ColorStateList(iArr2, iArr4));
                            btn.setTextSize(NUMPAD_MODE_PROTRACTOR, getResources().getDimension(R.dimen.NUMPAD_FONT_SIZE));
                            btn.setOnClickListener(this);
                            layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                            layoutParams.addRule(10);
                            layoutParams.addRule(9);
                            layoutParams.width = w;
                            layoutParams.height = h;
                            layoutParams.setMargins(x, y, NUMPAD_MODE_METRONOME, NUMPAD_MODE_METRONOME);
                            btn.setLayoutParams(layoutParams);
                            addView(btn);
                        }
                    }
                    w = this.mBitmapWidth;
                    h = (this.mBitmapHeight * NUMPAD_MODE_RULER) + nVSpace;
                    if (stateBitmap[NUMPAD_MODE_METRONOME] == null) {
                        stateBitmap[NUMPAD_MODE_METRONOME] = ImageUtility.scale9Bitmap(bitmap, w, h);
                    }
                    setBitmap = stateBitmap[NUMPAD_MODE_METRONOME];
                    btn = new SPButton(getContext());
                    btn.setBackgroundAutoTransparentBitmap(setBitmap);
                    btn.setId((col * 10) + row);
                    btn.setTypeface(font);
                    btn.setText(text);
                    iArr2 = new int[NUMPAD_MODE_RULER][];
                    iArr3 = new int[NUMPAD_MODE_PROTRACTOR];
                    iArr3[NUMPAD_MODE_METRONOME] = 16842919;
                    iArr2[NUMPAD_MODE_METRONOME] = iArr3;
                    iArr2[NUMPAD_MODE_PROTRACTOR] = new int[NUMPAD_MODE_METRONOME];
                    iArr4 = new int[NUMPAD_MODE_RULER];
                    iArr4[NUMPAD_MODE_METRONOME] = Color.argb(100, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK);
                    iArr4[NUMPAD_MODE_PROTRACTOR] = Color.argb(SeismometerGraphView.HISTORY_SIZE_PER_PAGE, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK);
                    btn.setTextColor(new ColorStateList(iArr2, iArr4));
                    btn.setTextSize(NUMPAD_MODE_PROTRACTOR, getResources().getDimension(R.dimen.NUMPAD_FONT_SIZE));
                    btn.setOnClickListener(this);
                    layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams.addRule(10);
                    layoutParams.addRule(9);
                    layoutParams.width = w;
                    layoutParams.height = h;
                    layoutParams.setMargins(x, y, NUMPAD_MODE_METRONOME, NUMPAD_MODE_METRONOME);
                    btn.setLayoutParams(layoutParams);
                    addView(btn);
                }
            }
        }
    }

    public void onClick(View v) {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(NUMPAD_MODE_PROTRACTOR, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mListener != null) {
            this.mListener.onNumpadEvent(((SPButton) v).getText().toString());
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(this.mClientFrame, this.mPaint);
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!(event.getAction() != 0 || this.mClientFrame.contains((int) event.getX(), (int) event.getY()) || this.mListener == null)) {
            this.mListener.onNumpadEvent("Done");
        }
        return true;
    }

    public interface NumpadViewEventListener {
        void onNumpadEvent(String str);
    }
}
