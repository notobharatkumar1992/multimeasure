package com.vivekwarde.measure.custom_controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;
import com.google.android.gms.cast.TextTrackStyle;
import java.util.ArrayList;

public class SPMultiStateButton extends Button {
    private int mState;
    private int mStateCount;
    ArrayList<BitmapDrawable> mStateDrawableArray;
    final String tag;

    public SPMultiStateButton(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mStateCount = 2;
        this.mState = 0;
        this.mStateDrawableArray = new ArrayList();
        setBackgroundColor(0);
        setSoundEffectsEnabled(false);
        setPadding(0, 0, 0, getPaddingBottom());
        setTextSize(TextTrackStyle.DEFAULT_FONT_SCALE);
    }

    void separateBackgroundBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight() / this.mStateCount;
        this.mStateDrawableArray.clear();
        for (int i = 0; i < this.mStateCount; i++) {
            BitmapDrawable bd = new BitmapDrawable(getResources(), Bitmap.createBitmap(bitmap, 0, i * height, width, height));
            bd.setGravity(17);
            this.mStateDrawableArray.add(bd);
        }
    }

    @SuppressLint({"NewApi"})
    void updateState() {
        if (this.mState < 0 || this.mState >= this.mStateDrawableArray.size()) {
            Log.i(this.tag, "Invalid state, must be in range [0, " + (this.mStateDrawableArray.size() - 1) + "]");
            return;
        }
        BitmapDrawable state = (BitmapDrawable) this.mStateDrawableArray.get(this.mState);
        if (state == null) {
            Log.i(this.tag, "Background bitmap is null");
        } else if (VERSION.SDK_INT < 16) {
            setBackgroundDrawable(state);
        } else {
            setBackground(state);
        }
    }

    public void setBackgroundBitmapId(int resId, int stateCount) {
        setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(getContext(), resId)).getBitmap(), stateCount);
    }

    public void setBackgroundBitmap(Bitmap bitmap, int stateCount) {
        this.mStateCount = stateCount;
        separateBackgroundBitmap(bitmap);
        updateState();
    }

    public void setState(int state) {
        this.mState = state;
        updateState();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            this.mState++;
            if (this.mState >= this.mStateCount) {
                this.mState = 0;
            }
            updateState();
        }
        return super.onTouchEvent(event);
    }
}
