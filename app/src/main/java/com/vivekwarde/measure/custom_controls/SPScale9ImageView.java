package com.vivekwarde.measure.custom_controls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.RelativeLayout;
import com.vivekwarde.measure.utilities.ImageUtility;

public class SPScale9ImageView extends RelativeLayout {
    Paint mBPaint;
    int mBottomCapHeight;
    Paint mCPaint;
    Rect mDstRect;
    Paint mLPaint;
    int mLeftCapWidth;
    Paint mRPaint;
    int mRightCapWidth;
    Bitmap mScale9Bitmap;
    Rect mSrcRect;
    Paint mTPaint;
    int mTopCapHeight;

    public SPScale9ImageView(Context context) {
        super(context);
        this.mScale9Bitmap = null;
        this.mLeftCapWidth = 0;
        this.mRightCapWidth = 0;
        this.mTopCapHeight = 0;
        this.mBottomCapHeight = 0;
        this.mSrcRect = new Rect();
        this.mDstRect = new Rect();
        this.mLPaint = new Paint();
        this.mTPaint = new Paint();
        this.mBPaint = new Paint();
        this.mRPaint = new Paint();
        this.mCPaint = new Paint();
        setBackgroundColor(0);
    }

    public void setBitmapResource(int resId) {
        setBitmap(((BitmapDrawable) ContextCompat.getDrawable(getContext(), resId)).getBitmap());
    }

    public void setBitmap(Bitmap scale9Bitmap, int leftCapWidth, int rightCapWidth, int topCapHeight, int bottomCapHeight) {
        if (scale9Bitmap == null) {
            Log.i(getClass().getName(), "Bitmap is null");
            return;
        }
        this.mScale9Bitmap = scale9Bitmap;
        this.mLeftCapWidth = leftCapWidth;
        this.mRightCapWidth = rightCapWidth;
        this.mTopCapHeight = topCapHeight;
        this.mBottomCapHeight = bottomCapHeight;
        int srcW = this.mScale9Bitmap.getWidth();
        int srcH = this.mScale9Bitmap.getHeight();
        this.mSrcRect.set(this.mLeftCapWidth, 0, srcW - this.mRightCapWidth, this.mTopCapHeight);
        this.mTPaint.setShader(new BitmapShader(Bitmap.createBitmap(this.mScale9Bitmap, this.mSrcRect.left, this.mSrcRect.top, this.mSrcRect.width(), this.mSrcRect.height()), TileMode.REPEAT, TileMode.REPEAT));
        this.mSrcRect.set(this.mLeftCapWidth, srcH - this.mBottomCapHeight, srcW - this.mRightCapWidth, srcH);
        this.mBPaint.setShader(new BitmapShader(Bitmap.createBitmap(this.mScale9Bitmap, this.mSrcRect.left, this.mSrcRect.top, this.mSrcRect.width(), this.mSrcRect.height()), TileMode.REPEAT, TileMode.REPEAT));
        this.mSrcRect.set(0, this.mTopCapHeight, this.mLeftCapWidth, srcH - this.mBottomCapHeight);
        this.mLPaint.setShader(new BitmapShader(Bitmap.createBitmap(this.mScale9Bitmap, this.mSrcRect.left, this.mSrcRect.top, this.mSrcRect.width(), this.mSrcRect.height()), TileMode.REPEAT, TileMode.REPEAT));
        this.mSrcRect.set(srcW - this.mRightCapWidth, this.mTopCapHeight, srcW, srcH - this.mBottomCapHeight);
        this.mRPaint.setShader(new BitmapShader(Bitmap.createBitmap(this.mScale9Bitmap, this.mSrcRect.left, this.mSrcRect.top, this.mSrcRect.width(), this.mSrcRect.height()), TileMode.REPEAT, TileMode.REPEAT));
        this.mSrcRect.set(this.mLeftCapWidth, this.mTopCapHeight, srcW - this.mRightCapWidth, srcH - this.mBottomCapHeight);
        this.mCPaint.setShader(new BitmapShader(Bitmap.createBitmap(this.mScale9Bitmap, this.mSrcRect.left, this.mSrcRect.top, this.mSrcRect.width(), this.mSrcRect.height()), TileMode.REPEAT, TileMode.REPEAT));
        invalidate();
    }

    public void setBitmap(Bitmap scale9Bitmap) {
        if (scale9Bitmap == null) {
            Log.i(getClass().getName(), "Bitmap is null");
            return;
        }
        int[] capSizes = ImageUtility.getNinePatchCapSizes(scale9Bitmap);
        int leftCapWidth = capSizes[0];
        int rightCapWidth = capSizes[2];
        int topCapHeight = capSizes[1];
        int bottomCapHeight = capSizes[3];
        setBitmap(Bitmap.createBitmap(scale9Bitmap, 1, 1, scale9Bitmap.getWidth() - 2, scale9Bitmap.getHeight() - 2), leftCapWidth, rightCapWidth, topCapHeight, bottomCapHeight);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mScale9Bitmap == null) {
            Log.i(getClass().getName(), "Bitmap not set yet");
        } else if (this.mLeftCapWidth + this.mRightCapWidth >= getWidth() || this.mTopCapHeight + this.mBottomCapHeight >= getHeight()) {
            Log.i(getClass().getName(), "Kich co scale phai lon hon kich co bitmap! Hoac kich co CAP qua to so voi bitmap");
        } else {
            int srcW = this.mScale9Bitmap.getWidth();
            int srcH = this.mScale9Bitmap.getHeight();
            int dstW = getWidth();
            int dstH = getHeight();
            this.mSrcRect.set(0, 0, this.mLeftCapWidth, this.mTopCapHeight);
            this.mDstRect.set(0, 0, this.mLeftCapWidth, this.mTopCapHeight);
            canvas.drawBitmap(this.mScale9Bitmap, this.mSrcRect, this.mDstRect, null);
            this.mSrcRect.set(srcW - this.mRightCapWidth, 0, srcW, this.mTopCapHeight);
            this.mDstRect.set(dstW - this.mRightCapWidth, 0, dstW, this.mTopCapHeight);
            canvas.drawBitmap(this.mScale9Bitmap, this.mSrcRect, this.mDstRect, null);
            this.mSrcRect.set(0, srcH - this.mBottomCapHeight, this.mLeftCapWidth, srcH);
            this.mDstRect.set(0, dstH - this.mBottomCapHeight, this.mLeftCapWidth, dstH);
            canvas.drawBitmap(this.mScale9Bitmap, this.mSrcRect, this.mDstRect, null);
            this.mSrcRect.set(srcW - this.mRightCapWidth, srcH - this.mBottomCapHeight, srcW, srcH);
            this.mDstRect.set(dstW - this.mRightCapWidth, dstH - this.mBottomCapHeight, dstW, dstH);
            canvas.drawBitmap(this.mScale9Bitmap, this.mSrcRect, this.mDstRect, null);
            canvas.save();
            this.mDstRect.set(0, 0, (dstW - this.mLeftCapWidth) - this.mRightCapWidth, this.mTopCapHeight);
            canvas.translate((float) this.mLeftCapWidth, 0.0f);
            canvas.drawRect(this.mDstRect, this.mTPaint);
            canvas.restore();
            canvas.save();
            this.mDstRect.set(0, 0, (dstW - this.mLeftCapWidth) - this.mRightCapWidth, this.mBottomCapHeight);
            canvas.translate((float) this.mLeftCapWidth, (float) (dstH - this.mBottomCapHeight));
            canvas.drawRect(this.mDstRect, this.mBPaint);
            canvas.restore();
            canvas.save();
            this.mDstRect.set(0, 0, this.mLeftCapWidth, (dstH - this.mTopCapHeight) - this.mBottomCapHeight);
            canvas.translate(0.0f, (float) this.mTopCapHeight);
            canvas.drawRect(this.mDstRect, this.mLPaint);
            canvas.restore();
            canvas.save();
            this.mDstRect.set(0, 0, this.mRightCapWidth, (dstH - this.mTopCapHeight) - this.mBottomCapHeight);
            canvas.translate((float) (dstW - this.mRightCapWidth), (float) this.mTopCapHeight);
            canvas.drawRect(this.mDstRect, this.mRPaint);
            canvas.restore();
            canvas.save();
            this.mDstRect.set(0, 0, (dstW - this.mLeftCapWidth) - this.mRightCapWidth, (dstH - this.mTopCapHeight) - this.mBottomCapHeight);
            canvas.translate((float) this.mLeftCapWidth, (float) this.mTopCapHeight);
            canvas.drawRect(this.mDstRect, this.mCPaint);
            canvas.restore();
            setDrawingCacheEnabled(true);
        }
    }
}
