package com.vivekwarde.measure.decibel;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.R.string;
import com.vivekwarde.measure.custom_controls.ActionView;
import com.vivekwarde.measure.custom_controls.ActionView.OnActionViewEventListener;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.surface_level.SurfaceLevelActivity.ConstantId;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

public class DecibelActivity extends BaseActivity implements OnClickListener, OnTouchListener, OnActionViewEventListener, DialogInterface.OnClickListener, Runnable, OnCancelListener {
    final String tag;
    DecibelLEDView mCurrentLabel;
    double mCurrentValue;
    SPImageButton mInfoButton;
    boolean mIsRunning;
    DecibelLEDView mMaxLabel;
    double mMaxValue;
    SPImageButton mMenuButton;
    DecibelLEDView mPeakLabel;
    int mPeakResetCount;
    double mPeakValue;
    long mPollingDurationValue;
    SPImageButton mStartStopButton;
    boolean mUserRunning;
    private ActionView mActionView;
    private AlertDialog mClearGraphDialog;
    private String mDataString;
    private ArrayList<String> mDecibelLevelNames;
    private DecibelGraphView mGraphView;
    private ImageView mHandView;
    private Handler mHandler;
    private int mHeaderBarHeight;
    private AlertDialog mResetMaxValueDialog;
    private int mSampleCount;
    private TextView mSoundLevelLabel;
    private SoundMeter2 mSoundMeter;

    public DecibelActivity() {
        this.tag = getClass().getSimpleName();
        this.mIsRunning = true;
        this.mUserRunning = true;
        this.mStartStopButton = null;
        this.mInfoButton = null;
        this.mMenuButton = null;
        this.mSoundLevelLabel = null;
        this.mActionView = null;
        this.mHandler = new Handler();
    }

    @SuppressLint({"NewApi"})
    public void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        initInfo();
        initSubviews();
        initSensor();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    void initSensor() {
        this.mSoundMeter = new SoundMeter2();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        updateSettingChanged();
        if (this.mUserRunning) {
            start();
        }
    }

    protected void onPause() {
        super.onPause();
        if (this.mUserRunning) {
            pause();
        }
    }

    void start() {
        if (this.mUserRunning) {
            this.mIsRunning = this.mSoundMeter.startRecording();
            if (this.mIsRunning) {
                this.mHandler.postDelayed(this, this.mPollingDurationValue);
            }
        }
        updateButtonState();
    }

    public void run() {
        double[] values = new double[2];
        this.mSoundMeter.getDecibelValues(values);
        this.mCurrentValue = values[0];
        this.mPeakValue = values[1];
        pollingDecibel();
        this.mHandler.postDelayed(this, this.mPollingDurationValue);
    }

    void pollingDecibel() {
        if (this.mCurrentValue > 0.0d) {
            if (this.mMaxValue < this.mPeakValue) {
                this.mMaxValue = this.mPeakValue;
            }
            recordValue();
            if (this.mIsRunning) {
                updateValueDecibel();
                decibelMeterAnalog(this.mCurrentValue);
                updateSoundLevel(this.mCurrentValue);
            }
        }
    }

    void recordValue() {
        if (this.mSampleCount > GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE) {
            MiscUtility.writeToFileWithLimitedSize(this, "DecibelData.csv", this.mDataString, true, 3);
            this.mDataString = null;
            this.mSampleCount = 0;
        }
        new Time().setToNow();
        String formatPattern = "%02d-%02d-%02d,%02d:%02d:%02d,%f,%f\n";
        Calendar calendar = Calendar.getInstance();
        String sTemp = String.format(Locale.US, "%02d-%02d-%02d,%02d:%02d:%02d,%f,%f\n", new Object[]{Integer.valueOf(calendar.get(Calendar.YEAR)), Integer.valueOf(calendar.get(Calendar.MONTH) + 1), Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH)), Integer.valueOf(calendar.get(Calendar.HOUR)), Integer.valueOf(calendar.get(Calendar.MINUTE)), Integer.valueOf(calendar.get(Calendar.SECOND)), Double.valueOf(this.mCurrentValue), Double.valueOf(this.mPeakValue)});
        if (this.mDataString == null) {
            this.mDataString = sTemp;
        } else {
            this.mDataString += sTemp;
        }
        this.mSampleCount++;
    }

    void updateSoundLevel(double decibel) {
        int index;
        if (decibel < 20.0d) {
            index = 0;
        } else if (decibel < 30.0d) {
            index = 1;
        } else if (decibel < 40.0d) {
            index = 2;
        } else if (decibel < 50.0d) {
            index = 3;
        } else if (decibel < 60.0d) {
            index = 4;
        } else if (decibel < 70.0d) {
            index = 5;
        } else if (decibel < 75.0d) {
            index = 6;
        } else if (decibel < 80.0d) {
            index = 7;
        } else if (decibel < 88.0d) {
            index = 8;
        } else if (decibel < 90.0d) {
            index = 9;
        } else if (decibel < 94.0d) {
            index = 10;
        } else if (decibel < 100.0d) {
            index = 11;
        } else if (decibel < 107.0d) {
            index = 12;
        } else if (decibel < 115.0d) {
            index = 13;
        } else if (decibel < 117.0d) {
            index = 14;
        } else if (decibel < 120.0d) {
            index = 15;
        } else if (decibel < 130.0d) {
            index = 16;
        } else {
            index = 17;
        }
        this.mSoundLevelLabel.setText((CharSequence) this.mDecibelLevelNames.get(index));
    }

    void pause() {
        this.mIsRunning = false;
        this.mHandler.removeCallbacks(this);
        this.mSoundMeter.stopRecording();
        updateButtonState();
    }

    void initInfo() {
        this.mSampleCount = 0;
        this.mDataString = null;
        this.mPeakResetCount = 0;
        this.mIsRunning = false;
        this.mDecibelLevelNames = new ArrayList();
        for (int i = 0; i < 18; i++) {
            this.mDecibelLevelNames.add(getResources().getString(MiscUtility.getId(String.format(Locale.US, "IDS_DECIBEL_LEVEL_NAME_%d", new Object[]{Integer.valueOf(i + 1)}), string.class)));
        }
        initDataFiles();
    }

    void initDataFiles() {
        deleteFile("DecibelData.csv");
        deleteFile("DecibelData.zip");
        MiscUtility.writeToFile(this, "DecibelData.csv", "Date,Time,Average,Peak\n", false);
    }

    @SuppressLint({"NewApi"})
    void initSubviews() {
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams layoutParams = new LayoutParams(-1, mScreenHeight);
        layoutParams.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(layoutParams);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        int frameMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_VERT_MARGIN_BTW_DOTLEDSCREEN_AND_HEADER);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap();
        this.mHeaderBarHeight = (bitmap.getHeight() / 2) + (vButtonScreenMargin * 2);
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(bitmap);
        this.mMenuButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        layoutParams.height = bitmap.getHeight() / 2;
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        this.mInfoButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mInfoButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mMenuButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mInfoButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap();
        SPScale9ImageView backgroundView = new SPScale9ImageView(this);
        backgroundView.setId(6);
        backgroundView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.topMargin = this.mHeaderBarHeight;
        backgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(backgroundView);
        float fontSize = getResources().getDimension(R.dimen.DECIBEL_OUTPUT_FONT_SIZE);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Eurostile LT Medium.ttf");
        this.mSoundLevelLabel = new TextView(this);
        this.mSoundLevelLabel.setId(100);
        this.mSoundLevelLabel.setPaintFlags(this.mSoundLevelLabel.getPaintFlags() | Barcode.ITF);
        this.mSoundLevelLabel.setTextColor(ContextCompat.getColor(this, R.color.DECIBEL_LABEL_COLOR));
        this.mSoundLevelLabel.setTypeface(font);
        this.mSoundLevelLabel.setTextSize(1, fontSize);
        this.mSoundLevelLabel.setGravity(19);
        this.mSoundLevelLabel.setText(BuildConfig.FLAVOR);
        this.mSoundLevelLabel.setOnTouchListener(this);
        layoutParams = new LayoutParams(-2, this.mHeaderBarHeight);
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        layoutParams.setMargins(hButtonButtonMargin, 0, 0, 0);
        this.mSoundLevelLabel.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mSoundLevelLabel);
        int tempMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_DOTLEDSCREEN_AND_SCREEN);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();
        int[] capSizes = ImageUtility.getContentCapSizes(bitmap);
        SPScale9ImageView footerLedBackground = new SPScale9ImageView(this);
        footerLedBackground.setId(121);
        footerLedBackground.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, bitmap.getHeight() - 2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.height = bitmap.getHeight() - 2;
        layoutParams.setMargins(tempMargin, 0, tempMargin, frameMargin);
        footerLedBackground.setLayoutParams(layoutParams);
        this.mUiLayout.addView(footerLedBackground);
        View footerLedDotBackground = new View(this);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_dot)).getBitmap());
        bitmapDrawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            footerLedDotBackground.setBackgroundDrawable(bitmapDrawable);
        } else {
            footerLedDotBackground.setBackground(bitmapDrawable);
        }
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, footerLedBackground.getId());
        layoutParams.addRule(6, footerLedBackground.getId());
        layoutParams.addRule(7, footerLedBackground.getId());
        layoutParams.addRule(8, footerLedBackground.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        footerLedDotBackground.setLayoutParams(layoutParams);
        this.mUiLayout.addView(footerLedDotBackground);
        this.mCurrentLabel = new DecibelLEDView(this, 3, "stopwatch");
        this.mCurrentLabel.setId(9);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(8, footerLedBackground.getId());
        layoutParams.setMargins(0, 0, 0, capSizes[3]);
        this.mCurrentLabel.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mCurrentLabel);
        this.mPeakLabel = new DecibelLEDView(this, 3, "timer");
        this.mPeakLabel.setId(8);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(7, footerLedBackground.getId());
        layoutParams.addRule(8, footerLedBackground.getId());
        layoutParams.setMargins(0, 0, capSizes[2], capSizes[3]);
        this.mPeakLabel.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mPeakLabel);
        this.mMaxLabel = new DecibelLEDView(this, 3, "timer");
        this.mMaxLabel.setId(7);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, footerLedBackground.getId());
        layoutParams.addRule(8, footerLedBackground.getId());
        layoutParams.setMargins(capSizes[0], 0, 0, capSizes[3]);
        this.mMaxLabel.setLayoutParams(layoutParams);
        this.mMaxLabel.setOnClickListener(this);
        this.mUiLayout.addView(this.mMaxLabel);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.decibel_max)).getBitmap();
        ImageView imageView = new ImageView(this);
        imageView.setId(10);
        imageView.setScaleType(ScaleType.CENTER);
        imageView.setImageBitmap(bitmap);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(5, this.mMaxLabel.getId());
        layoutParams2.addRule(7, this.mMaxLabel.getId());
        layoutParams2.addRule(6, footerLedBackground.getId());
        layoutParams2.setMargins(0, capSizes[1], 0, 0);
        imageView.setLayoutParams(layoutParams2);
        imageView.setOnClickListener(this);
        this.mUiLayout.addView(imageView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.decibel_peak)).getBitmap();
        imageView = new ImageView(this);
        imageView.setId(11);
        imageView.setImageBitmap(bitmap);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, this.mPeakLabel.getId());
        layoutParams.addRule(7, this.mPeakLabel.getId());
        layoutParams.addRule(6, footerLedBackground.getId());
        layoutParams.setMargins(0, capSizes[1], 0, 0);
        imageView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(imageView);
        footerLedBackground.bringToFront();
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();
        Bitmap gaugeBitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.decibel_gauge)).getBitmap();
        SPScale9ImageView gaugeFrameView = new SPScale9ImageView(this);
        gaugeFrameView.setId(123);
        gaugeFrameView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, gaugeBitmap.getHeight() + capSizes[1]);
        layoutParams.addRule(14);
        layoutParams.addRule(2, footerLedBackground.getId());
        layoutParams.setMargins(hButtonButtonMargin, 0, hButtonButtonMargin, hButtonButtonMargin);
        gaugeFrameView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(gaugeFrameView);
        View gaugeDotBackgroundView = new View(this);
        if (VERSION.SDK_INT < 16) {
            gaugeDotBackgroundView.setBackgroundDrawable(bitmapDrawable);
        } else {
            gaugeDotBackgroundView.setBackground(bitmapDrawable);
        }
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, gaugeFrameView.getId());
        layoutParams.addRule(6, gaugeFrameView.getId());
        layoutParams.addRule(7, gaugeFrameView.getId());
        layoutParams.addRule(8, gaugeFrameView.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        gaugeDotBackgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(gaugeDotBackgroundView);
        gaugeFrameView.bringToFront();
        imageView = new ImageView(this);
        imageView.setId(GamesStatusCodes.STATUS_REQUEST_UPDATE_TOTAL_FAILURE);
        imageView.setImageBitmap(gaugeBitmap);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(8, gaugeFrameView.getId());
        layoutParams.addRule(14);
        layoutParams.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(imageView);
        gaugeFrameView.bringToFront();
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.decibel_hand)).getBitmap();
        this.mHandView = new ImageView(this);
        this.mHandView.setId(ConstantId.TIMER_PERIOD_MAX);
        this.mHandView.setImageBitmap(bitmap);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(8, gaugeFrameView.getId());
        layoutParams.addRule(14);
        layoutParams.setMargins(0, 0, 0, 0);
        this.mHandView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mHandView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.decibel_button_start)).getBitmap();
        tempMargin = (footerLedBackground.getLayoutParams().height - (bitmap.getHeight() / 2)) / 2;
        this.mStartStopButton = new SPImageButton(this);
        this.mStartStopButton.setId(3);
        this.mStartStopButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(8, gaugeFrameView.getId());
        layoutParams.addRule(14);
        layoutParams.setMargins(0, 0, 0, (-bitmap.getHeight()) / 4);
        this.mStartStopButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mStartStopButton);
        updateButtonState();
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();


        SPScale9ImageView imageView1 = new SPScale9ImageView(this);
        imageView1.setId(1999);
        imageView1.setBitmap(bitmap);

        layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(10);
        layoutParams.addRule(2, gaugeFrameView.getId());
        layoutParams.setMargins(hButtonButtonMargin, this.mHeaderBarHeight + frameMargin, hButtonButtonMargin, hButtonButtonMargin);
        imageView1.setLayoutParams(layoutParams);
        this.mUiLayout.addView(imageView1);
        this.mGraphView = new DecibelGraphView(this);
        this.mGraphView.setId(4);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, imageView1.getId());
        layoutParams.addRule(6, imageView1.getId());
        layoutParams.addRule(7, imageView1.getId());
        layoutParams.addRule(8, imageView1.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        this.mGraphView.setLayoutParams(layoutParams);
        this.mGraphView.setOnClickListener(this);
        this.mUiLayout.addView(this.mGraphView);
        imageView1.bringToFront();
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mInfoButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mNoAdsButton.setLayoutParams(layoutParams);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
    }

    void onStartStop() {
        this.mUserRunning = !this.mUserRunning;
        if (this.mUserRunning) {
            start();
        } else {
            pause();
        }
    }

    void reset() {
        this.mMaxValue = 0.0d;
        Arrays.fill(DecibelGraphView.arrayCoordinateY, 0.0f);
        DecibelGraphView.count = 0;
        this.mGraphView.invalidate();
        this.mDataString = null;
        this.mSampleCount = 0;
        initDataFiles();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case Barcode.ALL_FORMATS /*0*/:
                onRemoveAdsButton();
            case CompletionEvent.STATUS_FAILURE /*1*/:
                onButtonMenu();
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                onButtonSettings();
            case CompletionEvent.STATUS_CANCELED /*3*/:
                onStartStop();
            case Barcode.PHONE /*4*/:
                openActionView(true);
            case Barcode.TEXT /*7*/:
            case Barcode.GEO /*10*/:
                resetMaxValue();
            default:
                Log.i(this.tag, "Unknown action id :(");
        }
    }

    void openActionView(boolean open) {
        if (open) {
            pause();
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            ArrayList<String> titleList = new ArrayList();
            titleList.add(getResources().getString(R.string.IDS_SEND_DATA_TO_EMAIL));
            titleList.add(getResources().getString(R.string.IDS_CLEAR_GRAPH));
            titleList.add(getResources().getString(R.string.IDS_CLOSE));
            Rect r = new Rect();
            this.mGraphView.getGlobalVisibleRect(r);
            int lmargin = ((LayoutParams) this.mGraphView.getLayoutParams()).leftMargin;
            int rmargin = ((LayoutParams) this.mGraphView.getLayoutParams()).rightMargin;
            this.mActionView = new ActionView(this);
            this.mActionView.construct(lmargin, rmargin, new Point(r.centerX(), r.centerY()), titleList, 0);
            this.mActionView.setLayoutParams(new LayoutParams(-1, -1));
            this.mActionView.setOnActionViewEventListener(this);
            this.mUiLayout.addView(this.mActionView);
        } else if (this.mActionView != null) {
            this.mUiLayout.removeView(this.mActionView);
            this.mActionView = null;
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    void updateButtonState() {
        this.mStartStopButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(this.mIsRunning ? R.drawable.decibel_button_stop : R.drawable.decibel_button_start)).getBitmap());
    }

    public void onActionViewEvent(ActionView actionView, int actionId) {
        if (actionId == 0) {
            emailData();
        } else if (actionId == 1) {
            clearGraph();
        } else if (actionId == 2) {
            start();
        }
        openActionView(false);
    }

    void clearGraph() {
        this.mClearGraphDialog = new Builder(this).create();
        this.mClearGraphDialog.setMessage(getResources().getString(R.string.IDS_CLEAR_ALL_HISTORY_ASKING));
        this.mClearGraphDialog.setButton(-1, getResources().getString(17039379), this);
        this.mClearGraphDialog.setButton(-2, getResources().getString(17039369), this);
        this.mClearGraphDialog.setIcon(R.drawable.icon_about);
        this.mClearGraphDialog.setTitle(getResources().getString(R.string.IDS_CONFIRMATION));
        this.mClearGraphDialog.setOnCancelListener(this);
        this.mClearGraphDialog.show();
        MiscUtility.setDialogFontSize(this, this.mClearGraphDialog);
    }

    void emailData() {
        if (this.mDataString != null) {
            MiscUtility.writeToFile(this, "DecibelData.csv", this.mDataString, true);
        }
        try {
            MiscUtility.zip(new String[]{getFilesDir() + "/DecibelData.csv"}, getFilesDir() + "/DecibelData.zip");
            MiscUtility.sendEmail(this, "mailto:", "Decibel recorded data", getResources().getString(R.string.IDS_THANKS_FOR_USING), "DecibelData.zip");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void resetMaxValue() {
        this.mResetMaxValueDialog = new Builder(this).create();
        this.mResetMaxValueDialog.setMessage(getResources().getString(R.string.IDS_RESET_MAX_VALUE_ASKING));
        this.mResetMaxValueDialog.setButton(-1, getResources().getString(17039379), this);
        this.mResetMaxValueDialog.setButton(-2, getResources().getString(17039369), this);
        this.mResetMaxValueDialog.setIcon(R.drawable.icon_about);
        this.mResetMaxValueDialog.setTitle(getResources().getString(R.string.IDS_CONFIRMATION));
        this.mResetMaxValueDialog.show();
        MiscUtility.setDialogFontSize(this, this.mResetMaxValueDialog);
    }

    public void onBackPressed() {
        if (this.mActionView != null) {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            openActionView(false);
            start();
            return;
        }
        super.onBackPressed();
    }

    public void onCancel(DialogInterface dialog) {
        if (dialog.equals(this.mClearGraphDialog)) {
            start();
        }
    }

    public void onClick(DialogInterface dialog, int which) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        switch (which) {
            case ValueAnimator.INFINITE /*-1*/:
                if (!dialog.equals(this.mClearGraphDialog)) {
                    this.mMaxValue = 0.0d;
                    this.mMaxLabel.setNumber(0);
                    break;
                }
                reset();
                break;
        }
        if (dialog.equals(this.mClearGraphDialog)) {
            start();
        }
        this.mClearGraphDialog = null;
        this.mResetMaxValueDialog = null;
    }

    void updateValueDecibel() {
        int bonus = 0;
        if (this.mCurrentValue < 0.0d) {
            bonus = 120;
        }
        if (DecibelGraphView.count < Strategy.TTL_SECONDS_DEFAULT) {
            DecibelGraphView.count++;
        }
        DecibelGraphView.arrayCoordinateX[0] = (float) this.mGraphView.getWidth();
        for (int i = DecibelGraphView.count - 1; i > 0; i--) {
            DecibelGraphView.arrayCoordinateY[i] = DecibelGraphView.arrayCoordinateY[i - 1];
            DecibelGraphView.arrayCoordinateX[i] = DecibelGraphView.arrayCoordinateX[i - 1] - DecibelGraphView.distancePoint;
        }
        DecibelGraphView.arrayCoordinateY[0] = (float) this.mCurrentValue;
        DecibelGraphView.startX = (int) (((float) DecibelGraphView.startX) - DecibelGraphView.distancePoint);
        DecibelGraphView.startX -= DecibelGraphView.gridLength * ((int) Math.floor((double) (DecibelGraphView.startX / DecibelGraphView.gridLength)));
        this.mGraphView.invalidate();
        this.mCurrentLabel.setNumber(((int) this.mCurrentValue) + bonus);
        this.mPeakLabel.setNumber(((int) this.mPeakValue) + bonus);
        this.mMaxLabel.setNumber((int) this.mMaxValue);
    }

    void decibelMeterAnalog(double decibel) {
        ViewHelper.setPivotX(this.mHandView, (float) (this.mHandView.getWidth() / 2));
        ViewHelper.setPivotY(this.mHandView, (float) this.mHandView.getHeight());
        float rotate_angle_decibel = (float) (55.38461538461539d * ((decibel / 55.0d) - 1.0d));
        ObjectAnimator a1 = ObjectAnimator.ofFloat(this.mHandView, "rotation", rotate_angle_decibel);
        a1.setDuration(this.mPollingDurationValue / 2);
        a1.setStartDelay(0);
        a1.start();
    }

    void updateSettingChanged() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        this.mPollingDurationValue = (long) (1000.0f / ((float) pref.getInt(SettingsKey.SETTINGS_DECIBEL_UPDATE_FREQUENCY_KEY, 10)));
        this.mSoundMeter.setCalibrationValue((double) pref.getInt(SettingsKey.SETTINGS_DECIBEL_CALIBRATION_VALUE_KEY, 0));
    }

    public class Define {
        public static final int ACTION_ID_CLEAR_GRAPH = 1;
        public static final int ACTION_ID_CLOSE = 2;
        public static final int ACTION_ID_SEND_DATA_TO_EMAIL = 0;
        public static final int ACTION_VIEW = 5;
        public static final int BACKGROUND_VIEW = 6;
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_START_STOP = 3;
        public static final int BUTTON_UPGRADE = 0;
        public static final int CURRENT_LED = 9;
        public static final int GRAPH_VIEW = 4;
        public static final int LABEL00 = 0;
        public static final int MAX_LABEL = 10;
        public static final int MAX_LED = 7;
        public static final int PEAK_LABEL = 11;
        public static final int PEAK_LED = 8;
    }
}
