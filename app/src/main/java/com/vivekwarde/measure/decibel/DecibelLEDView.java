package com.vivekwarde.measure.decibel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.vivekwarde.measure.R;

public class DecibelLEDView extends RelativeLayout {
    private Bitmap[] mDigit;
    private Bitmap mDigit8BacklitBitmap;
    private String mFile;
    private ImageView[] mLED;
    private int mNumberOfDigit;
    private int mOverlapWidth;
    final String tag;

    @Deprecated
    public DecibelLEDView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mLED = null;
        this.mDigit = null;
        this.mDigit8BacklitBitmap = null;
        this.mOverlapWidth = 0;
    }

    public DecibelLEDView(Context context, int digitCount, String file) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mLED = null;
        this.mDigit = null;
        this.mDigit8BacklitBitmap = null;
        this.mOverlapWidth = 0;
        this.mNumberOfDigit = digitCount;
        this.mFile = file;
        initImages();
        initSubviews();
    }

    void initImages() {
        Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(this.mFile.equalsIgnoreCase("stopwatch") ? R.drawable.stopwatch_digits : R.drawable.timer_digits)).getBitmap();
        int w = bitmap.getWidth() / (this.mFile.equalsIgnoreCase("stopwatch") ? 15 : 13);
        int h = bitmap.getHeight();
        this.mDigit = new Bitmap[10];
        for (int i = 0; i < 10; i++) {
            this.mDigit[i] = Bitmap.createBitmap(bitmap, i * w, 0, w, h);
        }
        this.mDigit8BacklitBitmap = Bitmap.createBitmap(bitmap, w * 10, 0, w, h);
        this.mOverlapWidth = (int) getResources().getDimension(this.mFile.equalsIgnoreCase("stopwatch") ? R.dimen.STOPWATCH_LED_OVERLAP_SIZE : R.dimen.TIMER_LED_OVERLAP_SIZE);
    }

    void initSubviews() {
        this.mLED = new ImageView[this.mNumberOfDigit];
        for (int i = 0; i < this.mNumberOfDigit; i++) {
            ImageView backlitLED = new ImageView(getContext());
            backlitLED.setImageBitmap(this.mDigit8BacklitBitmap);
            LayoutParams params = new LayoutParams(-2, -2);
            params.addRule(15);
            params.addRule(9);
            params.setMargins((this.mDigit8BacklitBitmap.getWidth() - this.mOverlapWidth) * i, 0, 0, 0);
            backlitLED.setLayoutParams(params);
            addView(backlitLED);
            this.mLED[i] = new ImageView(getContext());
            this.mLED[i].setImageBitmap(this.mDigit[0]);
            params = new LayoutParams(-2, -2);
            params.addRule(15);
            params.addRule(9);
            params.setMargins((this.mDigit[0].getWidth() - this.mOverlapWidth) * i, 0, 0, 0);
            this.mLED[i].setLayoutParams(params);
            addView(this.mLED[i]);
        }
    }

    void setNumber(int number) {
        int[] val = new int[this.mNumberOfDigit];
        if (number < 0) {
            Log.i(this.tag, "Number must be >0");
            return;
        }
        int i;
        int temp = (int) Math.pow(10.0d, (double) (this.mNumberOfDigit - 1));
        for (i = 0; i < this.mNumberOfDigit; i++) {
            val[i] = number / temp;
            number %= temp;
            temp /= 10;
        }
        boolean blur8 = true;
        for (i = 0; i < this.mNumberOfDigit; i++) {
            if (val[i] != 0) {
                blur8 = false;
            } else if (i == this.mNumberOfDigit - 1) {
                this.mLED[i].setImageBitmap(this.mDigit[0]);
            } else if (blur8) {
                this.mLED[i].setImageBitmap(this.mDigit8BacklitBitmap);
            }
            this.mLED[i].setImageBitmap(this.mDigit[val[i]]);
        }
    }
}
