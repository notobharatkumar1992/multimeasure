package com.vivekwarde.measure.decibel;

import android.media.MediaRecorder;
import java.io.IOException;

public class SoundMeter {
    private static final double EMA_FILTER = 0.6d;
    private double mEMA;
    private MediaRecorder mRecorder;

    public SoundMeter() {
        this.mRecorder = null;
        this.mEMA = 0.0d;
    }

    public boolean start() {
        if (this.mRecorder == null) {
            try {
                this.mRecorder = new MediaRecorder();
                this.mRecorder.setAudioSource(1);
                this.mRecorder.setOutputFormat(1);
                this.mRecorder.setOutputFile("/dev/null");
                this.mRecorder.setAudioEncoder(1);
                this.mEMA = 0.0d;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        try {
            this.mRecorder.prepare();
            this.mRecorder.start();
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public void stop() {
        try {
            this.mRecorder.stop();
            this.mRecorder.release();
            this.mRecorder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double getAmplitude() {
        if (this.mRecorder != null) {
            return 20.0d * Math.log10((double) this.mRecorder.getMaxAmplitude());
        }
        return 0.0d;
    }

    public double getAmplitudeEMA() {
        this.mEMA = (EMA_FILTER * getAmplitude()) + (0.4d * this.mEMA);
        return this.mEMA;
    }
}
