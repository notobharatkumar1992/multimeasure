package com.vivekwarde.measure.decibel;

import android.media.AudioRecord;

public class SoundMeter2 {
    private static final int RECORDER_AUDIO_ENCODING = 2;
    private static final int RECORDER_CHANNELS = 16;
    private static final int RECORDER_SAMPLERATE = 44100;
    private AudioRecord mAudioRecorder;
    private double mCalibrationValue;
    private double mCurrentValue;
    private boolean mIsRecording;
    private int mMinBufferSize;
    private double mPeakValue;
    private Thread mRecordingThread;

    public SoundMeter2() {
        this.mAudioRecorder = null;
        this.mMinBufferSize = 0;
        this.mRecordingThread = null;
        this.mIsRecording = false;
        this.mCurrentValue = 0.0d;
        this.mPeakValue = 0.0d;
        this.mCalibrationValue = 0.0d;
        initInfo();
    }

    private void initInfo() {
        this.mMinBufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
    }

    boolean startRecording() {
        boolean ret = true;
        try {
            this.mAudioRecorder = new AudioRecord(1, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, this.mMinBufferSize);
            this.mAudioRecorder.startRecording();
        } catch (Exception e) {
            ret = false;
            e.printStackTrace();
        }
        this.mIsRecording = ret;
        if (ret) {
            this.mRecordingThread = new Thread(new Runnable() {
                public void run() {
                    SoundMeter2.this.processRawData();
                }
            }, "AudioRecorder Thread");
            this.mRecordingThread.start();
        }
        return ret;
    }

    private void processRawData() {
        short[] data = new short[(this.mMinBufferSize / RECORDER_AUDIO_ENCODING)];
        while (this.mIsRecording) {
            int read = this.mAudioRecorder.read(data, 0, this.mMinBufferSize / RECORDER_AUDIO_ENCODING);
            if (!(read == -3 || read == -2)) {
                calculateSPL(data);
            }
        }
    }

    void calculateSPL(short[] samples) {
        double previousFilteredValueOfSampleAmplitude = 0.0d;
        double peakValue = 0.0d;
        double sampleDB = 0.0d;
        for (short abs : samples) {
            double currentFilteredValueOfSampleAmplitude = (0.001d * ((double) Math.abs(abs))) + ((1.0d - 0.001d) * previousFilteredValueOfSampleAmplitude);
            previousFilteredValueOfSampleAmplitude = currentFilteredValueOfSampleAmplitude;
            sampleDB = (20.0d * Math.log10(currentFilteredValueOfSampleAmplitude)) + 0.0d;
            if (sampleDB <= Double.MAX_VALUE && sampleDB >= -1.7976931348623157E308d && sampleDB > peakValue) {
                peakValue = sampleDB;
            }
        }
        this.mCurrentValue = this.mCalibrationValue + sampleDB;
        if (this.mPeakValue < this.mCalibrationValue + peakValue) {
            this.mPeakValue = this.mCalibrationValue + peakValue;
        }
    }

    void stopRecording() {
        try {
            if (this.mAudioRecorder != null) {
                this.mIsRecording = false;
                this.mAudioRecorder.stop();
                this.mAudioRecorder.release();
                this.mAudioRecorder = null;
                this.mRecordingThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDecibelValues(double[] values) {
        values[0] = this.mCurrentValue;
        values[1] = this.mPeakValue;
        this.mPeakValue = 0.0d;
    }

    public void setCalibrationValue(double calibrationValue) {
        this.mCalibrationValue = calibrationValue;
    }
}
