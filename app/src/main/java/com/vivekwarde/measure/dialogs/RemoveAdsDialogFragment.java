package com.vivekwarde.measure.dialogs;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

import com.vivekwarde.measure.R;

public class RemoveAdsDialogFragment extends DialogFragment {
    NoticeDialogListener mListener;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder builder = new Builder(getActivity());
        builder.setIcon(R.drawable.icon_about);
        builder.setTitle(getResources().getString(R.string.IDS_REMOVE_ADS_DIALOG_TITLE));
        builder.setMessage(getResources().getString(R.string.IDS_REMOVE_ADS_DIALOG_MESSAGE)).setPositiveButton(getResources().getString(R.string.IDS_OK_UPGRADE), new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                RemoveAdsDialogFragment.this.mListener.onDialogRemoveAdsClickOk(RemoveAdsDialogFragment.this);
            }
        }).setNegativeButton(getResources().getString(R.string.IDS_NO_THANKS), new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                RemoveAdsDialogFragment.this.mListener.onDialogRemoveAdsClickCancel(RemoveAdsDialogFragment.this);
            }
        });
        return builder.create();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement NoticeDialogListener");
        }
    }

    public interface NoticeDialogListener {
        void onDialogRemoveAdsClickCancel(DialogFragment dialogFragment);

        void onDialogRemoveAdsClickOk(DialogFragment dialogFragment);
    }
}
