package com.vivekwarde.measure.dialogs;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

import com.vivekwarde.measure.R;

public class UnlockBarometerDialogFragment extends DialogFragment {
    NoticeDialogListener mListener;

    public interface NoticeDialogListener {
        void onDialogUnlockBarometerClickCancel(DialogFragment dialogFragment);

        void onDialogUnlockBarometerClickOk(DialogFragment dialogFragment);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder builder = new Builder(getActivity());
        builder.setIcon(R.drawable.icon_barometer);
        builder.setTitle(getResources().getString(R.string.IDS_UNLOCK) + " " + getResources().getString(R.string.IDS_TOOL_NAME_14));
        builder.setMessage(getResources().getString(R.string.IDS_UNLOCK_BAROMETER_DIALOG_MESSAGE)).setPositiveButton(getResources().getString(R.string.IDS_OK_UNLOCK), new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                UnlockBarometerDialogFragment.this.mListener.onDialogUnlockBarometerClickOk(UnlockBarometerDialogFragment.this);
            }
        }).setNegativeButton(getResources().getString(R.string.IDS_NO_THANKS), new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                UnlockBarometerDialogFragment.this.mListener.onDialogUnlockBarometerClickCancel(UnlockBarometerDialogFragment.this);
            }
        });
        return builder.create();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement NoticeDialogListener");
        }
    }
}
