package com.vivekwarde.measure.menu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.vision.barcode.Barcode;
import com.sbstrm.appirater.Appirater;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.R.string;
import com.vivekwarde.measure.dialogs.UnlockAltimeterDialogFragment;
import com.vivekwarde.measure.dialogs.UnlockAltimeterDialogFragment.NoticeDialogListener;
import com.vivekwarde.measure.dialogs.UnlockBarometerDialogFragment;
import com.vivekwarde.measure.inapp_purchase.IabBroadcastReceiver;
import com.vivekwarde.measure.inapp_purchase.IabBroadcastReceiver.IabBroadcastListener;
import com.vivekwarde.measure.inapp_purchase.IabHelper;
import com.vivekwarde.measure.inapp_purchase.IabHelper.OnIabPurchaseFinishedListener;
import com.vivekwarde.measure.inapp_purchase.IabHelper.OnIabSetupFinishedListener;
import com.vivekwarde.measure.inapp_purchase.IabHelper.QueryInventoryFinishedListener;
import com.vivekwarde.measure.inapp_purchase.IabResult;
import com.vivekwarde.measure.inapp_purchase.Inventory;
import com.vivekwarde.measure.inapp_purchase.Purchase;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.Locale;

public class MenuActivity extends Activity implements AnimationListener, NoticeDialogListener, UnlockBarometerDialogFragment.NoticeDialogListener, IabBroadcastListener {
    public static final int IN_APP_REQUEST_CODE = 10001;
    public static final String IN_APP_UNLOCK_ALTIMETER_SKU = "com.vivek." + "unlock_altimeter";
    public static final String IN_APP_UNLOCK_BAROMETER_SKU = "com.vivek." + "unlock_barometer";
    final int LEDSCREEN_ID;
    final int MENUWHEELVIEW_ID;
    final String tag;
    IabBroadcastReceiver mBroadcastReceiver;
    TextView mCodeLabel;
    boolean mIsOK;
    RelativeLayout mLayout;
    RelativeLayout mLedScreen;
    ImageView mLockImage;
    MenuWheelView mMenuWheelView;
    TextView mNameLabel;
    int mScreenHeight;
    int mScreenWidth;
    private QueryInventoryFinishedListener mGotInventoryListener;
    private IabHelper mInAppHelper;
    private boolean mIsPressureSensorAvailable;
    private OnIabPurchaseFinishedListener mPurchaseFinishedListener;

    public MenuActivity() {
        this.tag = getClass().getSimpleName();
        this.LEDSCREEN_ID = 1;
        this.MENUWHEELVIEW_ID = 2;
        this.mLayout = null;
        this.mCodeLabel = null;
        this.mNameLabel = null;
        this.mLockImage = null;
        this.mLedScreen = null;
        this.mMenuWheelView = null;
        this.mIsOK = false;
        this.mScreenWidth = 0;
        this.mScreenHeight = 0;
        this.mBroadcastReceiver = null;
        this.mInAppHelper = null;
        this.mIsPressureSensorAvailable = false;
        this.mGotInventoryListener = new QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                boolean z = true;
                Log.d(MainApplication.TAG, "Query inventory finished.");
                if (MenuActivity.this.mInAppHelper != null) {
                    if (result.isFailure()) {
                        MenuActivity.this.complain("Failed to query inventory: " + result);
                        return;
                    }
                    boolean z2;
                    Log.d(MainApplication.TAG, "Query inventory was successful.");
                    Purchase unlockAltimeterPurchase = inventory.getPurchase(MenuActivity.IN_APP_UNLOCK_ALTIMETER_SKU);
                    if (unlockAltimeterPurchase == null || !MenuActivity.this.verifyDeveloperPayload(unlockAltimeterPurchase)) {
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                    MainApplication.mIsAltimeterUnlocked = z2;
                    Log.d(MainApplication.TAG, "Altimeter is " + (MainApplication.mIsAltimeterUnlocked ? "UNLOCKED" : "still LOCKED"));
                    Purchase unlockBarometerPurchase = inventory.getPurchase(MenuActivity.IN_APP_UNLOCK_BAROMETER_SKU);
                    if (unlockBarometerPurchase == null || !MenuActivity.this.verifyDeveloperPayload(unlockBarometerPurchase)) {
                        z = false;
                    }
                    MainApplication.mIsBarometerUnlocked = z;
                    Log.d(MainApplication.TAG, "Barometer is " + (MainApplication.mIsBarometerUnlocked ? "UNLOCKED" : "still LOCKED"));
                    MenuActivity.this.onUpdateLabels();
                    Log.d(MainApplication.TAG, "Initial inventory query finished; enabling main UI.");
                }
            }
        };
        this.mPurchaseFinishedListener = new OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                Log.d(MainApplication.TAG, "Purchase finished: " + result + ", purchase: " + purchase);
                if (MenuActivity.this.mInAppHelper != null) {
                    if (result.isFailure()) {
                        Log.d(MainApplication.TAG, "Error purchasing:" + result);
                    } else if (MenuActivity.this.verifyDeveloperPayload(purchase)) {
                        Log.d(MainApplication.TAG, "Purchase successful.");
                        if (purchase.getSku().equals(MenuActivity.IN_APP_UNLOCK_ALTIMETER_SKU)) {
                            Log.d(MainApplication.TAG, "Purchase is unlock altimeter. Congratulating user.");
                            MenuActivity.this.alert(MenuActivity.this.getString(R.string.IDS_THANK_YOU_FOR_UNLOCKING_ALTIMETER));
                            MainApplication.mIsAltimeterUnlocked = true;
                            MenuActivity.this.savePurchasesLocally();
                            MenuActivity.this.onUpdateLabels();
                        } else if (purchase.getSku().equals(MenuActivity.IN_APP_UNLOCK_BAROMETER_SKU)) {
                            Log.d(MainApplication.TAG, "Purchase is unlock barometer. Congratulating user.");
                            MenuActivity.this.alert(MenuActivity.this.getString(R.string.IDS_THANK_YOU_FOR_UNLOCKING_BAROMETER));
                            MainApplication.mIsBarometerUnlocked = true;
                            MenuActivity.this.savePurchasesLocally();
                            MenuActivity.this.onUpdateLabels();
                        }
                    } else {
                        MenuActivity.this.complain("Error purchasing. Authenticity verification failed.");
                    }
                }
            }
        };
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        boolean z;
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        this.mLayout = new RelativeLayout(this);
        this.mLayout.setBackgroundColor(0);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.menu_tile_background)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mLayout.setBackground(tilebitmap);
        }
        setContentView(this.mLayout, new LayoutParams(-1, -1));
        initInfo();
        initImages();
        initSubviews();
        if (((SensorManager) getSystemService("sensor")).getDefaultSensor(6) != null) {
            z = true;
        } else {
            z = false;
        }
        this.mIsPressureSensorAvailable = z;
        Appirater.appLaunched(this);
        initInAppPurchase();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onRestart() {
        super.onRestart();
    }

    protected void onResume() {
        super.onResume();
        setAlphaBackground(true, false);
        setPositionSubviews(false, true);
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
    }

    protected void onDestroy() {
        if (this.mBroadcastReceiver != null) {
            unregisterReceiver(this.mBroadcastReceiver);
        }
        Log.d(MainApplication.TAG, "Destroying inapp helper.");
        if (this.mInAppHelper != null) {
            this.mInAppHelper.dispose();
            this.mInAppHelper = null;
        }
        super.onDestroy();
    }

    void initInfo() {
        this.mIsOK = false;
    }

    void initImages() {
    }

    @SuppressLint({"NewApi"})
    protected void initSubviews() {
        int len;
        this.mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        this.mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        if (this.mScreenWidth < this.mScreenHeight) {
            len = this.mScreenWidth;
        } else {
            len = this.mScreenHeight;
        }
        int t = this.mScreenHeight - len;
        int w = len;
        int h = len;
        this.mMenuWheelView = new MenuWheelView(this);
        this.mMenuWheelView.setId(2);
        LayoutParams params = new LayoutParams(-2, -2);
        params.setMargins(0, t, 0, 0);
        params.width = w;
        params.height = h;
        this.mMenuWheelView.setLayoutParams(params);
        this.mMenuWheelView.mOriginX = len / 2;
        this.mMenuWheelView.mOriginY = len / 2;
        this.mLayout.addView(this.mMenuWheelView);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        bitmap = ImageUtility.scale9Bitmap(bitmap, (bitmap.getWidth() - 2) * 2, (bitmap.getHeight() - 2) * 2);
        this.mLedScreen = new RelativeLayout(this);
        this.mLedScreen.setId(1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), bitmap);
        if (VERSION.SDK_INT < 16) {
            this.mLedScreen.setBackgroundDrawable(tilebitmap);
        } else {
            this.mLedScreen.setBackground(tilebitmap);
        }
        params = new LayoutParams(bitmap.getWidth(), bitmap.getHeight());
        params.addRule(10);
        params.addRule(14);
        params.setMargins(0, (int) getResources().getDimension(R.dimen.MENU_VERT_MARGIN_BTW_LED_AND_SCREEN), 0, 0);
        params.width = bitmap.getWidth();
        params.height = bitmap.getHeight();
        this.mLedScreen.setLayoutParams(params);
        this.mLayout.addView(this.mLedScreen);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        int hmarginid = (int) getResources().getDimension(R.dimen.MENU_HORZ_MARGIN_BTW_TOOL_ID_LABEL_AND_LED);
        int hmarginname = (int) getResources().getDimension(R.dimen.MENU_HORZ_MARGIN_BTW_TOOL_NAME_LABEL_AND_LED);
        int vmargin = (int) getResources().getDimension(R.dimen.MENU_VERT_MARGIN_BTW_LABEL_AND_LED);
        this.mCodeLabel = new TextView(this);
        this.mCodeLabel.setPaintFlags(this.mCodeLabel.getPaintFlags() | Barcode.ITF);
        this.mCodeLabel.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mCodeLabel.setBackgroundColor(0);
        this.mCodeLabel.setTypeface(font);
        this.mCodeLabel.setTextSize(1, getResources().getDimension(R.dimen.MENU_TOOL_ID_FONT_SIZE));
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(11);
        params.setMargins(0, vmargin, hmarginid, 0);
        this.mCodeLabel.setLayoutParams(params);
        this.mLedScreen.addView(this.mCodeLabel);
        this.mNameLabel = new TextView(this);
        this.mNameLabel.setPaintFlags(this.mNameLabel.getPaintFlags() | Barcode.ITF);
        this.mNameLabel.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mNameLabel.setBackgroundColor(0);
        this.mNameLabel.setTypeface(font);
        this.mNameLabel.setTextSize(1, getResources().getDimension(R.dimen.MENU_TOOL_NAME_FONT_SIZE));
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(9);
        params.setMargins(hmarginname, 0, 0, vmargin);
        this.mNameLabel.setLayoutParams(params);
        this.mLedScreen.addView(this.mNameLabel);
        this.mLockImage = new ImageView(this);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.menu_icon_lock_tool)).getBitmap();
        this.mLockImage.setImageBitmap(bitmap);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.topMargin = bitmap.getWidth();
        params.addRule(9);
        params.leftMargin = this.mScreenWidth / 2;
        this.mLockImage.setLayoutParams(params);
        this.mMenuWheelView.addView(this.mLockImage);
        updateLabels(this.mMenuWheelView.getCurrentItem());
    }

    public void onUpdateLabels() {
        updateLabels(this.mMenuWheelView.getCurrentItem());
    }

    void updateLabels(int item) {
        if (item > -1 && item < 14) {
            this.mCodeLabel.setText(String.format(Locale.US, "%02d", new Object[]{Integer.valueOf(item + 1)}));
            this.mNameLabel.setText(getResources().getString(MiscUtility.getId(String.format(Locale.US, "IDS_TOOL_NAME_%d", new Object[]{Integer.valueOf(item + 1)}), string.class)));
            if ((item == 12 && !MainApplication.mIsAltimeterUnlocked && this.mIsPressureSensorAvailable) || (item == 13 && !MainApplication.mIsBarometerUnlocked && this.mIsPressureSensorAvailable)) {
                this.mLockImage.setVisibility(0);
            } else {
                this.mLockImage.setVisibility(8);
            }
        }
    }

    void setPositionSubviews(boolean init, boolean animated) {
        int fromydelta;
        int toydelta;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(8, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (init) {
            if (animated) {
                fromydelta = 0;
                toydelta = -this.mScreenHeight;
            } else {
                fromydelta = -this.mScreenHeight;
                toydelta = -this.mScreenHeight;
            }
        } else if (animated) {
            fromydelta = -this.mScreenHeight;
            toydelta = 0;
        } else {
            fromydelta = 0;
            toydelta = 0;
        }
        int duration = animated ? init ? 500 : 450 : 0;
        TranslateAnimation anim = new TranslateAnimation(0.0f, 0.0f, (float) fromydelta, (float) toydelta);
        anim.setDuration((long) duration);
        anim.setFillAfter(true);
        if (animated) {
            anim.setAnimationListener(this);
        }
        this.mMenuWheelView.startAnimation(anim);
        anim = new TranslateAnimation(0.0f, 0.0f, (float) (-fromydelta), (float) (-toydelta));
        anim.setDuration((long) duration);
        anim.setFillAfter(true);
        this.mLedScreen.startAnimation(anim);
    }

    void setAlphaBackground(boolean init, boolean animated) {
    }

    void onOK() {
        if (this.mMenuWheelView.getCurrentItem() == 12 && !MainApplication.mIsAltimeterUnlocked && this.mIsPressureSensorAvailable) {
            new UnlockAltimeterDialogFragment().show(getFragmentManager(), "UnlockAltimeterDialogFragment");
        } else if (this.mMenuWheelView.getCurrentItem() == 13 && !MainApplication.mIsBarometerUnlocked && this.mIsPressureSensorAvailable) {
            new UnlockBarometerDialogFragment().show(getFragmentManager(), "UnlockBarometerDialogFragment");
        } else {
            this.mMenuWheelView.mShutterView.setVisibility(0);
            this.mMenuWheelView.mShutterView.closeShutter();
        }
    }

    public void onShutterOpened() {
        this.mMenuWheelView.mShutterView.setVisibility(4);
        this.mMenuWheelView.setAnimating(false);
        this.mMenuWheelView.invalidate();
    }

    public void onShutterClosed() {
        setPositionSubviews(true, true);
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    public void onAnimationEnd(Animation animation) {
        if (!(animation instanceof TranslateAnimation)) {
            return;
        }
        if (animation.getDuration() == 450) {
            this.mMenuWheelView.mShutterView.openShutter();
            return;
        }
        String[] activityClasses = new String[]{"protractor.ProtractorActivity", "ruler.RulerActivity", "spirit_level.SpiritLevelActivity", "surface_level.SurfaceLevelActivity", "plumb_bob.PlumbBobActivity", "seismometer.SeismometerActivity", "stopwatch.StopwatchActivity", "timer.TimerActivity", "metronome.MetronomeActivity", "decibel.DecibelActivity", "teslameter.TeslameterActivity", "compass.CompassActivity", "altimeter.AltimeterActivity", "barometer.BarometerActivity"};
        try {
            Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putInt(SettingsKey.SETTINGS_GENERAL_LAST_USED_TOOL_KEY, this.mMenuWheelView.getCurrentItem());
            editor.apply();
            startActivity(new Intent(getApplicationContext(), Class.forName("com.vivekwarde.measure." + activityClasses[this.mMenuWheelView.getCurrentItem()])));
            overridePendingTransition(17432576, 17432577);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, "Tool n\u00e0y ch\u01b0a l\u00e0m, \u0111\u1ee3i l\u00e0m xong \u0111\u00e3 nh\u00e9 :D", 0).show();
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    protected void initInAppPurchase() {
        Log.d(MainApplication.TAG, "Creating IAB helper.");
        this.mInAppHelper = new IabHelper(this, MainApplication.mLicenseKey);
        this.mInAppHelper.enableDebugLogging(true);
        Log.d(MainApplication.TAG, "Starting setup.");
        this.mInAppHelper.startSetup(new OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(MainApplication.TAG, "Setup finished.");
                if (!result.isSuccess()) {
                    MenuActivity.this.complain("Problem setting up in-app billing: " + result);
                } else if (MenuActivity.this.mInAppHelper != null) {
                    MenuActivity.this.mBroadcastReceiver = new IabBroadcastReceiver(MenuActivity.this);
                    MenuActivity.this.registerReceiver(MenuActivity.this.mBroadcastReceiver, new IntentFilter(IabBroadcastReceiver.ACTION));
                    Log.d(MainApplication.TAG, "Setup successful. Querying inventory.");
                    MenuActivity.this.mInAppHelper.queryInventoryAsync(MenuActivity.this.mGotInventoryListener);
                }
            }
        });
    }

    public void receivedBroadcast() {
        Log.d(MainApplication.TAG, "Received broadcast notification. Querying inventory.");
        this.mInAppHelper.queryInventoryAsync(this.mGotInventoryListener);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(MainApplication.TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (this.mInAppHelper != null) {
            if (this.mInAppHelper.handleActivityResult(requestCode, resultCode, data)) {
                Log.d(MainApplication.TAG, "onActivityResult handled by IABUtil.");
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    void complain(String message) {
        Log.e(MainApplication.TAG, "**** Multi Measures 2 Error: " + message);
        alert("Error: " + message);
    }

    private void alert(String message) {
        Builder bld = new Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(MainApplication.TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    private void savePurchasesLocally() {
        Editor editor = getPreferences(0).edit();
        editor.putBoolean(SettingsKey.SETTINGS_IS_ALTIMETER_UNLOCKED, MainApplication.mIsAltimeterUnlocked);
        editor.putBoolean(SettingsKey.SETTINGS_IS_BAROMETER_UNLOCKED, MainApplication.mIsBarometerUnlocked);
        editor.apply();
    }

    private boolean verifyDeveloperPayload(Purchase p) {
        return true;
    }

    public void onDialogUnlockAltimeterClickOk(DialogFragment dialog) {
        this.mInAppHelper.launchPurchaseFlow(this, IN_APP_UNLOCK_ALTIMETER_SKU, IN_APP_REQUEST_CODE, this.mPurchaseFinishedListener, BuildConfig.FLAVOR);
    }

    public void onDialogUnlockAltimeterClickCancel(DialogFragment dialog) {
    }

    public void onDialogUnlockBarometerClickOk(DialogFragment dialog) {
        this.mInAppHelper.launchPurchaseFlow(this, IN_APP_UNLOCK_BAROMETER_SKU, IN_APP_REQUEST_CODE, this.mPurchaseFinishedListener, BuildConfig.FLAVOR);
    }

    public void onDialogUnlockBarometerClickCancel(DialogFragment dialog) {
    }
}
