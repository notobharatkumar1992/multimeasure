package com.vivekwarde.measure.menu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.cast.TextTrackStyle;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.ArrayList;

public class MenuWheelView extends RelativeLayout implements OnClickListener, OnTouchListener, AnimationListener {
    final int ACTION_ID_OK;
    final int EVENT_ID_MENU_ONOK;
    final int EVENT_ID_ROTATE_SUBMENU_WHEEL;
    final int EVENT_ID_UPDATE_MENU_LABEL;
    final double MAX_ANGLE;
    final int WHEELVIEW_ID;
    final String tag;
    ImageView bkgr;
    float mAllAngle;
    float mAngle;
    boolean mAnimating;
    int mCurrentItem;
    float mDownAngle;
    Point mDownPoint;
    float mInnerRadius;
    boolean mIsMoved;
    int mLastMenuItem;
    float mLastPreloadAngle;
    int mLastPreloadIndex;
    float mMoveAngle;
    SPImageButton mOKButton;
    int mOriginX;
    int mOriginY;
    float mOuterRadius;
    ArrayList<Object> mPhaseArray;
    ImageView mPivotView;
    Point mPrevPoint;
    int mRotatedCircle;
    ShutterView mShutterView;
    float mSlices;
    float mStartAngle;
    boolean mValidClick;
    ImageView mWheelGearView;
    ImageView mWheelRimView;
    WheelView mWheelView;

    public MenuWheelView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.ACTION_ID_OK = 1234;
        this.EVENT_ID_MENU_ONOK = 1235;
        this.EVENT_ID_ROTATE_SUBMENU_WHEEL = 1236;
        this.EVENT_ID_UPDATE_MENU_LABEL = 1237;
        this.WHEELVIEW_ID = 1238;
        this.MAX_ANGLE = 14.660765716752367d;
        this.mPivotView = null;
        this.mWheelGearView = null;
        this.mWheelRimView = null;
        this.mWheelView = null;
        this.bkgr = null;
        this.mOKButton = null;
        this.mShutterView = null;
        this.mIsMoved = false;
        this.mValidClick = true;
        this.mAnimating = false;
        this.mPhaseArray = null;
        setWillNotDraw(false);
        setBackgroundColor(0);
        initInfo();
        initImages();
        initSubviews();
    }

    void initInfo() {
        this.mPhaseArray = null;
        this.mAllAngle = 0.0f;
        this.mDownAngle = 0.0f;
        this.mMoveAngle = 0.0f;
        this.mLastPreloadAngle = 0.0f;
        this.mLastPreloadIndex = 0;
        this.mStartAngle = 0.0f;
        this.mAngle = 0.0f;
        this.mSlices = WheelView.ANGLE_PER_ITEM_IN_RAD;
        this.mIsMoved = false;
    }

    void initImages() {
    }

    void initSubviews() {
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_wheel_rim)).getBitmap();
        this.mWheelRimView = new ImageView(getContext());
        this.mWheelRimView.setImageBitmap(bitmap);
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(15);
        this.mWheelRimView.setLayoutParams(params);
        addView(this.mWheelRimView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_wheel_background)).getBitmap();
        this.mOuterRadius = (float) (bitmap.getWidth() / 2);
        this.mWheelView = new WheelView(getContext());
        this.mWheelView.setId(1238);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(15);
        params.width = bitmap.getWidth();
        params.height = bitmap.getHeight();
        this.mWheelView.setLayoutParams(params);
        addView(this.mWheelView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_wheel_gear)).getBitmap();
        this.mInnerRadius = (float) ((bitmap.getWidth() / 2) - 15);
        this.mWheelGearView = new ImageView(getContext());
        this.mWheelGearView.setImageBitmap(bitmap);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(15);
        params.width = bitmap.getWidth();
        params.height = bitmap.getHeight();
        this.mWheelGearView.setLayoutParams(params);
        addView(this.mWheelGearView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_wheel_pivot)).getBitmap();
        this.mPivotView = new ImageView(getContext());
        this.mPivotView.setImageBitmap(bitmap);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(15);
        this.mPivotView.setLayoutParams(params);
        addView(this.mPivotView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_ok_button)).getBitmap();
        this.mOKButton = new SPImageButton(getContext());
        this.mOKButton.setId(1234);
        this.mOKButton.setClickable(true);
        this.mOKButton.setBackgroundBitmap(bitmap);
        this.mOKButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(15);
        this.mOKButton.setLayoutParams(params);
        addView(this.mOKButton);
        this.mShutterView = new ShutterView(getContext());
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mShutterView.setLayoutParams(params);
        addView(this.mShutterView);
        int curItem = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(SettingsKey.SETTINGS_GENERAL_LAST_USED_TOOL_KEY, 0);
        if (curItem <= -1 || curItem >= 14) {
            curItem = 0;
        }
        rotateMenuToItem(curItem, false);
        setOnTouchListener(this);
    }

    void rotateMenuToItem(int item, boolean animated) {
        if (item > -1 && item < 14) {
            this.mCurrentItem = item;
            this.mLastMenuItem = item;
            this.mAllAngle = ((float) (-item)) * WheelView.ANGLE_PER_ITEM_IN_RAD;
            this.mWheelView.setSelection(item, getPieceIndexFromAngle(this.mAllAngle));
            MiscUtility.rotateView(this.mWheelView, MiscUtility.radian2Degree(this.mAllAngle), (float) (this.mWheelView.getLayoutParams().width / 2), (float) (this.mWheelView.getLayoutParams().height / 2));
            MiscUtility.rotateView(this.mWheelGearView, MiscUtility.radian2Degree(-this.mAllAngle), (float) (this.mWheelGearView.getLayoutParams().width / 2), (float) (this.mWheelGearView.getLayoutParams().height / 2));
        }
    }

    int getPieceIndexFromPoint(Point point) {
        return (int) Math.round(Math.floor(((double) MiscUtility.rotate360Angle(MiscUtility.reverseAngle(getAngleFromPoint(point)), -0.5235988f)) / 1.0471975511965976d));
    }

    int getPieceIndexFromAngle(float alpha) {
        float alg = ((float) Math.round(alpha / this.mSlices)) * this.mSlices;
        int sign = MiscUtility.signf(alg);
        alg = Math.abs(alg);
        alg = ((float) (((double) alg) - (Math.floor(((double) alg) / 6.283185307179586d) * 6.283185307179586d))) * ((float) sign);
        if (alg < 0.0f) {
            alg = (float) (((double) alg) + 6.283185307179586d);
        }
        float temp = alg;
        int curPiece = (int) Math.round((((double) temp) - ((Math.floor(((double) temp) / 6.283185307179586d) * 2.0d) * 3.141592653589793d)) / 1.0471975511965976d);
        if (curPiece >= 6) {
            return 0;
        }
        return curPiece;
    }

    int getItemIndexFromAngle(float alpha) {
        float alg = ((float) Math.round(alpha / this.mSlices)) * this.mSlices;
        int sign = MiscUtility.signf(alg);
        alg = Math.abs(alg);
        alg = ((float) (((double) alg) - (Math.floor(((double) alg) / 14.660765716752367d) * 14.660765716752367d))) * ((float) sign);
        if (alg < 0.0f) {
            alg = (float) (((double) alg) + 14.660765716752367d);
        }
        float temp = alg;
        int curItem = (int) Math.round((((double) temp) - (Math.floor(((double) temp) / 14.660765716752367d) * 14.660765716752367d)) / 1.0471975511965976d);
        if (curItem >= 14) {
            return 0;
        }
        return curItem;
    }

    int getPreloadIndexWithAngle(float curAngle) {
        return (int) Math.round(Math.floor(((double) MiscUtility.rotate360Angle(curAngle, -0.2617994f)) / 0.5235987755982988d));
    }

    int updateMenuItemIndexWithPieceIndexAndCurrentItem(int pieceIndex, int curItem) {
        if (pieceIndex > 3) {
            pieceIndex = (pieceIndex - 6) + 14;
        }
        return (curItem + pieceIndex) % 14;
    }

    float getAngleFromPoint(Point point) {
        float x = (float) (point.x - this.mOriginX);
        float y = (float) (-(point.y - this.mOriginY));
        float a = (float) Math.atan((double) (x / y));
        if (MiscUtility.signf(x) > 0 && MiscUtility.signf(y) < 0) {
            return (float) (((double) a) + 3.141592653589793d);
        }
        if (MiscUtility.signf(x) < 0 && MiscUtility.signf(y) < 0) {
            return (float) (((double) a) + 3.141592653589793d);
        }
        if (MiscUtility.signf(x) >= 0 || MiscUtility.signf(y) <= 0) {
            return a;
        }
        return (float) (((double) a) + 6.283185307179586d);
    }

    int getMoveType(Point curPoint, Point prePoint) {
        int type = 0;
        float curAngle = getAngleFromPoint(curPoint);
        float preAngle = this.mDownAngle;
        if (((double) Math.abs(curAngle - preAngle)) > 3.141592653589793d) {
            if (curAngle - preAngle < 0.0f) {
                type = 1;
            } else {
                type = -1;
            }
        }
        if (prePoint.x < this.mOriginX && curPoint.x >= this.mOriginX && curPoint.y < this.mOriginY) {
            this.mRotatedCircle++;
        } else if (prePoint.x >= this.mOriginX && curPoint.x < this.mOriginX && curPoint.y < this.mOriginY) {
            this.mRotatedCircle--;
        }
        return type;
    }

    int getCurrentItem() {
        return this.mCurrentItem;
    }

    public void onClick(View v) {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(6, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (v.getId() == 1234) {
            ((MenuActivity) getContext()).onOK();
        }
    }

    protected void onTouchDown(View source, Point currentPosition) {
        if (!this.mAnimating) {
            Point pt = currentPosition;
            this.mDownPoint = pt;
            this.mRotatedCircle = 0;
            this.mIsMoved = false;
            this.mValidClick = true;
            Point point = pt;
            float startX = (float) (point.x - this.mOriginX);
            float startY = (float) (point.y - this.mOriginY);
            this.mStartAngle = (float) (Math.atan2((double) startY, (double) startX) - ((double) this.mAngle));
            this.mDownAngle = getAngleFromPoint(point);
            this.mLastPreloadAngle = this.mAllAngle;
            float radius = (float) Math.sqrt((double) ((startX * startX) + (startY * startY)));
            if (radius > this.mOuterRadius || radius < this.mInnerRadius || getPieceIndexFromPoint(point) == 3) {
                this.mValidClick = false;
            }
            this.mPrevPoint = pt;
        }
    }

    protected void onTouchMove(View source, Point currentPosition) {
        if (!this.mAnimating) {
            Point pt = currentPosition;
            Point point = pt;
            if (this.mValidClick) {
                this.mValidClick = true;
                if (Math.sqrt((double) (((this.mDownPoint.x - pt.x) * (this.mDownPoint.x - pt.x)) + ((this.mDownPoint.y - pt.y) * (this.mDownPoint.y - pt.y)))) > 10.0d) {
                    this.mIsMoved = true;
                    this.mAngle = (float) (Math.atan2((double) ((float) (point.y - this.mOriginY)), (double) ((float) (point.x - this.mOriginX))) - ((double) this.mStartAngle));
                    this.mMoveAngle = getAngleFromPoint(point);
                    if (getMoveType(pt, this.mPrevPoint) != 0) {
                        this.mMoveAngle = (float) (((double) this.mMoveAngle) + ((((double) this.mRotatedCircle) * 3.141592653589793d) * 2.0d));
                    }
                    this.mAllAngle += this.mMoveAngle - this.mDownAngle;
                    this.mDownAngle = this.mMoveAngle;
                    MiscUtility.rotateView(this.mWheelView, MiscUtility.radian2Degree(this.mAllAngle), (float) (this.mWheelView.getLayoutParams().width / 2), (float) (this.mWheelView.getLayoutParams().height / 2));
                    MiscUtility.rotateView(this.mWheelGearView, MiscUtility.radian2Degree(-this.mAllAngle), (float) (this.mWheelGearView.getLayoutParams().width / 2), (float) (this.mWheelGearView.getLayoutParams().height / 2));
                    int curItem = getItemIndexFromAngle(this.mAllAngle);
                    int curPiece = getPieceIndexFromAngle(this.mAllAngle);
                    int direction = this.mAllAngle < this.mLastPreloadAngle ? -1 : 1;
                    if (curItem != this.mCurrentItem) {
                        this.mWheelView.changeHighlightIcon(curItem, curPiece, direction);
                        this.mLastMenuItem = this.mCurrentItem;
                        this.mCurrentItem = curItem;
                        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                        }
                        post(new Runnable() {
                            public void run() {
                                ((MenuActivity) MenuWheelView.this.getContext()).onUpdateLabels();
                            }
                        });
                    }
                    int item = getPreloadIndexWithAngle(this.mAllAngle);
                    if (item != this.mLastPreloadIndex) {
                        if ((direction > 0 && item % 2 == 1) || (direction < 0 && item % 2 == 1)) {
                            this.mWheelView.preloadIcon(this.mCurrentItem, curPiece, direction);
                        }
                        this.mLastPreloadIndex = item;
                        this.mLastPreloadAngle = this.mAllAngle;
                    }
                    this.mPrevPoint = pt;
                    return;
                }
                return;
            }
            this.mValidClick = false;
        }
    }

    protected void onTouchUp(View source, Point currentPosition) {
        if (!this.mAnimating) {
            if (this.mValidClick || this.mIsMoved) {
                Point pt = currentPosition;
                if (this.mIsMoved) {
                    float curAngle = this.mAllAngle;
                    this.mAllAngle = ((float) Math.round(this.mAllAngle / this.mSlices)) * this.mSlices;
                    MiscUtility.rotateViewAnimation(this.mWheelView, MiscUtility.radian2Degree(curAngle), MiscUtility.radian2Degree(this.mAllAngle), (float) (this.mWheelView.getLayoutParams().width / 2), (float) (this.mWheelView.getLayoutParams().height / 2), 150, null);
                    MiscUtility.rotateViewAnimation(this.mWheelGearView, MiscUtility.radian2Degree(-curAngle), MiscUtility.radian2Degree(-this.mAllAngle), (float) (this.mWheelGearView.getLayoutParams().width / 2), (float) (this.mWheelGearView.getLayoutParams().height / 2), 150, null);
                    this.mCurrentItem = getItemIndexFromAngle(this.mAllAngle);
                    return;
                }
                int selectedItem = getPieceIndexFromPoint(pt);
                int lastItem = this.mCurrentItem;
                parseRotatePhaseWithAngleAndOffset(this.mAllAngle, MiscUtility.getSign(3 - selectedItem) * Math.abs(((updateMenuItemIndexWithPieceIndexAndCurrentItem(selectedItem, this.mCurrentItem) - lastItem) + (MiscUtility.getSign(3 - selectedItem) * 14)) % 14));
                doAnimationPhases();
            }
        }
    }

    void parseRotatePhaseWithAngleAndOffset(float alpha, int offset) {
        if (this.mPhaseArray == null) {
            this.mPhaseArray = new ArrayList();
        }
        int direction = MiscUtility.getSign(offset);
        float deltaAngle = 0.0f;
        int itemCount = Math.abs(offset);
        for (int i = 0; i < itemCount; i++) {
            float beta = (float) (((double) (deltaAngle + alpha)) + ((((double) direction) * 3.141592653589793d) / 12.0d));
            SPPhaseInfo phase = new SPPhaseInfo();
            phase.setType(1);
            phase.setAngle(beta);
            phase.setItem(getItemIndexFromAngle(beta));
            phase.setPiece(getPieceIndexFromAngle(beta));
            phase.setDuration(37);
            phase.setDirection(direction);
            this.mPhaseArray.add(phase);
            beta = (float) (((double) (deltaAngle + alpha)) + ((((double) direction) * 3.141592653589793d) / 6.0d));
            phase = new SPPhaseInfo();
            phase.setType(2);
            phase.setAngle(beta);
            phase.setItem(getItemIndexFromAngle((float) (((double) beta) + ((((double) direction) * 3.141592653589793d) / 12.0d))));
            phase.setPiece(getPieceIndexFromAngle((float) (((double) beta) + ((((double) direction) * 3.141592653589793d) / 12.0d))));
            phase.setDuration(37);
            phase.setDirection(direction);
            this.mPhaseArray.add(phase);
            beta = (deltaAngle + alpha) - (((float) direction) * WheelView.ANGLE_PER_ITEM_IN_RAD);
            phase = new SPPhaseInfo();
            phase.setType(0);
            phase.setAngle(beta);
            phase.setItem(getItemIndexFromAngle(beta));
            phase.setPiece(getPieceIndexFromAngle(beta));
            phase.setDuration(75);
            phase.setDirection(direction);
            this.mPhaseArray.add(phase);
            deltaAngle = (float) (((double) deltaAngle) + ((((double) direction) * 3.141592653589793d) / 3.0d));
        }
    }

    void doAnimationPhases() {
        if (this.mPhaseArray != null && this.mPhaseArray.size() > 0) {
            this.mAnimating = true;
            SPPhaseInfo phase = (SPPhaseInfo) this.mPhaseArray.get(0);
            MiscUtility.rotateViewAnimation(this.mWheelView, MiscUtility.radian2Degree(this.mAllAngle), MiscUtility.radian2Degree(phase.getAngle()), (float) (this.mWheelView.getLayoutParams().width / 2), (float) (this.mWheelView.getLayoutParams().height / 2), phase.getDuration(), this);
            MiscUtility.rotateViewAnimation(this.mWheelGearView, MiscUtility.radian2Degree(-this.mAllAngle), MiscUtility.radian2Degree(-phase.getAngle()), (float) (this.mWheelGearView.getLayoutParams().width / 2), (float) (this.mWheelGearView.getLayoutParams().height / 2), phase.getDuration(), null);
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            onTouchDown(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 1) {
            onTouchUp(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 2) {
            onTouchMove(v, new Point((int) event.getX(), (int) event.getY()));
        }
        return true;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.mPhaseArray.size() > 0) {
            SPPhaseInfo phase = (SPPhaseInfo) this.mPhaseArray.get(0);
            if (animation instanceof RotateAnimation) {
                this.mAllAngle = phase.getAngle();
                if (phase.getType() == 1) {
                    int item = getPreloadIndexWithAngle(phase.getAngle());
                    this.mWheelView.preloadIcon(phase.getItem(), phase.getPiece(), phase.getDirection());
                    this.mLastPreloadIndex = item;
                    this.mLastPreloadAngle = phase.getAngle();
                } else if (phase.getType() == 2) {
                    this.mWheelView.changeHighlightIcon(phase.getItem(), phase.getPiece(), phase.getDirection());
                    this.mLastMenuItem = this.mCurrentItem;
                    this.mCurrentItem = phase.getItem();
                    if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                        SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                    }
                    post(new Runnable() {
                        public void run() {
                            ((MenuActivity) MenuWheelView.this.getContext()).onUpdateLabels();
                        }
                    });
                }
                this.mPhaseArray.remove(0);
                if (this.mPhaseArray.size() > 0) {
                    doAnimationPhases();
                } else {
                    this.mAnimating = false;
                }
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void setAnimating(boolean animating) {
        this.mAnimating = animating;
    }

    public class MenuToolId {
        public static final int MENU_TOOL_ALTIMETER = 12;
        public static final int MENU_TOOL_BAROMETER = 13;
        public static final int MENU_TOOL_COMPASS = 11;
        public static final int MENU_TOOL_COUNT = 14;
        public static final int MENU_TOOL_DECIBEL = 10;
        public static final int MENU_TOOL_METRONOME = 8;
        public static final int MENU_TOOL_NONE = -1;
        public static final int MENU_TOOL_PLUMB_BOB = 4;
        public static final int MENU_TOOL_PROTRACTOR = 0;
        public static final int MENU_TOOL_RULER = 1;
        public static final int MENU_TOOL_SEISMOMETER = 5;
        public static final int MENU_TOOL_SPIRIT_LEVEL = 2;
        public static final int MENU_TOOL_STOPWATCH = 6;
        public static final int MENU_TOOL_SURFACE_LEVEL = 3;
        public static final int MENU_TOOL_TESLAMETER = 9;
        public static final int MENU_TOOL_TIMER = 7;
    }

    class SPPhaseInfo {
        public static final int kPhaseChangeItem = 2;
        public static final int kPhaseNormal = 0;
        public static final int kPhasePreload = 1;
        float angle;
        int direction;
        long duration;
        int item;
        int piece;
        int type;

        SPPhaseInfo() {
        }

        public int getType() {
            return this.type;
        }

        public void setType(int t) {
            this.type = t;
        }

        public float getAngle() {
            return this.angle;
        }

        public void setAngle(float a) {
            this.angle = a;
        }

        public int getPiece() {
            return this.piece;
        }

        public void setPiece(int p) {
            this.piece = p;
        }

        public int getItem() {
            return this.item;
        }

        public void setItem(int i) {
            this.item = i;
        }

        public long getDuration() {
            return this.duration;
        }

        public void setDuration(long d) {
            this.duration = d;
        }

        public int getDirection() {
            return this.direction;
        }

        public void setDirection(int d) {
            this.direction = d;
        }
    }
}
