package com.vivekwarde.measure.menu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.cast.TextTrackStyle;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

public class ShutterView extends ViewGroup implements AnimationListener, AnimatorListener {
    final String tag;
    ImageView mBackgroundView;
    ImageView mFakeRim;
    int mHeight;
    ImageView[] mIris;
    boolean mIsOpenShutter;
    int mShutterAnimationCount;
    int mWidth;

    public ShutterView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mIris = new ImageView[]{null, null, null, null, null, null};
        this.mFakeRim = null;
        this.mBackgroundView = null;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mShutterAnimationCount = 0;
        this.mIsOpenShutter = false;
        setWillNotDraw(false);
        initSubviews();
    }

    void initSubviews() {
        RelativeLayout.LayoutParams params;
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_shutter_iris)).getBitmap();
        Bitmap bitmap2 = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_background)).getBitmap();
        this.mWidth = bitmap2.getWidth();
        this.mHeight = bitmap2.getHeight();
        float x0 = (float) (this.mWidth / 2);
        float y0 = (float) (this.mHeight / 2);
        float fR = (float) bitmap.getHeight();
        for (int i = 0; i < 6; i++) {
            float alpha = (float) ((((double) i) * 3.141592653589793d) / 3.0d);
            int x = (int) (((double) x0) + (((double) fR) * Math.sin((double) alpha)));
            int y = (int) (((double) y0) - (((double) fR) * Math.cos((double) alpha)));
            this.mIris[i] = new ImageView(getContext());
            this.mIris[i].setImageBitmap(bitmap);
            params = new RelativeLayout.LayoutParams(-2, -2);
            params.setMargins(x, y, 0, 0);
            params.width = bitmap.getWidth();
            params.height = bitmap.getHeight();
            this.mIris[i].setLayoutParams(params);
            addView(this.mIris[i]);
            MiscUtility.rotateView(this.mIris[i], MiscUtility.radian2Degree((float) (((double) i) * 1.0471975511965976d)), 0.0f, 0.0f);
        }
        this.mBackgroundView = new ImageView(getContext());
        this.mBackgroundView.setImageBitmap(bitmap2);
        params = new RelativeLayout.LayoutParams(-2, -2);
        params.addRule(13);
        params.width = bitmap2.getWidth();
        params.height = bitmap2.getHeight();
        this.mBackgroundView.setLayoutParams(params);
        addView(this.mBackgroundView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_fake_rim)).getBitmap();
        this.mFakeRim = new ImageView(getContext());
        this.mFakeRim.setImageBitmap(bitmap);
        params = new RelativeLayout.LayoutParams(-2, -2);
        params.addRule(13);
        params.width = bitmap.getWidth();
        params.height = bitmap.getHeight();
        this.mFakeRim.setLayoutParams(params);
        addView(this.mFakeRim);
        hideFakeRim();
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(this.mWidth, this.mHeight);
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int w = r - l;
        int h = b - t;
        int hmargin = (w - this.mBackgroundView.getLayoutParams().width) / 2;
        int vmargin = (h - this.mBackgroundView.getLayoutParams().height) / 2;
        this.mBackgroundView.layout(hmargin, vmargin, this.mBackgroundView.getLayoutParams().width + hmargin, this.mBackgroundView.getLayoutParams().height + vmargin);
        hmargin = (w - this.mFakeRim.getLayoutParams().width) / 2;
        vmargin = (h - this.mFakeRim.getLayoutParams().height) / 2;
        this.mFakeRim.layout(hmargin, vmargin, this.mFakeRim.getLayoutParams().width + hmargin, this.mFakeRim.getLayoutParams().height + vmargin);
        for (int i = 0; i < 6; i++) {
            hmargin = ((RelativeLayout.LayoutParams) this.mIris[i].getLayoutParams()).leftMargin;
            vmargin = ((RelativeLayout.LayoutParams) this.mIris[i].getLayoutParams()).topMargin;
            this.mIris[i].layout(hmargin, vmargin, this.mIris[i].getLayoutParams().width + hmargin, this.mIris[i].getLayoutParams().height + vmargin);
        }
    }

    void showIrises() {
        for (int i = 0; i < 6; i++) {
            this.mIris[i].setVisibility(0);
        }
    }

    void hideIrises() {
        for (int i = 0; i < 6; i++) {
            this.mIris[i].setVisibility(4);
        }
    }

    void showFakeRim() {
        this.mFakeRim.setVisibility(0);
    }

    void hideFakeRim() {
        this.mFakeRim.setVisibility(4);
    }

    void openShutter() {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(7, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        this.mShutterAnimationCount = 0;
        this.mIsOpenShutter = true;
        for (int i = 0; i < 6; i++) {
            RotateAnimation anim = new RotateAnimation(MiscUtility.radian2Degree((float) (((double) i) * 1.0471975511965976d)), MiscUtility.radian2Degree((float) ((((double) i) * 1.0471975511965976d) - 1.0471975511965976d)), 0.0f, 0.0f);
            anim.setDuration(500);
            anim.setFillEnabled(true);
            anim.setFillBefore(true);
            anim.setFillAfter(true);
            anim.setAnimationListener(this);
            this.mIris[i].startAnimation(anim);
        }
    }

    void closeShutter() {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(7, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        this.mShutterAnimationCount = 0;
        this.mIsOpenShutter = false;
        for (int i = 0; i < 6; i++) {
            RotateAnimation anim = new RotateAnimation(MiscUtility.radian2Degree((float) ((((double) i) * 1.0471975511965976d) - 1.0471975511965976d)), MiscUtility.radian2Degree((float) (((double) i) * 1.0471975511965976d)), 0.0f, 0.0f);
            anim.setDuration(500);
            anim.setFillEnabled(true);
            anim.setFillBefore(true);
            anim.setFillAfter(true);
            anim.setAnimationListener(this);
            this.mIris[i].startAnimation(anim);
        }
    }

    public void onAnimationEnd(Animation animation) {
        this.mShutterAnimationCount++;
        if (this.mShutterAnimationCount == 6) {
            this.mShutterAnimationCount = 0;
            if (this.mIsOpenShutter) {
                ((MenuActivity) getContext()).onShutterOpened();
            } else {
                ((MenuActivity) getContext()).onShutterClosed();
            }
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
        this.mShutterAnimationCount++;
        if (this.mShutterAnimationCount == 6) {
            this.mShutterAnimationCount = 0;
            if (this.mIsOpenShutter) {
                ((MenuActivity) getContext()).onShutterOpened();
            } else {
                ((MenuActivity) getContext()).onShutterClosed();
            }
        }
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }
}
