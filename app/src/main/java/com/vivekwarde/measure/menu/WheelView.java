package com.vivekwarde.measure.menu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.utilities.MiscUtility;

public class WheelView extends RelativeLayout {
    public static final int ANGLE_PER_ITEM = -60;
    public static final float ANGLE_PER_ITEM_IN_RAD = -1.0471976f;
    public static final int MENU_ITEM_COUNT = 14;
    public static final int MENU_ITEM_DISPLAY = 6;
    final String tag;
    ImageView mBackgroundView;
    int mCurrentItem;
    int mCurrentPiece;
    int mHeight;
    Bitmap[][] mMenuItemImages;
    ImageView[] mMenuItems;
    int[] mVisibleItems;
    int mWidth;
    Bitmap[][] r0;

    public WheelView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        r0 = new Bitmap[2][];
        Bitmap[] bitmapArr = new Bitmap[MENU_ITEM_COUNT];
        bitmapArr[0] = null;
        bitmapArr[1] = null;
        bitmapArr[2] = null;
        bitmapArr[3] = null;
        bitmapArr[4] = null;
        bitmapArr[5] = null;
        bitmapArr[MENU_ITEM_DISPLAY] = null;
        bitmapArr[7] = null;
        bitmapArr[8] = null;
        bitmapArr[9] = null;
        bitmapArr[10] = null;
        bitmapArr[11] = null;
        bitmapArr[12] = null;
        bitmapArr[13] = null;
        r0[0] = bitmapArr;
        bitmapArr = new Bitmap[MENU_ITEM_COUNT];
        bitmapArr[0] = null;
        bitmapArr[1] = null;
        bitmapArr[2] = null;
        bitmapArr[3] = null;
        bitmapArr[4] = null;
        bitmapArr[5] = null;
        bitmapArr[MENU_ITEM_DISPLAY] = null;
        bitmapArr[7] = null;
        bitmapArr[8] = null;
        bitmapArr[9] = null;
        bitmapArr[10] = null;
        bitmapArr[11] = null;
        bitmapArr[12] = null;
        bitmapArr[13] = null;
        r0[1] = bitmapArr;
        this.mMenuItemImages = r0;
        ImageView[] imageViewArr = new ImageView[MENU_ITEM_DISPLAY];
        imageViewArr[0] = null;
        imageViewArr[1] = null;
        imageViewArr[2] = null;
        imageViewArr[3] = null;
        imageViewArr[4] = null;
        imageViewArr[5] = null;
        this.mMenuItems = imageViewArr;
        this.mBackgroundView = null;
        this.mVisibleItems = new int[]{0, 0, 0, 0, 0, 0};
        this.mWidth = 0;
        this.mHeight = 0;
        setWillNotDraw(false);
        setBackgroundColor(0);
        initInfo();
        initImages();
        initSubviews();
    }

    void initInfo() {
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_background)).getBitmap();
        this.mWidth = bitmap.getWidth();
        this.mHeight = bitmap.getHeight();
        this.mCurrentPiece = 0;
        this.mCurrentItem = 0;
        updateVisibleItems();
    }

    void initImages() {
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_items)).getBitmap();
        int w = bitmap.getWidth() / MENU_ITEM_COUNT;
        int h = bitmap.getHeight() / 2;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < MENU_ITEM_COUNT; j++) {
                this.mMenuItemImages[i][j] = Bitmap.createBitmap(bitmap, j * w, i * h, w, h);
            }
        }
    }

    void initSubviews() {
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.menu_wheel_background)).getBitmap();
        this.mBackgroundView = new ImageView(getContext());
        this.mBackgroundView.setImageBitmap(bitmap);
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(MENU_ITEM_COUNT);
        params.addRule(15);
        this.mBackgroundView.setLayoutParams(params);
        addView(this.mBackgroundView);
        int height = bitmap.getHeight();
        for (int i = 0; i < MENU_ITEM_DISPLAY; i++) {
            bitmap = this.mMenuItemImages[0][0];
            this.mMenuItems[i] = new ImageView(getContext());
            this.mMenuItems[i].setImageBitmap(bitmap);
            params = new LayoutParams(-2, -2);
            params.addRule(MENU_ITEM_COUNT);
            params.addRule(10);
            this.mMenuItems[i].setLayoutParams(params);
            addView(this.mMenuItems[i]);
            MiscUtility.rotateView(this.mMenuItems[i], (float) (i * ANGLE_PER_ITEM), (float) (bitmap.getWidth() / 2), (float) (height / 2));
        }
        updateImagesToItems();
    }

    void updateVisibleItems() {
        this.mVisibleItems[0] = (this.mCurrentItem + 2) % MENU_ITEM_COUNT;
        this.mVisibleItems[1] = (this.mCurrentItem + 1) % MENU_ITEM_COUNT;
        this.mVisibleItems[2] = this.mCurrentItem;
        this.mVisibleItems[3] = ((this.mCurrentItem - 1) + MENU_ITEM_COUNT) % MENU_ITEM_COUNT;
        this.mVisibleItems[4] = ((this.mCurrentItem - 2) + MENU_ITEM_COUNT) % MENU_ITEM_COUNT;
        this.mVisibleItems[5] = -1;
    }

    void updateImagesToItems() {
        int[] itemIndexes = new int[]{2, 1, 0, 5, 4, 3};
        for (int i = 0; i < MENU_ITEM_DISPLAY; i++) {
            if (this.mVisibleItems[i] >= 0) {
                this.mMenuItems[itemIndexes[i]].setImageBitmap(this.mMenuItemImages[this.mVisibleItems[i] == this.mCurrentItem ? 1 : 0][this.mVisibleItems[i]]);
            }
        }
    }

    void changeHighlightIcon(int currentItem, int currentPiece, int direction) {
        if (this.mCurrentItem != currentItem) {
            int i;
            int i2 = currentItem + (direction * 0);
            if (direction < 0) {
                i = MENU_ITEM_COUNT;
            } else {
                i = 0;
            }
            int preItem = (i + i2) % MENU_ITEM_COUNT;
            i2 = currentPiece + (direction * 0);
            if (direction < 0) {
                i = MENU_ITEM_DISPLAY;
            } else {
                i = 0;
            }
            int prePiece = (i + i2) % MENU_ITEM_DISPLAY;
            this.mMenuItems[this.mCurrentPiece].setImageBitmap(this.mMenuItemImages[0][this.mCurrentItem]);
            this.mCurrentItem = currentItem;
            this.mCurrentPiece = currentPiece;
            this.mMenuItems[prePiece].setImageBitmap(this.mMenuItemImages[1][preItem]);
        }
    }

    void preloadIcon(int currentItem, int currentPiece, int direction) {
        int i;
        int i2 = currentItem + (direction * 3);
        if (direction < 0) {
            i = MENU_ITEM_COUNT;
        } else {
            i = 0;
        }
        int preItem = (i + i2) % MENU_ITEM_COUNT;
        i2 = currentPiece + (direction * 3);
        if (direction < 0) {
            i = MENU_ITEM_DISPLAY;
        } else {
            i = 0;
        }
        this.mMenuItems[(i + i2) % MENU_ITEM_DISPLAY].setImageBitmap(this.mMenuItemImages[0][preItem]);
    }

    void setSelection(int item, int piece) {
        this.mCurrentItem = item;
        this.mCurrentPiece = piece;
        updateVisibleItems();
        int[] itemIndexes = new int[]{0, 0, 0, 0, 0, 0};
        itemIndexes[0] = (this.mCurrentPiece + 2) % MENU_ITEM_DISPLAY;
        itemIndexes[1] = (this.mCurrentPiece + 1) % MENU_ITEM_DISPLAY;
        itemIndexes[2] = this.mCurrentPiece;
        itemIndexes[3] = ((this.mCurrentPiece - 1) + MENU_ITEM_DISPLAY) % MENU_ITEM_DISPLAY;
        itemIndexes[4] = ((this.mCurrentPiece - 2) + MENU_ITEM_DISPLAY) % MENU_ITEM_DISPLAY;
        itemIndexes[5] = -1;
        for (int i = 0; i < MENU_ITEM_DISPLAY; i++) {
            if (this.mVisibleItems[i] >= 0) {
                int i2;
                Bitmap[][] bitmapArr = this.mMenuItemImages;
                if (this.mVisibleItems[i] == this.mCurrentItem) {
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                this.mMenuItems[itemIndexes[i]].setImageBitmap(bitmapArr[i2][this.mVisibleItems[i]]);
            }
        }
    }
}
