package com.vivekwarde.measure.metronome;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.ActionView;
import com.vivekwarde.measure.custom_controls.ActionView.OnActionViewEventListener;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.MyImageView;
import com.vivekwarde.measure.custom_controls.NumpadView;
import com.vivekwarde.measure.custom_controls.NumpadView.NumpadViewEventListener;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.metronome.MetronomeMeterPicker.OnMeterViewEventListener;
import com.vivekwarde.measure.metronome.MetronomeSubdivisionPicker.OnSubdivisionViewEventListener;
import com.vivekwarde.measure.metronome.MetronomeVolumeControl.OnSliderEventListener;
import com.vivekwarde.measure.metronome.MetronomeWheelView.OnWheelViewEventListener;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SPTimer;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.ArrayList;

public class MetronomeActivity extends BaseActivity implements OnClickListener, OnTouchListener, OnWheelViewEventListener, OnSliderEventListener, AnimationListener, NumpadViewEventListener, OnActionViewEventListener, OnMeterViewEventListener, OnSubdivisionViewEventListener, Runnable, OnLoadCompleteListener {
    public static boolean mIsLocked;
    static ArrayList<String> mPresetList;

    static {
        mPresetList = null;
        mIsLocked = false;
    }

    final String tag;
    int mBPM;
    int mBeatCount;
    int mCount;
    double mDuration;
    SPImageButton mInfoButton;
    boolean mIsPlaying;
    boolean mIsVirginEdit;
    SPImageButton mLoadButton;
    SPImageButton mLockButton;
    SPImageButton mMenuButton;
    SPImageButton mSaveButton;
    SPImageButton mStartStopButton;
    double mSubDuration;
    int mSubdivCount;
    SPImageButton mSubdivisionButton;
    boolean mUserRunning;
    private boolean cacheFlashEnable;
    private boolean cacheFlashOnFirstBeat;
    private ImageView[] mBlinkNodeView;
    private int mEditIndex;
    private Bitmap mEmptyBlueImg;
    private Bitmap mEmptyOrangeImg;
    private View mFlashView;
    private Bitmap mFullBlueImg;
    private Bitmap mFullOrangeImg;
    private int mHeaderBarHeight;
    private SPScale9ImageView mLEDScreen;
    private TextView[] mLabelArray;
    private int mLoadCount;
    private MetronomeLoadView mLoadView;
    private SPScale9ImageView mMainLedScreen;
    private TextView[] mMeterLabel;
    private MetronomeMeterPicker mMeterView;
    private MyImageView[] mNodeBlue;
    private MyImageView[] mNodeOrange;
    private LinearLayout[] mNodesLayout;
    private NumpadView mNumpadView;
    private MetronomeSaveView mSaveView;
    private TextView mSubdivisionButtonLabel;
    private TextView mSubdivisionLabel;
    private MetronomeSubdivisionPicker mSubdivisionView;
    private SPTimer mTimer;
    private float mVolume;
    private MetronomeVolumeControl mVolumeControl;
    private MetronomeWheelView mWheelView;

    public MetronomeActivity() {
        this.tag = getClass().getSimpleName();
        this.mMenuButton = null;
        this.mSaveButton = null;
        this.mLoadButton = null;
        this.mInfoButton = null;
        this.mStartStopButton = null;
        this.mLockButton = null;
        this.mSubdivisionButton = null;
        this.mSubdivisionButtonLabel = null;
        this.mLEDScreen = null;
        this.mLabelArray = new TextView[]{null, null};
        this.mIsPlaying = false;
        this.mUserRunning = false;
        this.mNumpadView = null;
        this.mEditIndex = 0;
        this.mVolume = 50.0f;
        this.mLoadView = null;
        this.mSaveView = null;
        this.mMeterLabel = new TextView[]{null, null};
        this.mNodesLayout = new LinearLayout[]{null, null};
        this.cacheFlashEnable = true;
        this.cacheFlashOnFirstBeat = true;
        this.mLoadCount = 0;
    }

    static void loadPresetList(Context context) {
        mPresetList = MiscUtility.readFromFileToArrayList(context, "MetronomePresetList.dat");
        if (mPresetList.size() == 0) {
            mPresetList.add("Song 1\t60\t3\t4\t1");
            mPresetList.add("Song 2\t120\t6\t8\t2");
            mPresetList.add("Song 3\t90\t1\t2\t3");
            MiscUtility.writeArrayListToFile(context, "MetronomePresetList.dat", mPresetList);
        }
    }

    static void savePresetList(Context context) {
        MiscUtility.writeArrayListToFile(context, "MetronomePresetList.dat", mPresetList);
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        initInfo();
        initSubviews();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        if (this.mUserRunning) {
            startMetronome();
        }
    }

    protected void onPause() {
        super.onPause();
        stopMetronome();
        pauseAds();
    }

    void initInfo() {
        loadPresetList(this);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        this.mVolume = pref.getFloat(SettingsKey.SETTINGS_METRONOME_VOLUME_KEY, 50.0f);
        this.mBPM = pref.getInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, 100);
        this.mDuration = 60.0d / ((double) this.mBPM);
        this.mSubdivCount = 0;
        this.mBeatCount = 0;
        this.mCount = 0;
        this.mIsPlaying = false;
    }

    void initSubviews() {
        int i;
        Typeface musicFont = Typeface.createFromAsset(getResources().getAssets(), "fonts/MusiSync.ttf");
        Typeface meterFont = Typeface.createFromAsset(getResources().getAssets(), "fonts/Fraction.ttf");
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams layoutParams = new LayoutParams(-1, mScreenHeight);
        layoutParams.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(layoutParams);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        int frameMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_VERT_MARGIN_BTW_DOTLEDSCREEN_AND_HEADER);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap();
        this.mHeaderBarHeight = (bitmap.getHeight() / 2) + (vButtonScreenMargin * 2);
        Bitmap bitmapLed = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        int vledmargin = (this.mHeaderBarHeight - (bitmapLed.getHeight() - 2)) / 2;
        int leftledmargin = (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN);
        this.mLEDScreen = new SPScale9ImageView(this);
        this.mLEDScreen.setId(8);
        this.mLEDScreen.setBitmap(bitmapLed);
        layoutParams = new LayoutParams(bitmapLed.getWidth() - 2, bitmapLed.getHeight() - 2);
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        layoutParams.setMargins(leftledmargin, vledmargin, 0, 0);
        this.mLEDScreen.setLayoutParams(layoutParams);
        this.mLEDScreen.setOnTouchListener(this);
        this.mUiLayout.addView(this.mLEDScreen);
        if (((bitmap.getWidth() * 5) + (hButtonButtonMargin * 4)) + (hButtonScreenMargin * 2) > (getResources().getDisplayMetrics().widthPixels - (bitmapLed.getWidth() - 2)) - leftledmargin) {
            hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_NARROW_MARGIN_BTW_BUTTONS);
        }
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(bitmap);
        this.mMenuButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        layoutParams.height = bitmap.getHeight() / 2;
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        this.mInfoButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mInfoButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mMenuButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mInfoButton);
        this.mLoadButton = new SPImageButton(this);
        this.mLoadButton.setId(5);
        this.mLoadButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_setlist_load)).getBitmap());
        this.mLoadButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mInfoButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mLoadButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mLoadButton);
        this.mSaveButton = new SPImageButton(this);
        this.mSaveButton.setId(6);
        this.mSaveButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_setlist_save)).getBitmap());
        this.mSaveButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mLoadButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mSaveButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mSaveButton);
        this.mLockButton = new SPImageButton(this);
        this.mLockButton.setId(4);
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_unlock)).getBitmap());
        this.mLockButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mSaveButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mLockButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mLockButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap();
        SPScale9ImageView backgroundView = new SPScale9ImageView(this);
        backgroundView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.topMargin = this.mHeaderBarHeight;
        backgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(backgroundView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        int[] capSizes = ImageUtility.getContentCapSizes(bitmap);
        int ledHeight = (bitmap.getHeight() - 2) * 2;
        this.mMainLedScreen = new SPScale9ImageView(this);
        this.mMainLedScreen.setId(12);
        this.mMainLedScreen.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, ledHeight);
        int tempMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_DOTLEDSCREEN_AND_SCREEN);
        layoutParams.setMargins(tempMargin, this.mHeaderBarHeight + frameMargin, tempMargin, 0);
        this.mMainLedScreen.setLayoutParams(layoutParams);
        this.mMainLedScreen.setOnClickListener(this);
        this.mUiLayout.addView(this.mMainLedScreen);
        this.mSubdivisionLabel = new TextView(this);
        this.mSubdivisionLabel.setId(13);
        this.mSubdivisionLabel.setPaintFlags(this.mSubdivisionLabel.getPaintFlags() | Barcode.ITF);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(12);
        layoutParams.setMargins(capSizes[0], 0, 0, capSizes[3]);
        this.mSubdivisionLabel.setLayoutParams(layoutParams);
        this.mSubdivisionLabel.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mSubdivisionLabel.setBackgroundColor(0);
        this.mSubdivisionLabel.setTypeface(musicFont);
        this.mSubdivisionLabel.setTextSize(1, getResources().getDimension(R.dimen.METRONOME_MAIN_LED_SUB_DIVISION_FONT_SIZE));
        this.mSubdivisionLabel.setGravity(3);
        this.mMainLedScreen.addView(this.mSubdivisionLabel);
        for (i = 0; i < 2; i++) {
            this.mMeterLabel[i] = new TextView(this);
            this.mMeterLabel[i].setId(13);
            this.mMeterLabel[i].setPaintFlags(this.mMeterLabel[i].getPaintFlags() | Barcode.ITF);
            layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(9);
            layoutParams.addRule(10);
            layoutParams.setMargins(capSizes[0], capSizes[1], 0, 0);
            this.mMeterLabel[i].setLayoutParams(layoutParams);
            this.mMeterLabel[i].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            this.mMeterLabel[i].setBackgroundColor(0);
            this.mMeterLabel[i].setTypeface(meterFont);
            this.mMeterLabel[i].setTextSize(1, getResources().getDimension(R.dimen.METRONOME_MAIN_LED_METER_FONT_SIZE));
            this.mMeterLabel[i].setGravity(3);
            this.mMainLedScreen.addView(this.mMeterLabel[i]);
        }
        initMeterNodes();
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.metronome_volume_slider)).getBitmap();
        this.mVolumeControl = new MetronomeVolumeControl(this, bitmap);
        this.mVolumeControl.setId(9);
        this.mVolumeControl.setValue(PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_METRONOME_VOLUME_KEY, 50.0f));
        layoutParams = new LayoutParams(-1, bitmap.getHeight());
        layoutParams.addRule(3, this.mMainLedScreen.getId());
        layoutParams.setMargins(tempMargin, 5, tempMargin, 0);
        this.mVolumeControl.setLayoutParams(layoutParams);
        this.mVolumeControl.setOnSliderEventListener(this);
        this.mUiLayout.addView(this.mVolumeControl);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        LayoutParams wheelViewLayoutParams = new LayoutParams(-1, -1);
        wheelViewLayoutParams.addRule(14);
        wheelViewLayoutParams.addRule(12);
        wheelViewLayoutParams.addRule(3, this.mVolumeControl.getId());
        relativeLayout.setLayoutParams(wheelViewLayoutParams);
        this.mUiLayout.addView(relativeLayout);
        this.mWheelView = new MetronomeWheelView(this);
        this.mWheelView.setId(11);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(13);
        this.mWheelView.setLayoutParams(layoutParams);
        this.mWheelView.setOnWheelViewEventListener(this);
        relativeLayout.addView(this.mWheelView);
        this.mSubdivisionButton = new SPImageButton(this);
        this.mSubdivisionButton.setId(7);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.metronome_button_sub_div)).getBitmap();
        this.mSubdivisionButton.setBackgroundBitmap(bitmap);
        this.mSubdivisionButton.setOnClickListener(this);
        layoutParams = new LayoutParams(bitmap.getWidth(), bitmap.getHeight() / 2);
        layoutParams.addRule(3, this.mVolumeControl.getId());
        layoutParams.addRule(9);
        layoutParams.setMargins(tempMargin, 0, 0, 0);
        this.mSubdivisionButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mSubdivisionButton);
        this.mSubdivisionButtonLabel = new TextView(this);
        this.mSubdivisionButtonLabel.setPaintFlags(this.mSubdivisionButtonLabel.getPaintFlags() | Barcode.ITF);
        this.mSubdivisionButtonLabel.setLayoutParams(layoutParams);
        this.mSubdivisionButtonLabel.setTextColor(-1);
        this.mSubdivisionButtonLabel.setTypeface(musicFont);
        this.mSubdivisionButtonLabel.setTextSize(1, getResources().getDimension(R.dimen.METRONOME_SUBDIVISION_BUTTON_FONT_SIZE));
        this.mUiLayout.addView(this.mSubdivisionButtonLabel);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, this.mSubdivisionButton.getId());
        layoutParams.leftMargin = (bitmap.getWidth() - this.mSubdivisionButtonLabel.getWidth()) / 4;
        layoutParams.addRule(6, this.mSubdivisionButton.getId());
        layoutParams.topMargin = ((bitmap.getHeight() / 2) - this.mSubdivisionButtonLabel.getHeight()) / 4;
        this.mSubdivisionButtonLabel.setLayoutParams(layoutParams);
        this.mStartStopButton = new SPImageButton(this);
        this.mStartStopButton.setId(3);
        this.mStartStopButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.metronome_button_start)).getBitmap());
        this.mStartStopButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(12);
        layoutParams.addRule(11);
        layoutParams.setMargins(0, 0, 0, tempMargin);
        this.mStartStopButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mStartStopButton);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        float valueFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE);
        float unitFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE);
        for (i = 0; i < 2; i++) {
            float f;
            this.mLabelArray[i] = new TextView(this);
            this.mLabelArray[i].setId((i + 0) + 100);
            this.mLabelArray[i].setPaintFlags(this.mLabelArray[i].getPaintFlags() | Barcode.ITF);
            this.mLabelArray[i].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            this.mLabelArray[i].setBackgroundColor(0);
            this.mLabelArray[i].setTypeface(font);
            TextView textView = this.mLabelArray[i];
            if (i % 2 != 0) {
                f = unitFontSize;
            } else {
                f = valueFontSize;
            }
            textView.setTextSize(1, f);
            this.mLabelArray[i].setGravity((i % 2 == 0 ? 16 : 48) | 5);
            this.mUiLayout.addView(this.mLabelArray[i]);
        }
        new Paint().setTypeface(font);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, this.mLEDScreen.getId());
        layoutParams.addRule(7, this.mLEDScreen.getId());
        layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_UNIT_LABEL_RIGHT_MARGIN);
        layoutParams.topMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_VALUE_LABEL_RIGHT_MARGIN);
        this.mLabelArray[1].setLayoutParams(layoutParams);
        this.mLabelArray[1].setText("BPM");
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, this.mLEDScreen.getId());
        layoutParams.addRule(8, this.mLEDScreen.getId());
        layoutParams.addRule(5, this.mLEDScreen.getId());
        layoutParams.addRule(0, this.mLabelArray[1].getId());
        layoutParams.rightMargin = 3;
        this.mLabelArray[0].setLayoutParams(layoutParams);
        this.mLabelArray[0].setText(String.valueOf(this.mBPM));
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.mNoAdsButton.setLayoutParams(layoutParams);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        relativeLayout.addView(this.mNoAdsButton);
        updateSubdivisionButtonLabel();
        updateMeterLabel();
    }

    void initMeterNodes() {
        int id;
        this.mNodesLayout[0] = new LinearLayout(this);
        this.mNodesLayout[0].setOrientation(0);
        LayoutParams params = new LayoutParams(-1, -2);
        params.addRule(10);
        int margin = (int) getResources().getDimension(R.dimen.METRONOME_METER_AREA_LEFT_MARGIN);
        params.setMargins(margin, 0, 0, 0);
        this.mNodesLayout[0].setLayoutParams(params);
        this.mMainLedScreen.addView(this.mNodesLayout[0]);
        this.mNodesLayout[1] = new LinearLayout(this);
        this.mNodesLayout[1].setOrientation(0);
        params = new LayoutParams(-1, -2);
        params.addRule(12);
        params.setMargins(margin, 0, 0, 0);
        this.mNodesLayout[1].setLayoutParams(params);
        this.mMainLedScreen.addView(this.mNodesLayout[1]);
        this.mEmptyBlueImg = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.metronome_beat_blue_dimmed)).getBitmap();
        this.mEmptyOrangeImg = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.metronome_beat_orange_dimmed)).getBitmap();
        this.mFullBlueImg = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.metronome_beat_blue_glowed)).getBitmap();
        this.mFullOrangeImg = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.metronome_beat_orange_glowed)).getBitmap();
        this.mNodeOrange = new MyImageView[8];
        this.mNodeBlue = new MyImageView[13];
        this.mBlinkNodeView = new ImageView[2];
        int i = 0;
        int id2 = GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE;
        while (i < 8) {
            this.mNodeOrange[i] = new MyImageView(this);
            id = id2 + 1;
            this.mNodeOrange[i].setId(id2);
            this.mNodeOrange[i].setBitmaps(this.mEmptyBlueImg, this.mFullBlueImg);
            this.mNodesLayout[1].addView(this.mNodeOrange[i]);
            ViewHelper.setScaleX(this.mNodeOrange[i], 0.5f);
            ViewHelper.setScaleY(this.mNodeOrange[i], 0.5f);
            i++;
            id2 = id;
        }
        this.mBlinkNodeView[1] = new ImageView(this);
        id = id2 + 1;
        this.mBlinkNodeView[1].setId(id2);
        this.mBlinkNodeView[1].setImageBitmap(this.mFullBlueImg);
        i = 0;
        id2 = id;
        while (i < 13) {
            this.mNodeBlue[i] = new MyImageView(this);
            id = id2 + 1;
            this.mNodeBlue[i].setId(id2);
            this.mNodeBlue[i].setBitmaps(i == 0 ? this.mEmptyOrangeImg : this.mEmptyBlueImg, i == 0 ? this.mFullOrangeImg : this.mFullBlueImg);
            this.mNodesLayout[0].addView(this.mNodeBlue[i]);
            i++;
            id2 = id;
        }
        this.mBlinkNodeView[0] = new ImageView(this);
        id = id2 + 1;
        this.mBlinkNodeView[0].setId(id2);
        this.mBlinkNodeView[0].setImageBitmap(this.mFullOrangeImg);
        displayColorNode();
    }

    void displayColorNode() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        int subBeat = pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1);
        int beat = pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3);
        int i = 0;
        while (i < 8) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, -2, TextTrackStyle.DEFAULT_FONT_SCALE);
            if (i < subBeat) {
                this.mNodeOrange[i].setLayoutParams(params);
                if (this.mNodeOrange[i].getParent() == null) {
                    this.mNodesLayout[1].addView(this.mNodeOrange[i]);
                    ViewHelper.setScaleX(this.mNodeOrange[i], 0.5f);
                    ViewHelper.setScaleY(this.mNodeOrange[i], 0.5f);
                }
            } else {
                this.mNodeOrange[i].setLayoutParams(params);
                this.mNodesLayout[1].removeView(this.mNodeOrange[i]);
            }
            i = i == 0 ? i + 1 : i + 1;
        }
        i = 0;
        while (i < 13) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, -2, TextTrackStyle.DEFAULT_FONT_SCALE);
            if (i < beat) {
                this.mNodeBlue[i].setLayoutParams(params);
                if (this.mNodeBlue[i].getParent() == null) {
                    this.mNodesLayout[0].addView(this.mNodeBlue[i]);
                }
            } else {
                this.mNodeBlue[i].setLayoutParams(params);
                this.mNodesLayout[0].removeView(this.mNodeBlue[i]);
            }
            i = i == 0 ? i + 1 : i + 1;
        }
        resetBeatState();
    }

    void startMetronome() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        this.cacheFlashEnable = pref.getBoolean(SettingsKey.SETTINGS_METRONOME_FLASH_ENABLED_KEY, true);
        this.cacheFlashOnFirstBeat = pref.getString(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY, "0").equalsIgnoreCase("0");
        this.mSubDuration = (double) (60000.0f / ((float) (pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1) * pref.getInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, 92))));
        this.mLoadCount = 0;
        String[] sounds = new String[]{pref.getString(SettingsKey.SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY, "Wood 6"), pref.getString(SettingsKey.SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY, "Wood 5"), pref.getString(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY, "Wood 5")};
        for (int i = 0; i < 3; i++) {
            if (sounds[i].equalsIgnoreCase("No Sound")) {
                this.mLoadCount++;
            }
        }
        MetronomeSoundManager.getInstance(this).addSounds(sounds[0], sounds[1], sounds[2]);
    }

    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        this.mLoadCount++;
        if (this.mLoadCount == 3) {
            onMetronomeEvent();
            this.mTimer = new SPTimer(new Handler(), this, (int) this.mSubDuration, false);
            this.mTimer.start();
        }
    }

    public void run() {
        onMetronomeEvent();
    }

    void stopMetronome() {
        if (this.mTimer != null) {
            this.mTimer.stop();
            this.mTimer = null;
        }
        MetronomeSoundManager.getInstance(this).removeAllSounds();
        hideFlashView();
        resetBeatState();
    }

    void onMetronomeEvent() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean playSuvdiv = true;
        if (this.mSubdivCount == 0) {
            if (this.mBeatCount == 0) {
                playSuvdiv = false;
                playSound_FirstBeat();
            } else {
                playSuvdiv = false;
                playSound_MainBeat();
            }
            if (pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3) == 1) {
                blinkBeatNode();
            }
            runNode();
        } else {
            runNode();
        }
        if (playSuvdiv) {
            playSound_SubBeat();
        }
        this.mSubdivCount++;
        if (this.mSubdivCount >= pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 4)) {
            this.mSubdivCount = 0;
        }
        if (this.mSubdivCount == 0) {
            this.mBeatCount++;
            if (this.mBeatCount >= pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3)) {
                this.mBeatCount = 0;
            }
        }
    }

    void runNode() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        int subdiv = pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1);
        int beat = pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3);
        int i = 0;
        while (i < subdiv) {
            if (subdiv == 1) {
                this.mNodeOrange[i].setHighlighted(false);
            } else {
                this.mNodeOrange[i].setHighlighted(i == this.mSubdivCount);
            }
            i++;
        }
        if (subdiv == 1) {
            blinkSubdivisionNode();
        }
        i = 0;
        while (i < beat) {
            MyImageView myImageView = this.mNodeBlue[i];
            boolean z = i == this.mBeatCount && beat != 1;
            myImageView.setHighlighted(z);
            i++;
        }
        if (!this.cacheFlashEnable) {
            return;
        }
        if (this.cacheFlashOnFirstBeat) {
            showFlashView();
            if (this.mBeatCount != 0) {
                return;
            }
            if (this.mSubdivCount == 0) {
                animateAlpha(this.mFlashView, TextTrackStyle.DEFAULT_FONT_SCALE, 0.0f, (long) (this.mSubDuration / 2.0d));
                return;
            } else {
                animateAlpha(this.mFlashView, ViewHelper.getAlpha(this.mFlashView), 0.0f, (long) (this.mSubDuration * ((double) (pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1) - 1))));
                return;
            }
        }
        showFlashView();
        if (subdiv != 2) {
            if (this.mSubdivCount == 0) {
                animateAlpha(this.mFlashView, TextTrackStyle.DEFAULT_FONT_SCALE, 0.0f, (long) (this.mSubDuration / 2.0d));
            } else if (this.mSubdivCount < (subdiv * 3) / 4) {
                animateAlpha(this.mFlashView, ViewHelper.getAlpha(this.mFlashView), 0.0f, (long) (this.mSubDuration * ((double) ((pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1) * 3) / 4))));
            }
        } else if (this.mSubdivCount == 0) {
            animateAlpha(this.mFlashView, TextTrackStyle.DEFAULT_FONT_SCALE, 0.0f, (long) (this.mSubDuration / 2.0d));
        } else {
            animateAlpha(this.mFlashView, ViewHelper.getAlpha(this.mFlashView), 0.0f, (long) (this.mSubDuration / 2.0d));
        }
    }

    void animateAlpha(View view, float from, float to, long duration) {
        ObjectAnimator.ofFloat((Object) view, "alpha", from, to).setDuration(duration).start();
    }

    void blinkBeatNode() {
        ViewHelper.setAlpha(this.mBlinkNodeView[0], TextTrackStyle.DEFAULT_FONT_SCALE);
        animateAlpha(this.mBlinkNodeView[0], TextTrackStyle.DEFAULT_FONT_SCALE, 0.0f, (long) (this.mSubDuration / 2.0d));
    }

    void blinkSubdivisionNode() {
        ViewHelper.setAlpha(this.mBlinkNodeView[1], TextTrackStyle.DEFAULT_FONT_SCALE);
        animateAlpha(this.mBlinkNodeView[1], TextTrackStyle.DEFAULT_FONT_SCALE, 0.0f, (long) (this.mSubDuration / 2.0d));
    }

    void showFlashView() {
        if (this.mFlashView == null) {
            this.mFlashView = new View(this);
            this.mFlashView.setBackgroundColor(Color.argb(Barcode.ITF, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK, MotionEventCompat.ACTION_MASK));
            this.mUiLayout.addView(this.mFlashView);
        }
        this.mFlashView.setVisibility(0);
        ViewHelper.setAlpha(this.mFlashView, 0.0f);
    }

    void hideFlashView() {
        if (this.mFlashView != null) {
            this.mUiLayout.removeView(this.mFlashView);
            this.mFlashView = null;
        }
    }

    void playSound_FirstBeat() {
        String sSoundFile = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY, "Wood 6");
        if (!sSoundFile.equalsIgnoreCase("No Sound")) {
            MetronomeSoundManager.getInstance(this).playSound(sSoundFile, this.mVolume / 100.0f);
        }
    }

    void playSound_MainBeat() {
        String sSoundFile = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY, "Wood 5");
        if (!sSoundFile.equalsIgnoreCase("No Sound")) {
            MetronomeSoundManager.getInstance(this).playSound(sSoundFile, this.mVolume / 100.0f);
        }
    }

    void playSound_SubBeat() {
        String sSoundFile = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY, "Wood 5");
        if (!sSoundFile.equalsIgnoreCase("No Sound")) {
            MetronomeSoundManager.getInstance(this).playSound(sSoundFile, this.mVolume / 100.0f);
        }
    }

    void resetBeatState() {
        int i;
        this.mSubdivCount = 0;
        this.mBeatCount = 0;
        this.mCount = 0;
        for (i = 0; i < 13; i++) {
            this.mNodeBlue[i].setHighlighted(false);
        }
        ViewHelper.setAlpha(this.mBlinkNodeView[0], 0.0f);
        for (i = 0; i < 8; i++) {
            this.mNodeOrange[i].setHighlighted(false);
        }
        ViewHelper.setAlpha(this.mBlinkNodeView[1], 0.0f);
    }

    void onStartStop() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        this.mUserRunning = !this.mUserRunning;
        if (this.mIsPlaying) {
            stopMetronome();
            this.mIsPlaying = false;
        } else {
            startMetronome();
            this.mIsPlaying = true;
        }
        updateStartStopButtonState();
    }

    void updateStartStopButtonState() {
        this.mStartStopButton.setBackgroundBitmapId(this.mIsPlaying ? R.drawable.metronome_button_stop : R.drawable.metronome_button_start);
    }

    void onLock() {
        boolean z = true;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(2, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (mIsLocked) {
            z = false;
        }
        mIsLocked = z;
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(mIsLocked ? R.drawable.button_lock : R.drawable.button_unlock)).getBitmap());
    }

    void showNumpadView(boolean show) {
        float height;
        if (show) {
            height = (float) this.mUiLayout.getHeight();
        } else {
            height = 0.0f;
        }
        TranslateAnimation anim = new TranslateAnimation(0.0f, 0.0f, height, show ? 0.0f : (float) this.mUiLayout.getHeight());
        anim.setDuration(300);
        anim.setFillBefore(true);
        anim.setFillAfter(true);
        if (!show) {
            anim.setAnimationListener(this);
        }
        this.mNumpadView.startAnimation(anim);
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(3, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
    }

    void beginEditing() {
        if (this.mNumpadView == null) {
            this.mIsVirginEdit = true;
            this.mNumpadView = new NumpadView(this, 0, this.mUiLayout.getWidth(), this.mUiLayout.getHeight(), (int) (ViewHelper.getY(this.mVolumeControl) + ((float) this.mVolumeControl.getHeight())), 0);
            this.mNumpadView.setListener(this);
            this.mNumpadView.setLayoutParams(new LayoutParams(-1, -1));
            this.mUiLayout.addView(this.mNumpadView);
            showNumpadView(true);
            this.mLabelArray[this.mEditIndex].setTextColor(-1);
            View backgroundView = new View(this);
            backgroundView.setId(10);
            backgroundView.setBackgroundColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            LayoutParams params = new LayoutParams(-2, -2);
            params.addRule(6, this.mLabelArray[this.mEditIndex].getId());
            params.addRule(0, this.mLabelArray[this.mEditIndex + 1].getId());
            params.addRule(5, this.mLEDScreen.getId());
            params.addRule(8, this.mLabelArray[this.mEditIndex].getId());
            params.setMargins(10, 3, 0, 3);
            backgroundView.setLayoutParams(params);
            this.mUiLayout.addView(backgroundView);
            this.mLabelArray[this.mEditIndex].bringToFront();
            this.mNumpadView.bringToFront();
        }
    }

    void endEditing() {
        showNumpadView(false);
        this.mLabelArray[this.mEditIndex].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        View backgroundView = this.mUiLayout.findViewById(10);
        if (backgroundView != null) {
            this.mUiLayout.removeView(backgroundView);
        }
        int nVal = 0;
        try {
            nVal = Integer.parseInt(this.mLabelArray[this.mEditIndex].getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (nVal < 40) {
            nVal = 40;
        }
        if (nVal > 208) {
            nVal = 208;
        }
        this.mLabelArray[this.mEditIndex].setText(String.valueOf(nVal));
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, nVal);
        editor.apply();
        this.mWheelView.updateWheelViewPosition();
        restartMetronome();
    }

    public void onClick(View v) {
        int[] location;
        switch (v.getId()) {
            case Barcode.ALL_FORMATS /*0*/:
                onRemoveAdsButton();
            case CompletionEvent.STATUS_FAILURE /*1*/:
                onButtonMenu();
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                onButtonSettings();
            case CompletionEvent.STATUS_CANCELED /*3*/:
                onStartStop();
            case Barcode.PHONE /*4*/:
                onLock();
            case Barcode.PRODUCT /*5*/:
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                    SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                }
                location = new int[]{0, 0};
                this.mLoadButton.getLocationInWindow(location);
                this.mLoadView = new MetronomeLoadView(this);
                this.mLoadView.construct(30, 30, new Point(location[0] + (this.mLoadButton.getWidth() / 2), location[1] + this.mLoadButton.getHeight()));
                this.mLoadView.setLayoutParams(new LayoutParams(-1, -1));
                this.mLoadView.setOnActionViewEventListener(this);
                this.mUiLayout.addView(this.mLoadView);
            case Barcode.SMS /*6*/:
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                    SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                }
                location = new int[]{0, 0};
                this.mSaveButton.getLocationInWindow(location);
                this.mSaveView = new MetronomeSaveView(this);
                this.mSaveView.construct(30, 30, new Point(location[0] + (this.mSaveButton.getWidth() / 2), location[1] + this.mSaveButton.getHeight()));
                this.mSaveView.setLayoutParams(new LayoutParams(-1, -1));
                this.mSaveView.setOnActionViewEventListener(this);
                this.mUiLayout.addView(this.mSaveView);
            case Barcode.TEXT /*7*/:
                if (!mIsLocked) {
                    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                        SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                    }
                    location = new int[]{0, 0};
                    this.mSubdivisionButton.getLocationInWindow(location);
                    this.mSubdivisionView = new MetronomeSubdivisionPicker(this);
                    this.mSubdivisionView.construct(new Point(location[0] + this.mSubdivisionButton.getWidth(), location[1] + (this.mSubdivisionButton.getHeight() / 2)));
                    this.mSubdivisionView.setLayoutParams(new LayoutParams(-1, -1));
                    this.mSubdivisionView.setOnSubdivisionViewEventListener(this);
                    this.mUiLayout.addView(this.mSubdivisionView);
                }
            case Barcode.DRIVER_LICENSE /*12*/:
                if (!mIsLocked) {
                    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                        SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                    }
                    location = new int[]{0, 0};
                    this.mMainLedScreen.getLocationInWindow(location);
                    this.mMeterView = new MetronomeMeterPicker(this);
                    this.mMeterView.construct(30, 30, new Point(location[0] + (this.mMainLedScreen.getWidth() / 2), location[1] + (this.mMainLedScreen.getHeight() / 2)));
                    this.mMeterView.setLayoutParams(new LayoutParams(-1, -1));
                    this.mMeterView.setOnMeterViewEventListener(this);
                    this.mUiLayout.addView(this.mMeterView);
                }
            default:
                Log.i(this.tag, "Unknown action id :(");
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!v.equals(this.mLEDScreen) || event.getAction() != 0 || mIsLocked) {
            return false;
        }
        beginEditing();
        return true;
    }

    public void onBPMValueChanged() {
        updateBPMLabel();
    }

    public void onBPMValueChangedEnd() {
        updateBPMLabel();
        restartMetronome();
    }

    void restartMetronome() {
        if (this.mIsPlaying) {
            if (this.mTimer != null) {
                this.mTimer.stop();
                this.mTimer = null;
            }
            startMetronome();
        }
    }

    public void onSliderValueChanged(MetronomeVolumeControl control, float fVal) {
        this.mVolume = fVal;
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putFloat(SettingsKey.SETTINGS_METRONOME_VOLUME_KEY, fVal);
        editor.apply();
    }

    public void onNumpadEvent(String sKey) {
        if (sKey == "Done") {
            endEditing();
            return;
        }
        if (this.mIsVirginEdit) {
            this.mIsVirginEdit = false;
            onNumpadEvent("C");
        }
        if (sKey == "C") {
            this.mLabelArray[this.mEditIndex].setText("0");
            return;
        }
        int nVal = 0;
        try {
            nVal = Integer.parseInt(this.mLabelArray[this.mEditIndex].getText().toString() + sKey);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (nVal <= 208) {
            this.mLabelArray[this.mEditIndex].setText(String.valueOf(nVal));
        }
    }

    public void onAnimationEnd(Animation animation) {
        if (animation instanceof TranslateAnimation) {
            this.mNumpadView.post(new Runnable() {
                public void run() {
                    if (MetronomeActivity.this.mNumpadView.getParent() != null) {
                        ((RelativeLayout) MetronomeActivity.this.mNumpadView.getParent()).removeView(MetronomeActivity.this.mNumpadView);
                    }
                    MetronomeActivity.this.mNumpadView = null;
                }
            });
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onBackPressed() {
        if (this.mNumpadView != null) {
            endEditing();
        } else if (this.mLoadView != null) {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            onActionViewEvent(this.mLoadView, -1);
        } else if (this.mSaveView != null) {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            onActionViewEvent(this.mSaveView, -1);
        } else if (this.mMeterView != null) {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            this.mUiLayout.removeView(this.mMeterView);
            this.mMeterView = null;
        } else if (this.mSubdivisionView != null) {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            this.mUiLayout.removeView(this.mSubdivisionView);
            this.mSubdivisionView = null;
        } else {
            super.onBackPressed();
        }
    }

    public void onActionViewEvent(ActionView actionView, int actionId) {
        if (actionView.equals(this.mSaveView)) {
            this.mUiLayout.removeView(this.mSaveView);
            this.mSaveView = null;
        } else if (actionView.equals(this.mLoadView)) {
            if (actionId == 1) {
                updateBPMLabel();
                this.mWheelView.updateWheelViewPosition();
                updateMeterLabel();
                updateSubdivisionButtonLabel();
                displayColorNode();
                resetBeatState();
                restartMetronome();
            }
            this.mUiLayout.removeView(this.mLoadView);
            this.mLoadView = null;
        } else if (actionView.equals(this.mMeterView)) {
            this.mUiLayout.removeView(this.mMeterView);
            this.mMeterView = null;
        }
    }

    public void onMeterSelected(boolean selected) {
        if (selected) {
            updateMeterLabel();
            displayColorNode();
            restartMetronome();
        }
        this.mUiLayout.removeView(this.mMeterView);
        this.mMeterView = null;
    }

    public void onSubdivisionSelected(boolean selected) {
        if (selected) {
            updateSubdivisionButtonLabel();
            displayColorNode();
            restartMetronome();
        }
        this.mUiLayout.removeView(this.mSubdivisionView);
        this.mSubdivisionView = null;
    }

    void updateSubdivisionButtonLabel() {
        int nSubdivision = PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1);
        String[] sMusicNode = new String[]{"q", " n", " T", " y"};
        this.mSubdivisionButtonLabel.setText(sMusicNode[nSubdivision - 1]);
        this.mSubdivisionLabel.setText(sMusicNode[nSubdivision - 1]);
    }

    void updateMeterLabel() {
        int i = 3;
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        int nSign1 = pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3);
        int nSign2 = pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_KEY, 4);
        int os = 0;
        if (nSign2 == 4) {
            os = 1;
        } else if (nSign2 == 8) {
            os = 2;
        }
        this.mMeterLabel[0].setText(Character.toString((char) (nSign1 + 48)));
        TextView textView = this.mMeterLabel[1];
        int i2 = os + 65;
        if (nSign1 <= 9) {
            i = 0;
        }
        textView.setText(Character.toString((char) (i + i2)));
    }

    void updateBPMLabel() {
        this.mBPM = PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, 92);
        this.mLabelArray[0].setText(String.valueOf(this.mBPM));
    }

    public class Define {
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_LOAD = 5;
        public static final int BUTTON_LOCK = 4;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_SAVE = 6;
        public static final int BUTTON_START_STOP = 3;
        public static final int BUTTON_SUBDIVISION = 7;
        public static final int BUTTON_UPGRADE = 0;
        public static final int EDIT_BACKGROUND_VIEW = 10;
        public static final int LABEL00 = 0;
        public static final int LABEL01 = 1;
        public static final int LED_FRAME = 8;
        public static final int MAIN_LED_SCREEN = 12;
        public static final int SUB_DIVISION_LABEL = 13;
        public static final int VOLUME_VIEW = 9;
        public static final int WHEEL_VIEW = 11;
    }
}
