package com.vivekwarde.measure.metronome;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.ValueAnimator;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.ActionView;
import com.vivekwarde.measure.custom_controls.SPButton;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;

public class MetronomeLoadView extends ActionView implements OnItemClickListener, OnClickListener {
    final String tag;
    private MetronomePresetDataAdapter mListDataAdapter;
    private ListView mListView;

    public MetronomeLoadView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
    }

    public void construct(int leftMargin, int rightMargin, Point point) {
        ArrayList<String> titleList = new ArrayList();
        titleList.add(getResources().getString(R.string.IDS_DELETE));
        titleList.add(getResources().getString(R.string.IDS_LOAD));
        titleList.add(getResources().getString(R.string.IDS_CANCEL));
        super.construct(leftMargin, rightMargin, point, titleList, 490);
    }

    protected void initSubviews() {
        super.initSubviews();
        int rowHeight = (int) getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_ROW_HEIGHT);
        this.mListView = new ListView(getContext());
        int id = 10 + 1;
        this.mListView.setId(10);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, rowHeight * 6);
        params.addRule(5, 1);
        params.addRule(7, 1);
        params.addRule(10);
        params.setMargins(0, 0, 0, 0);
        this.mListView.setLayoutParams(params);
        this.mListView.setDivider(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.LISTVIEW_DIVIDER_COLOR)));
        this.mListView.setDividerHeight(1);
        this.mListView.setVerticalFadingEdgeEnabled(false);
        this.mListDataAdapter = new MetronomePresetDataAdapter(getContext());
        this.mListView.setAdapter(this.mListDataAdapter);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setChoiceMode(1);
        this.mListView.setSelector(new ColorDrawable(0));
        this.mBorderView.addView(this.mListView);
        SPButton btn = (SPButton) this.mBorderView.findViewById(1);
        if (!(btn == null || ((RelativeLayout.LayoutParams) btn.getLayoutParams()) == null)) {
            params = new RelativeLayout.LayoutParams(-2, -2);
            params.addRule(3, this.mListView.getId());
            params.setMargins(0, (int) getResources().getDimension(R.dimen.METRONOME_LOAD_SAVE_SPACE_BTW_BUTTONS_AND_CONTENT), 0, 0);
            params.addRule(14);
            btn.setLayoutParams(params);
        }
        updateButtonsState();
    }

    void updateButtonsState() {
        boolean z;
        boolean z2 = true;
        View findViewById = this.mBorderView.findViewById(1);
        if (this.mListDataAdapter.getSelection() != -1) {
            z = true;
        } else {
            z = false;
        }
        findViewById.setEnabled(z);
        View findViewById2 = this.mBorderView.findViewById(2);
        if (this.mListDataAdapter.getSelection() == -1) {
            z2 = false;
        }
        findViewById2.setEnabled(z2);
    }

    boolean loadSelectedPreset() {
        int index = this.mListDataAdapter.getSelection();
        if (index < 0 || index >= MetronomeActivity.mPresetList.size()) {
            return false;
        }
        StringTokenizer tokens = new StringTokenizer((String) MetronomeActivity.mPresetList.get(index), "\t");
        tokens.nextToken();
        String sBPM = tokens.nextToken();
        String sSign1 = tokens.nextToken();
        String sSign2 = tokens.nextToken();
        String sSubdiv = tokens.nextToken();
        Editor editor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        editor.putInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, Integer.valueOf(sBPM).intValue());
        editor.putInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, Integer.valueOf(sSign1).intValue());
        editor.putInt(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_KEY, Integer.valueOf(sSign2).intValue());
        editor.putInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, Integer.valueOf(sSubdiv).intValue());
        editor.apply();
        return true;
    }

    public void onClick(View v) {
        if (v.getId() == 1) {
            AlertDialog dialog = new Builder(getContext()).create();
            dialog.setMessage(getResources().getString(R.string.IDS_DELETE_SETLIST_ASKING));
            dialog.setButton(-1, getResources().getString(17039379), this);
            dialog.setButton(-2, getResources().getString(17039369), this);
            dialog.setIcon(R.drawable.icon_about);
            dialog.setTitle(getResources().getString(R.string.IDS_CONFIRMATION));
            dialog.show();
            MiscUtility.setDialogFontSize(getContext(), dialog);
            return;
        }
        if (v.getId() == 2) {
            loadSelectedPreset();
        }
        super.onClick(v);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mListDataAdapter.setSelection(position);
        updateButtonsState();
    }

    public void onClick(DialogInterface dialog, int which) {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        switch (which) {
            case ValueAnimator.INFINITE /*-1*/:
                this.mListDataAdapter.removeSelectedItem();
                updateButtonsState();
            default:
        }
    }

    static class ViewHolder {
        public TextView tv;
        public TextView tv1;
        public TextView tv2;

        ViewHolder() {
        }
    }

    public class MetronomePresetDataAdapter extends BaseAdapter {
        private int mColor;
        private Context mContext;
        private ArrayList<String> mData;
        private Typeface mFont;
        private float mFontSize;
        private int mMaxVisibleItemCount;
        private Typeface mMusicFont;
        private float mMusicFontSize;
        private int mRowHeight;
        private int mSelectionPosition;

        public MetronomePresetDataAdapter(Context context) {
            this.mData = null;
            this.mRowHeight = 0;
            this.mFont = null;
            this.mFontSize = 0.0f;
            this.mColor = -1;
            this.mContext = null;
            this.mMaxVisibleItemCount = 0;
            this.mMusicFont = null;
            this.mSelectionPosition = -1;
            this.mContext = context;
            this.mData = MetronomeActivity.mPresetList;
            this.mFont = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/Eurostile LT Medium.ttf");
            this.mMusicFont = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/MusiSync.ttf");
            this.mFontSize = this.mContext.getResources().getDimension(R.dimen.METRONOME_SAVE_LOAD_FONT_SIZE);
            this.mMusicFontSize = this.mContext.getResources().getDimension(R.dimen.METRONOME_LOAD_MUSIC_NOTATION_FONT_SIZE);
            this.mColor = ContextCompat.getColor(this.mContext, R.color.LED_BLUE);
            this.mRowHeight = (int) this.mContext.getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_ROW_HEIGHT);
        }

        public int getRowHeight() {
            return this.mRowHeight;
        }

        public void setListViewHeight(int height) {
            this.mMaxVisibleItemCount = (int) Math.round(((double) height) / ((double) getRowHeight()));
            updateDummyItems();
            notifyDataSetChanged();
        }

        private void updateDummyItems() {
            int count;
            int i;
            if (this.mData.size() < this.mMaxVisibleItemCount) {
                count = this.mMaxVisibleItemCount - this.mData.size();
                for (i = 0; i < count; i++) {
                    this.mData.add(BuildConfig.FLAVOR);
                }
            }
            if (this.mData.size() > this.mMaxVisibleItemCount) {
                count = this.mData.size() - this.mMaxVisibleItemCount;
                for (i = 0; i < count; i++) {
                    if (this.mData.get((this.mData.size() - i) - 1) == BuildConfig.FLAVOR) {
                        this.mData.remove(this.mData.size() - 1);
                    }
                }
            }
        }

        public void addItem(String sItem, boolean instantNotify) {
            this.mData.add(0, sItem);
            updateDummyItems();
            if (instantNotify) {
                notifyDataSetChanged();
            }
        }

        public void clear(boolean instantNotify) {
            this.mData.clear();
            updateDummyItems();
            if (instantNotify) {
                notifyDataSetChanged();
            }
        }

        public int getCount() {
            return this.mData.size();
        }

        public String getItem(int position) {
            return (String) this.mData.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public int getSelection() {
            return this.mSelectionPosition;
        }

        public void setSelection(int position) {
            if (position == this.mSelectionPosition) {
                this.mSelectionPosition = -1;
            } else {
                this.mSelectionPosition = position;
            }
            notifyDataSetChanged();
        }

        public void removeSelectedItem() {
            if (this.mSelectionPosition >= 0 && this.mSelectionPosition < this.mData.size()) {
                this.mData.remove(this.mSelectionPosition);
                if (this.mData.size() == 0) {
                    this.mSelectionPosition = -1;
                } else if (this.mSelectionPosition >= this.mData.size()) {
                    this.mSelectionPosition = this.mData.size() - 1;
                }
                notifyDataSetChanged();
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RelativeLayout listLayout = (RelativeLayout) convertView;
            if (listLayout == null) {
                listLayout = new RelativeLayout(this.mContext);
                listLayout.setLayoutParams(new LayoutParams(-1, this.mRowHeight));
                TextView tv = new TextView(this.mContext);
                int id = 1 + 1;
                tv.setId(1);
                tv.setPaintFlags(tv.getPaintFlags() | Barcode.ITF);
                tv.setTextSize(1, this.mFontSize);
                tv.setLayoutParams(new RelativeLayout.LayoutParams(-2, -1));
                tv.setGravity(19);
                listLayout.addView(tv);
                TextView tv2 = new TextView(this.mContext);
                int id2 = id + 1;
                tv2.setId(id);
                tv2.setPaintFlags(tv2.getPaintFlags() | Barcode.ITF);
                tv2.setTypeface(this.mMusicFont);
                tv2.setTextSize(1, this.mMusicFontSize);
                tv2.setTextColor(-1);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-2, -1);
                params.addRule(11);
                tv2.setLayoutParams(params);
                tv2.setGravity(21);
                listLayout.addView(tv2);
                TextView tv1 = new TextView(this.mContext);
                id = id2 + 1;
                tv1.setId(id2);
                tv1.setPaintFlags(tv1.getPaintFlags() | Barcode.ITF);
                tv1.setTypeface(this.mFont);
                tv1.setTextSize(1, this.mFontSize);
                tv1.setTextColor(-1);
                params = new RelativeLayout.LayoutParams(-2, -1);
                params.addRule(0, tv2.getId());
                tv1.setLayoutParams(params);
                tv1.setGravity(21);
                listLayout.addView(tv1);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.tv = tv;
                viewHolder.tv1 = tv1;
                viewHolder.tv2 = tv2;
                listLayout.setTag(viewHolder);
            }
            listLayout.setBackgroundColor(position == this.mSelectionPosition ? this.mColor : 0);
            StringTokenizer tokens = new StringTokenizer(getItem(position), "\t");
            ViewHolder holder = (ViewHolder) listLayout.getTag();
            holder.tv.setText(tokens.nextToken());
            holder.tv.setTextColor(position == this.mSelectionPosition ? -1 : this.mColor);
            holder.tv1.setText(String.format(Locale.US, "%s BPM, %s/%s, ", new Object[]{tokens.nextToken(), tokens.nextToken(), tokens.nextToken()}));
            holder.tv2.setText(new String[]{"q", "n", "T", "y"}[Integer.valueOf(tokens.nextToken()).intValue() - 1]);
            return listLayout;
        }
    }
}
