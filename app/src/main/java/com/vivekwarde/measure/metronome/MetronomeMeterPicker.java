package com.vivekwarde.measure.metronome;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.barcode.Barcode;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.BorderView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class MetronomeMeterPicker extends RelativeLayout implements OnItemClickListener, OnTouchListener {
    protected BorderView mBorderView;
    Point mArrowPosition;
    OnMeterViewEventListener mListener;
    int mSelectedIndex;
    int mWidth;
    private int mLeftMargin;
    private MetronomeMeterDataAdapter mListDataAdapter;
    private ListView mListView;
    private int mRightMargin;

    public MetronomeMeterPicker(Context context) {
        super(context);
        this.mListener = null;
        this.mArrowPosition = null;
        this.mBorderView = null;
        this.mWidth = 0;
        this.mSelectedIndex = 0;
        this.mLeftMargin = 0;
        this.mRightMargin = 0;
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.DIALOG_BACKGROUND_COLOR));
        setOnTouchListener(this);
        this.mWidth = getResources().getDisplayMetrics().widthPixels;
    }

    public void setOnMeterViewEventListener(OnMeterViewEventListener listener) {
        this.mListener = listener;
    }

    public void construct(int leftMargin, int rightMargin, Point point) {
        this.mLeftMargin = leftMargin;
        this.mRightMargin = rightMargin;
        this.mArrowPosition = point;
        initSubviews();
    }

    protected void initSubviews() {
        int margin = (int) getResources().getDimension(R.dimen.ACTION_VIEW_MARGIN);
        this.mBorderView = new BorderView(getContext());
        this.mBorderView.construct(2, (double) (((float) (this.mArrowPosition.x - this.mLeftMargin)) / (((((float) this.mWidth) - ((float) this.mLeftMargin)) - ((float) this.mRightMargin)) - ((float) this.mBorderView.getCornerCapSize()))));
        this.mBorderView.setId(BaseActivity.AD_VIEW_ID);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.mWidth / 2, -2);
        params.addRule(14);
        params.addRule(10);
        params.setMargins(this.mLeftMargin, this.mArrowPosition.y, this.mRightMargin, 0);
        this.mBorderView.setPadding(0, margin, 0, margin);
        this.mBorderView.setLayoutParams(params);
        this.mBorderView.setClickable(true);
        addView(this.mBorderView);
        int rowHeight = (int) getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_ROW_HEIGHT);
        this.mListView = new ListView(getContext());
        int id = 10 + 1;
        this.mListView.setId(10);
        params = new RelativeLayout.LayoutParams(-1, rowHeight * 8);
        params.addRule(5, 1);
        params.addRule(7, 1);
        params.addRule(10);
        params.setMargins(this.mBorderView.getCornerCapSize(), 0, this.mBorderView.getCornerCapSize(), 0);
        this.mListView.setLayoutParams(params);
        this.mListView.setDivider(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.LISTVIEW_DIVIDER_COLOR)));
        this.mListView.setDividerHeight(1);
        this.mListView.setVerticalFadingEdgeEnabled(false);
        this.mListDataAdapter = new MetronomeMeterDataAdapter(getContext());
        this.mListView.setAdapter(this.mListDataAdapter);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setChoiceMode(1);
        this.mListView.setSelector(new ColorDrawable(0));
        this.mBorderView.addView(this.mListView);
        this.mSelectedIndex = getCurrentMeterIndex();
        this.mListDataAdapter.setSelection(this.mSelectedIndex);
        this.mListView.post(new Runnable() {
            public void run() {
                MetronomeMeterPicker.this.mListView.setSelection(MetronomeMeterPicker.this.mSelectedIndex);
                View v = MetronomeMeterPicker.this.mListView.getChildAt(MetronomeMeterPicker.this.mSelectedIndex);
                if (v != null) {
                    v.requestFocus();
                }
            }
        });
    }

    int getCurrentMeterIndex() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        int nSign1 = pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3);
        return this.mListDataAdapter.indexOf(nSign1 + "/" + pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_KEY, 4));
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mListDataAdapter.setSelection(position);
        updateToPreferences();
        if (this.mListener != null) {
            this.mListener.onMeterSelected(true);
        }
    }

    void updateToPreferences() {
        int index = this.mListDataAdapter.getSelection();
        if (index >= 0 && index < this.mListDataAdapter.getCount()) {
            StringTokenizer tokens = new StringTokenizer(this.mListDataAdapter.getItem(index), "/");
            String sSign1 = tokens.nextToken();
            String sSign2 = tokens.nextToken();
            Editor editor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
            editor.putInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, Integer.valueOf(sSign1).intValue());
            editor.putInt(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_KEY, Integer.valueOf(sSign2).intValue());
            editor.apply();
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!v.equals(this)) {
            return false;
        }
        if (event.getAction() == 0) {
            this.mListener.onMeterSelected(false);
        }
        return true;
    }

    public interface OnMeterViewEventListener {
        void onMeterSelected(boolean z);
    }

    static class ViewHolder {
        public TextView tv;
        public TextView tv1;

        ViewHolder() {
        }
    }

    public class MetronomeMeterDataAdapter extends BaseAdapter {
        private int mColor;
        private Context mContext;
        private ArrayList<String> mData;
        private Typeface mFont;
        private float mFontSize;
        private int mRowHeight;
        private int mSelectionPosition;

        public MetronomeMeterDataAdapter(Context context) {
            this.mData = null;
            this.mRowHeight = 0;
            this.mFont = null;
            this.mFontSize = 0.0f;
            this.mColor = -1;
            this.mContext = null;
            this.mSelectionPosition = -1;
            this.mContext = context;
            initData();
            this.mFont = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/Fraction.ttf");
            this.mFontSize = this.mContext.getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_FONT_SIZE);
            this.mColor = ContextCompat.getColor(this.mContext, R.color.LED_BLUE);
            this.mRowHeight = (int) this.mContext.getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_ROW_HEIGHT);
        }

        void initData() {
            this.mData = new ArrayList();
            StringTokenizer tokens = new StringTokenizer("1/2 2/2 3/2 4/2 5/2 6/2 7/2 8/2 9/2 10/2 11/2 12/2 13/2 1/4 2/4 3/4 4/4 5/4 6/4 7/4 8/4 9/4 10/4 11/4 12/4 13/4 3/8 6/8 9/8 12/8", " ");
            while (tokens.hasMoreTokens()) {
                this.mData.add(tokens.nextToken());
            }
        }

        public int indexOf(String item) {
            return this.mData.indexOf(item);
        }

        public int getRowHeight() {
            return this.mRowHeight;
        }

        public int getCount() {
            return this.mData.size();
        }

        public String getItem(int position) {
            return (String) this.mData.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public int getSelection() {
            return this.mSelectionPosition;
        }

        public void setSelection(int position) {
            if (position == this.mSelectionPosition) {
                this.mSelectionPosition = -1;
            } else {
                this.mSelectionPosition = position;
            }
            notifyDataSetChanged();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RelativeLayout listLayout = (RelativeLayout) convertView;
            if (listLayout == null) {
                listLayout = new RelativeLayout(this.mContext);
                listLayout.setLayoutParams(new LayoutParams(-1, this.mRowHeight));
                TextView tv = new TextView(this.mContext);
                int id = 1 + 1;
                tv.setId(1);
                tv.setPaintFlags(tv.getPaintFlags() | Barcode.ITF);
                tv.setTypeface(this.mFont);
                tv.setTextSize(1, this.mFontSize);
                tv.setTextColor(-1);
                tv.setLayoutParams(new RelativeLayout.LayoutParams(-2, -1));
                tv.setGravity(19);
                listLayout.addView(tv);
                TextView tv1 = new TextView(this.mContext);
                int i = id + 1;
                tv1.setId(id);
                tv1.setPaintFlags(tv1.getPaintFlags() | Barcode.ITF);
                tv1.setTypeface(this.mFont);
                tv1.setTextSize(1, this.mFontSize);
                tv1.setTextColor(-1);
                tv1.setLayoutParams(new RelativeLayout.LayoutParams(-2, -1));
                tv1.setGravity(19);
                listLayout.addView(tv1);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.tv = tv;
                viewHolder.tv1 = tv1;
                listLayout.setTag(viewHolder);
            }
            listLayout.setBackgroundColor(position == this.mSelectionPosition ? this.mColor : 0);
            StringTokenizer tokens = new StringTokenizer(getItem(position), "/");
            int nSign1 = Integer.valueOf(tokens.nextToken()).intValue();
            int nSign2 = Integer.valueOf(tokens.nextToken()).intValue();
            ViewHolder holder = (ViewHolder) listLayout.getTag();
            int os = 0;
            if (nSign2 == 4) {
                os = 1;
            } else if (nSign2 == 8) {
                os = 2;
            }
            holder.tv.setText(Character.toString((char) (nSign1 + 48)));
            holder.tv1.setText(Character.toString((char) ((nSign1 > 9 ? 3 : 0) + (os + 65))));
            return listLayout;
        }
    }
}
