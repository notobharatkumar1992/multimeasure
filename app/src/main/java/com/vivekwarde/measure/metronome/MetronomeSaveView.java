package com.vivekwarde.measure.metronome;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.barcode.Barcode;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.ActionView;
import com.vivekwarde.measure.custom_controls.BorderView;
import com.vivekwarde.measure.custom_controls.SPButton;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;

import java.util.ArrayList;
import java.util.Locale;

public class MetronomeSaveView extends ActionView implements TextWatcher {
    final String tag;
    private EditText mSetListNameEditor;

    public MetronomeSaveView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
    }

    public void construct(int leftMargin, int rightMargin, Point point) {
        ArrayList<String> titleList = new ArrayList();
        titleList.add(getResources().getString(R.string.IDS_SAVE));
        titleList.add(getResources().getString(R.string.IDS_CANCEL));
        super.construct(leftMargin, rightMargin, point, titleList, 490);
    }

    protected void initSubviews() {
        super.initSubviews();
        int titleColor = ContextCompat.getColor(getContext(), R.color.METRONOME_SAVE_VIEW_TITLE_COLOR);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        float fontSize = getResources().getDimension(R.dimen.METRONOME_SAVE_LOAD_FONT_SIZE);
        Typeface euroFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Eurostile LT Medium.ttf");
        Typeface meterFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Fraction.ttf");
        TextView textView = new TextView(getContext());
        int id = 10 + 1;
        textView.setId(10);
        textView.setPaintFlags(textView.getPaintFlags() | Barcode.ITF);
        textView.setTextColor(titleColor);
        textView.setTextSize(1, fontSize);
        textView.setGravity(19);
        textView.setText(getResources().getString(R.string.IDS_SETLIST_NAME));
        LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(5, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        textView.setLayoutParams(layoutParams);
        this.mBorderView.addView(textView);
        BorderView editorBackgroundView = new BorderView(getContext());
        editorBackgroundView.construct(0, 0.0d);
        int i = id + 1;
        editorBackgroundView.setId(id);
        layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(3, textView.getId());
        int margin = (int) TypedValue.applyDimension(1, 10.0f, getResources().getDisplayMetrics());
        layoutParams.setMargins(margin, 0, margin, 0);
        editorBackgroundView.setLayoutParams(layoutParams);
        this.mBorderView.addView(editorBackgroundView);
        String sName = getResources().getString(R.string.IDS_MY_SONG) + " " + (MetronomeActivity.mPresetList.size() + 1);
        this.mSetListNameEditor = new EditText(getContext());
        id = i + 1;
        this.mSetListNameEditor.setId(i);
        this.mSetListNameEditor.setInputType(1);
        this.mSetListNameEditor.setTextColor(-1);
        this.mSetListNameEditor.setTextSize(1, fontSize);
        this.mSetListNameEditor.addTextChangedListener(this);
        this.mSetListNameEditor.setImeActionLabel(getResources().getString(R.string.IDS_DONE), 6);
        this.mSetListNameEditor.setBackgroundColor(0);
        this.mSetListNameEditor.setText(sName);
        layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        margin = (int) TypedValue.applyDimension(1, 5.0f, getResources().getDisplayMetrics());
        layoutParams.setMargins(margin, margin * 2, margin, margin * 2);
        this.mSetListNameEditor.setLayoutParams(layoutParams);
        editorBackgroundView.addView(this.mSetListNameEditor);
        textView = new TextView(getContext());
        i = id + 1;
        textView.setId(id);
        textView.setPaintFlags(textView.getPaintFlags() | Barcode.ITF);
        textView.setTextColor(titleColor);
        textView.setTextSize(1, fontSize);
        textView.setGravity(19);
        textView.setText(getResources().getString(R.string.IDS_TEMPO));
        layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(3, editorBackgroundView.getId());
        layoutParams.addRule(5, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        textView.setLayoutParams(layoutParams);
        this.mBorderView.addView(textView);
        textView = new TextView(getContext());
        id = i + 1;
        textView.setId(i);
        textView.setPaintFlags(textView.getPaintFlags() | Barcode.ITF);
        textView.setTextColor(-1);
        textView.setTypeface(euroFont);
        textView.setTextSize(1, fontSize);
        textView.setGravity(19);
        textView.setText(pref.getInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, 92) + " BPM");
        layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(3, textView.getId());
        layoutParams.addRule(5, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        textView.setLayoutParams(layoutParams);
        this.mBorderView.addView(textView);
        textView = new TextView(getContext());
        i = id + 1;
        textView.setId(id);
        textView.setPaintFlags(textView.getPaintFlags() | Barcode.ITF);
        textView.setTextColor(titleColor);
        textView.setTextSize(1, fontSize);
        textView.setGravity(19);
        textView.setText(getResources().getString(R.string.IDS_TIME_SIGNATURE));
        layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(3, textView.getId());
        layoutParams.addRule(5, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        textView.setLayoutParams(layoutParams);
        this.mBorderView.addView(textView);
        TextView[] timeSignatureValueLabel = new TextView[]{null, null};
        int i2 = 0;
        id = i;
        while (i2 < 2) {
            timeSignatureValueLabel[i2] = new TextView(getContext());
            i = id + 1;
            timeSignatureValueLabel[i2].setId(id);
            timeSignatureValueLabel[i2].setPaintFlags(timeSignatureValueLabel[i2].getPaintFlags() | Barcode.ITF);
            timeSignatureValueLabel[i2].setTextColor(-1);
            timeSignatureValueLabel[i2].setTypeface(meterFont);
            timeSignatureValueLabel[i2].setTextSize(1, getResources().getDimension(R.dimen.METRONOME_LOAD_SAVE_METER_FONT_SIZE));
            timeSignatureValueLabel[i2].setGravity(19);
            layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(3, textView.getId());
            layoutParams.addRule(5, 1);
            layoutParams.setMargins(0, 0, 0, 0);
            timeSignatureValueLabel[i2].setLayoutParams(layoutParams);
            this.mBorderView.addView(timeSignatureValueLabel[i2]);
            i2++;
            id = i;
        }
        int nSign1 = pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3);
        int nSign2 = pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_KEY, 4);
        int os = 0;
        if (nSign2 == 4) {
            os = 1;
        } else if (nSign2 == 8) {
            os = 2;
        }
        timeSignatureValueLabel[0].setText(Character.toString((char) (nSign1 + 48)));
        timeSignatureValueLabel[1].setText(Character.toString((char) ((nSign1 > 9 ? 3 : 0) + (os + 65))));
        textView = new TextView(getContext());
        i = id + 1;
        textView.setId(id);
        textView.setPaintFlags(textView.getPaintFlags() | Barcode.ITF);
        textView.setTextColor(titleColor);
        textView.setTextSize(1, fontSize);
        textView.setGravity(19);
        textView.setText(getResources().getString(R.string.IDS_SUBDIVISION));
        layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(3, timeSignatureValueLabel[0].getId());
        layoutParams.addRule(5, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        textView.setLayoutParams(layoutParams);
        this.mBorderView.addView(textView);
        textView = new TextView(getContext());
        id = i + 1;
        textView.setId(i);
        textView.setPaintFlags(textView.getPaintFlags() | Barcode.ITF);
        textView.setTextColor(-1);
        textView.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/MusiSync.ttf"));
        textView.setTextSize(1, fontSize);
        textView.setGravity(19);
        textView.setText(new String[]{"q", " n", " T", " y"}[pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1) - 1]);
        layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(3, textView.getId());
        layoutParams.addRule(5, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        textView.setLayoutParams(layoutParams);
        this.mBorderView.addView(textView);
        SPButton btn = (SPButton) this.mBorderView.findViewById(1);
        if (btn != null && ((RelativeLayout.LayoutParams) btn.getLayoutParams()) != null) {
            layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(3, textView.getId());
            layoutParams.setMargins(0, (int) getResources().getDimension(R.dimen.METRONOME_LOAD_SAVE_SPACE_BTW_BUTTONS_AND_CONTENT), 0, 0);
            layoutParams.addRule(14);
            btn.setLayoutParams(layoutParams);
        }
    }

    public void afterTextChanged(Editable editable) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        boolean z = true;
        SPButton btn = (SPButton) this.mBorderView.findViewById(1);
        if (btn != null) {
            if (s.length() <= 0) {
                z = false;
            }
            btn.setEnabled(z);
        }
    }

    public void onClick(View v) {
        if (v.getId() == 1) {
            if (this.mSetListNameEditor.getText().toString().length() != 0) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
                MetronomeActivity.mPresetList.add(String.format(Locale.US, "%s\t%d\t%d\t%d\t%d", new Object[]{"", Integer.valueOf(pref.getInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, 100)), Integer.valueOf(pref.getInt(SettingsKey.SETTINGS_METRONOME_METER_KEY, 3)), Integer.valueOf(pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_KEY, 4)), Integer.valueOf(pref.getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1))}));
                MetronomeActivity.savePresetList(getContext());
            } else {
                return;
            }
        }
        super.onClick(v);
    }
}
