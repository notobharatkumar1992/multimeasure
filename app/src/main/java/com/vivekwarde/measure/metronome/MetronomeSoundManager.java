package com.vivekwarde.measure.metronome;

import android.annotation.SuppressLint;
import android.media.SoundPool;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.common.annotation.KeepName;
import java.io.IOException;
import java.util.HashMap;

public class MetronomeSoundManager {
    private static MetronomeSoundManager mInstance;
    private MetronomeActivity mContext;
    private HashMap<String, Integer> mSoundIdMap;
    private SoundPool mSoundPool;
    private HashMap<Integer, Integer> mStreamIdMap;

    public class SoundId {
        public static final int METRONOME_BEAT = 100;
    }

    public class SoundVolume {
        public static final int MAX_VOLUME = 1;
        public static final int MIN_VOLUME = 0;
    }

    static {
        mInstance = null;
    }

    @SuppressLint({"UseSparseArrays"})
    private MetronomeSoundManager() {
        this.mSoundIdMap = new HashMap();
        this.mStreamIdMap = new HashMap();
    }

    public static MetronomeSoundManager getInstance(MetronomeActivity context) {
        if (mInstance == null) {
            mInstance = new MetronomeSoundManager();
        }
        mInstance.initSounds(context);
        return mInstance;
    }

    public void initSounds(MetronomeActivity theContext) {
        this.mContext = theContext;
        if (this.mSoundPool == null) {
            this.mSoundPool = new SoundPool(8, 3, 0);
        }
        this.mSoundPool.setOnLoadCompleteListener(this.mContext);
    }

    public void addSound(String sSoundFile) {
        try {
            this.mSoundIdMap.put(sSoundFile, Integer.valueOf(this.mSoundPool.load(this.mContext.getAssets().openFd("sounds/metronome/" + sSoundFile + ".mp3"), 1)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addSounds(String... sSoundFiles) {
        try {
            for (String sSoundFile : sSoundFiles) {
                if (!sSoundFile.equalsIgnoreCase("No Sound")) {
                    this.mSoundIdMap.put(sSoundFile, Integer.valueOf(this.mSoundPool.load(this.mContext.getAssets().openFd("sounds/metronome/" + sSoundFile + ".mp3"), 1)));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeSound(String sSoundFile) {
        this.mSoundPool.unload(((Integer) this.mSoundIdMap.get(sSoundFile)).intValue());
        this.mSoundIdMap.remove(sSoundFile);
    }

    public void removeAllSounds() {
        cleanup();
    }

    @KeepName
    public void removeAllUnusedSounds(String sSoundFile1, String sSoundFile2, String sSoundFile3) {
    }

    public void playSound(String sSoundFile, float volume) {
        try {
            if (!this.mSoundIdMap.containsKey(sSoundFile)) {
                addSound(sSoundFile);
            }
            this.mSoundPool.play(((Integer) this.mSoundIdMap.get(sSoundFile)).intValue(), volume, volume, 1, 0, TextTrackStyle.DEFAULT_FONT_SCALE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void stopSound(String sSoundFile) {
        try {
            this.mSoundPool.stop(((Integer) this.mStreamIdMap.get(sSoundFile)).intValue());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void cleanup() {
        this.mSoundPool.release();
        this.mSoundPool = null;
        this.mSoundIdMap.clear();
        this.mStreamIdMap.clear();
    }
}
