package com.vivekwarde.measure.metronome;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.vision.barcode.Barcode;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.BorderView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class MetronomeSubdivisionPicker extends RelativeLayout implements OnItemClickListener, OnTouchListener {
    Point mArrowPosition;
    protected BorderView mBorderView;
    private MetronomeSubdivisionDataAdapter mListDataAdapter;
    private ListView mListView;
    OnSubdivisionViewEventListener mListener;
    int mSelectedIndex;
    int mWidth;

    public class MetronomeSubdivisionDataAdapter extends BaseAdapter {
        private int mColor;
        private Context mContext;
        private ArrayList<String> mData;
        private Typeface mFont;
        private float mFontSize;
        private int mRowHeight;
        private int mSelectionPosition;

        public MetronomeSubdivisionDataAdapter(Context context) {
            this.mData = null;
            this.mRowHeight = 0;
            this.mFont = null;
            this.mFontSize = 0.0f;
            this.mColor = -1;
            this.mContext = null;
            this.mSelectionPosition = -1;
            this.mContext = context;
            initData();
            this.mFont = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/MusiSync.ttf");
            this.mFontSize = this.mContext.getResources().getDimension(R.dimen.METRONOME_SAVE_LOAD_FONT_SIZE);
            this.mColor = ContextCompat.getColor(this.mContext, R.color.LED_BLUE);
            this.mRowHeight = (int) this.mContext.getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_ROW_HEIGHT);
        }

        void initData() {
            this.mData = new ArrayList();
            StringTokenizer tokens = new StringTokenizer("q n T y", " ");
            while (tokens.hasMoreTokens()) {
                this.mData.add(tokens.nextToken());
            }
        }

        public int indexOf(String item) {
            return this.mData.indexOf(item);
        }

        public int getRowHeight() {
            return this.mRowHeight;
        }

        public int getCount() {
            return this.mData.size();
        }

        public String getItem(int position) {
            return (String) this.mData.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public void setSelection(int position) {
            if (position == this.mSelectionPosition) {
                this.mSelectionPosition = -1;
            } else {
                this.mSelectionPosition = position;
            }
            notifyDataSetChanged();
        }

        public int getSelection() {
            return this.mSelectionPosition;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RelativeLayout listLayout = (RelativeLayout) convertView;
            if (listLayout == null) {
                listLayout = new RelativeLayout(this.mContext);
                listLayout.setLayoutParams(new LayoutParams(-1, this.mRowHeight));
                TextView tv = new TextView(this.mContext);
                int id = 1 + 1;
                tv.setId(1);
                tv.setPaintFlags(tv.getPaintFlags() | Barcode.ITF);
                tv.setTypeface(this.mFont);
                tv.setTextSize(1, this.mFontSize);
                tv.setTextColor(-1);
                tv.setLayoutParams(new RelativeLayout.LayoutParams(-2, -1));
                tv.setGravity(19);
                listLayout.addView(tv);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.tv = tv;
                listLayout.setTag(viewHolder);
            }
            listLayout.setBackgroundColor(position == this.mSelectionPosition ? this.mColor : 0);
            ((ViewHolder) listLayout.getTag()).tv.setText(getItem(position));
            return listLayout;
        }
    }

    public interface OnSubdivisionViewEventListener {
        void onSubdivisionSelected(boolean z);
    }

    static class ViewHolder {
        public TextView tv;

        ViewHolder() {
        }
    }

    public void setOnSubdivisionViewEventListener(OnSubdivisionViewEventListener listener) {
        this.mListener = listener;
    }

    public MetronomeSubdivisionPicker(Context context) {
        super(context);
        this.mListener = null;
        this.mArrowPosition = null;
        this.mBorderView = null;
        this.mWidth = 0;
        this.mSelectedIndex = 0;
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.DIALOG_BACKGROUND_COLOR));
        setOnTouchListener(this);
        this.mWidth = getResources().getDisplayMetrics().widthPixels;
    }

    public void construct(Point point) {
        this.mArrowPosition = point;
        initSubviews();
    }

    protected void initSubviews() {
        int margin = (int) getResources().getDimension(R.dimen.ACTION_VIEW_MARGIN);
        this.mBorderView = new BorderView(getContext());
        this.mBorderView.construct(1, (double) 0.0f);
        this.mBorderView.setId(BaseActivity.AD_VIEW_ID);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.mWidth / 2, -2);
        params.addRule(9);
        params.addRule(10);
        params.setMargins(this.mArrowPosition.x, (this.mArrowPosition.y - this.mBorderView.getCornerCapSize()) - (this.mBorderView.getArrowCapSize() / 2), 0, 0);
        this.mBorderView.setPadding(0, margin, 0, margin);
        this.mBorderView.setLayoutParams(params);
        this.mBorderView.setClickable(true);
        addView(this.mBorderView);
        this.mListView = new ListView(getContext());
        int id = 10 + 1;
        this.mListView.setId(10);
        params = new RelativeLayout.LayoutParams(-1, -2);
        params.addRule(5, 1);
        params.addRule(7, 1);
        params.addRule(10);
        params.setMargins(this.mBorderView.getCornerCapSize(), 0, this.mBorderView.getCornerCapSize(), 0);
        this.mListView.setLayoutParams(params);
        this.mListView.setDivider(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.LISTVIEW_DIVIDER_COLOR)));
        this.mListView.setDividerHeight(1);
        this.mListView.setVerticalFadingEdgeEnabled(false);
        this.mListView.setVerticalScrollBarEnabled(false);
        this.mListDataAdapter = new MetronomeSubdivisionDataAdapter(getContext());
        this.mListView.setAdapter(this.mListDataAdapter);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setChoiceMode(1);
        this.mListView.setSelector(new ColorDrawable(0));
        this.mBorderView.addView(this.mListView);
        this.mSelectedIndex = getCurrentSubdivisionIndex();
        this.mListDataAdapter.setSelection(this.mSelectedIndex);
        this.mListView.post(new Runnable() {
            public void run() {
                MetronomeSubdivisionPicker.this.mListView.setSelection(MetronomeSubdivisionPicker.this.mSelectedIndex);
                View v = MetronomeSubdivisionPicker.this.mListView.getChildAt(MetronomeSubdivisionPicker.this.mSelectedIndex);
                if (v != null) {
                    v.requestFocus();
                }
            }
        });
    }

    int getCurrentSubdivisionIndex() {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, 1) - 1;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mListDataAdapter.setSelection(position);
        updateToPreferences();
        if (this.mListener != null) {
            this.mListener.onSubdivisionSelected(true);
        }
    }

    void updateToPreferences() {
        int index = this.mListDataAdapter.getSelection();
        if (index >= 0 && index < this.mListDataAdapter.getCount()) {
            Editor editor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
            editor.putInt(SettingsKey.SETTINGS_METRONOME_SUB_DIVISION_KEY, index + 1);
            editor.apply();
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!v.equals(this)) {
            return false;
        }
        if (event.getAction() == 0) {
            this.mListener.onSubdivisionSelected(false);
        }
        return true;
    }
}
