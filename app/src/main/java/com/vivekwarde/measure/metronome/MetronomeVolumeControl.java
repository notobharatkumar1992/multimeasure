package com.vivekwarde.measure.metronome;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.cast.TextTrackStyle;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.utilities.ImageUtility;

public class MetronomeVolumeControl extends RelativeLayout implements OnTouchListener {
    final String tag;
    float mCurValue;
    boolean mIsContinuousUpdate;
    OnSliderEventListener mListener;
    float mMaxValue;
    float mMinValue;
    Point mPrevPoint;
    float mStepValue;
    int mWidth;
    private Bitmap mBackgroundBitmap;
    private ImageView mMaxView;
    private ImageView mMinView;
    private ImageView mNobView;
    private Bitmap mSliderBitmap;

    @Deprecated
    public MetronomeVolumeControl(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mMinView = null;
        this.mMaxView = null;
        this.mNobView = null;
        this.mBackgroundBitmap = null;
        this.mSliderBitmap = null;
        this.mListener = null;
        this.mWidth = 0;
        this.mIsContinuousUpdate = true;
        this.mPrevPoint = null;
        this.mCurValue = 50.0f;
        this.mMinValue = 0.0f;
        this.mMaxValue = 100.0f;
        this.mStepValue = TextTrackStyle.DEFAULT_FONT_SCALE;
    }

    public MetronomeVolumeControl(Context context, Bitmap sliderBitmap) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mMinView = null;
        this.mMaxView = null;
        this.mNobView = null;
        this.mBackgroundBitmap = null;
        this.mSliderBitmap = null;
        this.mListener = null;
        this.mWidth = 0;
        this.mIsContinuousUpdate = true;
        this.mPrevPoint = null;
        this.mCurValue = 50.0f;
        this.mMinValue = 0.0f;
        this.mMaxValue = 100.0f;
        this.mStepValue = TextTrackStyle.DEFAULT_FONT_SCALE;
        setWillNotDraw(false);
        this.mSliderBitmap = sliderBitmap;
        initSubviews();
        setOnTouchListener(this);
    }

    void initSubviews() {
        this.mMinView = new ImageView(getContext());
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.metronome_volume_min)).getBitmap();
        this.mMinView.setImageBitmap(bitmap);
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(9);
        params.addRule(15);
        params.width = bitmap.getWidth();
        this.mMinView.setLayoutParams(params);
        addView(this.mMinView);
        this.mMaxView = new ImageView(getContext());
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.metronome_volume_max)).getBitmap();
        this.mMaxView.setImageBitmap(bitmap);
        params = new LayoutParams(-2, -2);
        params.addRule(11);
        params.addRule(15);
        params.width = bitmap.getWidth();
        this.mMaxView.setLayoutParams(params);
        addView(this.mMaxView);
        this.mNobView = new ImageView(getContext());
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.metronome_volume_nob)).getBitmap();
        this.mNobView.setImageBitmap(bitmap);
        params = new LayoutParams(-2, -2);
        params.addRule(9);
        params.addRule(15);
        params.width = bitmap.getWidth();
        this.mNobView.setLayoutParams(params);
        addView(this.mNobView);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w != oldw || h != oldh) {
            this.mBackgroundBitmap = ImageUtility.scale9Bitmap(this.mSliderBitmap, w, this.mSliderBitmap.getHeight() - 2);
            this.mWidth = (w - this.mMinView.getLayoutParams().width) - this.mMaxView.getLayoutParams().width;
            updateThumbPosition();
        }
    }

    void updateThumbPosition() {
        float pos = this.mCurValue / (this.mMaxValue - this.mMinValue);
        LayoutParams params = (LayoutParams) this.mNobView.getLayoutParams();
        params.leftMargin = (int) ((((float) (this.mWidth - this.mNobView.getLayoutParams().width)) * pos) + ((float) this.mMinView.getLayoutParams().width));
        this.mNobView.setLayoutParams(params);
    }

    protected void onDraw(Canvas canvas) {
        if (this.mBackgroundBitmap != null) {
            canvas.drawBitmap(this.mBackgroundBitmap, 0.0f, (float) ((getHeight() - this.mBackgroundBitmap.getHeight()) / 2), null);
        }
        setDrawingCacheEnabled(true);
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            onTouchDown(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 1) {
            onTouchUp(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 2) {
            onTouchMove(v, new Point((int) event.getX(), (int) event.getY()));
        }
        return true;
    }

    void updateWithNewPoint(Point currentPosition) {
        int x = (((LayoutParams) this.mNobView.getLayoutParams()).leftMargin - this.mMinView.getLayoutParams().width) + (currentPosition.x - this.mPrevPoint.x);
        if (x < 0) {
            x = 0;
        }
        int sliderLen = this.mWidth - this.mNobView.getLayoutParams().width;
        if (x > sliderLen) {
            x = sliderLen;
        }
        this.mCurValue = (this.mMaxValue - this.mMinValue) * (((float) x) / ((float) sliderLen));
        updateThumbPosition();
        this.mPrevPoint = currentPosition;
    }

    protected void onTouchDown(View source, Point currentPosition) {
        this.mPrevPoint = currentPosition;
    }

    protected void onTouchMove(View source, Point currentPosition) {
        updateWithNewPoint(currentPosition);
        if (this.mListener != null && this.mIsContinuousUpdate) {
            this.mListener.onSliderValueChanged(this, this.mCurValue);
        }
    }

    protected void onTouchUp(View source, Point currentPosition) {
        updateWithNewPoint(currentPosition);
        if (this.mListener != null) {
            this.mListener.onSliderValueChanged(this, this.mCurValue);
        }
    }

    void setOnSliderEventListener(OnSliderEventListener listener) {
        this.mListener = listener;
    }

    void setContinuousUpdate(boolean continuous) {
        this.mIsContinuousUpdate = continuous;
    }

    void setMinValue(float value) {
        this.mMinValue = value;
    }

    void setMaxValue(float value) {
        this.mMaxValue = value;
    }

    float getValue() {
        return this.mCurValue;
    }

    void setValue(float value) {
        this.mCurValue = value;
    }

    float getStep() {
        return this.mStepValue;
    }

    void setStep(float step) {
        this.mStepValue = step;
    }

    int getThumbXPosition() {
        return ((LayoutParams) this.mNobView.getLayoutParams()).leftMargin + (this.mNobView.getLayoutParams().width / 2);
    }

    public interface OnSliderEventListener {
        void onSliderValueChanged(MetronomeVolumeControl metronomeVolumeControl, float f);
    }
}
