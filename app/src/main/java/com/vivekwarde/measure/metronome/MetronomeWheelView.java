package com.vivekwarde.measure.metronome;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.TransportMediator;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.location.LocationRequest;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;

public class MetronomeWheelView extends RelativeLayout implements OnTouchListener {
    final String tag;
    double mAllAngle;
    double mAngle;
    Typeface mBoldFont;
    boolean mCanTouch;
    double mCenterX;
    double mCenterY;
    double mDownAngle;
    int[] mDrawTempoArray;
    boolean mIs2Phases;
    boolean mIsMoved;
    double mLastAngle;
    double mLastPreloadAngle;
    double mLastUseAngle;
    OnWheelViewEventListener mListener;
    double mMoveAngle;
    Typeface mNormalFont;
    RectF mOval;
    Paint mPaint;
    Path mPath;
    double mPhase2Duration;
    int mPreBPM;
    Point mPrevPoint;
    int mRotatedCircle;
    double mStartAngle;
    int[] mTempoArray;
    Rect mTextBounds;
    TextPaint mTextPaint;
    ImageView mWheelView;

    public MetronomeWheelView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mListener = null;
        this.mTempoArray = new int[]{40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 63, 66, 69, 72, 76, 80, 84, 92, 96, 100, LocationRequest.PRIORITY_LOW_POWER, 108, 112, 116, 120, TransportMediator.KEYCODE_MEDIA_PLAY, 132, 138, 144, 152, 160, 168, 176, 184, 192, SeismometerGraphView.HISTORY_SIZE_PER_PAGE, 208};
        this.mDrawTempoArray = new int[]{40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 63, 66, 69, 72, 76, 80, 84, 86, 92, 96, 100, LocationRequest.PRIORITY_LOW_POWER, 108, 112, 116, 120, TransportMediator.KEYCODE_MEDIA_PLAY, 132, 138, 144, 152, 160, 168, 176, 184, 192, SeismometerGraphView.HISTORY_SIZE_PER_PAGE, 208};
        this.mWheelView = null;
        this.mPaint = null;
        this.mTextPaint = null;
        this.mOval = new RectF();
        this.mPath = new Path();
        this.mTextBounds = new Rect();
        this.mNormalFont = null;
        this.mBoldFont = null;
        setWillNotDraw(false);
        this.mNormalFont = Typeface.createFromAsset(getResources().getAssets(), "fonts/Eurostile LT Medium.ttf");
        this.mBoldFont = Typeface.createFromAsset(getResources().getAssets(), "fonts/Eurostile LT Demi.ttf");
        this.mPaint = new Paint();
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setAntiAlias(true);
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.METRONOME_TEMPO_WHEEL_FONT_SIZE));
        initInfo();
        initSubviews();
        setOnTouchListener(this);
    }

    public void setOnWheelViewEventListener(OnWheelViewEventListener listener) {
        this.mListener = listener;
    }

    void initInfo() {
        this.mLastUseAngle = (double) PreferenceManager.getDefaultSharedPreferences(getContext()).getFloat(SettingsKey.SETTINGS_METRONOME_LAST_WHEEL_ANGLE_KEY, 0.0f);
        this.mAllAngle = this.mLastUseAngle;
        this.mDownAngle = 0.0d;
        this.mMoveAngle = 0.0d;
        this.mPreBPM = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, 100);
        this.mLastAngle = this.mAllAngle;
        this.mStartAngle = this.mLastUseAngle;
        this.mAngle = this.mLastUseAngle;
    }

    void initSubviews() {
        this.mWheelView = new ImageView(getContext());
        this.mWheelView.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.metronome_tempo_wheel)).getBitmap());
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mWheelView.setLayoutParams(params);
        addView(this.mWheelView);
        ViewHelper.setRotation(this.mWheelView, MiscUtility.radian2Degree((float) snapWithRad(this.mLastUseAngle)));
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (h != oldh || w != oldw) {
            this.mCenterX = (double) (getWidth() / 2);
            this.mCenterY = (double) (getHeight() / 2);
        }
    }

    protected void onDraw(Canvas canvas) {
        int lineWidth = (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics());
        this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.METRONOME_WHEEL_VIEW_WHITE_COLOR));
        this.mPaint.setStrokeWidth((float) lineWidth);
        int r = (this.mWheelView.getWidth() / 2) + ((int) getResources().getDimension(R.dimen.METRONOME_WHEEL_R1_R2_DELTA));
        int centerx = getWidth() / 2;
        int centery = getHeight() / 2;
        this.mOval.set((float) (centerx - r), (float) (centery - r), (float) (centerx + r), (float) (centery + r));
        canvas.drawOval(this.mOval, this.mPaint);
        this.mTextPaint.setTextAlign(Align.CENTER);
        this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.METRONOME_WHEEL_VIEW_WHITE_COLOR));
        this.mTextPaint.setTypeface(this.mNormalFont);
        int linelen = (int) TypedValue.applyDimension(1, 4.0f, getResources().getDisplayMetrics());
        int R1 = (int) (this.mOval.width() / 2.0f);
        int R2 = R1 + linelen;
        int count = this.mDrawTempoArray.length + 1;
        double arc = 6.283185307179586d / ((double) count);
        this.mOval.inset((float) (linelen * -2), (float) (linelen * -2));
        linelen += 5;
        for (int i = 0; i < count; i++) {
            double angle = 1.5707963267948966d + (((double) i) * arc);
            Canvas canvas2 = canvas;
            canvas2.drawLine((float) (this.mCenterX + (((double) R1) * Math.cos(angle))), (float) (this.mCenterY + (((double) R1) * Math.sin(angle))), (float) (this.mCenterX + (((double) R2) * Math.cos(angle))), (float) (this.mCenterY + (((double) R2) * Math.sin(angle))), this.mPaint);
            int id = i - 1;
            if (id >= 0 && id < this.mDrawTempoArray.length) {
                String sNo = String.valueOf(this.mDrawTempoArray[id]);
                this.mTextPaint.getTextBounds(sNo, 0, sNo.length(), this.mTextBounds);
                canvas.drawText(sNo, (float) ((((double) linelen) * Math.cos(angle)) + (this.mCenterX + (((double) ((this.mTextBounds.width() / 2) + R1)) * Math.cos(angle)))), (float) ((((double) linelen) * Math.sin(angle)) + ((this.mCenterY + (((double) ((this.mTextBounds.height() / 2) + R1)) * Math.sin(angle))) + ((double) (this.mTextBounds.height() / 2)))), this.mTextPaint);
            }
        }
        this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.METRONOME_WHEEL_VIEW_BLUE_COLOR));
        float degArc = MiscUtility.radian2Degree((float) arc);
        r = (this.mWheelView.getWidth() / 2) + ((int) getResources().getDimension(R.dimen.METRONOME_WHEEL_R2_WHEEL_DELTA));
        this.mOval.set((float) (centerx - r), (float) (centery - r), (float) (centerx + r), (float) (centery + r));
        canvas.drawArc(this.mOval, (-3.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (8.0f * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        canvas.drawArc(this.mOval, (5.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (3.0f * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        canvas.drawArc(this.mOval, (8.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (TextTrackStyle.DEFAULT_FONT_SCALE * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        canvas.drawArc(this.mOval, (11.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (10.0f * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        canvas.drawArc(this.mOval, (21.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (2.0f * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        canvas.drawArc(this.mOval, (23.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (3.0f * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        canvas.drawArc(this.mOval, (26.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (8.0f * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        canvas.drawArc(this.mOval, (34.0f * degArc) + TextTrackStyle.DEFAULT_FONT_SCALE, (3.0f * degArc) - (2.0f * TextTrackStyle.DEFAULT_FONT_SCALE), false, this.mPaint);
        this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.METRONOME_WHEEL_VIEW_BLUE_COLOR));
        this.mTextPaint.setTypeface(this.mBoldFont);
        this.mTextPaint.getTextBounds("A", 0, 1, this.mTextBounds);
        int textOffset = lineWidth + 1;
        this.mPath.reset();
        this.mPath.addArc(this.mOval, 23.0f * degArc, 3.0f * degArc);
        canvas.drawTextOnPath("ADAGIO", this.mPath, 0.0f, (float) (-textOffset), this.mTextPaint);
        this.mPath.reset();
        this.mPath.addArc(this.mOval, 26.0f * degArc, 8.0f * degArc);
        canvas.drawTextOnPath("ADANTE", this.mPath, 0.0f, (float) (-textOffset), this.mTextPaint);
        this.mPath.reset();
        this.mPath.addArc(this.mOval, 31.0f * degArc, 9.0f * degArc);
        canvas.drawTextOnPath("MODERATO", this.mPath, 0.0f, (float) (-textOffset), this.mTextPaint);
        this.mPath.reset();
        this.mPath.addArc(this.mOval, 8.0f * degArc, -3.0f * degArc);
        canvas.drawTextOnPath("PRESTO", this.mPath, 0.0f, (float) (this.mTextBounds.height() + textOffset), this.mTextPaint);
        this.mPath.reset();
        this.mPath.addArc(this.mOval, 21.0f * degArc, -10.0f * degArc);
        canvas.drawTextOnPath("LARGO", this.mPath, 0.0f, (float) (this.mTextBounds.height() + textOffset), this.mTextPaint);
        this.mPath.reset();
        this.mPath.addArc(this.mOval, 18.0f * degArc, 8.0f * degArc);
        canvas.drawTextOnPath("LARGHRETTO", this.mPath, 0.0f, (float) (this.mTextBounds.height() + textOffset), this.mTextPaint);
        this.mPath.reset();
        this.mPath.addArc(this.mOval, -3.0f * degArc, 8.0f * degArc);
        canvas.drawTextOnPath("ALLEGRO", this.mPath, 0.0f, (float) (this.mTextBounds.height() + textOffset), this.mTextPaint);
        this.mTextPaint.setTextAlign(Align.LEFT);
        this.mPath.reset();
        this.mPath.addArc(this.mOval, 9.0f * degArc, -4.0f * degArc);
        canvas.drawTextOnPath("PRESTISSIMO", this.mPath, 0.0f, (float) (-textOffset), this.mTextPaint);
        setDrawingCacheEnabled(true);
    }

    double snapWithRad(double rad, boolean playSound) {
        int numIndex;
        double finalDeg = (double) MiscUtility.radian2Degree((float) rad);
        int aPara = (int) (Math.abs(finalDeg) / 360.0d);
        if (finalDeg > 360.0d) {
            finalDeg -= (double) (aPara * 360);
        }
        if (finalDeg < 0.0d) {
            finalDeg += (double) ((aPara + 1) * 360);
        }
        double subDeg = finalDeg - ((double) (((int) (finalDeg / 9.0d)) * 9));
        int BPMreturn = 0;
        int intDeg = (int) (finalDeg - ((double) ((int) finalDeg)));
        if (finalDeg >= 189.0d && finalDeg < 279.0d) {
            numIndex = (int) (((finalDeg - ((double) intDeg)) - 189.0d) / 9.0d);
            if (subDeg - 2.25d <= 0.0d) {
                finalDeg -= subDeg;
                BPMreturn = this.mTempoArray[numIndex];
            } else if (subDeg > 2.25d && subDeg <= 6.75d) {
                finalDeg = (finalDeg - subDeg) + 4.5d;
                BPMreturn = this.mTempoArray[numIndex] + 1;
            } else if (subDeg - 6.75d > 0.0d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = this.mTempoArray[numIndex + 1];
            }
        }
        if (finalDeg >= 342.0d && finalDeg < 351.0d) {
            if (subDeg <= 2.25d) {
                finalDeg -= subDeg;
                BPMreturn = 84;
            } else if (subDeg > 2.25d && subDeg <= 6.75d) {
                finalDeg = (finalDeg - subDeg) + 4.5d;
                BPMreturn = 85;
            } else if (subDeg > 6.75d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = 86;
            }
        }
        if (finalDeg >= 279.0d && finalDeg < 315.0d) {
            numIndex = (int) (((finalDeg - ((double) intDeg)) - 189.0d) / 9.0d);
            if (subDeg <= SeismometerGraphView.MAX_ACCELERATION) {
                finalDeg -= subDeg;
                BPMreturn = this.mTempoArray[numIndex];
            } else if (subDeg > SeismometerGraphView.MAX_ACCELERATION && subDeg <= 4.5d) {
                finalDeg = (finalDeg - subDeg) + 3.0d;
                BPMreturn = this.mTempoArray[numIndex] + 1;
            } else if (subDeg > 4.5d && subDeg <= 7.5d) {
                finalDeg = (finalDeg - subDeg) + 6.0d;
                BPMreturn = this.mTempoArray[numIndex] + 2;
            } else if (subDeg > 7.5d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = this.mTempoArray[numIndex + 1];
            }
        }
        if (finalDeg >= 315.0d && finalDeg < 342.0d) {
            numIndex = (int) (((finalDeg - ((double) intDeg)) - 189.0d) / 9.0d);
            if (subDeg <= 1.125d) {
                finalDeg -= subDeg;
                BPMreturn = this.mTempoArray[numIndex];
            } else if (subDeg > 1.125d && subDeg <= 3.375d) {
                finalDeg = (finalDeg - subDeg) + 2.25d;
                BPMreturn = this.mTempoArray[numIndex] + 1;
            } else if (subDeg > 3.375d && subDeg <= 5.625d) {
                finalDeg = (finalDeg - subDeg) + 4.5d;
                BPMreturn = this.mTempoArray[numIndex] + 2;
            } else if (subDeg > 5.625d && subDeg <= 7.875d) {
                finalDeg = (finalDeg - subDeg) + 6.75d;
                BPMreturn = this.mTempoArray[numIndex] + 3;
            } else if (subDeg > 7.875d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = this.mTempoArray[numIndex + 1];
            }
        }
        if (finalDeg >= 0.0d && finalDeg < 63.0d) {
            numIndex = (int) (((finalDeg - ((double) intDeg)) + 162.0d) / 9.0d);
            if (subDeg <= 1.125d) {
                finalDeg -= subDeg;
                BPMreturn = this.mTempoArray[numIndex];
            } else if (subDeg > 1.125d && subDeg <= 3.375d) {
                finalDeg = (finalDeg - subDeg) + 2.25d;
                BPMreturn = this.mTempoArray[numIndex] + 1;
            } else if (subDeg > 3.375d && subDeg <= 5.625d) {
                finalDeg = (finalDeg - subDeg) + 4.5d;
                BPMreturn = this.mTempoArray[numIndex] + 2;
            } else if (subDeg > 5.625d && subDeg <= 7.875d) {
                finalDeg = (finalDeg - subDeg) + 6.75d;
                BPMreturn = this.mTempoArray[numIndex] + 3;
            } else if (subDeg > 7.875d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = this.mTempoArray[numIndex + 1];
            }
        }
        if (finalDeg >= 351.0d && finalDeg < 360.0d) {
            if (subDeg <= 0.75d) {
                finalDeg -= subDeg;
                BPMreturn = 86;
            } else if (subDeg > 0.75d && subDeg <= 2.25d) {
                finalDeg = (finalDeg - subDeg) + SeismometerGraphView.MAX_ACCELERATION;
                BPMreturn = 87;
            } else if (subDeg > 2.25d && subDeg <= 3.75d) {
                finalDeg = (finalDeg - subDeg) + 3.0d;
                BPMreturn = 88;
            } else if (subDeg > 3.75d && subDeg <= 5.25d) {
                finalDeg = (finalDeg - subDeg) + 4.5d;
                BPMreturn = 89;
            } else if (subDeg > 5.25d && subDeg <= 6.75d) {
                finalDeg = (finalDeg - subDeg) + 6.0d;
                BPMreturn = 90;
            } else if (subDeg > 6.75d && subDeg <= 8.25d) {
                finalDeg = (finalDeg - subDeg) + 7.5d;
                BPMreturn = 91;
            } else if (subDeg > 8.25d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = 92;
            }
        }
        if (finalDeg >= 63.0d && finalDeg < 99.0d) {
            numIndex = (int) (((finalDeg - ((double) intDeg)) + 162.0d) / 9.0d);
            if (subDeg <= 0.75d) {
                finalDeg -= subDeg;
                BPMreturn = this.mTempoArray[numIndex];
            } else if (subDeg > 0.75d && subDeg <= 2.25d) {
                finalDeg = (finalDeg - subDeg) + SeismometerGraphView.MAX_ACCELERATION;
                BPMreturn = this.mTempoArray[numIndex] + 1;
            } else if (subDeg > 2.25d && subDeg <= 3.75d) {
                finalDeg = (finalDeg - subDeg) + 3.0d;
                BPMreturn = this.mTempoArray[numIndex] + 2;
            } else if (subDeg > 3.75d && subDeg <= 5.25d) {
                finalDeg = (finalDeg - subDeg) + 4.5d;
                BPMreturn = this.mTempoArray[numIndex] + 3;
            } else if (subDeg > 5.25d && subDeg <= 6.75d) {
                finalDeg = (finalDeg - subDeg) + 6.0d;
                BPMreturn = this.mTempoArray[numIndex] + 4;
            } else if (subDeg > 6.75d && subDeg <= 8.25d) {
                finalDeg = (finalDeg - subDeg) + 7.5d;
                BPMreturn = this.mTempoArray[numIndex] + 5;
            } else if (subDeg > 8.25d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = this.mTempoArray[numIndex + 1];
            }
        }
        if (finalDeg >= 99.0d && finalDeg <= 171.0d) {
            numIndex = (int) (((finalDeg - ((double) intDeg)) + 162.0d) / 9.0d);
            if (subDeg <= 0.5625d) {
                finalDeg -= subDeg;
                BPMreturn = this.mTempoArray[numIndex];
            } else if (subDeg > 0.5625d && subDeg <= 1.6875d) {
                finalDeg = (finalDeg - subDeg) + 1.125d;
                BPMreturn = this.mTempoArray[numIndex] + 1;
            } else if (subDeg > 1.6875d && subDeg <= 2.8125d) {
                finalDeg = (finalDeg - subDeg) + 2.25d;
                BPMreturn = this.mTempoArray[numIndex] + 2;
            } else if (subDeg > 2.8125d && subDeg <= 3.9375d) {
                finalDeg = (finalDeg - subDeg) + 3.375d;
                BPMreturn = this.mTempoArray[numIndex] + 3;
            } else if (subDeg > 3.9375d && subDeg <= 5.0625d) {
                finalDeg = (finalDeg - subDeg) + 4.5d;
                BPMreturn = this.mTempoArray[numIndex] + 4;
            } else if (subDeg > 5.0625d && subDeg <= 6.1875d) {
                finalDeg = (finalDeg - subDeg) + 5.625d;
                BPMreturn = this.mTempoArray[numIndex] + 5;
            } else if (subDeg > 6.1875d && subDeg <= 7.3125d) {
                finalDeg = (finalDeg - subDeg) + 6.75d;
                BPMreturn = this.mTempoArray[numIndex] + 6;
            } else if (subDeg > 7.3125d && subDeg <= 8.4375d) {
                finalDeg = (finalDeg - subDeg) + 7.875d;
                BPMreturn = this.mTempoArray[numIndex] + 7;
            } else if (subDeg > 8.4375d) {
                finalDeg += (9.0d - subDeg) - 0.02d;
                BPMreturn = this.mTempoArray[numIndex + 1];
            }
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
        editor.putInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, BPMreturn);
        editor.putFloat(SettingsKey.SETTINGS_METRONOME_LAST_WHEEL_ANGLE_KEY, MiscUtility.degree2Radian((float) finalDeg));
        editor.apply();
        if (this.mPreBPM != BPMreturn) {
            if (playSound) {
                this.mPreBPM = BPMreturn;
            } else {
                this.mPreBPM = BPMreturn;
            }
        }
        return (double) MiscUtility.degree2Radian((float) finalDeg);
    }

    double snapWithRad(double rad) {
        return snapWithRad(rad, PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true));
    }

    double getAngleFromBPM(int bpm) {
        double angleReturn = 0.0d;
        int i = 40;
        while (i <= 208) {
            if (i == bpm) {
                if (i < 60) {
                    angleReturn = 189.0d + (((double) (i - 40)) * 4.5d);
                } else if (i >= 84 && i < 86) {
                    angleReturn = 342.0d + (((double) (i - 84)) * 4.5d);
                } else if (i >= 60 && i < 72) {
                    angleReturn = (double) (((i - 60) * 3) + 279);
                } else if (i >= 72 && i < 84) {
                    angleReturn = 315.0d + (((double) (i - 72)) * 2.25d);
                } else if (i >= 92 && i < 120) {
                    angleReturn = ((double) (i - 92)) * 2.25d;
                } else if (i >= 86 && i < 92) {
                    angleReturn = 351.0d + (((double) (i - 86)) * SeismometerGraphView.MAX_ACCELERATION);
                } else if (i >= 120 && i < 144) {
                    angleReturn = 63.0d + (((double) (i - 120)) * SeismometerGraphView.MAX_ACCELERATION);
                } else if (i >= 144 && i <= 208) {
                    angleReturn = 99.0d + (((double) (i - 144)) * 1.125d);
                }
            }
            i++;
        }
        return angleReturn;
    }

    double getAngleFromPoint(Point point) {
        double x = ((double) point.x) - this.mCenterX;
        double y = -(((double) point.y) - this.mCenterY);
        double a = Math.atan(x / y);
        if (MiscUtility.getSign(x) > 0 && MiscUtility.getSign(y) < 0) {
            return a + 3.141592653589793d;
        }
        if (MiscUtility.getSign(x) < 0 && MiscUtility.getSign(y) < 0) {
            return a + 3.141592653589793d;
        }
        if (MiscUtility.getSign(x) >= 0 || MiscUtility.getSign(y) <= 0) {
            return a;
        }
        return a + 6.283185307179586d;
    }

    int getMoveType(Point curPoint, Point prePoint) {
        int type = 0;
        double curAngle = getAngleFromPoint(curPoint);
        double preAngle = this.mDownAngle;
        if (Math.abs(curAngle - preAngle) > 3.141592653589793d) {
            if (curAngle - preAngle < 0.0d) {
                type = 1;
            } else {
                type = -1;
            }
        }
        if (((double) prePoint.x) < this.mCenterX && ((double) curPoint.x) >= this.mCenterX && ((double) curPoint.y) < this.mCenterY) {
            this.mRotatedCircle++;
        } else if (((double) prePoint.x) >= this.mCenterX && ((double) curPoint.x) < this.mCenterX && ((double) curPoint.y) < this.mCenterY) {
            this.mRotatedCircle--;
        }
        return type;
    }

    public boolean onTouch(View v, MotionEvent event) {
        Point pt = new Point((int) event.getX(), (int) event.getY());
        if (event.getAction() == 0) {
            onTouchDown(v, pt);
        } else if (event.getAction() == 1) {
            onTouchUp(v, pt);
        } else if (event.getAction() == 2) {
            onTouchMove(v, pt);
        }
        return true;
    }

    protected void onTouchDown(View source, Point currentPosition) {
        this.mCanTouch = !MetronomeActivity.mIsLocked;
        if (this.mCanTouch) {
            this.mRotatedCircle = 0;
            this.mIsMoved = false;
            double startX = ((double) currentPosition.x) - this.mCenterX;
            double startY = ((double) currentPosition.y) - this.mCenterY;
            this.mStartAngle = Math.atan2(startY, startX) - this.mAngle;
            this.mDownAngle = getAngleFromPoint(currentPosition);
            this.mLastPreloadAngle = this.mAllAngle;
            int outerRadius = this.mWheelView.getWidth() / 2;
            int innerRadius = (int) TypedValue.applyDimension(1, 50.0f, getResources().getDisplayMetrics());
            double radius = Math.sqrt((startX * startX) + (startY * startY));
            if (radius > ((double) outerRadius) || radius < ((double) innerRadius)) {
                this.mCanTouch = false;
            } else {
                this.mCanTouch = true;
            }
            this.mPrevPoint = currentPosition;
        }
    }

    protected void onTouchMove(View source, Point currentPosition) {
        if (this.mCanTouch) {
            this.mIsMoved = true;
            this.mAngle = Math.atan2(((double) currentPosition.y) - this.mCenterY, ((double) currentPosition.x) - this.mCenterX) - this.mStartAngle;
            this.mMoveAngle = getAngleFromPoint(currentPosition);
            if (getMoveType(currentPosition, this.mPrevPoint) != 0) {
                this.mMoveAngle += (((double) this.mRotatedCircle) * 3.141592653589793d) * 2.0d;
            }
            double offsetAngle = this.mMoveAngle - this.mDownAngle;
            double curDeg = (double) MiscUtility.radian2Degree((float) this.mAllAngle);
            int countCircle = (int) (Math.abs(curDeg) / 360.0d);
            if (curDeg > 360.0d) {
                curDeg -= (double) (countCircle * 360);
            }
            if (curDeg < 0.0d) {
                curDeg += (double) ((countCircle + 1) * 360);
            }
            if (curDeg <= 171.0d && offsetAngle > 0.0d && ((double) MiscUtility.radian2Degree((float) offsetAngle)) + curDeg >= 171.0d) {
                this.mAllAngle = (double) MiscUtility.degree2Radian(171.0f);
            } else if (curDeg < 189.0d || offsetAngle >= 0.0d || ((double) MiscUtility.radian2Degree((float) offsetAngle)) + curDeg > 189.0d) {
                this.mAllAngle += offsetAngle;
            } else {
                this.mAllAngle = (double) MiscUtility.degree2Radian(189.0f);
            }
            this.mDownAngle = this.mMoveAngle;
            ViewHelper.setRotation(this.mWheelView, MiscUtility.radian2Degree((float) snapWithRad(this.mAllAngle)));
            if (this.mListener != null) {
                this.mListener.onBPMValueChanged();
            }
            this.mPrevPoint = currentPosition;
        }
    }

    protected void onTouchUp(View source, Point currentPosition) {
        if (this.mListener != null && this.mIsMoved) {
            this.mListener.onBPMValueChangedEnd();
        }
    }

    public void updateWheelViewPosition() {
        float angleInRad = MiscUtility.degree2Radian((float) getAngleFromBPM(PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(SettingsKey.SETTINGS_METRONOME_BPM_KEY, 92)));
        this.mLastAngle = this.mAllAngle;
        this.mAllAngle = (double) angleInRad;
        this.mStartAngle = (double) angleInRad;
        this.mAngle = (double) angleInRad;
        ViewHelper.setRotation(this.mWheelView, MiscUtility.radian2Degree((float) snapWithRad(this.mAllAngle)));
    }

    public interface OnWheelViewEventListener {
        void onBPMValueChanged();

        void onBPMValueChangedEnd();
    }
}
