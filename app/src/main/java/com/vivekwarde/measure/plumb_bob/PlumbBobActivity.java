package com.vivekwarde.measure.plumb_bob;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.AccelerometerUtility;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;
import com.vivekwarde.measure.vendors.Matrix;

import java.util.Locale;

public class PlumbBobActivity extends BaseActivity implements OnClickListener, SensorEventListener {
    Matrix mCalibTransformMatrix;
    private ImageView mBoardImage;
    private ImageView mBobImage;
    private int mBobSoundPeriod;
    private Handler mBobSoundTimerHandler;
    private Runnable mBobSoundTimerTask;
    private float mBobSoundVolume;
    private ImageView mBobStringImage;
    private double mCalibAngleOxy;
    private double mCalibAngleOzx;
    private double mCalibAngleOzy;
    private SPImageButton mCalibrationButton;
    private ImageView mDockImage;
    private ImageView mDockIndicatorImage;
    private float mDockRedDotCurRotation;
    private float mDockRedDotCurScale;
    private ImageView mDockRedDotImage;
    private double mFilteringFactor;
    private ImageView mFloorImage;
    private double mGravityX;
    private double mGravityY;
    private double mGravityZ;
    private TextView mLabelXText;
    private TextView mLabelYText;
    private ImageView mLampImage;
    private ImageView mLampLightImage;
    private ImageView mLedScreenImage;
    private SPImageButton mLockButton;
    private SPImageButton mMenuButton;
    private TextView mOutputUnitXText;
    private TextView mOutputUnitYText;
    private TextView mOutputXText;
    private TextView mOutputYText;
    private SensorManager mSensorManager;
    private SPImageButton mSettingsButton;
    private ImageView mWallIndicatorImage;
    private int mWallRedDotCurPosX;
    private int mWallRedDotCurPosY;
    private ImageView mWallRedDotImage;

    public PlumbBobActivity() {
        this.mMenuButton = null;
        this.mSettingsButton = null;
        this.mLockButton = null;
        this.mCalibrationButton = null;
        this.mFloorImage = null;
        this.mBoardImage = null;
        this.mLampImage = null;
        this.mLampLightImage = null;
        this.mWallIndicatorImage = null;
        this.mWallRedDotImage = null;
        this.mDockImage = null;
        this.mDockIndicatorImage = null;
        this.mDockRedDotImage = null;
        this.mBobStringImage = null;
        this.mBobImage = null;
        this.mLedScreenImage = null;
        this.mBobSoundPeriod = GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE;
        this.mBobSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE;
        this.mCalibTransformMatrix = Matrix.identity(4);
        this.mCalibAngleOxy = 0.0d;
        this.mCalibAngleOzy = 0.0d;
        this.mCalibAngleOzx = 0.0d;
        this.mGravityX = 0.0d;
        this.mGravityY = 0.0d;
        this.mGravityZ = 0.0d;
        this.mFilteringFactor = 0.30000001192092896d;
        this.mWallRedDotCurPosX = 0;
        this.mWallRedDotCurPosY = 0;
        this.mDockRedDotCurScale = TextTrackStyle.DEFAULT_FONT_SCALE;
        this.mDockRedDotCurRotation = 0.0f;
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mBobSoundTimerHandler = new Handler();
        this.mBobSoundTimerTask = new Runnable() {
            public void run() {
                boolean isLocked = PreferenceManager.getDefaultSharedPreferences(PlumbBobActivity.this).getBoolean(SettingsKey.SETTINGS_PLUMB_BOB_LOCK_KEY, false);
                boolean soundOn = PreferenceManager.getDefaultSharedPreferences(PlumbBobActivity.this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true);
                if (!isLocked && soundOn) {
                    SoundUtility.getInstance().playSound(1, PlumbBobActivity.this.mBobSoundVolume);
                }
                PlumbBobActivity.this.mBobSoundTimerHandler.postDelayed(this, (long) PlumbBobActivity.this.mBobSoundPeriod);
            }
        };
        createUi();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    @SuppressLint({"NewApi"})
    private void createUi() {
        this.mMainLayout = new RelativeLayout(this);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_wall_dark)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                PlumbBobActivity.this.arrangeLayout();
                if (VERSION.SDK_INT >= 16) {
                    PlumbBobActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    PlumbBobActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mMainLayout.addView(this.mUiLayout);
        this.mFloorImage = new ImageView(this);
        this.mFloorImage.setId(5);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_floor)).getBitmap();
        this.mFloorImage.setImageBitmap(ImageUtility.scale9Bitmap(bitmap, getResources().getDisplayMetrics().widthPixels, bitmap.getHeight() - 2));
        this.mFloorImage.setScaleType(ScaleType.CENTER);
        this.mUiLayout.addView(this.mFloorImage);
        this.mBoardImage = new ImageView(this);
        this.mBoardImage.setId(6);
        this.mBoardImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_board)).getBitmap());
        this.mUiLayout.addView(this.mBoardImage);
        this.mWallIndicatorImage = new ImageView(this);
        this.mWallIndicatorImage.setId(9);
        this.mWallIndicatorImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_wall_indicator)).getBitmap());
        this.mUiLayout.addView(this.mWallIndicatorImage);
        this.mWallRedDotImage = new ImageView(this);
        this.mWallRedDotImage.setId(10);
        this.mWallRedDotImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_wall_red_dot)).getBitmap());
        this.mWallRedDotImage.setScaleType(ScaleType.CENTER);
        this.mUiLayout.addView(this.mWallRedDotImage);
        this.mDockImage = new ImageView(this);
        this.mDockImage.setId(11);
        this.mDockImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_dock)).getBitmap());
        this.mDockImage.setScaleType(ScaleType.CENTER);
        this.mUiLayout.addView(this.mDockImage);
        this.mDockIndicatorImage = new ImageView(this);
        this.mDockIndicatorImage.setId(12);
        this.mDockIndicatorImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_dock_indicator)).getBitmap());
        this.mUiLayout.addView(this.mDockIndicatorImage);
        this.mDockRedDotImage = new ImageView(this);
        this.mDockRedDotImage.setId(13);
        this.mDockRedDotImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_dock_red_dot)).getBitmap());
        this.mDockRedDotImage.setScaleType(ScaleType.CENTER);
        this.mUiLayout.addView(this.mDockRedDotImage);
        this.mLampImage = new ImageView(this);
        this.mLampImage.setId(7);
        this.mLampImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_lamp)).getBitmap());
        this.mLampImage.setOnClickListener(this);
        this.mUiLayout.addView(this.mLampImage);
        this.mLampLightImage = new ImageView(this);
        this.mLampLightImage.setId(8);
        this.mLampLightImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_lamp_light)).getBitmap());
        this.mUiLayout.addView(this.mLampLightImage);
        this.mBobStringImage = new ImageView(this);
        this.mBobStringImage.setId(14);
        this.mBobStringImage.setScaleType(ScaleType.CENTER);
        tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_string)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mBobStringImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mBobStringImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mBobStringImage);
        this.mBobImage = new ImageView(this);
        this.mBobImage.setId(15);
        this.mBobImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.plumb_bob_bob)).getBitmap());
        this.mUiLayout.addView(this.mBobImage);
        this.mLedScreenImage = new ImageView(this);
        this.mLedScreenImage.setId(16);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        this.mLedScreenImage.setImageBitmap(ImageUtility.scale9Bitmap(bitmap, (int) (((float) bitmap.getWidth()) + (20.0f * getResources().getDisplayMetrics().density)), (bitmap.getHeight() - 2) * 2));
        this.mUiLayout.addView(this.mLedScreenImage);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mLabelXText = new TextView(this);
        this.mLabelXText.setId(17);
        this.mLabelXText.setTypeface(face);
        this.mLabelXText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_LABEL_FONT_SIZE));
        this.mLabelXText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelXText.setPaintFlags(this.mLabelXText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mLabelXText);
        this.mLabelXText.setText("X:");
        this.mOutputXText = new TextView(this);
        this.mOutputXText.setId(18);
        this.mOutputXText.setTypeface(face);
        this.mOutputXText.setMaxLines(1);
        this.mOutputXText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE));
        this.mOutputXText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputXText.setPaintFlags(this.mOutputXText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputXText);
        this.mOutputXText.setText("20");
        this.mOutputUnitXText = new TextView(this);
        this.mOutputUnitXText.setId(19);
        this.mOutputUnitXText.setTypeface(face);
        this.mOutputUnitXText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE));
        this.mOutputUnitXText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputUnitXText.setPaintFlags(this.mOutputXText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputUnitXText);
        this.mLabelYText = new TextView(this);
        this.mLabelYText.setId(20);
        this.mLabelYText.setTypeface(face);
        this.mLabelYText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_LABEL_FONT_SIZE));
        this.mLabelYText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelYText.setPaintFlags(this.mLabelYText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mLabelYText);
        this.mLabelYText.setText("Y:");
        this.mOutputYText = new TextView(this);
        this.mOutputYText.setId(21);
        this.mOutputYText.setTypeface(face);
        this.mOutputYText.setMaxLines(1);
        this.mOutputYText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE));
        this.mOutputYText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputYText.setPaintFlags(this.mOutputYText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputYText);
        this.mOutputYText.setText("0");
        this.mOutputUnitYText = new TextView(this);
        this.mOutputUnitYText.setId(22);
        this.mOutputUnitYText.setTypeface(face);
        this.mOutputUnitYText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE));
        this.mOutputUnitYText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputUnitYText.setPaintFlags(this.mOutputYText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputUnitYText);
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PLUMB_BOB_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.mOutputUnitXText.setText("~");
            this.mOutputUnitYText.setText("~");
        } else if (unit.equals("1")) {
            this.mOutputUnitXText.setText("rad");
            this.mOutputUnitYText.setText("rad");
        } else if (unit.equals("2")) {
            this.mOutputUnitXText.setText("gra");
            this.mOutputUnitYText.setText("gra");
        } else {
            this.mOutputUnitXText.setText("%");
            this.mOutputUnitYText.setText("%");
        }
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu_dark)).getBitmap());
        this.mMenuButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mMenuButton);
        this.mSettingsButton = new SPImageButton(this);
        this.mSettingsButton.setId(2);
        this.mSettingsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info_dark)).getBitmap());
        this.mSettingsButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mSettingsButton);
        this.mCalibrationButton = new SPImageButton(this);
        this.mCalibrationButton.setId(4);
        this.mCalibrationButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_calibration_dark)).getBitmap());
        this.mCalibrationButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mCalibrationButton);
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PLUMB_BOB_LOCK_KEY, false);
        this.mLockButton = new SPImageButton(this);
        this.mLockButton.setId(3);
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(isLocked ? R.drawable.button_lock_dark : R.drawable.button_unlock_dark)).getBitmap());
        this.mLockButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mLockButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads_dark)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
    }

    private void arrangeLayout() {
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(12);
        this.mFloorImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(2, this.mFloorImage.getId());
        params.bottomMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_MARGIN_BTW_BOTTOM_BOARD_AND_TOP_FLOOR);
        this.mBoardImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mBoardImage.getId());
        params.addRule(6, this.mFloorImage.getId());
        params.leftMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_HORZ_MARGIN_BTW_LEFT_LAMP_AND_LEFT_BOARD);
        params.topMargin = (this.mFloorImage.getHeight() - this.mLampImage.getHeight()) / 2;
        this.mLampImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mLampImage.getId());
        params.addRule(8, this.mLampImage.getId());
        this.mLampLightImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(7, this.mLampLightImage.getId());
        params.addRule(6, this.mLampLightImage.getId());
        this.mWallIndicatorImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mWallIndicatorImage.getId());
        params.topMargin = (this.mWallIndicatorImage.getHeight() - this.mWallRedDotImage.getHeight()) / 2;
        params.addRule(5, this.mWallIndicatorImage.getId());
        params.leftMargin = (this.mWallIndicatorImage.getWidth() - this.mWallRedDotImage.getWidth()) / 2;
        this.mWallRedDotImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(8, this.mFloorImage.getId());
        params.bottomMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_MARGIN_BTW_BOTTOM_DOCK_AND_BOTTOM_FLOOR);
        this.mDockImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(6, this.mDockImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_MARGIN_BTW_TOP_DOCK_INDICATOR_AND_TOP_DOCK);
        this.mDockIndicatorImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mDockIndicatorImage.getId());
        params.topMargin = ((this.mDockIndicatorImage.getHeight() - this.mDockRedDotImage.getHeight()) / 2) - ((int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_OFFSET_OF_PERSPECTIVE_ORIGIN));
        params.addRule(5, this.mDockIndicatorImage.getId());
        params.leftMargin = (this.mDockIndicatorImage.getWidth() - this.mDockRedDotImage.getWidth()) / 2;
        this.mDockRedDotImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(14);
        params.addRule(2, this.mDockRedDotImage.getId());
        params.bottomMargin = ((int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_DISTANCE_BTW_BOTTOM_BOB_AND_TOP_DOCK_REDDOT)) - ((LayoutParams) this.mDockRedDotImage.getLayoutParams()).topMargin;
        this.mBobImage.setLayoutParams(params);
        params = new LayoutParams(-2, mScreenHeight);
        params.addRule(13);
        params.addRule(2, this.mBobImage.getId());
        this.mBobStringImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(9);
        params.addRule(10);
        params.leftMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_HORZ_MARGIN_BTW_LEFT_LEDSCREEN_AND_LEFT_SCREEN);
        params.topMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_MARGIN_BTW_TOP_LEDSCREEN_AND_TOP_SCREEN);
        this.mLedScreenImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mLedScreenImage.getId());
        params.addRule(4, this.mOutputXText.getId());
        params.leftMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_HORZ_MARGIN_BTW_LEFT_LEDSCREEN_AND_LEFT_OUTPUT_LABEL);
        this.mLabelXText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(7, this.mLedScreenImage.getId());
        params.addRule(4, this.mOutputXText.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_HORZ_MARGIN_BTW_UNIT_AND_LEDSCREEN);
        this.mOutputUnitXText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(0, this.mOutputUnitXText.getId());
        params.addRule(6, this.mLedScreenImage.getId());
        params.topMargin = ((this.mLedScreenImage.getHeight() - (this.mOutputXText.getHeight() * 2)) - ((int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_MARGIN_BTW_OUTPUT_X_AND_OUTPUT_Y))) / 2;
        params.rightMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_HORZ_MARGIN_BTW_OUTPUT_AND_UNIT);
        this.mOutputXText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mLabelXText.getId());
        params.addRule(4, this.mOutputYText.getId());
        this.mLabelYText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(7, this.mLedScreenImage.getId());
        params.addRule(4, this.mOutputYText.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_HORZ_MARGIN_BTW_UNIT_AND_LEDSCREEN);
        this.mOutputUnitYText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(0, this.mOutputUnitYText.getId());
        params.addRule(3, this.mOutputXText.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_MARGIN_BTW_OUTPUT_X_AND_OUTPUT_Y);
        params.rightMargin = (int) getResources().getDimension(R.dimen.PLUMB_BOB_HORZ_MARGIN_BTW_OUTPUT_AND_UNIT);
        this.mOutputYText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(11);
        params.topMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / 2;
        this.mMenuButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mMenuButton.getId());
        params.addRule(0, this.mMenuButton.getId());
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / 2;
        this.mSettingsButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mSettingsButton.getId());
        params.addRule(0, this.mSettingsButton.getId());
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / 2;
        this.mLockButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLockButton.getId());
        params.addRule(0, this.mLockButton.getId());
        params.leftMargin = ((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS)) / 2;
        this.mCalibrationButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(7, this.mMenuButton.getId());
        params.addRule(3, this.mMenuButton.getId());
        this.mNoAdsButton.setLayoutParams(params);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PLUMB_BOB_LOCK_KEY, false)) {
            this.mSensorManager.unregisterListener(this);
            this.mBobSoundTimerHandler.removeCallbacks(this.mBobSoundTimerTask);
        }
    }

    protected void onResume() {
        super.onResume();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PLUMB_BOB_LOCK_KEY, false)) {
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBobSoundTimerHandler.removeCallbacks(this.mBobSoundTimerTask);
            this.mBobSoundTimerHandler.postDelayed(this.mBobSoundTimerTask, (long) this.mBobSoundPeriod);
        }
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PLUMB_BOB_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.mOutputUnitXText.setText("~");
            this.mOutputUnitYText.setText("~");
        } else if (unit.equals("1")) {
            this.mOutputUnitXText.setText("rad");
            this.mOutputUnitYText.setText("rad");
        } else if (unit.equals("2")) {
            this.mOutputUnitXText.setText("gra");
            this.mOutputUnitYText.setText("gra");
        } else {
            this.mOutputUnitXText.setText("%");
            this.mOutputUnitYText.setText("%");
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                this.mCalibTransformMatrix.setData(i, j, (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(String.format(Locale.getDefault(), SettingsKey.SETTINGS_PLUMB_BOB_CALIB_MATRIX_KEY, new Object[]{Integer.valueOf(i), Integer.valueOf(j)}), 0.0f));
            }
        }
        this.mCalibAngleOxy = (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_PLUMB_BOB_CALIB_ANGLE_OXY, 0.0f);
        this.mCalibAngleOzy = (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZY, 0.0f);
        this.mCalibAngleOzx = (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZX, 0.0f);
    }

    public void onClick(View source) {
        if (source.getId() == 1) {
            onButtonMenu();
        } else if (source.getId() == 2) {
            onButtonSettings();
        } else if (source.getId() == 3) {
            onButtonLock();
        } else if (source.getId() == 4) {
            onButtonCalibration();
        } else if (source.getId() == 7) {
            onButtonLamp();
        } else if (source.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    @SuppressLint({"NewApi"})
    private void onButtonLamp() {
        int i = MotionEventCompat.ACTION_MASK;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mLampLightImage.getVisibility() == 0) {
            this.mLampLightImage.setVisibility(4);
            this.mWallIndicatorImage.setVisibility(4);
        } else {
            this.mLampLightImage.setVisibility(0);
            this.mWallIndicatorImage.setVisibility(0);
        }
        if (VERSION.SDK_INT >= 16) {
            ImageView imageView = this.mWallRedDotImage;
            if (this.mLampLightImage.getVisibility() != 0) {
                i = 0;
            }
            imageView.setImageAlpha(i);
            return;
        }
        ImageView imageView = this.mWallRedDotImage;
        if (this.mLampLightImage.getVisibility() != 0) {
            i = 0;
        }
        imageView.setAlpha(i);
    }

    private void onButtonLock() {
        boolean z;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(2, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PLUMB_BOB_LOCK_KEY, false);
        if (isLocked) {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_unlock_dark)).getBitmap());
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBobSoundTimerHandler.postDelayed(this.mBobSoundTimerTask, (long) this.mBobSoundPeriod);
        } else {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_lock_dark)).getBitmap());
            this.mSensorManager.unregisterListener(this);
            this.mBobSoundTimerHandler.removeCallbacks(this.mBobSoundTimerTask);
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        String str = SettingsKey.SETTINGS_PLUMB_BOB_LOCK_KEY;
        if (isLocked) {
            z = false;
        } else {
            z = true;
        }
        editor.putBoolean(str, z);
        editor.apply();
    }

    private void onButtonCalibration() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            double y = (double) (-event.values[1]);
            double z = (double) (-event.values[2]);
            this.mGravityX = (this.mFilteringFactor * ((double) (-event.values[0]))) + (this.mGravityX * (1.0d - this.mFilteringFactor));
            this.mGravityY = (this.mFilteringFactor * y) + (this.mGravityY * (1.0d - this.mFilteringFactor));
            this.mGravityZ = (this.mFilteringFactor * z) + (this.mGravityZ * (1.0d - this.mFilteringFactor));
            updateBobPosition();
        }
    }

    @SuppressLint({"DefaultLocale"})
    private void updateBobPosition() {
        double alpha;
        double dy;
        double[][] filteredAccelerationData = new double[1][];
        filteredAccelerationData[0] = new double[]{this.mGravityX, this.mGravityY, this.mGravityZ, 1.0d};
        Matrix matrix = new Matrix(filteredAccelerationData);
        matrix.times(this.mCalibTransformMatrix);
        double accx = matrix.getData(0, 0) / matrix.getData(0, 3);
        double accy = matrix.getData(0, 2) / matrix.getData(0, 3);
        float sensitivity = ((float) PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_PLUMB_BOB_SENSITIVITY_KEY, 80)) / 100.0f;
        int wallIndicatorRadius = (this.mWallIndicatorImage.getWidth() / 2) - ((this.mWallRedDotImage.getWidth() / 2) / 2);
        double offx = (((double) wallIndicatorRadius) * accx) / Math.sin((double) sensitivity);
        double offy = (((double) wallIndicatorRadius) * accy) / Math.sin((double) sensitivity);
        double diag = Math.sqrt((offx * offx) + (offy * offy));
        if (diag > ((double) wallIndicatorRadius)) {
            alpha = Math.atan2(offx, offy);
            offx = ((double) (MiscUtility.getSign(offx) * wallIndicatorRadius)) * Math.abs(Math.sin(alpha));
            offy = ((double) (MiscUtility.getSign(offy) * wallIndicatorRadius)) * Math.abs(Math.cos(alpha));
        }
        Animation translateAnimation = new TranslateAnimation((float) this.mWallRedDotCurPosX, (float) ((int) offx), (float) this.mWallRedDotCurPosY, (float) ((int) offy));
        translateAnimation.setDuration(100);
        translateAnimation.setFillAfter(true);
        this.mWallRedDotImage.startAnimation(translateAnimation);
        this.mWallRedDotCurPosX = (int) offx;
        this.mWallRedDotCurPosY = (int) offy;
        double offset = (double) getResources().getDimension(R.dimen.PLUMB_BOB_VERT_OFFSET_OF_PERSPECTIVE_ORIGIN);
        double rynho = ((double) (((float) this.mDockIndicatorImage.getHeight()) / 2.0f)) - offset;
        double ryto = ((double) this.mDockIndicatorImage.getHeight()) - rynho;
        double rx = ((double) (((float) this.mDockIndicatorImage.getWidth()) / 2.0f)) / Math.sin((double) sensitivity);
        double ry = ((double) (((float) this.mDockIndicatorImage.getHeight()) / 2.0f)) / Math.sin((double) sensitivity);
        rynho /= Math.sin((double) sensitivity);
        ryto /= Math.sin((double) sensitivity);
        int[] dockRedDotLocation = new int[2];
        this.mDockRedDotImage.getLocationInWindow(dockRedDotLocation);
        double H = (double) (dockRedDotLocation[1] + this.mDockRedDotImage.getHeight());
        double dx = (-accx) * rx;
        if (accy < 0.0d) {
            dy = ((accy * rynho) * rynho) / ry;
        } else {
            dy = ((accy * ryto) * ryto) / ry;
        }
        alpha = Math.atan2(dx, H + dy);
        double l = dx / Math.sin(alpha);
        dx = l * Math.sin(alpha);
        dy = ((double) MiscUtility.getSign(dy)) * Math.abs(H - (Math.cos(alpha) * l));
        rx = (double) (((float) this.mDockIndicatorImage.getWidth()) / 2.0f);
        ry = (double) (((float) this.mDockIndicatorImage.getHeight()) / 2.0f);
        double t = dy / dx;
        if (!MiscUtility.isInsideEllipse(0, 0, ((int) rx) * 2, ((int) ry) * 2, (int) dx, (int) (dy - offset))) {
            dx = (((double) MiscUtility.getSign(dx)) * (rx * ry)) / Math.sqrt((ry * ry) + (((rx * rx) * t) * t));
            dy = (dx * t) + offset;
            alpha = Math.atan2(dx, H + dy);
            l = Math.sqrt((dx * dx) + ((H + dy) * (H + dy)));
        }
        float rotationAngle = MiscUtility.radian2Degree((float) alpha);
        float scale = (float) (l / H);
        RotateAnimation stringRotateAnim = new RotateAnimation(this.mDockRedDotCurRotation, rotationAngle, 0, (float) (this.mBobStringImage.getWidth() / 2), 0, 0.0f);
        ScaleAnimation stringScaleAnim = new ScaleAnimation(this.mDockRedDotCurScale, scale, this.mDockRedDotCurScale, scale, 0, (float) (this.mBobStringImage.getWidth() / 2), 0, 0.0f);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(stringRotateAnim);
        animationSet.addAnimation(stringScaleAnim);
        animationSet.setFillAfter(true);
        this.mBobStringImage.startAnimation(animationSet);
        int[] bobLocation = new int[2];
        this.mBobImage.getLocationOnScreen(bobLocation);
        int bobPivotY = -bobLocation[1];
        RotateAnimation bobRotateAnim = new RotateAnimation(this.mDockRedDotCurRotation, rotationAngle, 0, (float) (this.mBobImage.getWidth() / 2), 0, (float) bobPivotY);
        ScaleAnimation bobScaleAnim = new ScaleAnimation(this.mDockRedDotCurScale, scale, this.mDockRedDotCurScale, scale, 0, (float) (this.mBobImage.getWidth() / 2), 0, (float) bobPivotY);
        animationSet = new AnimationSet(true);
        animationSet.addAnimation(bobRotateAnim);
        animationSet.addAnimation(bobScaleAnim);
        animationSet.setFillAfter(true);
        this.mBobImage.startAnimation(animationSet);
        int dockRedDotPivotY = -dockRedDotLocation[1];
        RotateAnimation dockRedDotRotateAnim = new RotateAnimation(this.mDockRedDotCurRotation, rotationAngle, 0, (float) (this.mDockRedDotImage.getWidth() / 2), 0, (float) dockRedDotPivotY);
        ScaleAnimation dockRedDotScaleAnim = new ScaleAnimation(this.mDockRedDotCurScale, scale, this.mDockRedDotCurScale, scale, 0, (float) (this.mDockRedDotImage.getWidth() / 2), 0, (float) dockRedDotPivotY);
        animationSet = new AnimationSet(true);
        animationSet.addAnimation(dockRedDotRotateAnim);
        animationSet.addAnimation(dockRedDotScaleAnim);
        animationSet.setFillAfter(true);
        this.mDockRedDotImage.startAnimation(animationSet);
        this.mDockRedDotCurRotation = rotationAngle;
        this.mDockRedDotCurScale = scale;
        double angleOxy = Math.atan2(this.mGravityY, this.mGravityX);
        double angleOzy = Math.atan2(this.mGravityZ, this.mGravityY);
        double angleOzx = Math.atan2(this.mGravityZ, this.mGravityX);
        angleOxy = AccelerometerUtility.validateAngle(angleOxy);
        angleOzy = AccelerometerUtility.validateAngle(angleOzy);
        angleOzx = AccelerometerUtility.validateAngle(angleOzx);
        angleOzy = AccelerometerUtility.rotateArbitraryAngle(angleOzy, 1.5707963267948966d);
        double calibAngleOxy = AccelerometerUtility.rotateArbitraryAngle(angleOxy, this.mCalibAngleOxy);
        double calibAngleOzy = AccelerometerUtility.rotateArbitraryAngle(angleOzy, this.mCalibAngleOzy);
        double calibAngleOzx = AccelerometerUtility.rotateArbitraryAngle(angleOzx, this.mCalibAngleOzx);
        calibAngleOxy = -AccelerometerUtility.to180(calibAngleOxy);
        calibAngleOzy = AccelerometerUtility.to180(calibAngleOzy);
        calibAngleOzx = -AccelerometerUtility.to180(calibAngleOzx);
        double outputAngleX = 0.0d;
        double outputAngleY = 0.0d;
        int orientation = ((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation();
        if (orientation == 0 || orientation == 2) {
            outputAngleX = calibAngleOxy;
            outputAngleY = calibAngleOzy;
        } else if (orientation == 1 || orientation == 3) {
            outputAngleX = calibAngleOxy;
            outputAngleY = calibAngleOzx;
        }
        float convertedUnitAngleX = (float) outputAngleX;
        float convertedUnitAngleY = (float) outputAngleY;
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PLUMB_BOB_UNIT_KEY, "0");
        if (unit.equals("0")) {
            convertedUnitAngleX = MiscUtility.radian2Degree(convertedUnitAngleX);
            convertedUnitAngleY = MiscUtility.radian2Degree(convertedUnitAngleY);
        } else {
            if (unit.equals("2")) {
                convertedUnitAngleX = MiscUtility.radian2Gradian(convertedUnitAngleX);
                convertedUnitAngleY = MiscUtility.radian2Gradian(convertedUnitAngleY);
            } else {
                if (unit.equals("3")) {
                    convertedUnitAngleX = MiscUtility.radian2Slope(convertedUnitAngleX);
                    convertedUnitAngleY = MiscUtility.radian2Slope(convertedUnitAngleY);
                }
            }
        }
        String outputX = String.format(Locale.getDefault(), "%.1f", new Object[]{Float.valueOf(convertedUnitAngleX)});
        String outputY = String.format(Locale.getDefault(), "%.1f", new Object[]{Float.valueOf(convertedUnitAngleY)});
        this.mOutputXText.setText(outputX);
        this.mOutputYText.setText(outputY);
        float distance = diag < ((double) wallIndicatorRadius) ? ((float) diag) / ((float) wallIndicatorRadius) : TextTrackStyle.DEFAULT_FONT_SCALE;
        ViewHelper.setAlpha(this.mDockIndicatorImage, TextTrackStyle.DEFAULT_FONT_SCALE - distance);
        this.mBobSoundPeriod = (int) (400.0f + (1600.0f * distance));
        this.mBobSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE - distance > 0.5f ? 0.5f : TextTrackStyle.DEFAULT_FONT_SCALE - distance;
    }

    public class ConstantId {
        public static final int TIMER_PERIOD_MAX = 2000;
        public static final int TIMER_PERIOD_MIN = 400;
    }

    public class ControlId {
        public static final int BOARD_IMAGE_ID = 6;
        public static final int BOB_IMAGE_ID = 15;
        public static final int BOB_STRING_IMAGE_ID = 14;
        public static final int CALIBRATION_BUTTON_ID = 4;
        public static final int DOCK_IMAGE_ID = 11;
        public static final int DOCK_INDICATOR_IMAGE_ID = 12;
        public static final int DOCK_RED_DOT_IMAGE_ID = 13;
        public static final int FLOOR_IMAGE_ID = 5;
        public static final int LABEL_X_TEXT_ID = 17;
        public static final int LABEL_Y_TEXT_ID = 20;
        public static final int LAMP_IMAGE_ID = 7;
        public static final int LAMP_LIGHT_IMAGE_ID = 8;
        public static final int LED_SCREEN_IMAGE_ID = 16;
        public static final int LOCK_BUTTON_ID = 3;
        public static final int MENU_BUTTON_ID = 1;
        public static final int OUTPUT_UNIT_X_TEXT_ID = 19;
        public static final int OUTPUT_UNIT_Y_TEXT_ID = 22;
        public static final int OUTPUT_X_TEXT_ID = 18;
        public static final int OUTPUT_Y_TEXT_ID = 21;
        public static final int SETTINGS_BUTTON_ID = 2;
        public static final int UPGRADE_BUTTON_ID = 0;
        public static final int WALL_INDICATOR_IMAGE_ID = 9;
        public static final int WALL_RED_DOT_IMAGE_ID = 10;
    }
}
