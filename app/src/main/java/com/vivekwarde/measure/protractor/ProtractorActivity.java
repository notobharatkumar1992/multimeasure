package com.vivekwarde.measure.protractor;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.LineView;
import com.vivekwarde.measure.custom_controls.NumpadView;
import com.vivekwarde.measure.custom_controls.NumpadView.NumpadViewEventListener;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.Locale;

public class ProtractorActivity extends BaseActivity implements NumpadViewEventListener, AnimationListener, OnClickListener, OnTouchListener, AnimatorListener {
    public static boolean mIsLocked;

    static {
        mIsLocked = false;
    }

    final String tag;
    protected float[] mOriginVertices;
    protected Matrix mTransformMatrix;
    protected float[] mTransformedVertices;
    float mCurrentAngle;
    Point mCurrentPoint;
    Point mDownPoint;
    int mEditIndex;
    ImageView mHandView;
    SPImageButton mInfoButton;
    boolean mIsMoved;
    boolean mIsVirginEdit;
    SPScale9ImageView mLEDScreen;
    TextView[] mLabelArray;
    float mLastAngle;
    LineView mLineView;
    SPImageButton mLockButton;
    SPImageButton mMenuButton;
    NumpadView mNumpadView;
    int mOx;
    int mOy;
    ProtractorView mProtractorView;
    SPImageButton mSwapButton;

    public ProtractorActivity() {
        this.tag = getClass().getSimpleName();
        this.mProtractorView = null;
        this.mNumpadView = null;
        this.mIsMoved = false;
        this.mDownPoint = null;
        this.mCurrentPoint = null;
        this.mCurrentAngle = 0.0f;
        this.mLastAngle = 0.0f;
        this.mOx = 0;
        this.mOy = 0;
        this.mMenuButton = null;
        this.mInfoButton = null;
        this.mLockButton = null;
        this.mSwapButton = null;
        this.mLEDScreen = null;
        this.mLabelArray = new TextView[]{null, null, null, null};
        this.mEditIndex = 0;
        this.mHandView = null;
        this.mLineView = null;
        this.mIsVirginEdit = true;
        this.mOriginVertices = new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        this.mTransformedVertices = new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        this.mTransformMatrix = new Matrix();
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(0);
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mOx = mScreenWidth / 2;
        this.mOy = 0;
        this.mLastAngle = PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_PROTRACTOR_LAST_ANGLE_KEY, 1.5707964f);
        this.mCurrentAngle = this.mLastAngle;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (VERSION.SDK_INT >= 16) {
                    ProtractorActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    ProtractorActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                ProtractorActivity.this.displayAngle(ProtractorActivity.this.mCurrentAngle);
                ProtractorActivity.this.rotateHandView(ProtractorActivity.this.mCurrentAngle, false);
                ProtractorActivity.this.updateHandViewBounds();
            }
        });
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        initSubviews();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        updateSettingsChanged();
    }

    protected void onPause() {
        super.onPause();
    }

    void updateSettingsChanged() {
        this.mLastAngle = PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_PROTRACTOR_LAST_ANGLE_KEY, 1.5707964f);
        this.mCurrentAngle = this.mLastAngle;
        displayAngle(this.mCurrentAngle);
        this.mProtractorView.invalidate();
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            onTouchDown(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 1) {
            onTouchUp(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 2) {
            onTouchMove(v, new Point((int) event.getX(), (int) event.getY()));
        }
        return true;
    }

    protected void onTouchDown(View source, Point currentPosition) {
        Point pt = currentPosition;
        this.mIsMoved = false;
        if (!mIsLocked) {
            this.mDownPoint = pt;
        }
    }

    protected void onTouchUp(View source, Point currentPosition) {
        Point pt = currentPosition;
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        if (this.mIsMoved) {
            if (!mIsLocked) {
                this.mCurrentAngle += calculateAngleFromDownPointAndCurrentPoint(this.mDownPoint, this.mCurrentPoint);
                if (this.mCurrentAngle < 0.0f) {
                    this.mCurrentAngle = 0.0f;
                }
                if (((double) this.mCurrentAngle) > 3.141592653589793d) {
                    this.mCurrentAngle = 3.1415927f;
                }
                editor.putFloat(SettingsKey.SETTINGS_PROTRACTOR_LAST_ANGLE_KEY, this.mCurrentAngle);
                editor.apply();
                updateHandViewBounds();
            }
        } else if (MiscUtility.isPointInPolygon((float) pt.x, (float) pt.y, this.mTransformedVertices, 4)) {
            boolean show = !PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PROTRACTOR_SHOW_HAND_LINE_KEY, false);
            editor.putBoolean(SettingsKey.SETTINGS_PROTRACTOR_SHOW_HAND_LINE_KEY, show);
            editor.apply();
            ViewHelper.setAlpha(this.mLineView, show ? TextTrackStyle.DEFAULT_FONT_SCALE : 0.0f);
            this.mUiLayout.invalidate();
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
        } else if (source.equals(this.mLabelArray[0])) {
            this.mEditIndex = 0;
            beginEditing();
        } else if (source.equals(this.mLabelArray[2])) {
            this.mEditIndex = 2;
            beginEditing();
        } else if (source.equals(this.mLabelArray[1])) {
            int nPrimaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, "0")) + 1;
            if (nPrimaryUnit > 3) {
                nPrimaryUnit = 0;
            }
            editor.putString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, String.valueOf(nPrimaryUnit));
            editor.apply();
            updateSettingsChanged();
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
        } else if (source.equals(this.mLabelArray[3])) {
            int nSecondaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, "1")) + 1;
            if (nSecondaryUnit > 3) {
                nSecondaryUnit = 0;
            }
            editor.putString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, String.valueOf(nSecondaryUnit));
            editor.apply();
            updateSettingsChanged();
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
        }
    }

    void beginEditing() {
        if (this.mNumpadView == null) {
            this.mIsVirginEdit = true;
            this.mNumpadView = new NumpadView(this, 1, mScreenWidth, this.mUiLayout.getHeight(), this.mUiLayout.getHeight() - this.mProtractorView.getProtractorHeight(), 0);
            this.mNumpadView.setListener(this);
            this.mNumpadView.setLayoutParams(new LayoutParams(-1, -1));
            this.mUiLayout.addView(this.mNumpadView);
            showNumpadView(true);
            this.mLabelArray[this.mEditIndex].setTextColor(-1);
            View backgroundView = new View(this);
            backgroundView.setId(6);
            backgroundView.setBackgroundColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            LayoutParams params = new LayoutParams(-2, -2);
            params.addRule(6, this.mLabelArray[this.mEditIndex].getId());
            params.addRule(0, this.mLabelArray[this.mEditIndex + 1].getId());
            params.addRule(5, this.mLEDScreen.getId());
            params.addRule(8, this.mLabelArray[this.mEditIndex].getId());
            params.setMargins(10, 3, 0, 3);
            backgroundView.setLayoutParams(params);
            this.mUiLayout.addView(backgroundView);
            this.mLabelArray[this.mEditIndex].bringToFront();
            this.mNumpadView.bringToFront();
        }
    }

    void endEditing() {
        int nUnit;
        showNumpadView(false);
        this.mLabelArray[this.mEditIndex].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        View backgroundView = this.mUiLayout.findViewById(6);
        if (backgroundView != null) {
            this.mUiLayout.removeView(backgroundView);
        }
        if (this.mEditIndex == 0) {
            nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, "0"));
        } else {
            nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, "1"));
        }
        float fVal = 0.0f;
        try {
            fVal = Float.parseFloat(this.mLabelArray[this.mEditIndex].getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (nUnit == 0) {
            if (fVal < 0.0f) {
                fVal = 0.0f;
            } else if (fVal > BitmapDescriptorFactory.HUE_CYAN) {
                fVal = BitmapDescriptorFactory.HUE_CYAN;
            }
            fVal = MiscUtility.degree2Radian(fVal);
        } else if (nUnit == 1) {
            if (fVal < 0.0f) {
                fVal = 0.0f;
            } else if (((double) fVal) > 3.14d) {
                fVal = 3.14f;
            }
        } else if (nUnit == 3) {
            if (fVal < 0.0f) {
                fVal = 0.0f;
            } else if (((double) fVal) > 0.5d) {
                fVal = 0.5f;
            }
            fVal = MiscUtility.revolution2Radian(fVal);
        } else {
            if (fVal < 0.0f) {
                fVal = 0.0f;
            } else if (fVal > 200.0f) {
                fVal = 200.0f;
            }
            fVal = MiscUtility.gradian2Radian(fVal);
        }
        this.mCurrentAngle = fVal;
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putFloat(SettingsKey.SETTINGS_PROTRACTOR_LAST_ANGLE_KEY, this.mCurrentAngle);
        editor.apply();
        displayAngle(fVal);
        rotateHandView(this.mCurrentAngle, true);
        updateHandViewBounds();
    }

    protected void rotateHandView(float rad, boolean animate) {
        float deg = MiscUtility.radian2Degree(rad);
        ViewHelper.setPivotX(this.mHandView, (float) this.mHandView.getWidth());
        ViewHelper.setPivotY(this.mHandView, (float) (this.mHandView.getHeight() / 2));
        ViewHelper.setPivotX(this.mLineView, (float) this.mLineView.getWidth());
        ViewHelper.setPivotY(this.mLineView, (float) (this.mLineView.getHeight() / 2));
        if (animate) {
            ObjectAnimator a1 = ObjectAnimator.ofFloat(this.mHandView, "rotation", deg);
            ObjectAnimator a2 = ObjectAnimator.ofFloat(this.mLineView, "rotation", deg);
            AnimatorSet animSet = new AnimatorSet();
            animSet.addListener(this);
            animSet.setDuration(500);
            animSet.setStartDelay(0);
            animSet.playTogether(a1, a2);
            animSet.start();
            return;
        }
        ViewHelper.setRotation(this.mHandView, deg);
        ViewHelper.setRotation(this.mLineView, deg);
    }

    void updateHandViewBounds() {
        initMatrixVertices();
        this.mTransformMatrix.reset();
        this.mTransformMatrix.setRotate(MiscUtility.radian2Degree(this.mCurrentAngle), (float) (this.mUiLayout.getWidth() / 2), (float) this.mUiLayout.getHeight());
        this.mTransformMatrix.mapPoints(this.mTransformedVertices, this.mOriginVertices);
    }

    void showNumpadView(boolean show) {
        float height;
        if (show) {
            height = (float) this.mUiLayout.getHeight();
        } else {
            height = 0.0f;
        }
        TranslateAnimation anim = new TranslateAnimation(0.0f, 0.0f, height, show ? 0.0f : (float) this.mUiLayout.getHeight());
        anim.setDuration(300);
        anim.setFillBefore(true);
        anim.setFillAfter(true);
        if (!show) {
            anim.setAnimationListener(this);
        }
        this.mNumpadView.startAnimation(anim);
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(3, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
    }

    protected float calculateAngleFromPoint(Point point) {
        float fAngle = (float) Math.atan((double) (((float) (mScreenHeight - point.y)) / ((float) (point.x - this.mOx))));
        return (float) (fAngle > 0.0f ? 3.141592653589793d - ((double) fAngle) : (double) (-fAngle));
    }

    protected float calculateAngleFromDownPointAndCurrentPoint(Point downPoint, Point currentPoint) {
        return calculateAngleFromPoint(currentPoint) - calculateAngleFromPoint(downPoint);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onTouchMove(View source, Point currentPosition) {
        Point pt = currentPosition;
        if (!this.mIsMoved) {
            if (Math.sqrt((double) (((this.mDownPoint.x - pt.x) * (this.mDownPoint.x - pt.x)) + ((this.mDownPoint.y - pt.y) * (this.mDownPoint.y - pt.y)))) > ((double) ViewConfiguration.get(this).getScaledTouchSlop())) {
                this.mIsMoved = true;
            } else {
                return;
            }
        }
        if (!mIsLocked) {
            float fAngle;
            float fLastAngle;
            this.mCurrentPoint = pt;
            Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            float fTransformAngle = this.mCurrentAngle + calculateAngleFromDownPointAndCurrentPoint(this.mDownPoint, this.mCurrentPoint);
            if (fTransformAngle < 0.0f) {
                fTransformAngle = 0.0f;
            }
            if (((double) fTransformAngle) > 3.141592653589793d) {
                fTransformAngle = 3.1415927f;
            }
            boolean bSnap = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_KEY, false);
            int nSnapUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY, "0"));
            int nPrimaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, "0"));
            int nSecondaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, "1"));
            bSnap &= (nSecondaryUnit == nSnapUnit ? true : false) | (nPrimaryUnit == nSnapUnit ? true : false);
            if (nSnapUnit == 0) {
                fAngle = MiscUtility.radian2Degree(fTransformAngle);
                fLastAngle = MiscUtility.radian2Degree(this.mLastAngle);
            } else if (nSnapUnit == 2) {
                fAngle = MiscUtility.radian2Gradian(fTransformAngle);
                fLastAngle = MiscUtility.radian2Gradian(this.mLastAngle);
            } else if (nSnapUnit == 3) {
                fAngle = MiscUtility.radian2Revolution(fTransformAngle);
                fLastAngle = MiscUtility.radian2Revolution(this.mLastAngle);
            } else {
                fAngle = fTransformAngle;
                fLastAngle = this.mLastAngle;
            }
            float nStep;
            float nDeg;
            float[] fArr;
            float fRad;
            if (fAngle > fLastAngle) {
                if (bSnap) {
                    nStep = Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(this).getString(new String[]{SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY}[nSnapUnit], new String[]{"1", "0.01", "1", "0.0277777777777778"}[nSnapUnit]));
                    nDeg = (float) (Math.floor((double) (fAngle / nStep)) * ((double) nStep));
                } else {
                    fArr = new float[4];
                    nStep = new float[]{5.0f, 0.01f, 5.0f, 0.027777778f}[nSnapUnit];
                    nDeg = (float) (Math.floor((double) (fAngle / nStep)) * ((double) nStep));
                }
                if (nDeg > fLastAngle) {
                    this.mLastAngle = fTransformAngle;
                    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                        SoundUtility.getInstance().playSound(4, 0.1f);
                    }
                    if (bSnap) {
                        if (nSnapUnit == 0) {
                            fRad = MiscUtility.degree2Radian(nDeg);
                        } else if (nSnapUnit == 2) {
                            fRad = MiscUtility.gradian2Radian(nDeg);
                        } else if (nSnapUnit == 3) {
                            fRad = MiscUtility.revolution2Radian(nDeg);
                        } else {
                            fRad = nDeg;
                        }
                        displayAngle(fRad);
                        rotateHandView(fRad, false);
                        editor.putFloat(SettingsKey.SETTINGS_PROTRACTOR_LAST_ANGLE_KEY, fRad);
                        editor.apply();
                    }
                }
            } else if (fAngle < fLastAngle) {
                if (bSnap) {
                    nStep = Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(this).getString(new String[]{SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY}[nSnapUnit], new String[]{"1", "0.01", "1", "0.0277777777777778"}[nSnapUnit]));
                    nDeg = (float) (Math.ceil((double) (fAngle / nStep)) * ((double) nStep));
                } else {
                    fArr = new float[4];
                    nStep = new float[]{5.0f, 0.01f, 5.0f, 0.027777778f}[nSnapUnit];
                    nDeg = (float) (Math.ceil((double) (fAngle / nStep)) * ((double) nStep));
                }
                if (nDeg < fLastAngle) {
                    this.mLastAngle = fTransformAngle;
                    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                        SoundUtility.getInstance().playSound(4, 0.1f);
                    }
                    if (bSnap) {
                        if (nSnapUnit == 0) {
                            fRad = MiscUtility.degree2Radian(nDeg);
                        } else if (nSnapUnit == 2) {
                            fRad = MiscUtility.gradian2Radian(nDeg);
                        } else if (nSnapUnit == 3) {
                            fRad = MiscUtility.revolution2Radian(nDeg);
                        } else {
                            fRad = nDeg;
                        }
                        displayAngle(fRad);
                        rotateHandView(fRad, false);
                        editor.putFloat(SettingsKey.SETTINGS_PROTRACTOR_LAST_ANGLE_KEY, fRad);
                        editor.apply();
                    }
                }
            }
            if (!bSnap) {
                displayAngle(fTransformAngle);
                rotateHandView(fTransformAngle, false);
                editor.putFloat(SettingsKey.SETTINGS_PROTRACTOR_LAST_ANGLE_KEY, fTransformAngle);
                editor.apply();
            }
        }
    }

    void initMatrixVertices() {
        int handWidth = this.mHandView.getWidth();
        int handHeight = this.mHandView.getHeight();
        this.mOriginVertices[0] = (float) ((this.mUiLayout.getWidth() / 2) - handWidth);
        this.mOriginVertices[1] = (float) (this.mUiLayout.getHeight() - (handHeight / 2));
        this.mOriginVertices[2] = (float) ((this.mUiLayout.getWidth() / 2) + handWidth);
        this.mOriginVertices[3] = (float) (this.mUiLayout.getHeight() - (handHeight / 2));
        this.mOriginVertices[4] = (float) ((this.mUiLayout.getWidth() / 2) + handWidth);
        this.mOriginVertices[5] = (float) (this.mUiLayout.getHeight() + (handHeight / 2));
        this.mOriginVertices[6] = (float) ((this.mUiLayout.getWidth() / 2) - handWidth);
        this.mOriginVertices[7] = (float) (this.mUiLayout.getHeight() + (handHeight / 2));
    }

    protected void initSubviews() {
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mUiLayout.setOnTouchListener(this);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        this.mProtractorView = new ProtractorView(this);
        this.mProtractorView.setLayoutParams(new LayoutParams(-1, -1));
        this.mUiLayout.addView(this.mProtractorView);
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap());
        this.mMenuButton.setOnClickListener(this);
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(11);
        params.setMargins(0, vButtonScreenMargin, hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        this.mInfoButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mInfoButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mMenuButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mInfoButton);
        this.mLockButton = new SPImageButton(this);
        this.mLockButton.setId(3);
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_unlock)).getBitmap());
        this.mLockButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mInfoButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mLockButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mLockButton);
        this.mSwapButton = new SPImageButton(this);
        this.mSwapButton.setId(4);
        this.mSwapButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_swap)).getBitmap());
        this.mSwapButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(14);
        params.setMargins(0, 0, 0, vButtonScreenMargin);
        this.mSwapButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mSwapButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mLockButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mNoAdsButton.setLayoutParams(params);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
        this.mLineView = new LineView(this);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(9);
        params.width = mScreenWidth;
        params.height = 20;
        params.setMargins((-mScreenWidth) / 2, 0, 0, -10);
        this.mLineView.setLayoutParams(params);
        this.mUiLayout.addView(this.mLineView);
        this.mHandView = new ImageView(this);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.protractor_hand)).getBitmap();
        this.mHandView.setImageBitmap(bitmap);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(9);
        params.setMargins((mScreenWidth / 2) - bitmap.getWidth(), 0, 0, (-bitmap.getHeight()) / 2);
        this.mHandView.setLayoutParams(params);
        this.mUiLayout.addView(this.mHandView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        this.mLEDScreen = new SPScale9ImageView(this);
        this.mLEDScreen.setId(5);
        this.mLEDScreen.setBitmap(bitmap);
        params = new LayoutParams(bitmap.getWidth() - 2, (bitmap.getHeight() - 2) * 2);
        params.addRule(10);
        params.addRule(9);
        params.setMargins((int) getResources().getDimension(R.dimen.PROTRACTOR_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN), (int) getResources().getDimension(R.dimen.PROTRACTOR_VERT_MARGIN_BTW_LEDSCREEN_AND_SCREEN), 0, 0);
        this.mLEDScreen.setLayoutParams(params);
        this.mUiLayout.addView(this.mLEDScreen);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        float valueFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE);
        float unitFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE);
        for (int i = 0; i < 4; i++) {
            float f;
            this.mLabelArray[i] = new TextView(this);
            this.mLabelArray[i].setId((i + 0) + 100);
            this.mLabelArray[i].setPaintFlags(this.mLabelArray[i].getPaintFlags() | Barcode.ITF);
            this.mLabelArray[i].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            this.mLabelArray[i].setBackgroundColor(0);
            this.mLabelArray[i].setTypeface(font);
            TextView textView = this.mLabelArray[i];
            if (i % 2 != 0) {
                f = unitFontSize;
            } else {
                f = valueFontSize;
            }
            textView.setTextSize(1, f);
            this.mLabelArray[i].setGravity((i % 2 == 0 ? 16 : 48) | 5);
            this.mLabelArray[i].setOnTouchListener(this);
            this.mUiLayout.addView(this.mLabelArray[i]);
        }
        int rightUnitMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_UNIT_LABEL_RIGHT_MARGIN);
        int rightValueMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_VALUE_LABEL_RIGHT_MARGIN);
        int vUnitMargin = (int) getResources().getDimension(R.dimen.PROTRACTOR_VERT_MARGIN_BTW_LEDSCREEN_AND_UNIT_LABEL);
        int vValueMargin = (int) getResources().getDimension(R.dimen.PROTRACTOR_VERT_MARGIN_BTW_LEDSCREEN_AND_VALUE_LABEL);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLabelArray[0].getId());
        params.addRule(7, this.mLEDScreen.getId());
        params.rightMargin = rightUnitMargin;
        params.topMargin = vUnitMargin;
        this.mLabelArray[1].setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLEDScreen.getId());
        params.addRule(0, this.mLabelArray[1].getId());
        params.topMargin = vValueMargin;
        params.rightMargin = rightValueMargin;
        this.mLabelArray[0].setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLabelArray[2].getId());
        params.addRule(7, this.mLEDScreen.getId());
        params.rightMargin = rightUnitMargin;
        params.topMargin = vUnitMargin;
        this.mLabelArray[3].setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(8, this.mLEDScreen.getId());
        params.addRule(0, this.mLabelArray[3].getId());
        params.bottomMargin = vValueMargin;
        params.rightMargin = rightValueMargin;
        this.mLabelArray[2].setLayoutParams(params);
    }

    public void onClick(View v) {
        if (v.getId() == 1) {
            onButtonMenu();
        } else if (v.getId() == 2) {
            onButtonSettings();
        } else if (v.getId() == 3) {
            onLock();
        } else if (v.getId() == 4) {
            onSwap();
        } else if (v.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    protected void displayAngle(float fRad) {
        String sAngle1 = null;
        String sAngle2 = null;
        int nPrimaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, "0"));
        int nSecondaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, "1"));
        switch (nPrimaryUnit) {
            case Barcode.ALL_FORMATS /*0*/:
                this.mLabelArray[1].setText("deg");
                sAngle1 = MiscUtility.getDegreeStringFromRad(fRad);
                break;
            case CompletionEvent.STATUS_FAILURE /*1*/:
                this.mLabelArray[1].setText("rad");
                sAngle1 = String.format(Locale.US, "%.2f", new Object[]{Float.valueOf(fRad)});
                break;
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                this.mLabelArray[1].setText("grad");
                sAngle1 = MiscUtility.getGradianStringFromRad(fRad);
                break;
            case CompletionEvent.STATUS_CANCELED /*3*/:
                this.mLabelArray[1].setText("rev");
                sAngle1 = MiscUtility.getRevolutionStringFromRad(fRad);
                break;
        }
        switch (nSecondaryUnit) {
            case Barcode.ALL_FORMATS /*0*/:
                this.mLabelArray[3].setText("deg");
                sAngle2 = MiscUtility.getDegreeStringFromRad(fRad);
                break;
            case CompletionEvent.STATUS_FAILURE /*1*/:
                this.mLabelArray[3].setText("rad");
                sAngle2 = String.format(Locale.US, "%.2f", new Object[]{Float.valueOf(fRad)});
                break;
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                this.mLabelArray[3].setText("grad");
                sAngle2 = MiscUtility.getGradianStringFromRad(fRad);
                break;
            case CompletionEvent.STATUS_CANCELED /*3*/:
                this.mLabelArray[3].setText("rev");
                sAngle2 = MiscUtility.getRevolutionStringFromRad(fRad);
                break;
        }
        this.mLabelArray[0].setText(sAngle1);
        this.mLabelArray[2].setText(sAngle2);
    }

    void onLock() {
        boolean z = true;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(2, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (mIsLocked) {
            z = false;
        }
        mIsLocked = z;
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(mIsLocked ? R.drawable.button_lock : R.drawable.button_unlock)).getBitmap());
    }

    void onSwap() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        String nPrimaryUnit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, "0");
        String nSecondaryUnit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, "1");
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, nSecondaryUnit);
        editor.putString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, nPrimaryUnit);
        editor.apply();
        updateSettingsChanged();
    }

    public void onNumpadEvent(String sKey) {
        if (sKey == "Done") {
            endEditing();
            return;
        }
        if (this.mIsVirginEdit) {
            this.mIsVirginEdit = false;
            onNumpadEvent("C");
        }
        String editText = BuildConfig.FLAVOR;
        if (sKey == "C") {
            this.mLabelArray[this.mEditIndex].setText("0");
        } else {
            editText = appendString(sKey, this.mLabelArray[this.mEditIndex].getText().toString());
        }
        this.mLabelArray[this.mEditIndex].setText(editText);
    }

    String appendString(String sAppend, String sEdit) {
        if (this.mIsVirginEdit) {
            return BuildConfig.FLAVOR;
        }
        if (sAppend == ".") {
            if (sEdit.length() == 0) {
                this.mIsVirginEdit = false;
                return "0.";
            } else if (sEdit.indexOf(".") != -1) {
                return sEdit;
            }
        } else if (sEdit == "0") {
            return sAppend;
        }
        if (!(sAppend == "." || sEdit == BuildConfig.FLAVOR)) {
            int nUnit;
            if (this.mEditIndex == 0) {
                nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, "0"));
            } else {
                nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, "1"));
            }
            int pos = sEdit.indexOf(".");
            if (pos != -1) {
                int nCount = (sEdit.length() - pos) - 1;
                if (nUnit == 0 || nUnit == 2) {
                    if (nCount >= 1) {
                        return sEdit;
                    }
                } else if (nUnit == 3) {
                    if (nCount >= 3) {
                        return sEdit;
                    }
                } else if (nCount >= 2) {
                    return sEdit;
                }
            }
            int count = 0;
            int i = 0;
            while (i < sEdit.length() && sEdit.charAt(i) >= '0' && sEdit.charAt(i) <= '9') {
                count++;
                i++;
            }
            if (nUnit == 0 || nUnit == 2) {
                if (count >= 3 && sEdit.indexOf(".") == -1) {
                    return sEdit;
                }
            } else if (count >= 1 && sEdit.indexOf(".") == -1) {
                return sEdit;
            }
        }
        sEdit = sEdit.concat(sAppend);
        this.mIsVirginEdit = false;
        return sEdit;
    }

    public void onAnimationEnd(Animation animation) {
        if (animation instanceof TranslateAnimation) {
            this.mNumpadView.post(new Runnable() {
                public void run() {
                    if (ProtractorActivity.this.mNumpadView.getParent() != null) {
                        ((RelativeLayout) ProtractorActivity.this.mNumpadView.getParent()).removeView(ProtractorActivity.this.mNumpadView);
                    }
                    ProtractorActivity.this.mNumpadView = null;
                }
            });
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }

    public void onBackPressed() {
        if (this.mNumpadView != null) {
            endEditing();
        } else {
            super.onBackPressed();
        }
    }

    public class Define {
        public static final int ANGLE_UNIT_DEGREE = 0;
        public static final int ANGLE_UNIT_GRADIAN = 2;
        public static final int ANGLE_UNIT_RADIAN = 1;
        public static final int ANGLE_UNIT_REVOLUTION = 3;
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_LOCK = 3;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_SWAP = 4;
        public static final int BUTTON_UPGRADE = 0;
        public static final int EDIT_BACKGROUND_VIEW = 6;
        public static final int LABEL00 = 0;
        public static final int LABEL01 = 1;
        public static final int LABEL10 = 2;
        public static final int LABEL11 = 3;
        public static final int LED_FRAME = 5;
    }
}
