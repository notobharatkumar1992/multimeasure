package com.vivekwarde.measure.protractor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.vision.barcode.Barcode;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;

import java.util.Locale;

public class ProtractorView extends View {
    final String tag;
    int HEIGHT;
    int WIDTH;
    Paint mLinePaint;
    int mLineTextDelta;
    Rect mNumBounds;
    String mNumberString;
    int mOR1Delta;
    RectF mOval;
    Path mPath;
    Bitmap mProtractorBitmap;
    int mR1R2Delta;
    int mRR1Delta;
    Rect mTextBounds;
    TextPaint mTextPaint;
    float fRad;
    private int mRadianR1R2Delta;

    public ProtractorView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mProtractorBitmap = null;
        this.mLinePaint = null;
        this.mRR1Delta = 13;
        this.mR1R2Delta = 27;
        this.mOR1Delta = 80;
        this.mLineTextDelta = 3;
        this.mNumberString = new String();
        this.mTextPaint = null;
        this.mTextBounds = null;
        this.mNumBounds = new Rect();
        this.mPath = new Path();
        this.mOval = new RectF();
        this.mRadianR1R2Delta = 20;
        this.mRR1Delta = (int) getResources().getDimension(R.dimen.PROTRACTOR_R_R1_DELTA);
        this.mR1R2Delta = (int) getResources().getDimension(R.dimen.PROTRACTOR_R1_R2_DELTA);
        this.mRadianR1R2Delta = (int) getResources().getDimension(R.dimen.PROTRACTOR_SECONDARY_RADIAN_R1_R2_DELTA);
        this.mOR1Delta = (int) getResources().getDimension(R.dimen.PROTRACTOR_O_R1_DELTA);
        this.mLineTextDelta = (int) getResources().getDimension(R.dimen.PROTRACTOR_LINE_TEXT_DELTA);
        this.mProtractorBitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.protractor_background)).getBitmap();
        this.mLinePaint = new Paint();
        this.mLinePaint.setColor(ContextCompat.getColor(getContext(), R.color.RULER_PROTRACTOR_LINE_COLOR));
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        this.mLinePaint.setStrokeWidth(metrics.density + 0.5f);
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.RULER_PROTRACTOR_NUMBER_COLOR));
        this.mTextPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Eurostile LT Medium.ttf"));
        this.mTextPaint.setTextAlign(Align.CENTER);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.PROTRACTOR_FONT_SIZE));
        this.mTextBounds = new Rect();
        this.mTextPaint.getTextBounds("0123456789/", 0, 11, this.mTextBounds);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != oldw || h != oldh) {
            this.WIDTH = w;
            this.HEIGHT = h;
        }
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawColor(0);
        canvas.drawBitmap(this.mProtractorBitmap, (float) ((canvas.getWidth() - this.mProtractorBitmap.getWidth()) / 2), (float) (canvas.getHeight() - this.mProtractorBitmap.getHeight()), this.mLinePaint);
        int outsideUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY, "0"));
        int insideUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY, "1"));
        switch (outsideUnit) {
            case Barcode.ALL_FORMATS /*0*/:
                drawDegree(canvas, true, this.mProtractorBitmap);
                break;
            case CompletionEvent.STATUS_FAILURE /*1*/:
                drawRadian(canvas, true, this.mProtractorBitmap);
                break;
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                drawGradian(canvas, true, this.mProtractorBitmap);
                break;
            case CompletionEvent.STATUS_CANCELED /*3*/:
                drawRevolution(canvas, true, this.mProtractorBitmap);
                break;
        }
        switch (insideUnit) {
            case Barcode.ALL_FORMATS /*0*/:
                drawDegree(canvas, false, null);
                break;
            case CompletionEvent.STATUS_FAILURE /*1*/:
                drawRadian(canvas, false, null);
                break;
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                drawGradian(canvas, false, null);
                break;
            case CompletionEvent.STATUS_CANCELED /*3*/:
                drawRevolution(canvas, false, null);
                break;
        }
        setDrawingCacheEnabled(true);
    }

    void drawDegree(Canvas canvas, boolean bOutside, Bitmap bmpProtractor) {
        float fR1 = 0;
        float fR2, r;
        int i;
        float fDeltaR;
        if (bOutside) {
            fR1 = ((float) (bmpProtractor.getWidth() / 2)) - ((float) this.mRR1Delta);
            fR2 = fR1 - ((float) this.mR1R2Delta);
            i = 0;
            while (i <= 180) {
                fDeltaR = 0.0f;
                if (i % 10 == 0) {
                    fDeltaR = (float) this.mR1R2Delta;
                } else if (i % 5 == 0) {
                    fDeltaR = (float) this.mRR1Delta;
                }
                float fRad = MiscUtility.degree2Radian((float) i);
                Canvas canvas2 = canvas;
                canvas2.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR1))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR1))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR2 - fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR2 - fDeltaR)))), this.mLinePaint);
                if (!(i % 10 != 0 || i == 0 || i == 180)) {
                    r = ((fR2 - fDeltaR) - ((float) this.mTextBounds.height())) - ((float) this.mLineTextDelta);
                    this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                    this.mPath.reset();
                    this.mPath.addArc(this.mOval, (float) ((i - 180) - 10), 20.0f);
                    Canvas canvas3 = canvas;
                    canvas3.drawTextOnPath(String.format(Locale.US, "%d", new Object[]{Integer.valueOf(i)}), this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
                i++;
            }
            return;
        }
        float tempValueInPx = (float) this.mOR1Delta;
        fR2 = fR1 - ((float) this.mR1R2Delta);
        i = 0;
        while (((double) i) <= 72.0d) {
            fDeltaR = 0.0f;
            if (i % 2 == 0) {
                fDeltaR = (float) this.mR1R2Delta;
            }
            if (i % 4 == 2) {
                fDeltaR = (float) this.mRR1Delta;
            }
            fRad = MiscUtility.degree2Radian((float) (((double) i) * 2.5d));
            Canvas canvas2 = canvas;
            canvas2.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR2))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR2))), this.mLinePaint);
            if (!(i % 8 != 0 || i == 0 || ((double) i) == 72.0d)) {
                r = ((fR1 + fDeltaR) + ((float) this.mTextBounds.height())) - this.mTextPaint.descent();
                this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                this.mPath.reset();
                this.mPath.addArc(this.mOval, ((((float) i) * 2.5f) - BitmapDescriptorFactory.HUE_CYAN) - 10.0f, 20.0f);
                Canvas canvas3 = canvas;
                canvas3.drawTextOnPath(String.format(Locale.US, "%.0f", new Object[]{Double.valueOf(((double) i) * 2.5d)}), this.mPath, 0.0f, 0.0f, this.mTextPaint);
            }
            i++;
        }
    }

    void drawRadian(Canvas canvas, boolean bOutside, Bitmap bmpProtractor) {
        float fR1 = 0;
        float fR2, r;
        int i;
        float fDeltaR;
        float fRad;
        if (bOutside) {
            fR1 = ((float) (bmpProtractor.getWidth() / 2)) - ((float) this.mRR1Delta);
            fR2 = fR1 - ((float) this.mR1R2Delta);
            i = 0;
            while (((double) i) <= 157.0d) {
                fDeltaR = 0.0f;
                if (i % 10 == 0) {
                    fDeltaR = (float) this.mR1R2Delta;
                } else if (i % 5 == 0) {
                    fDeltaR = (float) this.mRR1Delta;
                }
                fRad = (float) (((double) i) * 0.02d);
                Canvas canvas2 = canvas;
                canvas2.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR1))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR1))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR2 - fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR2 - fDeltaR)))), this.mLinePaint);
                if (!(i % 10 != 0 || i == 0 || ((double) i) == 157.0d)) {
                    r = ((fR2 - fDeltaR) - ((float) this.mTextBounds.height())) - ((float) this.mLineTextDelta);
                    this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                    this.mPath.reset();
                    this.mPath.addArc(this.mOval, (MiscUtility.radian2Degree(fRad) - BitmapDescriptorFactory.HUE_CYAN) - 10.0f, 20.0f);
                    Canvas canvas3 = canvas;
                    canvas3.drawTextOnPath(String.format(Locale.US, "%.1f", new Object[]{Double.valueOf(((double) i) * 0.02d)}), this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
                i++;
            }
            return;
        }
        float tempValueInPx = (float) this.mOR1Delta;
        fR2 = fR1 - ((float) this.mR1R2Delta);
        i = 0;
        while (((double) i) <= 62.8d) {
            fDeltaR = 0.0f;
            if (i % 2 == 0) {
                fDeltaR = (float) this.mRadianR1R2Delta;
            }
            fRad = (float) (((double) i) * 0.05d);
            Canvas canvas2 = canvas;
            canvas2.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR2))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR2))), this.mLinePaint);
            if (!(i % 8 != 4 || i == 0 || ((double) i) == 62.8d)) {
                r = ((fR1 + fDeltaR) + ((float) this.mTextBounds.height())) - this.mTextPaint.descent();
                this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                this.mPath.reset();
                this.mPath.addArc(this.mOval, (MiscUtility.radian2Degree(fRad) - BitmapDescriptorFactory.HUE_CYAN) - 10.0f, 20.0f);
                Canvas canvas3 = canvas;
                canvas3.drawTextOnPath(String.format(Locale.US, "%.1f", new Object[]{Double.valueOf(((double) i) * 0.05d)}), this.mPath, 0.0f, 0.0f, this.mTextPaint);
            }
            i++;
        }
    }

    void drawGradian(Canvas canvas, boolean bOutside, Bitmap bmpProtractor) {
        float fR1 = 0;
        float fR2, r;
        int i;
        float fDeltaR;
        float fRad;
        if (bOutside) {
            fR1 = ((float) (bmpProtractor.getWidth() / 2)) - ((float) this.mRR1Delta);
            fR2 = fR1 - ((float) this.mR1R2Delta);
            i = 0;
            while (i <= 200) {
                fDeltaR = 0.0f;
                if (i % 10 == 0) {
                    fDeltaR = (float) this.mR1R2Delta;
                } else if (i % 5 == 0) {
                    fDeltaR = (float) this.mRR1Delta;
                }
                fRad = MiscUtility.gradian2Radian((float) i);
                Canvas canvas2 = canvas;
                canvas2.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR1))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR1))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR2 - fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR2 - fDeltaR)))), this.mLinePaint);
                if (!(i % 20 != 10 || i == 0 || i == 200)) {
                    r = ((fR2 - fDeltaR) - ((float) this.mTextBounds.height())) - ((float) this.mLineTextDelta);
                    this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                    this.mPath.reset();
                    this.mPath.addArc(this.mOval, (MiscUtility.radian2Degree(fRad) - BitmapDescriptorFactory.HUE_CYAN) - 10.0f, 20.0f);
                    Canvas canvas3 = canvas;
                    canvas3.drawTextOnPath(String.format(Locale.US, "%d", new Object[]{Integer.valueOf(i)}), this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
                i++;
            }
            return;
        }
        float tempValueInPx = (float) this.mOR1Delta;
        fR2 = fR1 - ((float) this.mR1R2Delta);
        i = 0;
        while (((double) i) <= 80.0d) {
            fDeltaR = 0.0f;
            if (i % 2 == 0) {
                fDeltaR = (float) this.mR1R2Delta;
            }
            if (i % 4 == 2) {
                fDeltaR = (float) this.mRR1Delta;
            }
            fRad = MiscUtility.gradian2Radian((float) (((double) i) * 2.5d));
            Canvas canvas2 = canvas;
            canvas2.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR2))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR2))), this.mLinePaint);
            if (!(i % 8 != 0 || i == 0 || ((double) i) == 80.0d)) {
                r = ((fR1 + fDeltaR) + ((float) this.mTextBounds.height())) - this.mTextPaint.descent();
                this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                this.mPath.reset();
                this.mPath.addArc(this.mOval, (MiscUtility.radian2Degree(fRad) - BitmapDescriptorFactory.HUE_CYAN) - 10.0f, 20.0f);
                Canvas canvas3 = canvas;
                canvas3.drawTextOnPath(String.format(Locale.US, "%.0f", new Object[]{Double.valueOf(((double) i) * 2.5d)}), this.mPath, 0.0f, 0.0f, this.mTextPaint);
            }
            i++;
        }
    }

    void drawRevolution(Canvas canvas, boolean bOutside, Bitmap bmpProtractor) {
        float fR1 = 0;
        float fR2, r;
        int i;
        float fDeltaR;
        float fRad;
        int[] frac;
        float width;
        if (bOutside) {
            this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.PROTRACTOR_FONT_SIZE) - 2.0f);
            fR1 = ((float) (bmpProtractor.getWidth() / 2)) - ((float) this.mRR1Delta);
            fR2 = fR1 - ((float) this.mR1R2Delta);
            i = 0;
            while (i <= 144) {
                fDeltaR = 0.0f;
                if (i % 8 == 0) {
                    fDeltaR = (float) this.mR1R2Delta;
                } else if (i % 4 == 0) {
                    fDeltaR = (float) this.mRR1Delta;
                }
                fRad = (float) ((((double) i) * 3.141592653589793d) / ((double) 144));
                canvas.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR1))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR1))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR2 - fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR2 - fDeltaR)))), this.mLinePaint);
                if (!(i % 8 != 0 || i == 0 || i == 144)) {
                    r = ((fR2 - fDeltaR) - ((float) this.mTextBounds.height())) - ((float) this.mLineTextDelta);
                    this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                    this.mPath.reset();
                    this.mPath.addArc(this.mOval, (MiscUtility.radian2Degree(fRad) - BitmapDescriptorFactory.HUE_CYAN) - 10.0f, 20.0f);
                    canvas.drawTextOnPath("/", this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    frac = new int[]{i, 288};
                    reduceFraction(frac);
                    this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(frac[0])});
                    this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                    canvas.drawTextOnPath(this.mNumberString, this.mPath, frac[0] < 10 ? (float) (-this.mNumBounds.width()) : (float) (-((this.mNumBounds.width() * 2) / 3)), this.mTextPaint.ascent() / 2.0f, this.mTextPaint);
                    this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(frac[1])});
                    this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                    String str = this.mNumberString;
                    Path path = this.mPath;
                    if (frac[1] < 10) {
                        width = (float) this.mNumBounds.width();
                    } else {
                        width = (float) ((this.mNumBounds.width() * 2) / 3);
                    }
                    canvas.drawTextOnPath(str, path, width, (-this.mTextPaint.ascent()) / 2.0f, this.mTextPaint);
                }
                i++;
            }
            this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.PROTRACTOR_FONT_SIZE));
            return;
        }
        float tempValueInPx = (float) this.mOR1Delta;
        fR2 = fR1 - ((float) this.mR1R2Delta);
        i = 0;
        while (i <= 144) {
            fDeltaR = 0.0f;
            if (i % 8 == 0) {
                fDeltaR = (float) this.mR1R2Delta;
            } else if (i % 4 == 0) {
                fDeltaR = (float) this.mRR1Delta;
            } else if (i % 4 != 2) {
                i++;
            }
            fRad = (float) ((((double) i) * 3.141592653589793d) / ((double) 144));
            canvas.drawLine((float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) (fR1 + fDeltaR)))), (float) (((double) (this.WIDTH / 2)) - (Math.cos((double) fRad) * ((double) fR2))), (float) (((double) this.HEIGHT) - (Math.sin((double) fRad) * ((double) fR2))), this.mLinePaint);
            if (!(i % 16 != 0 || i == 0 || i == 144)) {
                r = ((fR1 + fDeltaR) + ((float) this.mTextBounds.height())) - this.mTextPaint.descent();
                this.mOval.set(((float) (this.WIDTH / 2)) - r, ((float) this.HEIGHT) - r, ((float) (this.WIDTH / 2)) + r, ((float) this.HEIGHT) + r);
                this.mPath.reset();
                this.mPath.addArc(this.mOval, (MiscUtility.radian2Degree(fRad) - BitmapDescriptorFactory.HUE_CYAN) - 10.0f, 20.0f);
                canvas.drawTextOnPath("/", this.mPath, 0.0f, 0.0f, this.mTextPaint);
                frac = new int[]{i, 288};
                reduceFraction(frac);
                this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(frac[0])});
                this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                canvas.drawTextOnPath(this.mNumberString, this.mPath, frac[0] < 10 ? (float) (-this.mNumBounds.width()) : (float) (-((this.mNumBounds.width() * 2) / 3)), this.mTextPaint.ascent() / 2.0f, this.mTextPaint);
                this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(frac[1])});
                this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                String str = this.mNumberString;
                Path path = this.mPath;
                if (frac[1] < 10) {
                    width = (float) this.mNumBounds.width();
                } else {
                    width = (float) ((this.mNumBounds.width() * 2) / 3);
                }
                canvas.drawTextOnPath(str, path, width, (-this.mTextPaint.ascent()) / 2.0f, this.mTextPaint);
            }
            i++;
        }
    }

    int gcd(int a, int b) {
        a = Math.abs(a);
        b = Math.abs(b);
        if (a == 0 || b == 0) {
            return a + b;
        }
        while (a != b) {
            if (a > b) {
                a -= b;
            } else {
                b -= a;
            }
        }
        return a;
    }

    void reduceFraction(int[] frac) {
        int gcd = gcd(frac[0], frac[1]);
        if (gcd != 0) {
            frac[0] = frac[0] / gcd;
            frac[1] = frac[1] / gcd;
        }
    }

    String reduceFractionToString(int[] frac) {
        reduceFraction(frac);
        return String.format(Locale.US, "%d/%d", new Object[]{Integer.valueOf(frac[0]), Integer.valueOf(frac[1])});
    }

    public int getProtractorHeight() {
        return this.mProtractorBitmap.getHeight();
    }
}
