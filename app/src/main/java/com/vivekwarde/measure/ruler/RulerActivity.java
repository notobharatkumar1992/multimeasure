package com.vivekwarde.measure.ruler;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.LineView;
import com.vivekwarde.measure.custom_controls.NumpadView;
import com.vivekwarde.measure.custom_controls.NumpadView.NumpadViewEventListener;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

public class RulerActivity extends BaseActivity implements OnTouchListener, OnClickListener, NumpadViewEventListener, AnimationListener, AnimatorListener {
    private static final float SWIPE_VELOCITY_THRESHOLD = 1000.0f;
    public static boolean mIsLocked;

    static {
        mIsLocked = false;
    }

    final String tag;
    boolean mByPassScrollDidStop;
    int mCurrentPage;
    int mCurrentX;
    float mDPI;
    Point mDownPoint;
    int mEditIndex;
    SPImageButton mFlipButton;
    ImageView mHandView;
    int mHeaderBarHeight;
    SPImageButton mInfoButton;
    boolean mIsMoved;
    boolean mIsVirginEdit;
    SPScale9ImageView[] mLEDScreen;
    TextView[] mLabelArray;
    Point mLastMovePoint;
    int mLastX;
    LineView mLineView;
    SPImageButton mLockButton;
    SPImageButton mMenuButton;
    NumpadView mNumpadView;
    SPImageButton mResetButton;
    SPImageButton mSwapButton;
    private ImageView mDitchView;
    private boolean mMoveEnabled;
    private boolean mTemporaryLock;
    private VelocityTracker velocityTracker;

    public RulerActivity() {
        this.tag = getClass().getSimpleName();
        this.mLEDScreen = new SPScale9ImageView[]{null, null};
        this.mLabelArray = new TextView[]{null, null, null, null};
        this.mNumpadView = null;
        this.mHeaderBarHeight = 0;
        this.mTemporaryLock = false;
    }

    public static double getDPI(int width, int height, double diagonal) {
        return ((double) width) / (Math.cos(Math.atan2((double) height, (double) width)) * diagonal);
    }

    public static float getScreenDPI(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.xdpi;
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(0);
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        LayoutParams params = new LayoutParams(mScreenWidth * 3, -1);
        params.addRule(13);
        setContentView(this.mMainLayout, params);
        initInfo();
        initSubviews();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        updateSettingsChanged();
    }

    protected void onPause() {
        super.onPause();
    }

    float getScreenDPI() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.xdpi;
    }

    void initInfo() {
        this.mTemporaryLock = false;
        this.mDPI = PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, getScreenDPI(this));
        this.mIsMoved = false;
        this.mByPassScrollDidStop = false;
        this.mCurrentPage = PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_RULER_CURRENT_PAGE_KEY, 0);
        this.mCurrentX = PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_RULER_CURRENT_X_KEY, mScreenWidth / 2);
    }

    void initSubviews() {
        int i;
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(mScreenWidth * 3, mScreenHeight);
        uiLayoutParam.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mUiLayout.setOnTouchListener(this);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap();
        this.mHeaderBarHeight = (bitmap.getHeight() / 2) + (vButtonScreenMargin * 2);
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(bitmap);
        this.mMenuButton.setOnClickListener(this);
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(11);
        params.setMargins(0, vButtonScreenMargin, (mScreenWidth * 2) + hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        this.mInfoButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mInfoButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mMenuButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mInfoButton);
        this.mLockButton = new SPImageButton(this);
        this.mLockButton.setId(3);
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_unlock)).getBitmap());
        this.mLockButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mInfoButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mLockButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mLockButton);
        this.mResetButton = new SPImageButton(this);
        this.mResetButton.setId(6);
        this.mResetButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_reset)).getBitmap());
        this.mResetButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(11);
        params.setMargins(0, 0, (mScreenWidth * 2) + hButtonScreenMargin, vButtonScreenMargin);
        this.mResetButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mResetButton);
        this.mFlipButton = new SPImageButton(this);
        this.mFlipButton.setId(5);
        this.mFlipButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_rotate)).getBitmap());
        this.mFlipButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(0, this.mResetButton.getId());
        params.setMargins(0, 0, hButtonButtonMargin, vButtonScreenMargin);
        this.mFlipButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mFlipButton);
        this.mSwapButton = new SPImageButton(this);
        this.mSwapButton.setId(4);
        this.mSwapButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_swap)).getBitmap());
        this.mSwapButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(0, this.mFlipButton.getId());
        params.setMargins(0, 0, hButtonButtonMargin, vButtonScreenMargin);
        this.mSwapButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mSwapButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mLockButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mNoAdsButton.setLayoutParams(params);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        int vledmargin = (this.mHeaderBarHeight - (bitmap.getHeight() - 2)) / 2;
        int leftledmargin = (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN);
        for (i = 0; i < 2; i++) {
            this.mLEDScreen[i] = new SPScale9ImageView(this);
            this.mLEDScreen[i].setId((i + 0) + SeismometerGraphView.HISTORY_SIZE_PER_PAGE);
            this.mLEDScreen[i].setBitmap(bitmap);
            params = new LayoutParams((int) (SeismometerGraphView.MAX_ACCELERATION * ((double) (bitmap.getWidth() - 2))), bitmap.getHeight() - 2);
            params.addRule(9);
            if (i == 0) {
                params.addRule(10);
                params.setMargins(leftledmargin, vledmargin, 0, 0);
            } else {
                params.addRule(12);
                params.setMargins(leftledmargin, 0, 0, vledmargin);
            }
            this.mLEDScreen[i].setLayoutParams(params);
            this.mUiLayout.addView(this.mLEDScreen[i]);
        }
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        float valueFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE);
        float unitFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE);
        for (i = 0; i < 4; i++) {
            float f;
            this.mLabelArray[i] = new TextView(this);
            this.mLabelArray[i].setId((i + 0) + 100);
            this.mLabelArray[i].setPaintFlags(this.mLabelArray[i].getPaintFlags() | Barcode.ITF);
            this.mLabelArray[i].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            this.mLabelArray[i].setBackgroundColor(0);
            this.mLabelArray[i].setTypeface(font);
            TextView textView = this.mLabelArray[i];
            if (i % 2 != 0) {
                f = unitFontSize;
            } else {
                f = valueFontSize;
            }
            textView.setTextSize(1, f);
            this.mLabelArray[i].setGravity((i % 2 == 0 ? 16 : 48) | 5);
            this.mLabelArray[i].setOnTouchListener(this);
            this.mUiLayout.addView(this.mLabelArray[i]);
        }
        new Paint().setTypeface(font);
        int rightUnitMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_UNIT_LABEL_RIGHT_MARGIN);
        int rightValueMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_VALUE_LABEL_RIGHT_MARGIN);
        int vUnitMargin = (int) getResources().getDimension(R.dimen.RULER_VERT_MARGIN_BTW_LEDSCREEN_AND_UNIT_LABEL);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLEDScreen[0].getId());
        params.addRule(7, this.mLEDScreen[0].getId());
        params.rightMargin = rightUnitMargin;
        params.topMargin = vUnitMargin;
        this.mLabelArray[1].setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLEDScreen[0].getId());
        params.addRule(8, this.mLEDScreen[0].getId());
        params.addRule(5, this.mLEDScreen[0].getId());
        params.addRule(0, this.mLabelArray[1].getId());
        params.rightMargin = rightValueMargin;
        this.mLabelArray[0].setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLEDScreen[1].getId());
        params.addRule(7, this.mLEDScreen[1].getId());
        params.rightMargin = rightUnitMargin;
        params.topMargin = vUnitMargin;
        this.mLabelArray[3].setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mLEDScreen[1].getId());
        params.addRule(8, this.mLEDScreen[1].getId());
        params.addRule(5, this.mLEDScreen[1].getId());
        params.addRule(0, this.mLabelArray[3].getId());
        params.rightMargin = rightValueMargin;
        this.mLabelArray[2].setLayoutParams(params);
        this.mDitchView = new ImageView(this);
        this.mDitchView.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_ditch)).getBitmap());
        this.mDitchView.setScaleType(ScaleType.FIT_XY);
        params = new LayoutParams(-1, -2);
        params.addRule(15);
        params.addRule(14);
        this.mDitchView.setLayoutParams(params);
        this.mUiLayout.addView(this.mDitchView);
        this.mLineView = new LineView(this);
        params = new LayoutParams(-2, -1);
        params.addRule(15);
        params.addRule(9);
        params.width = 20;
        this.mLineView.setLayoutParams(params);
        this.mUiLayout.addView(this.mLineView);
        this.mHandView = new ImageView(this);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.ruler_hand)).getBitmap();
        this.mHandView.setImageBitmap(bitmap);
        params = new LayoutParams(-2, -2);
        params.addRule(15);
        params.addRule(9);
        params.width = bitmap.getWidth();
        this.mHandView.setLayoutParams(params);
        this.mUiLayout.addView(this.mHandView);
        preparePagesWithIndex(this.mCurrentPage);
        setHandViewPosition(this.mCurrentX, false);
        updateLabels();
    }

    public void onClick(View v) {
        if (v.getId() == 1) {
            onButtonMenu();
        } else if (v.getId() == 2) {
            onButtonSettings();
        } else if (v.getId() == 3) {
            onLock();
        } else if (v.getId() == 4) {
            onSwap();
        } else if (v.getId() == 5) {
            onFlip();
        } else if (v.getId() == 6) {
            onReset();
        } else if (v.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    void onLock() {
        boolean z = true;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(2, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (mIsLocked) {
            z = false;
        }
        mIsLocked = z;
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(mIsLocked ? R.drawable.button_lock : R.drawable.button_unlock)).getBitmap());
    }

    void onSwap() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        String sPrimaryUnit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, "0");
        String sSecondaryUnit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, "1");
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, sSecondaryUnit);
        editor.putString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, sPrimaryUnit);
        editor.apply();
        updateSettingsChanged();
    }

    void onFlip() {
        boolean flip;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false)) {
            flip = false;
        } else {
            flip = true;
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, flip);
        editor.apply();
        this.mCurrentX = mScreenWidth - this.mCurrentX;
        setHandViewPosition(this.mCurrentX, false);
        clearPagesWithIndex(this.mCurrentPage);
        preparePagesWithIndex(this.mCurrentPage);
    }

    void onReset() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mCurrentPage != 0) {
            clearPagesWithIndex(this.mCurrentPage);
            this.mCurrentPage = 0;
            preparePagesWithIndex(this.mCurrentPage);
            Editor editor;
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false)) {
                editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
                editor.putInt(SettingsKey.SETTINGS_RULER_CURRENT_PAGE_KEY, this.mCurrentPage);
                editor.apply();
                updateLabels();
            } else {
                editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
                editor.putInt(SettingsKey.SETTINGS_RULER_CURRENT_PAGE_KEY, this.mCurrentPage);
                editor.apply();
                updateLabels();
            }
        }
    }

    String getMillimeterString() {
        float fMillimeter = ((float) ((PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false) ? mScreenWidth - this.mCurrentX : this.mCurrentX) + (mScreenWidth * this.mCurrentPage))) / (this.mDPI / 25.4f);
        return String.format(Locale.US, "%.2f", new Object[]{Double.valueOf(((double) fMillimeter) / 10.0d)});
    }

    String getInchString() {
        float fInch = ((float) ((PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false) ? mScreenWidth - this.mCurrentX : this.mCurrentX) + (mScreenWidth * this.mCurrentPage))) / this.mDPI;
        return String.format(Locale.US, "%.2f", new Object[]{Float.valueOf(fInch)});
    }

    String getPixelString() {
        float fPixelLen = (float) ((PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false) ? mScreenWidth - this.mCurrentX : this.mCurrentX) + (mScreenWidth * this.mCurrentPage));
        return String.format(Locale.US, "%.0f", new Object[]{Float.valueOf(fPixelLen)});
    }

    String getPicaString() {
        float fPica = ((float) ((PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false) ? mScreenWidth - this.mCurrentX : this.mCurrentX) + (mScreenWidth * this.mCurrentPage))) / (this.mDPI / 6.0f);
        return String.format(Locale.US, "%.2f", new Object[]{Float.valueOf(fPica)});
    }

    void updateLabels() {
        int nPrimaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, "0"));
        int nSecondaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, "1"));
        if (nPrimaryUnit == 0) {
            this.mLabelArray[1].setText("cm");
            this.mLabelArray[0].setText(getMillimeterString());
        } else if (nPrimaryUnit == 1) {
            this.mLabelArray[1].setText("in");
            this.mLabelArray[0].setText(getInchString());
        } else if (nPrimaryUnit == 2) {
            this.mLabelArray[1].setText("px");
            this.mLabelArray[0].setText(getPixelString());
        } else {
            this.mLabelArray[1].setText("pc");
            this.mLabelArray[0].setText(getPicaString());
        }
        if (nSecondaryUnit == 0) {
            this.mLabelArray[3].setText("cm");
            this.mLabelArray[2].setText(getMillimeterString());
        } else if (nSecondaryUnit == 1) {
            this.mLabelArray[3].setText("in");
            this.mLabelArray[2].setText(getInchString());
        } else if (nSecondaryUnit == 2) {
            this.mLabelArray[3].setText("px");
            this.mLabelArray[2].setText(getPixelString());
        } else {
            this.mLabelArray[3].setText("pc");
            this.mLabelArray[2].setText(getPicaString());
        }
    }

    void clearUnusedPagesWithIndex(int nIndexPage) {
        if (nIndexPage >= 0) {
            for (int i = -2; i < 3; i++) {
                if (Math.abs(i) == 2) {
                    int nPage = nIndexPage + i;
                    if (nPage >= 0) {
                        RulerImageView rulerView = (RulerImageView) this.mUiLayout.findViewById(nPage + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                        if (rulerView != null) {
                            this.mUiLayout.removeView(rulerView);
                        }
                    }
                }
            }
        }
    }

    void clearPagesWithIndex(int nIndexPage) {
        if (nIndexPage >= 0) {
            for (int i = -1; i < 2; i++) {
                int nPage = nIndexPage + i;
                if (nPage >= 0) {
                    RulerImageView rulerView = (RulerImageView) this.mUiLayout.findViewById(nPage + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                    if (rulerView != null) {
                        this.mUiLayout.removeView(rulerView);
                    }
                }
            }
        }
    }

    void preparePagesWithIndex(int nIndexPage) {
        if (nIndexPage >= 0) {
            boolean bHFlipped = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false);
            for (int i = -2; i < 3; i++) {
                int nPage = nIndexPage + i;
                if (nPage >= 0) {
                    RulerImageView rulerView = (RulerImageView) this.mUiLayout.findViewById(nPage + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                    if (Math.abs(i) == 2) {
                        if (rulerView != null) {
                            this.mUiLayout.removeView(rulerView);
                        }
                    } else if (rulerView == null) {
                        int x;
                        if (bHFlipped) {
                            x = (-i) * mScreenWidth;
                        } else {
                            x = i * mScreenWidth;
                        }
                        rulerView = new RulerImageView(this);
                        rulerView.setId(nPage + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                        LayoutParams params = new LayoutParams(mScreenWidth, -1);
                        params.addRule(15);
                        params.addRule(9);
                        params.setMargins(x, this.mHeaderBarHeight, 0, this.mHeaderBarHeight);
                        rulerView.setLayoutParams(params);
                        rulerView.setRulerPage(nPage);
                        this.mUiLayout.addView(rulerView);
                    }
                }
            }
            this.mDitchView.bringToFront();
            this.mLineView.bringToFront();
            this.mHandView.bringToFront();
            if (this.mNumpadView != null) {
                this.mNumpadView.bringToFront();
            }
        }
    }

    void updateSettingsChanged() {
        this.mDPI = PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, getScreenDPI(this));
        for (int i = -1; i < 2; i++) {
            RulerImageView rulerView = (RulerImageView) this.mUiLayout.findViewById((this.mCurrentPage + i) + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
            if (rulerView != null) {
                rulerView.invalidate();
            }
        }
        updateLabels();
    }

    Rect getHandViewBounds() {
        int w = this.mHandView.getWidth();
        return new Rect(this.mCurrentX - (w / 2), this.mHeaderBarHeight, this.mCurrentX + (w / 2), mScreenHeight - this.mHeaderBarHeight);
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            this.velocityTracker = VelocityTracker.obtain();
            this.velocityTracker.addMovement(event);
            onTouchDown(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 1) {
            this.velocityTracker.addMovement(event);
            onTouchUp(v, new Point((int) event.getX(), (int) event.getY()));
            this.velocityTracker.recycle();
        } else if (event.getAction() == 2) {
            this.velocityTracker.addMovement(event);
            onTouchMove(v, new Point((int) event.getX(), (int) event.getY()));
        }
        return true;
    }

    protected void onTouchDown(View source, Point currentPosition) {
        Point pt = currentPosition;
        this.mIsMoved = false;
        this.mMoveEnabled = false;
        this.mDownPoint = pt;
        this.mLastMovePoint = pt;
        if (!mIsLocked && getHandViewBounds().contains(pt.x, pt.y)) {
            this.mMoveEnabled = true;
        }
    }

    protected void onTouchUp(View source, Point currentPosition) {
        if (!this.mIsMoved) {
            Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            if (this.mMoveEnabled) {
                boolean show;
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_SHOW_HAND_LINE_KEY, false)) {
                    show = false;
                } else {
                    show = true;
                }
                editor.putBoolean(SettingsKey.SETTINGS_RULER_SHOW_HAND_LINE_KEY, show);
                editor.apply();
                ViewHelper.setAlpha(this.mLineView, show ? TextTrackStyle.DEFAULT_FONT_SCALE : 0.0f);
                this.mUiLayout.invalidate();
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                    SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                }
            } else if (source.equals(this.mLabelArray[0])) {
                this.mEditIndex = 0;
                beginEditing();
            } else if (source.equals(this.mLabelArray[2])) {
                this.mEditIndex = 2;
                beginEditing();
            } else if (source.equals(this.mLabelArray[1])) {
                int nPrimaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, "0")) + 1;
                if (nPrimaryUnit > 3) {
                    nPrimaryUnit = 0;
                }
                editor.putString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, String.valueOf(nPrimaryUnit));
                editor.apply();
                updateSettingsChanged();
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                    SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                }
            } else if (source.equals(this.mLabelArray[3])) {
                int nSecondaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, "1")) + 1;
                if (nSecondaryUnit > 3) {
                    nSecondaryUnit = 0;
                }
                editor.putString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, String.valueOf(nSecondaryUnit));
                editor.apply();
                updateSettingsChanged();
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                    SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                }
            }
        } else if (!this.mMoveEnabled && !this.mTemporaryLock && this.velocityTracker != null) {
            this.velocityTracker.computeCurrentVelocity(GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
            float vx = this.velocityTracker.getXVelocity();
            if (Math.abs(vx) <= SWIPE_VELOCITY_THRESHOLD) {
                onSwipeLeft(false);
            } else if (vx < 0.0f) {
                onSwipeLeft(true);
            } else {
                onSwipeRight(true);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onTouchMove(View source, Point currentPosition) {
        Point pt = currentPosition;
        if (pt.x != this.mLastMovePoint.x) {
            if (!this.mIsMoved) {
                if (Math.sqrt((double) (((this.mDownPoint.x - pt.x) * (this.mDownPoint.x - pt.x)) + ((this.mDownPoint.y - pt.y) * (this.mDownPoint.y - pt.y)))) > ((double) ViewConfiguration.get(this).getScaledTouchSlop())) {
                    this.mIsMoved = true;
                } else {
                    return;
                }
            }
            if (this.mDownPoint.y >= this.mHeaderBarHeight) {
                if (this.mDownPoint.y <= mScreenHeight - this.mHeaderBarHeight && !mIsLocked) {
                    if (this.mMoveEnabled) {
                        float fLength;
                        float fLastLength;
                        String sSnapValue;
                        int nSnapId;
                        this.mLastMovePoint = pt;
                        this.mIsMoved = true;
                        this.mCurrentX = pt.x;
                        boolean bSnap = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_SNAPPING_KEY, false);
                        int nSnapUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY, "0"));
                        bSnap &= (Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, "1")) == nSnapUnit ? true : false) | (Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, "0")) == nSnapUnit ? true : false);
                        boolean hFlip = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false);
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
                        if (nSnapUnit == 0) {
                            fLength = getCentimeterLengthFromPageAndPoint(this.mCurrentPage, hFlip ? mScreenWidth - this.mCurrentX : this.mCurrentX);
                            fLastLength = getCentimeterLengthFromPageAndPoint(this.mCurrentPage, hFlip ? mScreenWidth - this.mLastX : this.mLastX);
                            sSnapValue = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_CENTIMETER_SNAP_KEY, "1");
                            nSnapId = 0;
                        } else if (nSnapUnit == 1) {
                            fLength = getInchLengthFromPageAndPoint(this.mCurrentPage, hFlip ? mScreenWidth - this.mCurrentX : this.mCurrentX);
                            fLastLength = getInchLengthFromPageAndPoint(this.mCurrentPage, hFlip ? mScreenWidth - this.mLastX : this.mLastX);
                            sSnapValue = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_INCH_SNAP_KEY, "0.25");
                            nSnapId = 1;
                        } else if (nSnapUnit == 3) {
                            fLength = getPicaLengthFromPageAndPoint(this.mCurrentPage, hFlip ? mScreenWidth - this.mCurrentX : this.mCurrentX);
                            fLastLength = getPicaLengthFromPageAndPoint(this.mCurrentPage, hFlip ? mScreenWidth - this.mLastX : this.mLastX);
                            sSnapValue = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PICA_SNAP_KEY, "0.25");
                            nSnapId = 3;
                        } else {
                            int i;
                            fLength = getPixelLengthFromPageAndPoint(this.mCurrentPage, hFlip ? mScreenWidth - this.mCurrentX : this.mCurrentX);
                            int i2 = this.mCurrentPage;
                            if (hFlip) {
                                i = mScreenWidth - this.mLastX;
                            } else {
                                i = this.mLastX;
                            }
                            fLastLength = getPixelLengthFromPageAndPoint(i2, i);
                            sSnapValue = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PIXEL_SNAP_KEY, "50");
                            nSnapId = 2;
                        }
                        float nStep;
                        float nDeg;
                        float[] fArr;
                        float fRad;
                        if (fLength > fLastLength) {
                            if (bSnap) {
                                nStep = Float.valueOf(sSnapValue).floatValue();
                                nDeg = (float) (Math.floor((double) (fLength / nStep)) * ((double) nStep));
                            } else {
                                fArr = new float[4];
                                nStep = new float[]{TextTrackStyle.DEFAULT_FONT_SCALE, 0.25f, 50.0f, 0.25f}[nSnapId];
                                nDeg = (float) (Math.floor((double) (fLength / nStep)) * ((double) nStep));
                            }
                            if (nDeg > fLastLength) {
                                this.mLastX = this.mCurrentX;
                                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                                    SoundUtility.getInstance().playSound(4, 0.1f);
                                }
                                if (bSnap) {
                                    if (nSnapUnit == 0) {
                                        fRad = centimeter2Pixel(nDeg);
                                    } else if (nSnapUnit == 1) {
                                        fRad = inch2Pixel(nDeg);
                                    } else if (nSnapUnit == 3) {
                                        fRad = pica2Pixel(nDeg);
                                    } else {
                                        fRad = nDeg;
                                    }
                                    this.mCurrentX = (int) (fRad - ((float) (mScreenWidth * ((int) (fRad / ((float) mScreenWidth))))));
                                    editor.putInt(SettingsKey.SETTINGS_RULER_CURRENT_X_KEY, this.mCurrentX);
                                    editor.apply();
                                    updateLabels();
                                    setHandViewPosition(this.mCurrentX, false);
                                }
                            }
                        } else if (fLength < fLastLength) {
                            if (bSnap) {
                                nStep = Float.valueOf(sSnapValue).floatValue();
                                nDeg = (float) (Math.ceil((double) (fLength / nStep)) * ((double) nStep));
                            } else {
                                fArr = new float[4];
                                nStep = new float[]{TextTrackStyle.DEFAULT_FONT_SCALE, 0.25f, 50.0f, 0.25f}[nSnapId];
                                nDeg = (float) (Math.ceil((double) (fLength / nStep)) * ((double) nStep));
                            }
                            if (nDeg < fLastLength) {
                                this.mLastX = this.mCurrentX;
                                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                                    SoundUtility.getInstance().playSound(4, 0.1f);
                                }
                                if (bSnap) {
                                    if (nSnapUnit == 0) {
                                        fRad = centimeter2Pixel(nDeg);
                                    } else if (nSnapUnit == 1) {
                                        fRad = inch2Pixel(nDeg);
                                    } else if (nSnapUnit == 3) {
                                        fRad = pica2Pixel(nDeg);
                                    } else {
                                        fRad = nDeg;
                                    }
                                    this.mCurrentX = (int) (fRad - ((float) (mScreenWidth * ((int) (fRad / ((float) mScreenWidth))))));
                                    editor.putInt(SettingsKey.SETTINGS_RULER_CURRENT_X_KEY, this.mCurrentX);
                                    editor.apply();
                                    updateLabels();
                                    setHandViewPosition(this.mCurrentX, false);
                                }
                            }
                        }
                        if (!bSnap) {
                            editor.putInt(SettingsKey.SETTINGS_RULER_CURRENT_X_KEY, this.mCurrentX);
                            editor.apply();
                            updateLabels();
                            setHandViewPosition(this.mCurrentX, false);
                        }
                    } else if (!this.mTemporaryLock) {
                        moveRulerByOffset(pt.x - this.mLastMovePoint.x);
                    }
                }
            }
        }
    }

    void onSwipeLeft(boolean swipeDone) {
        boolean flip = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false);
        if (!swipeDone || !flip || this.mCurrentPage > 0) {
            if (swipeDone) {
                this.mTemporaryLock = true;
            }
            int nIndexPage = this.mCurrentPage;
            AnimatorSet animSet = new AnimatorSet();
            animSet.addListener(this);
            animSet.setDuration(swipeDone ? 150 : 200);
            animSet.setStartDelay(0);
            Collection list = new ArrayList();
            for (int i = -2; i < 3; i++) {
                int nPage = nIndexPage + i;
                if (nPage >= 0) {
                    Object rulerView = (RulerImageView) this.mMainLayout.findViewById(nPage + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                    if (rulerView != null) {
                        int x;
                        int i2;
                        if (flip) {
                            x = (-i) * mScreenWidth;
                        } else {
                            x = i * mScreenWidth;
                        }
                        if (swipeDone) {
                            i2 = mScreenWidth;
                        } else {
                            i2 = 0;
                        }
                        x -= i2;
                        list.add(ObjectAnimator.ofFloat(rulerView, "x", (float) x));
                    }
                }
            }
            if (swipeDone) {
                if (!flip) {
                    this.mCurrentPage++;
                } else if (this.mCurrentPage > 0) {
                    this.mCurrentPage--;
                }
            }
            animSet.playTogether(list);
            animSet.start();
        }
    }

    void onSwipeRight(boolean swipeDone) {
        boolean flip = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false);
        if (flip || this.mCurrentPage > 0) {
            if (swipeDone) {
                this.mTemporaryLock = true;
            }
            int nIndexPage = this.mCurrentPage;
            AnimatorSet animSet = new AnimatorSet();
            animSet.addListener(this);
            animSet.setDuration(swipeDone ? 150 : 200);
            animSet.setStartDelay(0);
            Collection list = new ArrayList();
            for (int i = -2; i < 3; i++) {
                int nPage = nIndexPage + i;
                if (nPage >= 0) {
                    Object rulerView = (RulerImageView) this.mMainLayout.findViewById(nPage + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                    if (rulerView != null) {
                        int x;
                        int i2;
                        if (flip) {
                            x = (-i) * mScreenWidth;
                        } else {
                            x = i * mScreenWidth;
                        }
                        if (swipeDone) {
                            i2 = mScreenWidth;
                        } else {
                            i2 = 0;
                        }
                        x += i2;
                        list.add(ObjectAnimator.ofFloat(rulerView, "x", (float) x));
                    }
                }
            }
            if (swipeDone) {
                if (flip) {
                    this.mCurrentPage++;
                } else if (this.mCurrentPage > 0) {
                    this.mCurrentPage--;
                }
            }
            animSet.playTogether(list);
            animSet.start();
        }
    }

    private void moveRulerByOffset(int offset) {
        boolean flip = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false);
        int nIndexPage = this.mCurrentPage;
        int i = -2;
        while (i < 3) {
            int nPage = nIndexPage + i;
            if (nPage >= 0) {
                RulerImageView rulerView = (RulerImageView) this.mMainLayout.findViewById(nPage + GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE);
                if (rulerView != null) {
                    float x;
                    if (flip) {
                        x = (float) ((-i) * mScreenWidth);
                    } else {
                        x = (float) (mScreenWidth * i);
                    }
                    x += (float) offset;
                    if (flip) {
                        if (this.mCurrentPage == 0 && i == 0 && x < 0.0f) {
                            x = 0.0f;
                        }
                    } else if (this.mCurrentPage == 0 && i == 0 && x > 0.0f) {
                        x = 0.0f;
                    }
                    ViewHelper.setX(rulerView, x);
                }
            }
            i++;
        }
    }

    private void setCenterX(View v, int x) {
        int xx;
        if (v.getLayoutParams() != null) {
            xx = x - (v.getLayoutParams().width / 2);
        } else {
            xx = x - (v.getWidth() / 2);
        }
        ViewHelper.setX(v, (float) xx);
    }

    void setHandViewPosition(int x, boolean animate) {
        if (animate) {
            int x1;
            int x2;
            if (this.mHandView.getLayoutParams() != null) {
                x1 = x - (this.mHandView.getLayoutParams().width / 2);
            } else {
                x1 = x - (this.mHandView.getWidth() / 2);
            }
            if (this.mLineView.getLayoutParams() != null) {
                x2 = x - (this.mLineView.getLayoutParams().width / 2);
            } else {
                x2 = x - (this.mLineView.getWidth() / 2);
            }
            ObjectAnimator a1 = ObjectAnimator.ofFloat(this.mHandView, "x", (float) x1);
            ObjectAnimator a2 = ObjectAnimator.ofFloat(this.mLineView, "x", (float) x2);
            AnimatorSet animSet = new AnimatorSet();
            animSet.addListener(this);
            animSet.setDuration(500);
            animSet.setStartDelay(0);
            animSet.playTogether(a1, a2);
            animSet.start();
            return;
        }
        setCenterX(this.mHandView, x);
        setCenterX(this.mLineView, x);
    }

    float centimeter2Inch(float cm) {
        return cm / 2.54f;
    }

    float centimeter2Pixel(float cm) {
        return (this.mDPI * cm) / 2.54f;
    }

    float inch2Centimeter(float inch) {
        return 2.54f * inch;
    }

    float inch2Pixel(float inch) {
        return this.mDPI * inch;
    }

    float pixel2Inch(float pixel) {
        return pixel / this.mDPI;
    }

    float pixel2Centimeter(float pixel) {
        return (2.54f * pixel) / this.mDPI;
    }

    float pixel2Pica(float pixel) {
        return (6.0f * pixel) / this.mDPI;
    }

    float pica2Pixel(float pica) {
        return (this.mDPI * pica) / 6.0f;
    }

    float getCentimeterLengthFromPageAndPoint(int nPage, int nPoint) {
        return pixel2Centimeter((float) ((mScreenWidth * nPage) + nPoint));
    }

    float getInchLengthFromPageAndPoint(int nPage, int nPoint) {
        return pixel2Inch((float) ((mScreenWidth * nPage) + nPoint));
    }

    float getPixelLengthFromPageAndPoint(int nPage, int nPoint) {
        return (float) ((mScreenWidth * nPage) + nPoint);
    }

    float getPicaLengthFromPageAndPoint(int nPage, int nPoint) {
        return pixel2Pica((float) ((mScreenWidth * nPage) + nPoint));
    }

    void beginEditing() {
        if (this.mNumpadView == null) {
            this.mIsVirginEdit = true;
            this.mNumpadView = new NumpadView(this, 2, mScreenWidth, this.mUiLayout.getHeight(), this.mHeaderBarHeight, this.mHeaderBarHeight);
            this.mNumpadView.setListener(this);
            this.mNumpadView.setLayoutParams(new LayoutParams(-1, -1));
            this.mUiLayout.addView(this.mNumpadView);
            showNumpadView(true);
            this.mLabelArray[this.mEditIndex].setTextColor(-1);
            View backgroundView = new View(this);
            backgroundView.setId(7);
            backgroundView.setBackgroundColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            LayoutParams params = new LayoutParams(-2, -2);
            params.addRule(6, this.mLabelArray[this.mEditIndex].getId());
            params.addRule(0, this.mLabelArray[this.mEditIndex + 1].getId());
            params.addRule(5, this.mLEDScreen[this.mEditIndex / 2].getId());
            params.addRule(8, this.mLabelArray[this.mEditIndex].getId());
            params.setMargins(10, 3, 0, 6);
            backgroundView.setLayoutParams(params);
            this.mUiLayout.addView(backgroundView);
            this.mLabelArray[this.mEditIndex].bringToFront();
            this.mNumpadView.bringToFront();
        }
    }

    void endEditing() {
        int nUnit;
        showNumpadView(false);
        this.mLabelArray[this.mEditIndex].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        View backgroundView = this.mUiLayout.findViewById(7);
        if (backgroundView != null) {
            this.mUiLayout.removeView(backgroundView);
        }
        if (this.mEditIndex == 0) {
            nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, "0"));
        } else {
            nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, "1"));
        }
        float fVal = 0.0f;
        try {
            fVal = Float.parseFloat(this.mLabelArray[this.mEditIndex].getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (nUnit == 0) {
            if (fVal < 0.0f) {
                fVal = 0.0f;
            } else if (fVal > 25400.0f) {
                fVal = 25400.0f;
            }
            fVal = centimeter2Pixel(fVal);
        } else if (nUnit == 1) {
            if (fVal < 0.0f) {
                fVal = 0.0f;
            } else if (fVal > 10000.0f) {
                fVal = 10000.0f;
            }
            fVal = inch2Pixel(fVal);
        } else if (nUnit == 3) {
            if (fVal < 0.0f) {
                fVal = 0.0f;
            } else if (fVal > 10000.0f) {
                fVal = 10000.0f;
            }
            fVal = pica2Pixel(fVal);
        } else if (fVal < 0.0f) {
            fVal = 0.0f;
        } else if (fVal > this.mDPI * 10000.0f) {
            fVal = 10000.0f * this.mDPI;
        }
        clearPagesWithIndex(this.mCurrentPage);
        this.mCurrentPage = (int) (fVal / ((float) mScreenWidth));
        boolean bHFlipped = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false);
        this.mCurrentX = (int) (fVal - ((float) (this.mCurrentPage * mScreenWidth)));
        if (bHFlipped) {
            this.mCurrentX = mScreenWidth - this.mCurrentX;
        }
        preparePagesWithIndex(this.mCurrentPage);
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putInt(SettingsKey.SETTINGS_RULER_CURRENT_X_KEY, this.mCurrentX);
        editor.putInt(SettingsKey.SETTINGS_RULER_CURRENT_PAGE_KEY, this.mCurrentPage);
        editor.apply();
        updateLabels();
        setHandViewPosition(this.mCurrentX, true);
    }

    void showNumpadView(boolean show) {
        float f;
        if (show) {
            f = (float) (-mScreenWidth);
        } else {
            f = 0.0f;
        }
        TranslateAnimation anim = new TranslateAnimation(f, show ? 0.0f : (float) (-mScreenWidth), 0.0f, 0.0f);
        anim.setDuration(300);
        anim.setFillBefore(true);
        anim.setFillAfter(true);
        if (!show) {
            anim.setAnimationListener(this);
        }
        this.mNumpadView.startAnimation(anim);
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(3, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
    }

    public void onNumpadEvent(String sKey) {
        if (sKey == "Done") {
            endEditing();
            return;
        }
        if (this.mIsVirginEdit) {
            this.mIsVirginEdit = false;
            onNumpadEvent("C");
        }
        String editText = BuildConfig.FLAVOR;
        if (sKey == "C") {
            this.mLabelArray[this.mEditIndex].setText("0");
        } else {
            editText = appendString(sKey, this.mLabelArray[this.mEditIndex].getText().toString());
        }
        this.mLabelArray[this.mEditIndex].setText(editText);
    }

    String appendString(String sAppend, String sEdit) {
        if (this.mIsVirginEdit) {
            return BuildConfig.FLAVOR;
        }
        if (sAppend == ".") {
            if (sEdit.length() == 0) {
                this.mIsVirginEdit = false;
                return "0.";
            } else if (sEdit.indexOf(".") != -1) {
                return sEdit;
            }
        } else if (sEdit == "0") {
            return sAppend;
        }
        if (!(sAppend == "." || sEdit == BuildConfig.FLAVOR)) {
            int nUnit;
            if (this.mEditIndex == 0) {
                nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, "0"));
            } else {
                nUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, "1"));
            }
            int pos = sEdit.indexOf(".");
            if (pos != -1) {
                int nCount = (sEdit.length() - pos) - 1;
                if (nUnit == 2) {
                    if (nCount >= 0) {
                        return sEdit;
                    }
                } else if (nCount >= 2) {
                    return sEdit;
                }
            }
            int count = 0;
            int i = 0;
            while (i < sEdit.length() && sEdit.charAt(i) >= '0' && sEdit.charAt(i) <= '9') {
                count++;
                i++;
            }
            if (count >= 9 && sEdit.indexOf(".") == -1) {
                return sEdit;
            }
        }
        sEdit = sEdit.concat(sAppend);
        this.mIsVirginEdit = false;
        return sEdit;
    }

    public void onAnimationEnd(Animation animation) {
        if (animation instanceof TranslateAnimation) {
            this.mNumpadView.post(new Runnable() {
                public void run() {
                    if (RulerActivity.this.mNumpadView.getParent() != null) {
                        ((RelativeLayout) RulerActivity.this.mNumpadView.getParent()).removeView(RulerActivity.this.mNumpadView);
                    }
                    RulerActivity.this.mNumpadView = null;
                }
            });
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
        clearUnusedPagesWithIndex(this.mCurrentPage);
        preparePagesWithIndex(this.mCurrentPage);
        updateLabels();
        this.mTemporaryLock = false;
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }

    public void onBackPressed() {
        if (this.mNumpadView != null) {
            endEditing();
        } else {
            super.onBackPressed();
        }
    }

    public class Define {
        public static final int BUTTON_FLIP = 5;
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_LOCK = 3;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_RESET = 6;
        public static final int BUTTON_SWAP = 4;
        public static final int BUTTON_UPGRADE = 0;
        public static final int EDIT_BACKGROUND_VIEW = 7;
        public static final int LABEL00 = 0;
        public static final int LABEL01 = 1;
        public static final int LABEL10 = 2;
        public static final int LABEL11 = 3;
        public static final int LED_FRAME0 = 0;
        public static final int LED_FRAME1 = 1;
        public static final int LENGTH_UNIT_CENTIMETER = 0;
        public static final int LENGTH_UNIT_COUNT = 4;
        public static final int LENGTH_UNIT_INCH = 1;
        public static final int LENGTH_UNIT_PICA = 3;
        public static final int LENGTH_UNIT_PIXEL = 2;
        public static final int MIN_PAGE_ID = 1000;
    }
}
