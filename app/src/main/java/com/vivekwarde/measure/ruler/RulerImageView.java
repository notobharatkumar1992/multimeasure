package com.vivekwarde.measure.ruler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;

import java.util.Locale;

public class RulerImageView extends RelativeLayout {
    private static Bitmap mBackgroundBitmap;

    static {
        mBackgroundBitmap = null;
    }

    final String tag;
    int mH2;
    int mH3;
    int mH4;
    int mINH1;
    int mLineTextDelta;
    int mMMH1;
    int mOffsetY;
    int mPCH1;
    int mPXH1;
    private Paint mLinePaint;
    private Rect mNumBounds;
    private String mNumberString;
    private Path mPath;
    private int mRulerPage;
    private int mStripeHeight;
    private Paint mStripePaint;
    private TextPaint mTextPaint;

    public RulerImageView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mRulerPage = 0;
        this.mTextPaint = null;
        this.mPath = new Path();
        this.mNumBounds = new Rect();
        this.mLinePaint = null;
        this.mStripePaint = null;
        this.mStripeHeight = 0;
        this.mLineTextDelta = 7;
        this.mOffsetY = 17;
        this.mMMH1 = 20;
        this.mINH1 = 17;
        this.mPXH1 = 13;
        this.mPCH1 = 17;
        this.mH2 = 30;
        this.mH3 = 40;
        this.mH4 = 53;
        setWillNotDraw(false);
        setBackgroundColor(0);
        this.mLineTextDelta = (int) getResources().getDimension(R.dimen.RULER_LINE_TEXT_DELTA);
        this.mOffsetY = (int) getResources().getDimension(R.dimen.RULER_OFFSET_Y_DELTA);
        this.mMMH1 = (int) getResources().getDimension(R.dimen.RULER_MM_H1_DELTA);
        this.mINH1 = (int) getResources().getDimension(R.dimen.RULER_IN_H1_DELTA);
        this.mPXH1 = (int) getResources().getDimension(R.dimen.RULER_PX_H1_DELTA);
        this.mPCH1 = (int) getResources().getDimension(R.dimen.RULER_PC_H1_DELTA);
        this.mH2 = (int) getResources().getDimension(R.dimen.RULER_H2_DELTA);
        this.mH3 = (int) getResources().getDimension(R.dimen.RULER_H3_DELTA);
        this.mH4 = (int) getResources().getDimension(R.dimen.RULER_H4_DELTA);
        this.mRulerPage = 0;
        this.mLinePaint = new Paint();
        this.mLinePaint.setColor(ContextCompat.getColor(getContext(), R.color.RULER_PROTRACTOR_LINE_COLOR));
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        this.mLinePaint.setStrokeWidth(metrics.density + 0.5f);
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.RULER_PROTRACTOR_NUMBER_COLOR));
        this.mTextPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Eurostile LT Medium.ttf"));
        this.mTextPaint.setTextAlign(Align.CENTER);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.RULER_FONT_SIZE));
        this.mStripePaint = new Paint();
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.tile_hole)).getBitmap();
        this.mStripeHeight = bitmap.getHeight();
        this.mStripePaint.setShader(new BitmapShader(bitmap, TileMode.REPEAT, TileMode.REPEAT));
        this.mPath = new Path();
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (!((w == oldw && h == oldh) || (mBackgroundBitmap != null && mBackgroundBitmap.getWidth() == w && mBackgroundBitmap.getHeight() == h))) {
            mBackgroundBitmap = ImageUtility.scale9Bitmap(((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.tile_base)).getBitmap(), w, h);
        }
        super.onSizeChanged(w, h, oldw, oldh);
    }

    void setRulerPage(int nPage) {
        this.mRulerPage = nPage;
        invalidate();
    }

    void drawMillimeterRuler(Canvas canvas, boolean bVFlip, boolean bHFlip) {
        float fPixelPerMillimeter = PreferenceManager.getDefaultSharedPreferences(getContext()).getFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, RulerActivity.getScreenDPI(getContext())) / 25.4f;
        float fMillimeter = ((float) (this.mRulerPage * getWidth())) / fPixelPerMillimeter;
        float fCentimeter = (float) Math.floor(((double) fMillimeter) / 10.0d);
        float fOddMillimeter = fMillimeter - (10.0f * fCentimeter);
        float fOffsetY = (float) this.mOffsetY;
        int H1 = this.mMMH1;
        int H2 = this.mH2;
        int H3 = this.mH3;
        int textSize = (int) getResources().getDimension(R.dimen.RULER_FONT_SIZE);
        int smallTextSize = (int) getResources().getDimension(R.dimen.RULER_SMALL_FONT_SIZE);
        float fX;
        int nCentimeter;
        int maxLen;
        int i;
        int h;
        float x;
        TextPaint textPaint;
        float f;
        float fyy;
        if (bHFlip) {
            fX = (-fOddMillimeter) * fPixelPerMillimeter;
            nCentimeter = (int) fCentimeter;
            maxLen = getWidth() + 100;
            i = 0;
            while ((((float) i) * fPixelPerMillimeter) + fX <= ((float) maxLen)) {
                if (bVFlip) {
                    h = H1;
                    if (i % 10 == 0) {
                        h = H3;
                        if (!(i == 0 && this.mRulerPage == 0) && (nCentimeter <= 10000 || (nCentimeter > 10000 && i % 20 == 0))) {
                            x = ((float) getWidth()) - ((((float) i) * fPixelPerMillimeter) + fX);
                            textPaint = this.mTextPaint;
                            if (nCentimeter >= 1000) {
                                f = (float) smallTextSize;
                            } else {
                                f = (float) textSize;
                            }
                            textPaint.setTextSize(f);
                            this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nCentimeter)});
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) (getHeight() - h)) - fOffsetY) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                        nCentimeter++;
                        if (i == 0 && this.mRulerPage == 0) {
                            x = (float) getWidth();
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = "cm";
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                    } else if (i % 5 == 0) {
                        h = H2;
                    }
                    canvas.drawLine(((float) getWidth()) - ((((float) i) * fPixelPerMillimeter) + fX), ((float) (getHeight() + 0)) - fOffsetY, ((float) getWidth()) - ((((float) i) * fPixelPerMillimeter) + fX), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
                } else {
                    h = H1;
                    if (i % 10 == 0) {
                        h = H3;
                        if (!(i == 0 && this.mRulerPage == 0) && (nCentimeter <= 10000 || (nCentimeter > 10000 && i % 20 == 0))) {
                            x = ((float) getWidth()) - ((((float) i) * fPixelPerMillimeter) + fX);
                            textPaint = this.mTextPaint;
                            if (nCentimeter >= 1000) {
                                f = (float) smallTextSize;
                            } else {
                                f = (float) textSize;
                            }
                            textPaint.setTextSize(f);
                            this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nCentimeter)});
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                        nCentimeter++;
                        if (i == 0 && this.mRulerPage == 0) {
                            x = (float) getWidth();
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = "cm";
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                    } else if (i % 5 == 0) {
                        h = H2;
                    }
                    canvas.drawLine(((float) getWidth()) - ((((float) i) * fPixelPerMillimeter) + fX), 0.0f + fOffsetY, ((float) getWidth()) - ((((float) i) * fPixelPerMillimeter) + fX), ((float) h) + fOffsetY, this.mLinePaint);
                }
                i++;
            }
            return;
        }
        fX = (-fOddMillimeter) * fPixelPerMillimeter;
        nCentimeter = (int) fCentimeter;
        maxLen = getWidth() + 100;
        i = 0;
        while ((((float) i) * fPixelPerMillimeter) + fX <= ((float) maxLen)) {
            if (bVFlip) {
                h = H1;
                if (i % 10 == 0) {
                    h = H3;
                    if (!(i == 0 && this.mRulerPage == 0) && (nCentimeter <= 10000 || (nCentimeter > 10000 && i % 20 == 0))) {
                        x = fX + (((float) i) * fPixelPerMillimeter);
                        textPaint = this.mTextPaint;
                        if (nCentimeter >= 1000) {
                            f = (float) smallTextSize;
                        } else {
                            f = (float) textSize;
                        }
                        textPaint.setTextSize(f);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nCentimeter)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    nCentimeter++;
                    if (i == 0 && this.mRulerPage == 0) {
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "cm";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(0.0f, fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 5 == 0) {
                    h = H2;
                }
                canvas.drawLine(fX + (((float) i) * fPixelPerMillimeter), ((float) (getHeight() + 0)) - fOffsetY, fX + (((float) i) * fPixelPerMillimeter), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
            } else {
                h = H1;
                if (i % 10 == 0) {
                    h = H3;
                    if (!(i == 0 && this.mRulerPage == 0) && (nCentimeter <= 10000 || (nCentimeter > 10000 && i % 20 == 0))) {
                        x = fX + (((float) i) * fPixelPerMillimeter);
                        textPaint = this.mTextPaint;
                        if (nCentimeter >= 1000) {
                            f = (float) smallTextSize;
                        } else {
                            f = (float) textSize;
                        }
                        textPaint.setTextSize(f);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nCentimeter)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    nCentimeter++;
                    if (i == 0 && this.mRulerPage == 0) {
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "cm";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(0.0f, fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 5 == 0) {
                    h = H2;
                }
                canvas.drawLine(fX + (((float) i) * fPixelPerMillimeter), 0.0f + fOffsetY, fX + (((float) i) * fPixelPerMillimeter), ((float) h) + fOffsetY, this.mLinePaint);
            }
            i++;
        }
    }

    void drawInchRuler(Canvas canvas, boolean bVFlip, boolean bHFlip) {
        float fPixelPerInch16 = PreferenceManager.getDefaultSharedPreferences(getContext()).getFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, RulerActivity.getScreenDPI(getContext())) / 16.0f;
        float fInch16 = ((float) (this.mRulerPage * getWidth())) / fPixelPerInch16;
        float fInch = (float) Math.floor(((double) fInch16) / 16.0d);
        float fOddInch16 = fInch16 - (16.0f * fInch);
        float fOffsetY = (float) this.mOffsetY;
        int H1 = this.mINH1;
        int H2 = this.mH2;
        int H3 = this.mH3;
        int H4 = this.mH4;
        int textSize = (int) getResources().getDimension(R.dimen.RULER_FONT_SIZE);
        int maxLen = getWidth() + 100;
        float fX;
        int nInch;
        int i;
        int h;
        float x;
        float fyy;
        if (bHFlip) {
            fX = (-fOddInch16) * fPixelPerInch16;
            nInch = (int) fInch;
            if (bVFlip) {
                for (i = 0; (((float) i) * fPixelPerInch16) + fX <= ((float) maxLen); i++) {
                    h = H1;
                    if (i % 16 == 0) {
                        h = H4;
                        if (!(i == 0 && this.mRulerPage == 0)) {
                            x = ((float) getWidth()) - ((((float) i) * fPixelPerInch16) + fX);
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nInch)});
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                        nInch++;
                        if (i == 0 && this.mRulerPage == 0) {
                            x = (float) getWidth();
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = "inch";
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                    } else if (i % 8 == 0) {
                        h = H3;
                    } else if (i % 2 == 0) {
                        h = H2;
                    }
                    canvas.drawLine(((float) getWidth()) - ((((float) i) * fPixelPerInch16) + fX), ((float) (getHeight() + 0)) - fOffsetY, ((float) getWidth()) - ((((float) i) * fPixelPerInch16) + fX), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
                }
                canvas.drawLine(0.0f, ((float) (getHeight() + 0)) - fOffsetY, (float) getWidth(), ((float) (getHeight() + 0)) - fOffsetY, this.mLinePaint);
                canvas.drawLine(0.0f, ((float) (getHeight() - H1)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H1)) - fOffsetY, this.mLinePaint);
                canvas.drawLine(0.0f, ((float) (getHeight() - H2)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H2)) - fOffsetY, this.mLinePaint);
                return;
            }
            for (i = 0; (((float) i) * fPixelPerInch16) + fX <= ((float) maxLen); i++) {
                h = H1;
                if (i % 16 == 0) {
                    h = H4;
                    if (!(i == 0 && this.mRulerPage == 0)) {
                        x = ((float) getWidth()) - ((((float) i) * fPixelPerInch16) + fX);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nInch)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    nInch++;
                    if (i == 0 && this.mRulerPage == 0) {
                        x = (float) getWidth();
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "inch";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 8 == 0) {
                    h = H3;
                } else if (i % 2 == 0) {
                    h = H2;
                }
                canvas.drawLine(((float) getWidth()) - ((((float) i) * fPixelPerInch16) + fX), 0.0f + fOffsetY, ((float) getWidth()) - ((((float) i) * fPixelPerInch16) + fX), ((float) h) + fOffsetY, this.mLinePaint);
            }
            canvas.drawLine(0.0f, 0.0f + fOffsetY, (float) getWidth(), 0.0f + fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) H1) + fOffsetY, (float) getWidth(), ((float) H1) + fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) H2) + fOffsetY, (float) getWidth(), ((float) H2) + fOffsetY, this.mLinePaint);
            return;
        }
        fX = (-fOddInch16) * fPixelPerInch16;
        nInch = (int) fInch;
        if (bVFlip) {
            for (i = 0; (((float) i) * fPixelPerInch16) + fX <= ((float) maxLen); i++) {
                h = H1;
                if (i % 16 == 0) {
                    h = H4;
                    if (!(i == 0 && this.mRulerPage == 0)) {
                        x = fX + (((float) i) * fPixelPerInch16);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nInch)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    nInch++;
                    if (i == 0 && this.mRulerPage == 0) {
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "inch";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(0.0f, fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 8 == 0) {
                    h = H3;
                } else if (i % 2 == 0) {
                    h = H2;
                }
                canvas.drawLine(fX + (((float) i) * fPixelPerInch16), ((float) (getHeight() + 0)) - fOffsetY, fX + (((float) i) * fPixelPerInch16), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
            }
            canvas.drawLine(0.0f, ((float) (getHeight() + 0)) - fOffsetY, (float) getWidth(), ((float) (getHeight() + 0)) - fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) (getHeight() - H1)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H1)) - fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) (getHeight() - H2)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H2)) - fOffsetY, this.mLinePaint);
            return;
        }
        for (i = 0; (((float) i) * fPixelPerInch16) + fX <= ((float) maxLen); i++) {
            h = H1;
            if (i % 16 == 0) {
                h = H4;
                if (!(i == 0 && this.mRulerPage == 0)) {
                    x = fX + (((float) i) * fPixelPerInch16);
                    this.mTextPaint.setTextSize((float) textSize);
                    this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nInch)});
                    this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                    fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                    this.mPath.reset();
                    this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                    this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                    canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
                nInch++;
                if (i == 0 && this.mRulerPage == 0) {
                    this.mTextPaint.setTextSize((float) textSize);
                    this.mNumberString = "inch";
                    this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                    fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                    this.mPath.reset();
                    this.mPath.moveTo(0.0f, fyy);
                    this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                    canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
            } else if (i % 8 == 0) {
                h = H3;
            } else if (i % 2 == 0) {
                h = H2;
            }
            canvas.drawLine(fX + (((float) i) * fPixelPerInch16), 0.0f + fOffsetY, fX + (((float) i) * fPixelPerInch16), ((float) h) + fOffsetY, this.mLinePaint);
        }
        canvas.drawLine(0.0f, 0.0f + fOffsetY, (float) getWidth(), 0.0f + fOffsetY, this.mLinePaint);
        canvas.drawLine(0.0f, ((float) H1) + fOffsetY, (float) getWidth(), ((float) H1) + fOffsetY, this.mLinePaint);
        canvas.drawLine(0.0f, ((float) H2) + fOffsetY, (float) getWidth(), ((float) H2) + fOffsetY, this.mLinePaint);
    }

    void drawPixelRuler(Canvas canvas, boolean bVFlip, boolean bHFlip) {
        float fPixel2 = ((float) (this.mRulerPage * getWidth())) / 2.0f;
        float fPixel100 = (float) Math.floor(((double) fPixel2) / 50.0d);
        float fOddPixel = fPixel2 - (50.0f * fPixel100);
        float fOffsetY = (float) this.mOffsetY;
        int H1 = this.mPXH1;
        int H2 = this.mH2;
        int H3 = this.mH3;
        int textSize = (int) getResources().getDimension(R.dimen.RULER_FONT_SIZE);
        int maxLen = getWidth() + 100;
        float fX;
        int nPixel100;
        int i;
        int h;
        float x;
        float fyy;
        if (bHFlip) {
            fX = (-fOddPixel) * 2.0f;
            nPixel100 = (int) fPixel100;
            i = 0;
            while ((((float) i) * 2.0f) + fX <= ((float) maxLen)) {
                if (bVFlip) {
                    h = H1;
                    if (i % 25 == 0) {
                        h = H3;
                        if (!(i == 0 && this.mRulerPage == 0) && i % 50 == 0) {
                            x = ((float) getWidth()) - ((((float) i) * 2.0f) + fX);
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nPixel100 * 100)});
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) (getHeight() - h)) - fOffsetY) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                        if (i % 50 == 0) {
                            nPixel100++;
                        }
                        if (i == 0 && this.mRulerPage == 0) {
                            x = (float) getWidth();
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = "px";
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                    } else if (i % 5 == 0) {
                        h = H2;
                    }
                    canvas.drawLine(((float) getWidth()) - ((((float) i) * 2.0f) + fX), ((float) (getHeight() + 0)) - fOffsetY, ((float) getWidth()) - ((((float) i) * 2.0f) + fX), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
                } else {
                    h = H1;
                    if (i % 25 == 0) {
                        h = H3;
                        if (!(i == 0 && this.mRulerPage == 0) && i % 50 == 0) {
                            x = ((float) getWidth()) - ((((float) i) * 2.0f) + fX);
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nPixel100 * 100)});
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                        if (i % 50 == 0) {
                            nPixel100++;
                        }
                        if (i == 0 && this.mRulerPage == 0) {
                            x = (float) getWidth();
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = "px";
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                    } else if (i % 5 == 0) {
                        h = H2;
                    }
                    canvas.drawLine(((float) getWidth()) - ((((float) i) * 2.0f) + fX), 0.0f + fOffsetY, ((float) getWidth()) - ((((float) i) * 2.0f) + fX), ((float) h) + fOffsetY, this.mLinePaint);
                }
                i++;
            }
            return;
        }
        fX = (-fOddPixel) * 2.0f;
        nPixel100 = (int) fPixel100;
        i = 0;
        while ((((float) i) * 2.0f) + fX <= ((float) maxLen)) {
            if (bVFlip) {
                h = H1;
                if (i % 25 == 0) {
                    h = H3;
                    if (!(i == 0 && this.mRulerPage == 0) && i % 50 == 0) {
                        x = fX + (((float) i) * 2.0f);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nPixel100 * 100)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    if (i % 50 == 0) {
                        nPixel100++;
                    }
                    if (i == 0 && this.mRulerPage == 0) {
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "px";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(0.0f, fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 5 == 0) {
                    h = H2;
                }
                canvas.drawLine(fX + (((float) i) * 2.0f), ((float) (getHeight() + 0)) - fOffsetY, fX + (((float) i) * 2.0f), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
            } else {
                h = H1;
                if (i % 25 == 0) {
                    h = H3;
                    if (!(i == 0 && this.mRulerPage == 0) && i % 50 == 0) {
                        x = fX + (((float) i) * 2.0f);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(nPixel100 * 100)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    if (i % 50 == 0) {
                        nPixel100++;
                    }
                    if (i == 0 && this.mRulerPage == 0) {
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "px";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(0.0f, fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 5 == 0) {
                    h = H2;
                }
                canvas.drawLine(fX + (((float) i) * 2.0f), 0.0f + fOffsetY, fX + (((float) i) * 2.0f), ((float) h) + fOffsetY, this.mLinePaint);
            }
            i++;
        }
    }

    void drawPicaRuler(Canvas canvas, boolean bVFlip, boolean bHFlip) {
        float fPixelPerPica2 = PreferenceManager.getDefaultSharedPreferences(getContext()).getFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, RulerActivity.getScreenDPI(getContext())) / 12.0f;
        float fPica2 = ((float) (this.mRulerPage * getWidth())) / fPixelPerPica2;
        float f10Pica = (float) Math.floor(((double) fPica2) / 20.0d);
        float fOddPica2 = fPica2 - (20.0f * f10Pica);
        float fOffsetY = (float) this.mOffsetY;
        int H1 = this.mPCH1;
        int H2 = this.mH2;
        int H3 = this.mH3;
        int H4 = this.mH4;
        int textSize = (int) getResources().getDimension(R.dimen.RULER_FONT_SIZE);
        int maxLen = getWidth() + 100;
        float fX;
        int n10Pica;
        int i;
        int h;
        float x;
        float fyy;
        if (bHFlip) {
            fX = (-fOddPica2) * fPixelPerPica2;
            n10Pica = (int) f10Pica;
            if (bVFlip) {
                for (i = 0; (((float) i) * fPixelPerPica2) + fX <= ((float) maxLen); i++) {
                    h = H1;
                    if (i % 20 == 0) {
                        h = H4;
                        if (!(i == 0 && this.mRulerPage == 0)) {
                            x = ((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX);
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(n10Pica * 10)});
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) (getHeight() - h)) - fOffsetY) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                        n10Pica++;
                        if (i == 0 && this.mRulerPage == 0) {
                            x = (float) getWidth();
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = "pica";
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                    } else if (i % 10 == 0) {
                        h = H3;
                        if (i != 0 || this.mRulerPage != 0) {
                            x = ((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX);
                            this.mTextPaint.setTextSize((float) textSize);
                            this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf((n10Pica * 10) - 5)});
                            this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                            fyy = ((((float) getHeight()) - ((float) H4)) - fOffsetY) - ((float) this.mLineTextDelta);
                            this.mPath.reset();
                            this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                            this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                            canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                        }
                    } else if (i % 2 == 0) {
                        h = H2;
                    }
                    canvas.drawLine(((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX), ((float) (getHeight() + 0)) - fOffsetY, ((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
                }
                canvas.drawLine(0.0f, ((float) (getHeight() + 0)) - fOffsetY, (float) getWidth(), ((float) (getHeight() + 0)) - fOffsetY, this.mLinePaint);
                canvas.drawLine(0.0f, ((float) (getHeight() - H1)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H1)) - fOffsetY, this.mLinePaint);
                canvas.drawLine(0.0f, ((float) (getHeight() - H2)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H2)) - fOffsetY, this.mLinePaint);
                return;
            }
            for (i = 0; (((float) i) * fPixelPerPica2) + fX <= ((float) maxLen); i++) {
                h = H1;
                if (i % 20 == 0) {
                    h = H4;
                    if (!(i == 0 && this.mRulerPage == 0)) {
                        x = ((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(n10Pica * 10)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    n10Pica++;
                    if (i == 0 && this.mRulerPage == 0) {
                        x = (float) getWidth();
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "pica";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 10 == 0) {
                    h = H3;
                    if (i != 0 || this.mRulerPage != 0) {
                        x = ((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf((n10Pica * 10) - 5)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = ((((float) H4) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 2 == 0) {
                    h = H2;
                }
                canvas.drawLine(((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX), 0.0f + fOffsetY, ((float) getWidth()) - ((((float) i) * fPixelPerPica2) + fX), ((float) h) + fOffsetY, this.mLinePaint);
            }
            canvas.drawLine(0.0f, 0.0f + fOffsetY, (float) getWidth(), 0.0f + fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) H1) + fOffsetY, (float) getWidth(), ((float) H1) + fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) H2) + fOffsetY, (float) getWidth(), ((float) H2) + fOffsetY, this.mLinePaint);
            return;
        }
        fX = (-fOddPica2) * fPixelPerPica2;
        n10Pica = (int) f10Pica;
        if (bVFlip) {
            for (i = 0; (((float) i) * fPixelPerPica2) + fX <= ((float) maxLen); i++) {
                h = H1;
                if (i % 20 == 0) {
                    h = H4;
                    if (!(i == 0 && this.mRulerPage == 0)) {
                        x = fX + (((float) i) * fPixelPerPica2);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(n10Pica * 10)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                    n10Pica++;
                    if (i == 0 && this.mRulerPage == 0) {
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = "pica";
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) h) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(0.0f, fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 10 == 0) {
                    h = H3;
                    if (i != 0 || this.mRulerPage != 0) {
                        x = fX + (((float) i) * fPixelPerPica2);
                        this.mTextPaint.setTextSize((float) textSize);
                        this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf((n10Pica * 10) - 5)});
                        this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                        fyy = (((float) getHeight()) - (((float) H4) + fOffsetY)) - ((float) this.mLineTextDelta);
                        this.mPath.reset();
                        this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                        this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                        canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                    }
                } else if (i % 2 == 0) {
                    h = H2;
                }
                canvas.drawLine(fX + (((float) i) * fPixelPerPica2), ((float) (getHeight() + 0)) - fOffsetY, fX + (((float) i) * fPixelPerPica2), ((float) (getHeight() - h)) - fOffsetY, this.mLinePaint);
            }
            canvas.drawLine(0.0f, ((float) (getHeight() + 0)) - fOffsetY, (float) getWidth(), ((float) (getHeight() + 0)) - fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) (getHeight() - H1)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H1)) - fOffsetY, this.mLinePaint);
            canvas.drawLine(0.0f, ((float) (getHeight() - H2)) - fOffsetY, (float) getWidth(), ((float) (getHeight() - H2)) - fOffsetY, this.mLinePaint);
            return;
        }
        for (i = 0; (((float) i) * fPixelPerPica2) + fX <= ((float) maxLen); i++) {
            h = H1;
            if (i % 20 == 0) {
                h = H4;
                if (!(i == 0 && this.mRulerPage == 0)) {
                    x = fX + (((float) i) * fPixelPerPica2);
                    this.mTextPaint.setTextSize((float) textSize);
                    this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(n10Pica * 10)});
                    this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                    fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                    this.mPath.reset();
                    this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                    this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                    canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
                n10Pica++;
                if (i == 0 && this.mRulerPage == 0) {
                    this.mTextPaint.setTextSize((float) textSize);
                    this.mNumberString = "pica";
                    this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                    fyy = ((((float) h) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                    this.mPath.reset();
                    this.mPath.moveTo(0.0f, fyy);
                    this.mPath.lineTo(((float) this.mNumBounds.width()) + 0.0f, fyy);
                    canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
            } else if (i % 10 == 0) {
                h = H3;
                if (i != 0 || this.mRulerPage != 0) {
                    x = fX + (((float) i) * fPixelPerPica2);
                    this.mTextPaint.setTextSize((float) textSize);
                    this.mNumberString = String.format(Locale.US, "%d", new Object[]{Integer.valueOf((n10Pica * 10) - 5)});
                    this.mTextPaint.getTextBounds(this.mNumberString, 0, this.mNumberString.length(), this.mNumBounds);
                    fyy = ((((float) H4) + fOffsetY) + ((float) this.mNumBounds.height())) + ((float) this.mLineTextDelta);
                    this.mPath.reset();
                    this.mPath.moveTo(x - ((float) this.mNumBounds.width()), fyy);
                    this.mPath.lineTo(((float) this.mNumBounds.width()) + x, fyy);
                    canvas.drawTextOnPath(this.mNumberString, this.mPath, 0.0f, 0.0f, this.mTextPaint);
                }
            } else if (i % 2 == 0) {
                h = H2;
            }
            canvas.drawLine(fX + (((float) i) * fPixelPerPica2), 0.0f + fOffsetY, fX + (((float) i) * fPixelPerPica2), ((float) h) + fOffsetY, this.mLinePaint);
        }
        canvas.drawLine(0.0f, 0.0f + fOffsetY, (float) getWidth(), 0.0f + fOffsetY, this.mLinePaint);
        canvas.drawLine(0.0f, ((float) H1) + fOffsetY, (float) getWidth(), ((float) H1) + fOffsetY, this.mLinePaint);
        canvas.drawLine(0.0f, ((float) H2) + fOffsetY, (float) getWidth(), ((float) H2) + fOffsetY, this.mLinePaint);
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawARGB(0, 0, 0, 0);
        if (mBackgroundBitmap != null) {
            canvas.drawBitmap(mBackgroundBitmap, 0.0f, 0.0f, null);
        }
        int stripeH = this.mStripeHeight * 2;
        canvas.save();
        canvas.translate(0.0f, (float) (((getHeight() / 2) - 20) - stripeH));
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) stripeH, this.mStripePaint);
        canvas.restore();
        canvas.save();
        canvas.translate(0.0f, (float) ((getHeight() / 2) + 20));
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) stripeH, this.mStripePaint);
        canvas.restore();
        int nPrimaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY, "0"));
        int nSecondaryUnit = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY, "1"));
        boolean bHFlip = PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_RULER_HFLIPPED_KEY, false);
        if (nPrimaryUnit == 0) {
            drawMillimeterRuler(canvas, false, bHFlip);
        } else if (nPrimaryUnit == 1) {
            drawInchRuler(canvas, false, bHFlip);
        } else if (nPrimaryUnit == 2) {
            drawPixelRuler(canvas, false, bHFlip);
        } else {
            drawPicaRuler(canvas, false, bHFlip);
        }
        if (nSecondaryUnit == 0) {
            drawMillimeterRuler(canvas, true, bHFlip);
        } else if (nSecondaryUnit == 1) {
            drawInchRuler(canvas, true, bHFlip);
        } else if (nSecondaryUnit == 2) {
            drawPixelRuler(canvas, true, bHFlip);
        } else {
            drawPicaRuler(canvas, true, bHFlip);
        }
        setDrawingCacheEnabled(true);
    }
}
