package com.vivekwarde.measure.seismometer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;

public class AlarmLineView extends View {
    Paint mPaint;
    Rect mTextBounds;
    TextPaint mTextPaint;

    public AlarmLineView(Context context) {
        super(context);
        this.mPaint = null;
        this.mTextPaint = null;
        this.mTextBounds = new Rect();
        this.mPaint = new Paint();
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeWidth(2.0f);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.SEISMOMETER_ALARM_LINE_COLOR));
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.SEISMOMETER_ALARM_LINE_COLOR));
        this.mTextPaint.setTextAlign(Align.CENTER);
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.TESLAMETER_METER_INDEX_FONT_SIZE));
        this.mTextPaint.getTextBounds("8", 0, 1, this.mTextBounds);
    }

    protected void onDraw(Canvas canvas) {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_ALARM_ENABLED_KEY, true)) {
            double midx = (double) (getWidth() / 2);
            int level = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY, "0"));
            double kAlarmThreshold1 = (double) ((int) TypedValue.applyDimension(1, 20.0f, getResources().getDisplayMetrics()));
            double kAlarmThreshold2 = (double) ((int) TypedValue.applyDimension(1, 40.0f, getResources().getDisplayMetrics()));
            double kAlarmThreshold3 = (double) ((int) TypedValue.applyDimension(1, BitmapDescriptorFactory.HUE_YELLOW, getResources().getDisplayMetrics()));
            if (Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_SEISMOMETER_SCALE_TYPE_KEY, "1")) == 1) {
                kAlarmThreshold1 = (double) ((int) TypedValue.applyDimension(1, 40.0f, getResources().getDisplayMetrics()));
                kAlarmThreshold2 = (double) ((int) TypedValue.applyDimension(1, BitmapDescriptorFactory.HUE_YELLOW, getResources().getDisplayMetrics()));
                kAlarmThreshold3 = (double) ((int) TypedValue.applyDimension(1, 80.0f, getResources().getDisplayMetrics()));
            }
            double[] threshold = new double[]{-kAlarmThreshold3, -kAlarmThreshold2, -kAlarmThreshold1, 0.0d, kAlarmThreshold1, kAlarmThreshold2, kAlarmThreshold3};
            int i = -3;
            while (i <= 3) {
                if (i != 0) {
                    int index = i + 3;
                    if (Math.abs(i) - 1 == level) {
                        float x = (float) (threshold[index] + midx);
                        canvas.drawLine(x, 0.0f, x, (float) getHeight(), this.mPaint);
                        String sNumber = String.valueOf(Math.abs(i));
                        this.mTextPaint.setTextAlign(i < 0 ? Align.RIGHT : Align.LEFT);
                        canvas.drawText(sNumber, 0, sNumber.length(), x, (float) (this.mTextBounds.height() * 2), this.mTextPaint);
                    }
                }
                i++;
            }
        }
        setDrawingCacheEnabled(true);
    }
}
