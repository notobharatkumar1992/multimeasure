package com.vivekwarde.measure.seismometer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.utilities.SPTimer;

public class CountDownClockView extends View implements Runnable, OnTouchListener {
    private Paint mCirclePaint;
    boolean mClockMode;
    int mCount;
    OnCountDownClockViewListener mListener;
    RectF mOval;
    Path mPath;
    Rect mTextBounds;
    private TextPaint mTextPaint;
    SPTimer mTimer;

    public interface OnCountDownClockViewListener {
        void onCountDownClockClicked();

        void onCountDownClockDone();
    }

    @Deprecated
    public CountDownClockView(Context context) {
        super(context);
        this.mClockMode = true;
        this.mCount = 3;
        this.mTextBounds = new Rect();
        this.mPath = new Path();
        this.mTimer = null;
        this.mOval = new RectF();
        this.mListener = null;
    }

    public CountDownClockView(Context context, boolean clockMode) {
        super(context);
        this.mClockMode = true;
        this.mCount = 3;
        this.mTextBounds = new Rect();
        this.mPath = new Path();
        this.mTimer = null;
        this.mOval = new RectF();
        this.mListener = null;
        this.mClockMode = clockMode;
        this.mCirclePaint = new Paint();
        this.mCirclePaint.setAntiAlias(true);
        this.mCirclePaint.setStyle(Style.STROKE);
        this.mCirclePaint.setStrokeWidth(10.0f);
        this.mCirclePaint.setColor(ContextCompat.getColor(getContext(), R.color.SEISMOMETER_ALARM_LINE_COLOR));
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setAntiAlias(true);
        if (clockMode) {
            this.mTextPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/My-LED-Digital.ttf"));
            this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.SEISMOMETER_CLOCK_NUMBER_FONT_SIZE));
            this.mTextPaint.setTextAlign(Align.RIGHT);
            this.mTextPaint.getTextBounds("8", 0, 1, this.mTextBounds);
            this.mTimer = new SPTimer(new Handler(), this, GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE, false);
            return;
        }
        this.mTextPaint.setTextAlign(Align.LEFT);
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.SEISMOMETER_CLOCK_STOP_FONT_SIZE));
        setOnTouchListener(this);
    }

    void setOnCountDownClockViewListener(OnCountDownClockViewListener listener) {
        this.mListener = listener;
    }

    void setCount(int count) {
        this.mCount = count;
    }

    int getCount() {
        return this.mCount;
    }

    void startCountDown() {
        this.mTimer.start();
    }

    void stopTimer() {
        this.mTimer.stop();
    }

    public void run() {
        this.mCount--;
        if (this.mCount == -1) {
            stopTimer();
            if (this.mListener != null) {
                this.mListener.onCountDownClockDone();
                return;
            }
            return;
        }
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        int x = getWidth() / 2;
        int y = getHeight() / 2;
        int r = (int) ((2.0f * ((float) x)) / 3.0f);
        this.mOval.set((float) (x - r), (float) (y - r), (float) (x + r), (float) (y + r));
        canvas.drawArc(this.mOval, 0.0f, 360.0f, false, this.mCirclePaint);
        if (this.mClockMode) {
            x = (getWidth() - this.mTextBounds.width()) / 2;
            y = ((getHeight() - this.mTextBounds.height()) / 2) + this.mTextBounds.height();
            this.mPath.reset();
            this.mPath.moveTo((float) x, (float) y);
            this.mPath.lineTo((float) (this.mTextBounds.width() + x), (float) y);
            this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.SEISMOMETER_CLOCK_BACKLIT_COLOR));
            canvas.drawTextOnPath("8", this.mPath, 0.0f, 0.0f, this.mTextPaint);
            this.mPath.reset();
            this.mPath.moveTo((float) x, (float) y);
            this.mPath.lineTo((float) (this.mTextBounds.width() + x), (float) y);
            this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.LED_BLUE));
            canvas.drawTextOnPath(String.valueOf(this.mCount), this.mPath, 0.0f, 0.0f, this.mTextPaint);
        } else {
            String sStop = getResources().getString(R.string.IDS_STOP);
            this.mTextPaint.getTextBounds(sStop, 0, sStop.length(), this.mTextBounds);
            x = (getWidth() - this.mTextBounds.width()) / 2;
            y = (((getHeight() - this.mTextBounds.height()) / 2) + this.mTextBounds.height()) - ((int) this.mTextPaint.descent());
            this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.LED_BLUE));
            canvas.drawText(sStop, 0, sStop.length(), (float) x, (float) y, this.mTextPaint);
        }
        setDrawingCacheEnabled(true);
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (v.equals(this) && event.getAction() == 0) {
            int x = getWidth() / 2;
            float cx = event.getX() - ((float) x);
            float cy = event.getY() - ((float) (getHeight() / 2));
            if (Math.sqrt((double) ((cx * cx) + (cy * cy))) <= ((double) ((int) ((2.0f * ((float) x)) / 3.0f))) && this.mListener != null) {
                this.mListener.onCountDownClockClicked();
                return true;
            }
        }
        return false;
    }
}
