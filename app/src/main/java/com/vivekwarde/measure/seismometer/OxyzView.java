package com.vivekwarde.measure.seismometer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.view.View;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;

public class OxyzView extends View {
    private Bitmap[] mImages;

    public OxyzView(Context context) {
        super(context);
        this.mImages = new Bitmap[]{null, null, null, null};
        setBackgroundColor(0);
        initImages();
    }

    void initImages() {
        int[] id = new int[]{R.drawable.seismometer_axis_x, R.drawable.seismometer_axis_y, R.drawable.seismometer_axis_z, R.drawable.seismometer_iphone};
        for (int i = 0; i < 4; i++) {
            this.mImages[i] = ((BitmapDrawable) getResources().getDrawable(id[i])).getBitmap();
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(this.mImages[0].getWidth(), this.mImages[1].getHeight());
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(this.mImages[3], 0.0f, 0.0f, null);
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY, true)) {
            canvas.drawBitmap(this.mImages[0], 0.0f, 0.0f, null);
        }
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY, true)) {
            canvas.drawBitmap(this.mImages[1], 0.0f, 0.0f, null);
        }
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY, true)) {
            canvas.drawBitmap(this.mImages[2], 0.0f, 0.0f, null);
        }
        setDrawingCacheEnabled(true);
    }
}
