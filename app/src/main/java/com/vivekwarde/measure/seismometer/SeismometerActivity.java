package com.vivekwarde.measure.seismometer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.format.Time;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.ActionView;
import com.vivekwarde.measure.custom_controls.ActionView.OnActionViewEventListener;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPMultiStateButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.seismometer.CountDownClockView.OnCountDownClockViewListener;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class SeismometerActivity extends BaseActivity implements OnClickListener, SensorEventListener, OnCountDownClockViewListener, OnActionViewEventListener, DialogInterface.OnClickListener, AnimatorListener, OnCancelListener {
    public static final double FILTERING_FACTOR = 0.6d;
    public static boolean mUserRunning;

    static {
        mUserRunning = true;
    }

    final String tag;
    SPImageButton mAlarmButton;
    MediaPlayer mAlarmPlayer;
    boolean mAlarmReady;
    Time mCurrentTime;
    RelativeLayout mGraphLayout;
    SPImageButton mInfoButton;
    Time mInitTime;
    boolean mIsRunning;
    String mLoadedFile;
    SPImageButton mMenuButton;
    boolean mSFXWritingOpened;
    MediaPlayer mSFXWritingPlayer;
    SPImageButton mStartStopButton;
    private xUIAcceleration mAcc;
    private xUIAcceleration mAcceleration;
    private Sensor mAccelerometer;
    private ActionView mActionView;
    private AlarmLineView mAlarmLineView;
    private SPMultiStateButton[] mAxisButton;
    private int mBypassSampleCount;
    private int mCaroHeight;
    private CountDownClockView mClockView;
    private String mDataString;
    private SeismometerGraphView mGraphView;
    private int mHeaderBarHeight;
    private ImageView mLaserLight;
    private RelativeLayout mLaserMachine;
    private boolean mMachineReady;
    private OxyzView mOxyzView;
    private View mPaperView;
    private int mSampleCount;
    private SensorManager mSensorManager;
    private CountDownClockView mStopButton;

    public SeismometerActivity() {
        this.tag = getClass().getSimpleName();
        this.mIsRunning = true;
        this.mSensorManager = null;
        this.mAccelerometer = null;
        this.mStartStopButton = null;
        this.mAlarmButton = null;
        this.mInfoButton = null;
        this.mMenuButton = null;
        this.mAxisButton = new SPMultiStateButton[]{null, null, null};
        this.mOxyzView = null;
        this.mLaserMachine = null;
        this.mActionView = null;
        this.mClockView = null;
        this.mStopButton = null;
        this.mAlarmLineView = null;
        this.mGraphView = null;
        this.mAcceleration = new xUIAcceleration();
        this.mAcc = new xUIAcceleration();
        this.mBypassSampleCount = 0;
        this.mInitTime = new Time();
        this.mCurrentTime = new Time();
        this.mMachineReady = false;
        this.mPaperView = null;
        this.mCaroHeight = 0;
        this.mGraphLayout = null;
        this.mSFXWritingPlayer = null;
        this.mSFXWritingOpened = false;
        this.mAlarmPlayer = null;
        this.mLoadedFile = BuildConfig.FLAVOR;
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        initInfo();
        initSubviews();
        initSensor();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    void initSensor() {
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mAccelerometer = this.mSensorManager.getDefaultSensor(1);
    }

    protected void onDestroy() {
        if (this.mLoadedFile.length() > 0) {
            stopSoundAlarm();
            this.mAlarmPlayer.release();
        }
        stopWritingSound();
        this.mSFXWritingPlayer.release();
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        updateSettingsChanged();
        start();
    }

    private void updateSettingsChanged() {
        if (this.mAlarmLineView != null) {
            this.mAlarmLineView.invalidate();
        }
    }

    protected void onPause() {
        super.onPause();
        pause();
    }

    void start() {
        if (mUserRunning) {
            this.mIsRunning = this.mSensorManager.registerListener(this, this.mAccelerometer, 0);
        }
        updateStartStopButtonState();
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true) && isSeismometerWriting()) {
            playWritingSound();
        }
        if (this.mStopButton != null) {
            playSoundAlarm(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SEISMOMETER_ALARM_SOUND_KEY, "Alarm 1.mp3"));
        }
    }

    void pause() {
        this.mIsRunning = false;
        this.mSensorManager.unregisterListener(this);
        updateStartStopButtonState();
        stopWritingSound();
        stopSoundAlarm();
    }

    void initInfo() {
        this.mMachineReady = false;
        this.mAlarmReady = false;
        this.mBypassSampleCount = 0;
        this.mInitTime.setToNow();
        this.mCurrentTime.set(this.mInitTime);
        this.mSFXWritingOpened = false;
        this.mSFXWritingPlayer = new MediaPlayer();
        this.mAlarmPlayer = new MediaPlayer();
        this.mLoadedFile = BuildConfig.FLAVOR;
        this.mSampleCount = 0;
        this.mDataString = null;
        initDataFiles();
    }

    void initDataFiles() {
        deleteFile("SeismometerData.csv");
        deleteFile("SeismometerData.zip");
        MiscUtility.writeToFile(this, "SeismometerData.csv", "Date,Time,X-Axis,Y-Axis,Z-Axis\n", false);
    }

    @SuppressLint({"NewApi"})
    void initSubviews() {
        this.mUiLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, mScreenHeight);
        layoutParams.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(layoutParams);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        int frameMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_VERT_MARGIN_BTW_DOTLEDSCREEN_AND_HEADER);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap();
        this.mHeaderBarHeight = (bitmap.getHeight() / 2) + (vButtonScreenMargin * 2);
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(bitmap);
        this.mMenuButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        layoutParams.height = bitmap.getHeight() / 2;
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        this.mInfoButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mInfoButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mMenuButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mInfoButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mInfoButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mNoAdsButton.setLayoutParams(layoutParams);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
        this.mAlarmButton = new SPImageButton(this);
        this.mAlarmButton.setId(4);
        this.mAlarmButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_alarm_off)).getBitmap());
        this.mAlarmButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(9);
        layoutParams.setMargins(0, 0, 0, 0);
        this.mAlarmButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mAlarmButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap();
        SPScale9ImageView backgroundView = new SPScale9ImageView(this);
        backgroundView.setId(8);
        backgroundView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.topMargin = this.mHeaderBarHeight;
        backgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(backgroundView);
        int tempMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_DOTLEDSCREEN_AND_SCREEN);
        this.mStartStopButton = new SPImageButton(this);
        this.mStartStopButton.setId(3);
        this.mStartStopButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(12);
        layoutParams.setMargins(tempMargin, 0, 0, frameMargin);
        this.mStartStopButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mStartStopButton);
        updateStartStopButtonState();
        this.mGraphLayout = new RelativeLayout(this);
        this.mGraphLayout.setId(11);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(6, backgroundView.getId());
        layoutParams.addRule(2, this.mStartStopButton.getId());
        layoutParams.setMargins(tempMargin, frameMargin, tempMargin, hButtonButtonMargin / 2);
        this.mGraphLayout.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mGraphLayout);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();
        int[] capSizes = ImageUtility.getContentCapSizes(bitmap);
        SPScale9ImageView graphFrameView = new SPScale9ImageView(this);
        graphFrameView.setId(12);
        graphFrameView.setBitmap(bitmap);
        graphFrameView.setLayoutParams(new LayoutParams(-1, -1));
        this.mGraphLayout.addView(graphFrameView);
        this.mOxyzView = new OxyzView(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        layoutParams.setMargins(0, frameMargin, frameMargin, 0);
        this.mOxyzView.setLayoutParams(layoutParams);
        this.mGraphLayout.addView(this.mOxyzView);
        this.mLaserMachine = new RelativeLayout(this);
        this.mLaserMachine.setId(10);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(14);
        layoutParams.setMargins(0, capSizes[1], 0, 0);
        this.mLaserMachine.setLayoutParams(layoutParams);
        this.mGraphLayout.addView(this.mLaserMachine);
        ImageView imageView = new ImageView(this);
        imageView.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.seismometer_lamp)).getBitmap());
        this.mLaserMachine.addView(imageView);
        this.mLaserLight = new ImageView(this);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.seismometer_lamp_light)).getBitmap();
        int laserOffset = ImageUtility.getRightBottomCapSize(bitmap);
        Bitmap laserBitmap = Bitmap.createBitmap(bitmap, 1, 1, bitmap.getWidth() - 2, bitmap.getHeight() - 2);
        this.mLaserLight.setImageBitmap(laserBitmap);
        this.mLaserMachine.addView(this.mLaserLight);
        this.mPaperView = new View(this);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_caro)).getBitmap();
        this.mCaroHeight = bitmap.getHeight();
        int maxtop = this.mCaroHeight * 1;
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
        bitmapDrawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mPaperView.setBackgroundDrawable(bitmapDrawable);
        } else {
            this.mPaperView.setBackground(bitmapDrawable);
        }
        layoutParams = new LayoutParams(-1, (getResources().getDisplayMetrics().heightPixels * 2) + maxtop);
        layoutParams.setMargins(capSizes[0], 0, capSizes[2], 0);
        this.mPaperView.setLayoutParams(layoutParams);
        this.mGraphLayout.addView(this.mPaperView);
        ViewHelper.setY(this.mPaperView, (float) (-maxtop));
        this.mGraphView = new SeismometerGraphView(this);
        this.mGraphView.setId(9);
        this.mGraphView.setDelegate(this);
        this.mGraphView.setGraphOffsetY(laserBitmap.getHeight() - laserOffset);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        this.mGraphView.setLayoutParams(layoutParams);
        this.mGraphLayout.addView(this.mGraphView);
        graphFrameView.bringToFront();
        this.mLaserMachine.bringToFront();
        this.mOxyzView.bringToFront();
        int i = 3;
        int[] id = new int[]{R.drawable.seismometer_button_axis_z, R.drawable.seismometer_button_axis_y, R.drawable.seismometer_button_axis_x};
        String[] key = new String[]{SettingsKey.SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY, SettingsKey.SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY, SettingsKey.SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY};
        for (int i2 = 0; i2 < 3; i2++) {
            this.mAxisButton[i2] = new SPMultiStateButton(this);
            this.mAxisButton[i2].setId(7 - i2);
            this.mAxisButton[i2].setBackgroundBitmapId(id[i2], 2);
            this.mAxisButton[i2].setState(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(key[i2], true) ? 0 : 1);
            this.mAxisButton[i2].setOnClickListener(this);
            layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(6, this.mStartStopButton.getId());
            layoutParams.addRule(8, this.mStartStopButton.getId());
            if (i2 == 0) {
                layoutParams.addRule(11);
                layoutParams.setMargins(0, 0, tempMargin, 0);
            } else {
                layoutParams.addRule(0, this.mAxisButton[i2 - 1].getId());
                layoutParams.setMargins(0, 0, hButtonButtonMargin, 0);
            }
            this.mAxisButton[i2].setLayoutParams(layoutParams);
            this.mUiLayout.addView(this.mAxisButton[i2]);
        }
        updateAxisButtonState(false);
    }

    void moveGraph(float offset) {
        float y = ViewHelper.getY(this.mPaperView) + ((float) (((double) offset) - (Math.floor((double) (offset / ((float) this.mCaroHeight))) * ((double) this.mCaroHeight))));
        if (y >= (0)) {
            y = (y - (0)) - ((float) (this.mCaroHeight * 1));
        }
        if (y <= ((float) (this.mCaroHeight * -1))) {
            y += (float) (this.mCaroHeight * 1);
        }
        ViewHelper.setY(this.mPaperView, y);
    }

    void toggleAxisX() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean bAxisOn = !PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY, true);
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY, bAxisOn);
        editor.apply();
        updateAxisButtonState(true);
    }

    void toggleAxisY() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean bAxisOn = !PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY, true);
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY, bAxisOn);
        editor.apply();
        updateAxisButtonState(true);
    }

    void toggleAxisZ() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean bAxisOn = !PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY, true);
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY, bAxisOn);
        editor.apply();
        updateAxisButtonState(true);
    }

    void playSoundAlarm(String soundFile) {
        try {
            AssetFileDescriptor descriptor = getAssets().openFd("sounds/seismometer/alarm/" + soundFile);
            if (this.mLoadedFile.length() == 0) {
                this.mAlarmPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                this.mAlarmPlayer.prepare();
                this.mAlarmPlayer.setLooping(true);
                this.mAlarmPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
            } else if (this.mLoadedFile != soundFile) {
                stopSoundAlarm();
                this.mAlarmPlayer.reset();
                this.mAlarmPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                this.mAlarmPlayer.prepare();
                this.mAlarmPlayer.setLooping(true);
                this.mAlarmPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
            } else if (!this.mAlarmPlayer.isPlaying()) {
                this.mAlarmPlayer.prepare();
            }
            descriptor.close();
            if (!this.mAlarmPlayer.isPlaying()) {
                this.mAlarmPlayer.start();
            }
            this.mLoadedFile = soundFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void stopSoundAlarm() {
        if (this.mLoadedFile.length() != 0 && this.mAlarmPlayer.isPlaying()) {
            this.mAlarmPlayer.stop();
        }
    }

    void playWritingSound() {
        try {
            if (this.mSFXWritingOpened) {
                this.mSFXWritingPlayer.prepare();
            } else {
                AssetFileDescriptor descriptor = getAssets().openFd("sounds/seismometer/writing.mp3");
                this.mSFXWritingPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                descriptor.close();
                this.mSFXWritingPlayer.prepare();
                this.mSFXWritingPlayer.setLooping(true);
                this.mSFXWritingPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
                this.mSFXWritingOpened = true;
            }
            if (!this.mSFXWritingPlayer.isPlaying()) {
                this.mSFXWritingPlayer.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void stopWritingSound() {
        if (this.mSFXWritingOpened && this.mSFXWritingPlayer.isPlaying()) {
            this.mSFXWritingPlayer.stop();
        }
    }

    boolean isSeismometerWriting() {
        return ((PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY, true) | PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY, true)) | PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY, true)) & mUserRunning;
    }

    void startWriting(boolean start) {
        if (!start || !this.mMachineReady) {
            if (start || this.mMachineReady) {
                if (!start) {
                    stopWritingSound();
                    this.mMachineReady = false;
                }
                Object obj = this.mLaserLight;
                String str = "alpha";
                float[] fArr = new float[1];
                fArr[0] = start ? TextTrackStyle.DEFAULT_FONT_SCALE : 0.0f;
                ObjectAnimator a = ObjectAnimator.ofFloat(obj, str, fArr);
                a.setDuration(150);
                if (start) {
                    a.addListener(this);
                }
                a.start();
            }
        }
    }

    void updateAxisButtonState(boolean animate) {
        boolean z = false;
        boolean[] state = new boolean[]{true, true, true};
        state[0] = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY, true);
        state[1] = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY, true);
        state[2] = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY, true);
        this.mOxyzView.invalidate();
        if (animate) {
            if (state[0] || state[1] || state[2]) {
                z = true;
            }
            startWriting(z);
            return;
        }
        if (state[0] || state[1] || state[2]) {
            z = true;
        }
        this.mMachineReady = z;
        ViewHelper.setAlpha(this.mLaserLight, this.mMachineReady ? TextTrackStyle.DEFAULT_FONT_SCALE : 0.0f);
    }

    void updateStartStopButtonState() {
        this.mStartStopButton.setBackgroundBitmapId(this.mIsRunning ? R.drawable.seismometer_button_pause : R.drawable.seismometer_button_start);
    }

    void onStartStop() {
        mUserRunning = !mUserRunning;
        if (mUserRunning) {
            start();
        } else {
            pause();
        }
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
    }

    void onToggleAlarm() {
        boolean z = true;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        SPImageButton sPImageButton = this.mAlarmButton;
        if (this.mAlarmButton.isStateOn()) {
            z = false;
        }
        sPImageButton.setStateOn(z);
        this.mAlarmReady = false;
        this.mAlarmButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(this.mAlarmButton.isStateOn() ? R.drawable.button_alarm_on : R.drawable.button_alarm_off)).getBitmap());
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SettingsKey.SETTINGS_SEISMOMETER_ALARM_ENABLED_KEY, this.mAlarmButton.isStateOn());
        editor.apply();
        showAlarmLine(this.mAlarmButton.isStateOn());
        if (this.mAlarmButton.isStateOn()) {
            startCountdown();
            return;
        }
        stopCountdown();
        showAlarmBigButton(false);
        stopSoundAlarm();
    }

    void showAlarmLine(boolean show) {
        if (show && this.mAlarmLineView == null) {
            this.mAlarmLineView = new AlarmLineView(this);
            LayoutParams params = new LayoutParams(-2, -2);
            params.addRule(5, this.mGraphView.getId());
            params.addRule(6, this.mGraphView.getId());
            params.addRule(7, this.mGraphView.getId());
            params.addRule(8, this.mGraphView.getId());
            this.mAlarmLineView.setLayoutParams(params);
            this.mGraphLayout.addView(this.mAlarmLineView);
        }
        if (!show && this.mAlarmLineView != null) {
            this.mGraphLayout.removeView(this.mAlarmLineView);
            this.mAlarmLineView = null;
        }
    }

    void startCountdown() {
        if (this.mClockView == null) {
            this.mClockView = new CountDownClockView(this, true);
            this.mClockView.setOnCountDownClockViewListener(this);
            this.mUiLayout.addView(this.mClockView);
            this.mClockView.startCountDown();
        }
    }

    void stopCountdown() {
        if (this.mClockView != null) {
            this.mClockView.stopTimer();
            this.mUiLayout.removeView(this.mClockView);
            this.mClockView = null;
        }
    }

    void stopAlarm() {
        showAlarmBigButton(false);
        stopSoundAlarm();
    }

    void showAlarmBigButton(boolean show) {
        if (show && this.mStopButton == null) {
            this.mStopButton = new CountDownClockView(this, false);
            this.mStopButton.setOnCountDownClockViewListener(this);
            this.mUiLayout.addView(this.mStopButton);
        }
        if (!show && this.mStopButton != null) {
            this.mUiLayout.removeView(this.mStopButton);
            this.mStopButton = null;
        }
    }

    void onAlarm() {
        if (this.mAlarmReady && this.mStopButton == null) {
            showAlarmBigButton(true);
            Log.i(this.tag, PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SEISMOMETER_ALARM_SOUND_KEY, "seismometer sound not set yet"));
            playSoundAlarm("Alarm 1.mp3");
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case Barcode.ALL_FORMATS /*0*/:
                onRemoveAdsButton();
            case CompletionEvent.STATUS_FAILURE /*1*/:
                onButtonMenu();
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                onButtonSettings();
            case CompletionEvent.STATUS_CANCELED /*3*/:
                onStartStop();
            case Barcode.PHONE /*4*/:
                onToggleAlarm();
            case Barcode.PRODUCT /*5*/:
                toggleAxisX();
            case Barcode.SMS /*6*/:
                toggleAxisY();
            case Barcode.TEXT /*7*/:
                toggleAxisZ();
            case Barcode.WIFI /*9*/:
                openActionView(true);
            default:
                Log.i(this.tag, "Unknown action id :(");
        }
    }

    void openActionView(boolean open) {
        if (open) {
            pause();
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            ArrayList<String> titleList = new ArrayList();
            titleList.add(getResources().getString(R.string.IDS_SEND_DATA_TO_EMAIL));
            titleList.add(getResources().getString(R.string.IDS_CLEAR_GRAPH));
            titleList.add(getResources().getString(R.string.IDS_CLOSE));
            Rect r = new Rect();
            this.mGraphView.getGlobalVisibleRect(r);
            int lmargin = ((LayoutParams) this.mGraphView.getLayoutParams()).leftMargin;
            int rmargin = ((LayoutParams) this.mGraphView.getLayoutParams()).rightMargin;
            this.mActionView = new ActionView(this);
            this.mActionView.construct(lmargin, rmargin, new Point(r.centerX(), r.centerY()), titleList, 0);
            this.mActionView.setLayoutParams(new LayoutParams(-1, -1));
            this.mActionView.setOnActionViewEventListener(this);
            this.mUiLayout.addView(this.mActionView);
        } else if (this.mActionView != null) {
            this.mUiLayout.removeView(this.mActionView);
            this.mActionView = null;
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.i(this.tag, "New accuracy = " + accuracy);
    }

    public void onSensorChanged(SensorEvent event) {
        this.mAcceleration.clear();
        this.mAcc.x = (((double) event.values[0]) * FILTERING_FACTOR) + (this.mAcc.x * 0.4d);
        this.mAcceleration.x = ((double) event.values[0]) - this.mAcc.x;
        this.mAcc.y = (((double) event.values[1]) * FILTERING_FACTOR) + (this.mAcc.y * 0.4d);
        this.mAcceleration.y = ((double) event.values[1]) - this.mAcc.y;
        this.mAcc.z = (((double) event.values[2]) * FILTERING_FACTOR) + (this.mAcc.z * 0.4d);
        this.mAcceleration.z = ((double) event.values[2]) - this.mAcc.z;
        this.mCurrentTime.setToNow();
        if (Time.compare(this.mCurrentTime, this.mInitTime) != 0) {
            this.mInitTime.set(this.mCurrentTime);
            this.mAcceleration.timestamp = this.mCurrentTime.toMillis(true);
        }
        this.mAcceleration.monitor = ((PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY, true) ? 1 : 0) | (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY, true) ? 2 : 0)) | (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY, true) ? 4 : 0);
        if (this.mBypassSampleCount > 10) {
            updateAcceleration(this.mAcceleration);
        } else {
            this.mBypassSampleCount++;
        }
    }

    void updateAcceleration(xUIAcceleration acceleration) {
        this.mGraphView.updateHistoryInfo(acceleration);
        int scaleType = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SEISMOMETER_SCALE_TYPE_KEY, "1"));
        double value = SeismometerGraphView.calculateValueFromAcceleration(acceleration, scaleType);
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_ALARM_ENABLED_KEY, true)) {
            double kAlarmThreshold1 = (double) ((int) TypedValue.applyDimension(1, 20.0f, getResources().getDisplayMetrics()));
            double kAlarmThreshold2 = (double) ((int) TypedValue.applyDimension(1, 40.0f, getResources().getDisplayMetrics()));
            double kAlarmThreshold3 = (double) ((int) TypedValue.applyDimension(1, BitmapDescriptorFactory.HUE_YELLOW, getResources().getDisplayMetrics()));
            if (scaleType == 1) {
                kAlarmThreshold1 = (double) ((int) TypedValue.applyDimension(1, 40.0f, getResources().getDisplayMetrics()));
                kAlarmThreshold2 = (double) ((int) TypedValue.applyDimension(1, BitmapDescriptorFactory.HUE_YELLOW, getResources().getDisplayMetrics()));
                kAlarmThreshold3 = (double) ((int) TypedValue.applyDimension(1, 80.0f, getResources().getDisplayMetrics()));
            }
            int level = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY, "0"));
            if ((level == 0 && Math.abs(value) > kAlarmThreshold1) || ((level == 1 && Math.abs(value) > kAlarmThreshold2) || (level == 2 && Math.abs(value) > kAlarmThreshold3))) {
                onAlarm();
            }
        }
        if (this.mMachineReady) {
            double width = (double) (this.mLaserMachine.getWidth() / 2);
            ViewHelper.setX(this.mLaserMachine, (float) ((((double) (((View) this.mLaserMachine.getParent()).getWidth() / 2)) + value) - width));
        }
        float y = ViewHelper.getY(this.mPaperView) + this.mGraphView.getBackgroundPassLength();
        if (y >= 0) {
            y = (y - 0) - ((float) (this.mCaroHeight * 1));
        }
        ViewHelper.setY(this.mPaperView, y);
        recordValue(this.mAcceleration);
    }

    void recordValue(xUIAcceleration acceleration) {
        if (this.mSampleCount > GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE) {
            MiscUtility.writeToFileWithLimitedSize(this, "SeismometerData.csv", this.mDataString, true, 3);
            this.mDataString = null;
            this.mSampleCount = 0;
        }
        String sX = BuildConfig.FLAVOR;
        String sY = BuildConfig.FLAVOR;
        String sZ = BuildConfig.FLAVOR;
        if ((acceleration.monitor & 1) != 0) {
            sX = String.format(Locale.US, "%.3f", new Object[]{Double.valueOf(acceleration.x)});
        }
        if ((acceleration.monitor & 2) != 0) {
            sY = String.format(Locale.US, "%.3f", new Object[]{Double.valueOf(acceleration.y)});
        }
        if ((acceleration.monitor & 4) != 0) {
            sZ = String.format(Locale.US, "%.3f", new Object[]{Double.valueOf(acceleration.z)});
        }
        new Time().setToNow();
        String formatPattern = "%02d-%02d-%02d,%02d:%02d:%02d,%s,%s,%s\n";
        Calendar calendar = Calendar.getInstance();
        String sTemp = String.format(Locale.US, "%02d-%02d-%02d,%02d:%02d:%02d,%s,%s,%s\n", new Object[]{Integer.valueOf(calendar.get(Calendar.YEAR)), Integer.valueOf(calendar.get(Calendar.MONTH) + 1), Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH)), Integer.valueOf(calendar.get(Calendar.HOUR)), Integer.valueOf(calendar.get(Calendar.MINUTE)), Integer.valueOf(calendar.get(Calendar.SECOND)), sX, sY, sZ});
        if (this.mDataString == null) {
            this.mDataString = sTemp;
        } else {
            this.mDataString += sTemp;
        }
        this.mSampleCount++;
    }

    public void onCountDownClockDone() {
        stopCountdown();
        this.mAlarmReady = true;
    }

    public void onCountDownClockClicked() {
        stopAlarm();
    }

    public void onActionViewEvent(ActionView actionView, int actionId) {
        if (actionId == 0) {
            emailData();
        } else if (actionId == 1) {
            clearGraph();
        } else if (actionId == 2) {
            start();
        }
        openActionView(false);
    }

    void emailData() {
        if (this.mDataString != null) {
            MiscUtility.writeToFile(this, "SeismometerData.csv", this.mDataString, true);
        }
        try {
            MiscUtility.zip(new String[]{getFilesDir() + "/SeismometerData.csv"}, getFilesDir() + "/SeismometerData.zip");
            MiscUtility.sendEmail(this, "mailto:", "Seismometer recorded data", getResources().getString(R.string.IDS_THANKS_FOR_USING), "SeismometerData.zip");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void clearGraph() {
        AlertDialog dialog = new Builder(this).create();
        dialog.setMessage(getResources().getString(R.string.IDS_CLEAR_ALL_HISTORY_ASKING));
        dialog.setButton(-1, getResources().getString(17039379), this);
        dialog.setButton(-2, getResources().getString(17039369), this);
        dialog.setIcon(R.drawable.icon_about);
        dialog.setTitle(getResources().getString(R.string.IDS_CONFIRMATION));
        dialog.setOnCancelListener(this);
        dialog.show();
        MiscUtility.setDialogFontSize(this, dialog);
    }

    public void onBackPressed() {
        if (this.mActionView != null) {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            openActionView(false);
            start();
            return;
        }
        super.onBackPressed();
    }

    public void onCancel(DialogInterface dialog) {
        start();
    }

    public void onClick(DialogInterface dialog, int which) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        switch (which) {
            case ValueAnimator.INFINITE /*-1*/:
                this.mBypassSampleCount = 0;
                this.mGraphView.clearGraph();
                this.mDataString = null;
                this.mSampleCount = 0;
                initDataFiles();
                break;
        }
        start();
    }

    public void onAnimationCancel(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true) && isSeismometerWriting()) {
            playWritingSound();
        }
        this.mMachineReady = true;
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }

    public static class xUIAcceleration {
        int monitor;
        long timestamp;
        double x;
        double y;
        double z;

        public xUIAcceleration() {
            this.x = 0.0d;
            this.y = 0.0d;
            this.z = 0.0d;
            this.timestamp = 0;
            this.monitor = 0;
        }

        void clear() {
            this.monitor = 0;
            long j = 0;
            this.timestamp = j;
            double d = (double) j;
            this.z = d;
            this.y = d;
            this.x = d;
        }

        void copy(xUIAcceleration acc) {
            this.x = acc.x;
            this.y = acc.y;
            this.z = acc.z;
            this.timestamp = acc.timestamp;
            this.monitor = acc.monitor;
        }
    }

    public class Define {
        public static final int ACTION_ID_CLEAR_GRAPH = 1;
        public static final int ACTION_ID_CLOSE = 2;
        public static final int ACTION_ID_SEND_DATA_TO_EMAIL = 0;
        public static final int BACKGROUND_VIEW = 8;
        public static final int BUTTON_ALARM = 4;
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_START_STOP = 3;
        public static final int BUTTON_UPGRADE = 0;
        public static final int BUTTON_X = 5;
        public static final int BUTTON_Y = 6;
        public static final int BUTTON_Z = 7;
        public static final int GRAPH_FRAME_VIEW = 12;
        public static final int GRAPH_LAYOUT = 11;
        public static final int GRAPH_VIEW = 9;
        public static final int LASER_MACHINE_VIEW = 10;
    }
}
