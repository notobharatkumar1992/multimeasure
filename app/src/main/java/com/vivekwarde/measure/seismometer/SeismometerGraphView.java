package com.vivekwarde.measure.seismometer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.format.Time;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;

import com.google.android.gms.cast.TextTrackStyle;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.seismometer.SeismometerActivity.xUIAcceleration;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;

public class SeismometerGraphView extends View implements OnTouchListener {
    public static final int HISTORY_PAGE_MAX = 10;
    public static final int HISTORY_SIZE = 2000;
    public static final int HISTORY_SIZE_PER_PAGE = 200;
    public static final double MAX_ACCELERATION = 1.5d;
    public static final int MAX_VALUE = 550;
    final String tag;
    float mBackgroundPassLength;
    int mCurrentDrawPointer;
    int mCurrentPointer;
    SeismometerActivity mDelegate;
    xUIAcceleration[] mHistoryData;
    boolean mIsMoved;
    Paint mPaint;
    Path mPath;
    Point mPrevPoint;
    TextPaint mTextPaint;
    Time mTime;
    private Point mDownPoint;
    private int mGraphOffsetY;

    public SeismometerGraphView(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mHistoryData = null;
        this.mBackgroundPassLength = 0.0f;
        this.mCurrentPointer = 0;
        this.mCurrentDrawPointer = 0;
        this.mIsMoved = false;
        this.mPrevPoint = null;
        this.mPaint = null;
        this.mTextPaint = null;
        this.mPath = new Path();
        this.mTime = new Time();
        this.mGraphOffsetY = 0;
        this.mDelegate = null;
        this.mDownPoint = null;
        setBackgroundColor(0);
        setOnTouchListener(this);
        this.mPaint = new Paint();
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setAntiAlias(true);
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.SEISMOMETER_ALARM_LINE_COLOR));
        this.mTextPaint.setTextAlign(Align.LEFT);
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.SEISMOMETER_TIMELINE_FONT_SIZE));
        initInfo();
    }

    static int getSignFromAccelerationAndMonitor(xUIAcceleration acceleration, int monitor) {
        double vx = acceleration.x;
        double vy = acceleration.y;
        double vz = acceleration.z;
        if (monitor == 1) {
            return MiscUtility.getSign(vx);
        }
        if (monitor == 2) {
            return MiscUtility.getSign(vy);
        }
        if (monitor == 4) {
            return MiscUtility.getSign(vz);
        }
        if (monitor == 3) {
            if (vx > 0.0d && vx > Math.abs(vy)) {
                return 1;
            }
            if (vy <= 0.0d || vy <= Math.abs(vx)) {
                return -1;
            }
            return 1;
        } else if (monitor == 5) {
            if (vx > 0.0d && vx > Math.abs(vz)) {
                return 1;
            }
            if (vz <= 0.0d || vz <= Math.abs(vx)) {
                return -1;
            }
            return 1;
        } else if (monitor == 6) {
            if (vy > 0.0d && vy > Math.abs(vz)) {
                return 1;
            }
            if (vz <= 0.0d || vz <= Math.abs(vy)) {
                return -1;
            }
            return 1;
        } else if (monitor != 7) {
            return 1;
        } else {
            double vxy = (double) getSignFromAccelerationAndMonitor(acceleration, 3);
            if (vxy > 0.0d && vxy > Math.abs(vz)) {
                return 1;
            }
            if (vz <= 0.0d || vz <= Math.abs(vxy)) {
                return -1;
            }
            return 1;
        }
    }

    static double calculateValueFromAcceleration(xUIAcceleration acceleration, int scaleType) {
        double value = 0.0d;
        double vx = acceleration.x;
        double vy = acceleration.y;
        double vz = acceleration.z;
        int monitor = acceleration.monitor;
        if (monitor == 1) {
            value = vx;
        } else if (monitor == 2) {
            value = vy;
        } else if (monitor == 4) {
            value = vz;
        } else if (monitor == 3) {
            value = Math.sqrt((vx * vx) + (vy * vy));
            if ((vx <= 0.0d || vx <= Math.abs(vy)) && (vy <= 0.0d || vy <= Math.abs(vx))) {
                value = -value;
            }
        } else if (monitor == 5) {
            value = Math.sqrt((vx * vx) + (vz * vz));
            if ((vx <= 0.0d || vx <= Math.abs(vz)) && (vz <= 0.0d || vz <= Math.abs(vx))) {
                value = -value;
            }
        } else if (monitor == 6) {
            value = Math.sqrt((vy * vy) + (vz * vz));
            if ((vy <= 0.0d || vy <= Math.abs(vz)) && (vz <= 0.0d || vz <= Math.abs(vy))) {
                value = -value;
            }
        } else if (monitor == 7) {
            value = ((double) getSignFromAccelerationAndMonitor(acceleration, 7)) * Math.sqrt(((vx * vx) + (vy * vy)) + (vz * vz));
        }
        if (monitor == 0) {
            return value;
        }
        if (Math.abs(value) > MAX_ACCELERATION) {
            value = ((double) MiscUtility.getSign(value)) * MAX_ACCELERATION;
        }
        if (value != 0.0d) {
            value = (((double) MiscUtility.getSign(value)) * Math.abs(value)) * ((double) 366.66666f);
        }
        if (Math.abs(value) > 314.2857142857143d) {
            value = ((double) MiscUtility.getSign(value)) * 314.2857142857143d;
        }
        if (scaleType != 1) {
            return value;
        }
        if (value != 0.0d) {
            value += ((double) MiscUtility.getSign(value)) * (Math.log(Math.abs(value)) / Math.log(1.3d));
        }
        if (Math.abs(value) > 314.2857142857143d) {
            return ((double) MiscUtility.getSign(value)) * 314.2857142857143d;
        }
        return value;
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (h != oldh) {
            this.mBackgroundPassLength = ((float) (h - this.mGraphOffsetY)) / 200.0f;
        }
    }

    void initInfo() {
        int i;
        if (this.mHistoryData == null) {
            this.mHistoryData = new xUIAcceleration[HISTORY_SIZE];
            for (i = 0; i < HISTORY_SIZE; i++) {
                this.mHistoryData[i] = new xUIAcceleration();
            }
        } else {
            for (i = 0; i < HISTORY_SIZE; i++) {
                this.mHistoryData[i].clear();
            }
        }
        this.mCurrentDrawPointer = 0;
        this.mCurrentPointer = 0;
    }

    public void setDelegate(SeismometerActivity dlg) {
        this.mDelegate = dlg;
    }

    void setGraphOffsetY(int offset) {
        this.mGraphOffsetY = offset;
    }

    void unlock() {
        this.mCurrentDrawPointer = this.mCurrentPointer;
    }

    void clearGraph() {
        initInfo();
        invalidate();
    }

    void updateHistoryInfo(xUIAcceleration acceleration) {
        if (this.mCurrentPointer == 1999) {
            for (int i = 0; i < 1999; i++) {
                this.mHistoryData[i].copy(this.mHistoryData[i + 1]);
            }
        }
        this.mHistoryData[this.mCurrentPointer].copy(acceleration);
        this.mCurrentPointer++;
        this.mCurrentDrawPointer++;
        if (this.mCurrentPointer == HISTORY_SIZE) {
            this.mCurrentDrawPointer = 1999;
            this.mCurrentPointer = 1999;
        }
        invalidate();
    }

    float getBackgroundPassLength() {
        return this.mBackgroundPassLength;
    }

    protected void onDraw(Canvas canvas) {
        int i;
        int pos;
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_SEISMOMETER_TIMELINE_ENABLED_KEY, false)) {
            this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.SEISMOMETER_TIMELINE_COLOR));
            this.mPaint.setStrokeWidth(2.0f);
            int xoffset = (int) TypedValue.applyDimension(1, 5.0f, getResources().getDisplayMetrics());
            int yoffset = (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics());
            for (i = 0; i < HISTORY_SIZE_PER_PAGE; i++) {
                pos = (this.mCurrentDrawPointer - i) - 1;
                if (pos < 0) {
                    pos = 0;
                }
                if (this.mHistoryData[pos].timestamp > 0) {
                    float y = (((float) i) * this.mBackgroundPassLength) + ((float) this.mGraphOffsetY);
                    canvas.drawLine(0.0f, y, (float) getWidth(), y, this.mPaint);
                    this.mTime.set(this.mHistoryData[pos].timestamp);
                    canvas.drawText(this.mTime.format("%H:%M:%S"), (float) xoffset, y - ((float) yoffset), this.mTextPaint);
                }
            }
        }
        this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.SEISMOMETER_GRAPH_LINE_COLOR));
        this.mPaint.setStrokeWidth(2.0f);
        boolean firstPoint = true;
        boolean hasLine = false;
        int scaleType = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_SEISMOMETER_SCALE_TYPE_KEY, "1"));
        this.mPath.reset();
        for (i = 0; i < HISTORY_SIZE_PER_PAGE; i++) {
            pos = (this.mCurrentDrawPointer - i) - 1;
            if (pos < 0 || this.mHistoryData[pos].monitor == 0) {
                if (hasLine) {
                    canvas.drawPath(this.mPath, this.mPaint);
                    this.mPath.reset();
                }
                firstPoint = true;
                hasLine = false;
            } else {
                float x = (float) (((double) (getWidth() / 2)) + (((double) TextTrackStyle.DEFAULT_FONT_SCALE) * calculateValueFromAcceleration(this.mHistoryData[pos], scaleType)));
                float y = (((float) i) * this.mBackgroundPassLength) + ((float) this.mGraphOffsetY);
                if (firstPoint) {
                    this.mPath.moveTo(x, y);
                    firstPoint = false;
                    hasLine = false;
                } else {
                    this.mPath.lineTo(x, y);
                    hasLine = true;
                }
            }
        }
        if (hasLine) {
            canvas.drawPath(this.mPath, this.mPaint);
        }
        setDrawingCacheEnabled(true);
    }

    void onTouchDown(View source, Point currentPosition) {
        this.mIsMoved = false;
        this.mPrevPoint = currentPosition;
        this.mDownPoint = currentPosition;
    }

    void onTouchMove(View source, Point currentPosition) {
        Point pt = currentPosition;
        if (!this.mIsMoved) {
            if (Math.sqrt((double) (((this.mDownPoint.x - pt.x) * (this.mDownPoint.x - pt.x)) + ((this.mDownPoint.y - pt.y) * (this.mDownPoint.y - pt.y)))) > ((double) ViewConfiguration.get(getContext()).getScaledTouchSlop())) {
                this.mIsMoved = true;
            } else {
                return;
            }
        }
        this.mIsMoved = true;
        if (!SeismometerActivity.mUserRunning && this.mCurrentPointer > HISTORY_SIZE_PER_PAGE) {
            Point point = currentPosition;
            float offsetDraw = (float) Math.round(((float) (point.y - this.mPrevPoint.y)) / this.mBackgroundPassLength);
            this.mCurrentDrawPointer = (int) (((float) this.mCurrentDrawPointer) + offsetDraw);
            if (this.mCurrentDrawPointer < HISTORY_SIZE_PER_PAGE) {
                this.mCurrentDrawPointer = HISTORY_SIZE_PER_PAGE;
            }
            if (this.mCurrentDrawPointer >= this.mCurrentPointer) {
                this.mCurrentDrawPointer = this.mCurrentPointer;
            }
            if (this.mCurrentDrawPointer > HISTORY_SIZE_PER_PAGE && this.mCurrentDrawPointer < this.mCurrentPointer && this.mDelegate != null) {
                this.mDelegate.moveGraph(this.mBackgroundPassLength * offsetDraw);
            }
            this.mPrevPoint = point;
            invalidate();
        }
    }

    void onTouchUp(View source, Point currentPosition) {
        if (!this.mIsMoved && this.mDelegate != null) {
            this.mDelegate.onClick(this);
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            onTouchDown(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 1) {
            onTouchUp(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 2) {
            onTouchMove(v, new Point((int) event.getX(), (int) event.getY()));
        }
        return true;
    }

    public class SeismometerAlarmLevel {
        public static final int SEISMOMETER_ALARM_LEVEL_1 = 0;
        public static final int SEISMOMETER_ALARM_LEVEL_2 = 1;
        public static final int SEISMOMETER_ALARM_LEVEL_3 = 2;
    }

    public class SeismometerAlarmSound {
        public static final int SEISMOMETER_ALARM_SOUND_1 = 0;
        public static final int SEISMOMETER_ALARM_SOUND_2 = 1;
        public static final int SEISMOMETER_ALARM_SOUND_3 = 2;
    }

    public class SeismometerMonitorAxis {
        public static final int SEISMOMETER_MONITOR_NONE = 0;
        public static final int SEISMOMETER_MONITOR_X = 1;
        public static final int SEISMOMETER_MONITOR_XY = 3;
        public static final int SEISMOMETER_MONITOR_XYZ = 7;
        public static final int SEISMOMETER_MONITOR_XZ = 5;
        public static final int SEISMOMETER_MONITOR_Y = 2;
        public static final int SEISMOMETER_MONITOR_YZ = 6;
        public static final int SEISMOMETER_MONITOR_Z = 4;
    }

    public class SeismometerScaleType {
        public static final int SEISMOMETER_SCALE_LINEAR = 0;
        public static final int SEISMOMETER_SCALE_LOGARITHMIC = 1;
    }
}
