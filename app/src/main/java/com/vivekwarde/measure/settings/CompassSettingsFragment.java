package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;

@SuppressLint({"NewApi"})
public class CompassSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_compass);
        Preference pref = findPreference(SettingsKey.SETTINGS_COMPASS_HEADING_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(CompassSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
    }
}
