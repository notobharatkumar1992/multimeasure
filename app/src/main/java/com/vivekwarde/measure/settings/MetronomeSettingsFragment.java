package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.io.IOException;

@SuppressLint({"NewApi"})
public class MetronomeSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_metronome);
        findPreference(SettingsKey.SETTINGS_METRONOME_FLASH_ENABLED_KEY).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                MetronomeSettingsFragment.this.updateFlashingControls();
                return false;
            }
        });
        updateFlashingControls();
        Preference pref = findPreference(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(MetronomeSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        try {
            String[] metronomeSoundFileNameList = getActivity().getAssets().list("sounds/metronome");
            String[] metronomeSoundNameList = new String[(metronomeSoundFileNameList.length + 1)];
            metronomeSoundNameList[0] = "No Sound";
            int i = 0;
            while (true) {
                int length = metronomeSoundFileNameList.length;
                if (i < length) {
                    metronomeSoundNameList[i + 1] = metronomeSoundFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
                    i++;
                } else {
                    String firstBeatSoundSettingsKey = SettingsKey.SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY;
                    String mainBeatSoundSettingsKey = SettingsKey.SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY;
                    String subBeatSoundSettingsKey = SettingsKey.SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY;
                    Preference firstBeatSoundPref = findPreference(firstBeatSoundSettingsKey);
                    Preference mainBeatSoundPref = findPreference(mainBeatSoundSettingsKey);
                    Preference subBeatSoundPref = findPreference(subBeatSoundSettingsKey);
                    ((ListPreference) firstBeatSoundPref).setEntries(metronomeSoundNameList);
                    ((ListPreference) firstBeatSoundPref).setEntryValues(metronomeSoundNameList);
                    ((ListPreference) mainBeatSoundPref).setEntries(metronomeSoundNameList);
                    ((ListPreference) mainBeatSoundPref).setEntryValues(metronomeSoundNameList);
                    ((ListPreference) subBeatSoundPref).setEntries(metronomeSoundNameList);
                    ((ListPreference) subBeatSoundPref).setEntryValues(metronomeSoundNameList);
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    firstBeatSoundPref.setSummary(sharedPref.getString(firstBeatSoundSettingsKey, metronomeSoundNameList[24]));
                    firstBeatSoundPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                        public boolean onPreferenceChange(Preference preference, Object newValue) {
                            preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                            preference.setDefaultValue(newValue);
                            Editor editor = PreferenceManager.getDefaultSharedPreferences(MetronomeSettingsFragment.this.getActivity()).edit();
                            editor.putString(preference.getKey(), newValue.toString());
                            editor.apply();
                            SoundUtility.getInstance().previewSound("sounds/metronome/" + newValue.toString() + ".mp3");
                            return false;
                        }
                    });
                    mainBeatSoundPref.setSummary(sharedPref.getString(mainBeatSoundSettingsKey, metronomeSoundNameList[23]));
                    mainBeatSoundPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                        public boolean onPreferenceChange(Preference preference, Object newValue) {
                            preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                            preference.setDefaultValue(newValue);
                            Editor editor = PreferenceManager.getDefaultSharedPreferences(MetronomeSettingsFragment.this.getActivity()).edit();
                            editor.putString(preference.getKey(), newValue.toString());
                            editor.apply();
                            SoundUtility.getInstance().previewSound("sounds/metronome/" + newValue.toString() + ".mp3");
                            return false;
                        }
                    });
                    subBeatSoundPref.setSummary(sharedPref.getString(subBeatSoundSettingsKey, metronomeSoundNameList[23]));
                    subBeatSoundPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                        public boolean onPreferenceChange(Preference preference, Object newValue) {
                            preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                            preference.setDefaultValue(newValue);
                            Editor editor = PreferenceManager.getDefaultSharedPreferences(MetronomeSettingsFragment.this.getActivity()).edit();
                            editor.putString(preference.getKey(), newValue.toString());
                            editor.apply();
                            SoundUtility.getInstance().previewSound("sounds/metronome/" + newValue.toString() + ".mp3");
                            return false;
                        }
                    });
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateFlashingControls() {
        findPreference(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY).setEnabled(Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(SettingsKey.SETTINGS_METRONOME_FLASH_ENABLED_KEY, true)).booleanValue());
    }
}
