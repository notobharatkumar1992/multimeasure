package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;

import java.util.Locale;

@SuppressLint({"NewApi"})
public class PlumbBobSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_plumb_bob);
        Preference pref = findPreference(SettingsKey.SETTINGS_PLUMB_BOB_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(PlumbBobSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        findPreference(SettingsKey.SETTINGS_PLUMB_BOB_RESET_CALIBRATION).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                Builder builder = new Builder(PlumbBobSettingsFragment.this.getActivity());
                builder.setMessage(PlumbBobSettingsFragment.this.getString(R.string.IDS_RESET_CALIBRATION_ASKING)).setCancelable(false).setPositiveButton("OK", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(PlumbBobSettingsFragment.this.getActivity()).edit();
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 4; j++) {
                                editor.putFloat(String.format(Locale.getDefault(), SettingsKey.SETTINGS_PLUMB_BOB_CALIB_MATRIX_KEY, new Object[]{Integer.valueOf(i), Integer.valueOf(j)}), 0.0f);
                            }
                        }
                        editor.putFloat(SettingsKey.SETTINGS_PLUMB_BOB_CALIB_ANGLE_OXY, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZY, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZX, 0.0f);
                        editor.apply();
                    }
                }).setNegativeButton(PlumbBobSettingsFragment.this.getString(R.string.IDS_CANCEL), new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                MiscUtility.setDialogFontSize(PlumbBobSettingsFragment.this.getActivity(), alert);
                return true;
            }
        });
    }
}
