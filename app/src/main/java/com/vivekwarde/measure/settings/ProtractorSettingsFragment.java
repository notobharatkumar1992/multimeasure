package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import java.util.Arrays;

@SuppressLint({"NewApi"})
public class ProtractorSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_protractor);
        Preference pref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(ProtractorSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        pref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(ProtractorSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_KEY).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                ProtractorSettingsFragment.this.updateSnappingControls();
                return false;
            }
        });
        updateSnappingControls();
        pref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY);
        String sId = ((ListPreference) pref).getValue();
        pref.setSummary(getResources().getStringArray(R.array.protractor_angle_unit_names)[Integer.parseInt(sId)]);
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(ProtractorSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                ProtractorSettingsFragment.this.loadSnappingListAccordingTo(newValue.toString());
                return false;
            }
        });
        loadSnappingListAccordingTo(sId);
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY).setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ProtractorSettingsFragment.this.getActivity());
                Editor editor = sharedPref.edit();
                Integer unitToSnapIndex = Integer.valueOf(Integer.parseInt(sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY, "0")));
                String[] unitToSnapStringEntries = ProtractorSettingsFragment.this.getResources().getStringArray(R.array.protractor_angle_unit_values);
                if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("0")) {
                    editor.putString(SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, newValue.toString());
                } else if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("1")) {
                    editor.putString(SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, newValue.toString());
                } else if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("2")) {
                    editor.putString(SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, newValue.toString());
                } else if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("3")) {
                    editor.putString(SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY, newValue.toString());
                }
                editor.apply();
                return false;
            }
        });
    }

    private void loadSnappingListAccordingTo(String unitToSnap) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Preference pref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY);
        String snappingSensitivityValue = null;
        String[] sensitivityEntries = null;
        String[] sensitivityValues = null;
        if (unitToSnap.equals("0")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.angle_degree_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.angle_degree_sensitivity_values));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.angle_degree_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_degree_sensitivity_values);
        } else if (unitToSnap.equals("1")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.angle_radian_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.angle_radian_sensitivity_values));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, "0.01");
            sensitivityEntries = getResources().getStringArray(R.array.angle_radian_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_radian_sensitivity_values);
        } else if (unitToSnap.equals("2")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.angle_gradian_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.angle_gradian_sensitivity_values));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.angle_gradian_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_gradian_sensitivity_values);
        } else if (unitToSnap.equals("3")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.angle_revolution_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.angle_revolution_sensitivity_names));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY, "0.0277777777777778");
            sensitivityEntries = getResources().getStringArray(R.array.angle_revolution_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_revolution_sensitivity_values);
        }
        pref.setSummary(sensitivityEntries[Arrays.asList(sensitivityValues).indexOf(snappingSensitivityValue)]);
    }

    private void updateSnappingControls() {
        Boolean isSnapping = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_KEY, false));
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY).setEnabled(isSnapping.booleanValue());
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY).setEnabled(isSnapping.booleanValue());
    }
}
