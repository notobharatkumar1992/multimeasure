package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.ruler.RulerActivity;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import java.util.Arrays;
import java.util.Locale;

@SuppressLint({"NewApi"})
public class RulerSettingsFragment extends PreferenceFragment {

    /* renamed from: com.vivekwarde.measure.settings.RulerSettingsFragment.6 */
    class AnonymousClass6 implements OnPreferenceClickListener {
        final /* synthetic */ Preference val$calibrationPref;

        AnonymousClass6(Preference preference) {
            this.val$calibrationPref = preference;
        }

        public boolean onPreferenceClick(Preference preference) {
            CharSequence[] str = new CharSequence[]{RulerSettingsFragment.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_1), RulerSettingsFragment.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_2), RulerSettingsFragment.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_3)};
            Builder b = new Builder(RulerSettingsFragment.this.getActivity());
            b.setTitle(RulerSettingsFragment.this.getResources().getString(R.string.IDS_CALIBRATION));
            b.setItems(str, new OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    SettingsActivity.onCalibrate(RulerSettingsFragment.this.getActivity(), position, AnonymousClass6.this.val$calibrationPref);
                }
            });
            b.create().show();
            return false;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_ruler);
        Preference pref = findPreference(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(RulerSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        pref = findPreference(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(RulerSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_KEY).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                RulerSettingsFragment.this.updateSnappingControls();
                return false;
            }
        });
        updateSnappingControls();
        pref = findPreference(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY);
        String sId = ((ListPreference) pref).getValue();
        pref.setSummary(getResources().getStringArray(R.array.length_unit_names)[Integer.parseInt(sId)]);
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(RulerSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                RulerSettingsFragment.this.loadSnappingListAccordingTo(newValue.toString());
                return false;
            }
        });
        loadSnappingListAccordingTo(sId);
        findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY).setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(RulerSettingsFragment.this.getActivity());
                Editor editor = sharedPref.edit();
                Integer unitToSnapIndex = Integer.valueOf(Integer.parseInt(sharedPref.getString(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY, "0")));
                String[] unitToSnapStringEntries = RulerSettingsFragment.this.getResources().getStringArray(R.array.length_unit_values);
                if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("0")) {
                    editor.putString(SettingsKey.SETTINGS_RULER_CENTIMETER_SNAP_KEY, newValue.toString());
                } else if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("1")) {
                    editor.putString(SettingsKey.SETTINGS_RULER_INCH_SNAP_KEY, newValue.toString());
                } else if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("2")) {
                    editor.putString(SettingsKey.SETTINGS_RULER_PIXEL_SNAP_KEY, newValue.toString());
                } else if (unitToSnapStringEntries[unitToSnapIndex.intValue()].equals("3")) {
                    editor.putString(SettingsKey.SETTINGS_RULER_PICA_SNAP_KEY, newValue.toString());
                }
                editor.apply();
                return false;
            }
        });
        Preference calibrationPref = findPreference(SettingsKey.SETTINGS_RULER_DPI_KEY);
        float dpi = PreferenceManager.getDefaultSharedPreferences(getActivity()).getFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, RulerActivity.getScreenDPI(getActivity()));
        calibrationPref.setSummary(String.format(Locale.US, "%s: %.2f", new Object[]{getResources().getString(R.string.IDS_CALIBRATION_CURRENT_PPI), Float.valueOf(dpi)}));
        calibrationPref.setOnPreferenceClickListener(new AnonymousClass6(calibrationPref));
    }

    private void loadSnappingListAccordingTo(String unitToSnap) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Preference pref = findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY);
        String snappingSensitivityValue = null;
        String[] sensitivityEntries = null;
        String[] sensitivityValues = null;
        if (unitToSnap.equals("0")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.length_centimeter_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.length_centimeter_sensitivity_values));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_CENTIMETER_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.length_centimeter_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_centimeter_sensitivity_values);
        } else if (unitToSnap.equals("1")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.length_inch_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.length_inch_sensitivity_values));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_INCH_SNAP_KEY, "0.25");
            sensitivityEntries = getResources().getStringArray(R.array.length_inch_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_inch_sensitivity_values);
        } else if (unitToSnap.equals("2")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.length_pixel_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.length_pixel_sensitivity_values));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_PIXEL_SNAP_KEY, "50");
            sensitivityEntries = getResources().getStringArray(R.array.length_pixel_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_pixel_sensitivity_values);
        } else if (unitToSnap.equals("3")) {
            ((ListPreference) pref).setEntries(getResources().getStringArray(R.array.length_pica_sensitivity_names));
            ((ListPreference) pref).setEntryValues(getResources().getStringArray(R.array.length_pica_sensitivity_values));
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_PICA_SNAP_KEY, "0.25");
            sensitivityEntries = getResources().getStringArray(R.array.length_pica_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_pica_sensitivity_values);
        }
        pref.setSummary(sensitivityEntries[Arrays.asList(sensitivityValues).indexOf(snappingSensitivityValue)]);
    }

    private void updateSnappingControls() {
        Boolean isSnapping = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(SettingsKey.SETTINGS_RULER_SNAPPING_KEY, false));
        findPreference(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY).setEnabled(isSnapping.booleanValue());
        findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY).setEnabled(isSnapping.booleanValue());
    }
}
