package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.SoundUtility;
import java.io.IOException;

@SuppressLint({"NewApi"})
public class SeismometerSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_seismometer);
        Preference pref = findPreference(SettingsKey.SETTINGS_SEISMOMETER_SCALE_TYPE_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(SeismometerSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        pref = findPreference(SettingsKey.SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(SeismometerSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        try {
            String[] seisAlertFileNameList = getActivity().getAssets().list("sounds/seismometer/alarm");
            String[] seisAlertSoundNameList = new String[seisAlertFileNameList.length];
            for (int i = 0; i < seisAlertFileNameList.length; i++) {
                seisAlertSoundNameList[i] = seisAlertFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
            }
            String alertSoundSettingsKey = SettingsKey.SETTINGS_SEISMOMETER_ALARM_SOUND_KEY;
            pref = findPreference(alertSoundSettingsKey);
            ((ListPreference) pref).setEntries(seisAlertSoundNameList);
            ((ListPreference) pref).setEntryValues(seisAlertSoundNameList);
            pref.setSummary(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(alertSoundSettingsKey, seisAlertSoundNameList[0]));
            pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                    preference.setDefaultValue(newValue);
                    Editor editor = PreferenceManager.getDefaultSharedPreferences(SeismometerSettingsFragment.this.getActivity()).edit();
                    editor.putString(preference.getKey(), newValue.toString());
                    editor.apply();
                    SoundUtility.getInstance().previewSound("sounds/seismometer/alarm/" + newValue.toString() + ".mp3");
                    return false;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
