package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.view.InputDeviceCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.ruler.RulerActivity;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener, OnPreferenceClickListener {

    static void onCalibrate(Context context, int type, Preference calibrationPref) {
        String msg;
        View view;
        String title = context.getResources().getString(R.string.IDS_CALIBRATION);
        if (type == 0) {
            msg = context.getResources().getString(R.string.IDS_CALIBRATION_MESSAGE_1);
        } else if (type == 1) {
            msg = context.getResources().getString(R.string.IDS_CALIBRATION_MESSAGE_2);
        } else {
            msg = context.getResources().getString(R.string.IDS_CALIBRATION_MESSAGE_3);
        }
        String done = context.getResources().getString(R.string.IDS_DONE);
        String close = context.getResources().getString(R.string.IDS_CLOSE);
        EditText input = new EditText(context);
        input.setInputType(InputDeviceCompat.SOURCE_MOUSE);
        Builder builder = new Builder(context);
        if (type <= 1) {
            view = input;
        } else {
            view = null;
        }
        AlertDialog d = builder.setView(view).setTitle(title).setMessage(msg).setPositiveButton(done, null).setNegativeButton(close, null).create();
        d.setOnShowListener(new AnonymousClass2(d, input, type, context, calibrationPref));
        d.show();
    }

    protected void onCreate(Bundle savedInstanceState) {
        int i;
        super.onCreate(savedInstanceState);
        String action = getIntent().getAction();
        if (action != null) {
            if (action.equals("settings_general")) {
                addPreferencesFromResource(R.xml.settings_general);
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_protractor")) {
                addPreferencesFromResource(R.xml.settings_protractor);
                Preference primaryUnitPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY);
                primaryUnitPref.setSummary(((ListPreference) primaryUnitPref).getEntry());
                Preference secondaryUnitPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY);
                secondaryUnitPref.setSummary(((ListPreference) secondaryUnitPref).getEntry());
                Preference unitToSnapPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY);
                unitToSnapPref.setSummary(((ListPreference) unitToSnapPref).getEntry());
                Preference sensitivityPref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY);
                sensitivityPref.setSummary(((ListPreference) sensitivityPref).getEntry());
                loadProtractorSnappingListAccordingTo(((ListPreference) unitToSnapPref).getValue());
                updateProtractorSnappingControls();
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_ruler")) {
                addPreferencesFromResource(R.xml.settings_ruler);
                Preference primaryUnitPref = findPreference(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY);
                primaryUnitPref.setSummary(((ListPreference) primaryUnitPref).getEntry());
                Preference secondaryUnitPref = findPreference(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY);
                secondaryUnitPref.setSummary(((ListPreference) secondaryUnitPref).getEntry());
                Preference unitToSnapPref = findPreference(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY);
                unitToSnapPref.setSummary(((ListPreference) unitToSnapPref).getEntry());
                Preference sensitivityPref = findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY);
                sensitivityPref.setSummary(((ListPreference) sensitivityPref).getEntry());
                Preference calibrationPref = findPreference(SettingsKey.SETTINGS_RULER_DPI_KEY);
                float dpi = PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, RulerActivity.getScreenDPI(this));
                calibrationPref.setSummary(String.format(Locale.US, "%s: %.2f", new Object[]{getResources().getString(R.string.IDS_CALIBRATION_CURRENT_PPI), Float.valueOf(dpi)}));
                calibrationPref.setOnPreferenceClickListener(new AnonymousClass1(calibrationPref));
                loadRulerSnappingListAccordingTo(((ListPreference) unitToSnapPref).getValue());
                updateRulerSnappingControls();
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_surface_level")) {
                addPreferencesFromResource(R.xml.settings_surface_level);
                Preference unitPref = findPreference(SettingsKey.SETTINGS_SURFACE_LEVEL_UNIT_KEY);
                unitPref.setSummary(((ListPreference) unitPref).getEntry());
                Preference themePref = findPreference(SettingsKey.SETTINGS_SURFACE_LEVEL_THEME_KEY);
                themePref.setSummary(((ListPreference) themePref).getEntry());
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_spirit_level")) {
                addPreferencesFromResource(R.xml.settings_spirit_level);
                Preference unitPref = findPreference(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY);
                unitPref.setSummary(((ListPreference) unitPref).getEntry());
                Preference themePref = findPreference(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY);
                themePref.setSummary(((ListPreference) themePref).getEntry());
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_plumb_bob")) {
                addPreferencesFromResource(R.xml.settings_plumb_bob);
                Preference unitPref = findPreference(SettingsKey.SETTINGS_PLUMB_BOB_UNIT_KEY);
                unitPref.setSummary(((ListPreference) unitPref).getEntry());
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_seismometer")) {
                addPreferencesFromResource(R.xml.settings_seismometer);
                Preference graphScalePref = findPreference(SettingsKey.SETTINGS_SEISMOMETER_SCALE_TYPE_KEY);
                graphScalePref.setSummary(((ListPreference) graphScalePref).getEntry());
                Preference alertSensitivityPref = findPreference(SettingsKey.SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY);
                alertSensitivityPref.setSummary(((ListPreference) alertSensitivityPref).getEntry());
                try {
                    String[] seisAlertFileNameList = getAssets().list("sounds/seismometer/alarm");
                    String[] seisAlertSoundNameList = new String[seisAlertFileNameList.length];
                    for (i = 0; i < seisAlertFileNameList.length; i++) {
                        seisAlertSoundNameList[i] = seisAlertFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
                    }
                    String alertSoundSettingsKey = SettingsKey.SETTINGS_SEISMOMETER_ALARM_SOUND_KEY;
                    Preference alertSoundPref = findPreference(alertSoundSettingsKey);
                    ((ListPreference) alertSoundPref).setEntries(seisAlertSoundNameList);
                    ((ListPreference) alertSoundPref).setEntryValues(seisAlertSoundNameList);
                    alertSoundPref.setSummary(PreferenceManager.getDefaultSharedPreferences(this).getString(alertSoundSettingsKey, seisAlertSoundNameList[0]));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_stopwatch")) {
                addPreferencesFromResource(R.xml.settings_stopwatch);
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_timer")) {
                addPreferencesFromResource(R.xml.settings_timer);
                for (int timerIndex = 1; timerIndex <= 5; timerIndex++) {
                    Preference configureScreenPreference = findPreference(String.format(Locale.getDefault(), SettingsKey.SETTINGS_TIMER_CONFIGURE_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)}));
                    configureScreenPreference.setTitle(configureScreenPreference.getTitle().toString() + String.format(Locale.US, " %1$d", new Object[]{Integer.valueOf(timerIndex)}));
                    EditTextPreference timerNamePreference = (EditTextPreference) findPreference(String.format(Locale.getDefault(), SettingsKey.SETTINGS_TIMER_NAME_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)}));
                    timerNamePreference.setDefaultValue(String.format(getString(R.string.IDS_TIMER) + " %1$d", new Object[]{Integer.valueOf(timerIndex)}));
                    timerNamePreference.setSummary(timerNamePreference.getText());
                    String[] durationNamesFormat = getResources().getStringArray(R.array.timer_duration_names);
                    String[] durationNames = durationNamesFormat;
                    for (i = 0; i < durationNamesFormat.length; i++) {
                        if (i < 6) {
                            durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_SECONDS)});
                        } else if (i < 7) {
                            durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_MINUTE)});
                        } else if (i < 12) {
                            durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_MINUTES)});
                        } else if (i < 13) {
                            durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_HOUR)});
                        } else {
                            durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_HOURS)});
                        }
                    }
                    ((ListPreference) findPreference(String.format(Locale.getDefault(), SettingsKey.SETTINGS_TIMER_DEFAULT_DURATION_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)}))).setEntries(durationNames);
                    try {
                        String[] timerSoundFileNameList = getAssets().list("sounds/timer");
                        String[] timerSoundNameList = new String[timerSoundFileNameList.length];
                        for (i = 0; i < timerSoundNameList.length; i++) {
                            timerSoundNameList[i] = timerSoundFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
                        }
                        String timerNotificationSoundSettingsKey = String.format(Locale.getDefault(), SettingsKey.SETTINGS_TIMER_ALARM_SOUND_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)});
                        Preference notificationSoundPreference = findPreference(timerNotificationSoundSettingsKey);
                        ((ListPreference) notificationSoundPreference).setEntries(timerSoundNameList);
                        ((ListPreference) notificationSoundPreference).setEntryValues(timerSoundNameList);
                        notificationSoundPreference.setSummary(PreferenceManager.getDefaultSharedPreferences(this).getString(timerNotificationSoundSettingsKey, timerSoundNameList[timerIndex - 1]));
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_metronome")) {
                addPreferencesFromResource(R.xml.settings_metronome);
                updateMetronomeFlashingControls();
                Preference flashingOnPref = findPreference(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY);
                flashingOnPref.setSummary(((ListPreference) flashingOnPref).getEntry());
                try {
                    String[] metronomeSoundFileNameList = getAssets().list("sounds/metronome");
                    String[] metronomeSoundNameList = new String[(metronomeSoundFileNameList.length + 1)];
                    metronomeSoundNameList[0] = "No Sound";
                    for (i = 0; i < metronomeSoundFileNameList.length; i++) {
                        metronomeSoundNameList[i + 1] = metronomeSoundFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
                    }
                    String firstBeatSoundSettingsKey = SettingsKey.SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY;
                    String mainBeatSoundSettingsKey = SettingsKey.SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY;
                    String subBeatSoundSettingsKey = SettingsKey.SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY;
                    Preference firstBeatSoundPref = findPreference(firstBeatSoundSettingsKey);
                    Preference mainBeatSoundPref = findPreference(mainBeatSoundSettingsKey);
                    Preference subBeatSoundPref = findPreference(subBeatSoundSettingsKey);
                    ((ListPreference) firstBeatSoundPref).setEntries(metronomeSoundNameList);
                    ((ListPreference) firstBeatSoundPref).setEntryValues(metronomeSoundNameList);
                    ((ListPreference) mainBeatSoundPref).setEntries(metronomeSoundNameList);
                    ((ListPreference) mainBeatSoundPref).setEntryValues(metronomeSoundNameList);
                    ((ListPreference) subBeatSoundPref).setEntries(metronomeSoundNameList);
                    ((ListPreference) subBeatSoundPref).setEntryValues(metronomeSoundNameList);
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                    firstBeatSoundPref.setSummary(sharedPref.getString(firstBeatSoundSettingsKey, metronomeSoundNameList[24]));
                    mainBeatSoundPref.setSummary(sharedPref.getString(mainBeatSoundSettingsKey, metronomeSoundNameList[23]));
                    subBeatSoundPref.setSummary(sharedPref.getString(subBeatSoundSettingsKey, metronomeSoundNameList[23]));
                } catch (IOException e22) {
                    e22.printStackTrace();
                }
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_decibel")) {
                addPreferencesFromResource(R.xml.settings_decibel);
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_teslameter")) {
                addPreferencesFromResource(R.xml.settings_teslameter);
                Preference unitPref = findPreference(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY);
                unitPref.setSummary(((ListPreference) unitPref).getEntry());
                try {
                    String[] teslameterSoundFileNameList = getAssets().list("sounds/teslameter");
                    String[] teslameterSoundNameList = new String[teslameterSoundFileNameList.length];
                    for (i = 0; i < teslameterSoundNameList.length; i++) {
                        teslameterSoundNameList[i] = teslameterSoundFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
                    }
                    Preference alertSoundSettingsPref = findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY);
                    ((ListPreference) alertSoundSettingsPref).setEntries(teslameterSoundNameList);
                    ((ListPreference) alertSoundSettingsPref).setEntryValues(teslameterSoundNameList);
                    alertSoundSettingsPref.setSummary(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY, teslameterSoundNameList[3]));
                } catch (IOException e222) {
                    e222.printStackTrace();
                }
                updateTeslameterAlertControls();
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_compass")) {
                addPreferencesFromResource(R.xml.settings_compass);
                Preference headingPref = findPreference(SettingsKey.SETTINGS_COMPASS_HEADING_KEY);
                headingPref.setSummary(((ListPreference) headingPref).getEntry());
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_altimeter")) {
                addPreferencesFromResource(R.xml.settings_altimeter);
                Preference getMethodPref = findPreference(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY);
                getMethodPref.setSummary(((ListPreference) getMethodPref).getEntry());
                Preference unitPref = findPreference(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY);
                unitPref.setSummary(((ListPreference) unitPref).getEntry());
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (action != null) {
            if (action.equals("settings_about")) {
                addPreferencesFromResource(R.xml.settings_about);
                MainApplication.getTracker().setScreenName(getClass().getSimpleName());
                MainApplication.getTracker().send(new ScreenViewBuilder().build());
            }
        }
        if (VERSION.SDK_INT < 11) {
            addPreferencesFromResource(R.xml.preference_headers_legacy);
        }
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    @SuppressLint({"NewApi"})
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preference_headers, target);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_RULER_PRIMARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_RULER_SECONDARY_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY) || key.equals(SettingsKey.SETTINGS_SURFACE_LEVEL_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SURFACE_LEVEL_THEME_KEY) || key.equals(SettingsKey.SETTINGS_PLUMB_BOB_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_SEISMOMETER_SCALE_TYPE_KEY) || key.equals(SettingsKey.SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY) || key.equals(SettingsKey.SETTINGS_SEISMOMETER_UPDATE_FREQUENCY_KEY) || key.equals(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY) || key.startsWith("SETTINGS_TIMER_DEFAULT_DURATION") || key.equals(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY) || key.equals(SettingsKey.SETTINGS_COMPASS_HEADING_KEY) || key.equals(SettingsKey.SETTINGS_ALTIMETER_GET_METHOD_KEY) || key.equals(SettingsKey.SETTINGS_ALTIMETER_UNIT_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
        } else if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            loadProtractorSnappingListAccordingTo(getListPreferenceValue(key));
        } else if (key.equals(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            loadRulerSnappingListAccordingTo(getListPreferenceValue(key));
        } else if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            Editor editor = sharedPreferences.edit();
            String unitToSnap = getListPreferenceValue(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY);
            String snapValue = ((ListPreference) findPreference(key)).getValue();
            if (unitToSnap.equals("0")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("1")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("2")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("3")) {
                editor.putString(SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY, snapValue);
            }
            editor.apply();
        } else if (key.equals(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            Editor editor = sharedPreferences.edit();
            String unitToSnap = getListPreferenceValue(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY);
            String snapValue = ((ListPreference) findPreference(key)).getValue();
            if (unitToSnap.equals("0")) {
                editor.putString(SettingsKey.SETTINGS_RULER_CENTIMETER_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("1")) {
                editor.putString(SettingsKey.SETTINGS_RULER_INCH_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("2")) {
                editor.putString(SettingsKey.SETTINGS_RULER_PIXEL_SNAP_KEY, snapValue);
            } else if (unitToSnap.equals("3")) {
                editor.putString(SettingsKey.SETTINGS_RULER_PICA_SNAP_KEY, snapValue);
            }
            editor.apply();
        } else if (key.equals(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY) || key.startsWith("SETTINGS_TIMER_NAME") || key.startsWith("SETTINGS_TIMER_MESSAGE")) {
            EditTextPreference pref = (EditTextPreference) findPreference(key);
            pref.setSummary(pref.getText());
        } else if (key.equals(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_KEY)) {
            updateProtractorSnappingControls();
        } else if (key.equals(SettingsKey.SETTINGS_RULER_SNAPPING_KEY)) {
            updateRulerSnappingControls();
        } else if (key.equals(SettingsKey.SETTINGS_METRONOME_FLASH_ENABLED_KEY)) {
            updateMetronomeFlashingControls();
        } else if (key.equals(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY)) {
            updateTeslameterAlertControls();
        } else if (key.equals(SettingsKey.SETTINGS_SEISMOMETER_ALARM_SOUND_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/seismometer/alarm/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        } else if (key.equals(SettingsKey.SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY) || key.equals(SettingsKey.SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY) || key.equals(SettingsKey.SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/metronome/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        } else if (key.equals(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY)) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/teslameter/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        } else if (key.startsWith("SETTINGS_TIMER_ALARM_SOUND")) {
            findPreference(key).setSummary(getListPreferenceEntry(key));
            SoundUtility.getInstance().previewSound("sounds/timer/" + ((ListPreference) findPreference(key)).getValue() + ".mp3");
        }
    }

    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

    public void onResume() {
        super.onResume();
        if (VERSION.SDK_INT < 11) {
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }
    }

    public void onPause() {
        if (VERSION.SDK_INT < 11) {
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }
        super.onPause();
    }

    private void loadProtractorSnappingListAccordingTo(String unitToSnap) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Preference pref = findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY);
        String snappingSensitivityValue = null;
        String[] sensitivityEntries = null;
        String[] sensitivityValues = null;
        if (unitToSnap.equals("0")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.angle_degree_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_degree_sensitivity_values);
        } else if (unitToSnap.equals("1")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY, "0.01");
            sensitivityEntries = getResources().getStringArray(R.array.angle_radian_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_radian_sensitivity_values);
        } else if (unitToSnap.equals("2")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.angle_gradian_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_gradian_sensitivity_values);
        } else if (unitToSnap.equals("3")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY, "0.0277777777777778");
            sensitivityEntries = getResources().getStringArray(R.array.angle_revolution_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.angle_revolution_sensitivity_values);
        }
        ((ListPreference) pref).setEntries(sensitivityEntries);
        ((ListPreference) pref).setEntryValues(sensitivityValues);
        pref.setSummary(sensitivityEntries[Arrays.asList(sensitivityValues).indexOf(snappingSensitivityValue)]);
    }

    private void loadRulerSnappingListAccordingTo(String unitToSnap) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Preference pref = findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY);
        String snappingSensitivityValue = null;
        String[] sensitivityEntries = null;
        String[] sensitivityValues = null;
        if (unitToSnap.equals("0")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_CENTIMETER_SNAP_KEY, "1");
            sensitivityEntries = getResources().getStringArray(R.array.length_centimeter_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_centimeter_sensitivity_values);
        } else if (unitToSnap.equals("1")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_INCH_SNAP_KEY, "0.25");
            sensitivityEntries = getResources().getStringArray(R.array.length_inch_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_inch_sensitivity_values);
        } else if (unitToSnap.equals("2")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_PIXEL_SNAP_KEY, "50");
            sensitivityEntries = getResources().getStringArray(R.array.length_pixel_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_pixel_sensitivity_values);
        } else if (unitToSnap.equals("3")) {
            snappingSensitivityValue = sharedPref.getString(SettingsKey.SETTINGS_RULER_PICA_SNAP_KEY, "0.25");
            sensitivityEntries = getResources().getStringArray(R.array.length_pica_sensitivity_names);
            sensitivityValues = getResources().getStringArray(R.array.length_pica_sensitivity_values);
        }
        ((ListPreference) pref).setEntries(sensitivityEntries);
        ((ListPreference) pref).setEntryValues(sensitivityValues);
        pref.setSummary(sensitivityEntries[Arrays.asList(sensitivityValues).indexOf(snappingSensitivityValue)]);
    }

    private String getListPreferenceEntry(String key) {
        return ((ListPreference) findPreference(key)).getEntry().toString();
    }

    private String getListPreferenceValue(String key) {
        return ((ListPreference) findPreference(key)).getValue();
    }

    private void updateProtractorSnappingControls() {
        Boolean isSnapping = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_KEY, false));
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY).setEnabled(isSnapping.booleanValue());
        findPreference(SettingsKey.SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY).setEnabled(isSnapping.booleanValue());
    }

    private void updateRulerSnappingControls() {
        Boolean isSnapping = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_RULER_SNAPPING_KEY, false));
        findPreference(SettingsKey.SETTINGS_RULER_UNIT_TO_SNAP_KEY).setEnabled(isSnapping.booleanValue());
        findPreference(SettingsKey.SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY).setEnabled(isSnapping.booleanValue());
    }

    private void updateMetronomeFlashingControls() {
        findPreference(SettingsKey.SETTINGS_METRONOME_FLASH_TYPE_KEY).setEnabled(Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_METRONOME_FLASH_ENABLED_KEY, true)).booleanValue());
    }

    private void updateTeslameterAlertControls() {
        Boolean enableAlert = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY, true));
        findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY).setEnabled(enableAlert.booleanValue());
        findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY).setEnabled(enableAlert.booleanValue());
    }

    public void onAskForSupport(View v) {
        MiscUtility.sendEmail(this, "mailto:support@skypaw.com", getResources().getString(R.string.IDS_MULTI_MEASURES), BuildConfig.FLAVOR, null);
    }

    public void onReviewOnMarket(View v) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
        startActivity(intent);
    }

    public void onShareViaEmail(View v) {
        MiscUtility.sendEmail(this, "mailto:", getResources().getString(R.string.IDS_MULTI_MEASURES), getResources().getString(R.string.IDS_CHECK_OUT_THIS_APP) + "\nhttp://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName(), null);
    }

    protected boolean isValidFragment(String fragmentName) {
        return true;
    }

    /* renamed from: com.vivekwarde.measure.settings.SettingsActivity.2 */
    static class AnonymousClass2 implements OnShowListener {
        final /* synthetic */ Preference val$calibrationPref;
        final /* synthetic */ Context val$context;
        final /* synthetic */ AlertDialog val$d;
        final /* synthetic */ EditText val$input;
        final /* synthetic */ int val$type;

        AnonymousClass2(AlertDialog alertDialog, EditText editText, int i, Context context, Preference preference) {
            this.val$d = alertDialog;
            this.val$input = editText;
            this.val$type = i;
            this.val$context = context;
            this.val$calibrationPref = preference;
        }

        public void onShow(DialogInterface dialog) {
            Button doneBtn = this.val$d.getButton(-1);
            doneBtn.setOnClickListener(new AnonymousClass1(doneBtn));
        }

        /* renamed from: com.vivekwarde.measure.settings.SettingsActivity.2.1 */
        class AnonymousClass1 implements View.OnClickListener {
            final /* synthetic */ Button val$doneBtn;

            AnonymousClass1(Button button) {
                this.val$doneBtn = button;
            }

            public void onClick(View view) {
                if (view.equals(this.val$doneBtn)) {
                    String sValue = AnonymousClass2.this.val$input.getText().toString();
                    try {
                        float value;
                        if (AnonymousClass2.this.val$type <= 1) {
                            value = Float.parseFloat(sValue);
                        } else {
                            value = RulerActivity.getScreenDPI(AnonymousClass2.this.val$context);
                        }
                        if (value <= 0.0f) {
                            throw new NumberFormatException();
                        }
                        double newDPI;
                        if (AnonymousClass2.this.val$type == 0) {
                            newDPI = RulerActivity.getDPI(AnonymousClass2.this.val$context.getResources().getDisplayMetrics().widthPixels, AnonymousClass2.this.val$context.getResources().getDisplayMetrics().heightPixels, (double) value);
                        } else {
                            newDPI = (double) value;
                        }
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(AnonymousClass2.this.val$context).edit();
                        editor.putFloat(SettingsKey.SETTINGS_RULER_DPI_KEY, (float) newDPI);
                        editor.apply();
                        AnonymousClass2.this.val$calibrationPref.setSummary(String.format(Locale.US, "%s: %.2f", new Object[]{AnonymousClass2.this.val$context.getResources().getString(R.string.IDS_CALIBRATION_CURRENT_PPI), Double.valueOf(newDPI)}));
                        AnonymousClass2.this.val$d.dismiss();
                    } catch (NumberFormatException e) {
                        Toast.makeText(AnonymousClass2.this.val$context, AnonymousClass2.this.val$context.getResources().getString(R.string.IDS_CALIBRATION_INVALID_VALUE), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public static class SettingsKey {
        public static final String SETTINGS_ALTIMETER_GET_METHOD_KEY = "SETTINGS_ALTIMETER_GET_METHOD_KEY";
        public static final String SETTINGS_ALTIMETER_UNIT_KEY = "SETTINGS_ALTIMETER_UNIT_KEY";
        public static final String SETTINGS_COMPASS_HEADING_KEY = "SETTINGS_COMPASS_HEADING_KEY";
        public static final String SETTINGS_DECIBEL_CALIBRATION_VALUE_KEY = "SETTINGS_DECIBEL_CALIBRATION_VALUE_KEY";
        public static final String SETTINGS_DECIBEL_UPDATE_FREQUENCY_KEY = "SETTINGS_DECIBEL_UPDATE_FREQUENCY_KEY";
        public static final String SETTINGS_GENERAL_LAST_USED_TOOL_KEY = "SETTINGS_GENERAL_LAST_USED_TOOL_KEY";
        public static final String SETTINGS_GENERAL_MENU_ORIENTATION_KEY = "SETTINGS_GENERAL_MENU_ORIENTATION_KEY";
        public static final String SETTINGS_GENERAL_SHOW_TOOLTIP_KEY = "SETTINGS_GENERAL_SHOW_TOOLTIP_KEY";
        public static final String SETTINGS_GENERAL_SOUNDFX_KEY = "SETTINGS_GENERAL_SOUNDFX_KEY";
        public static final String SETTINGS_IS_ALTIMETER_UNLOCKED = "SETTINGS_IS_ALTIMETER_UNLOCKED";
        public static final String SETTINGS_IS_BAROMETER_UNLOCKED = "SETTINGS_IS_BAROMETER_UNLOCKED";
        public static final String SETTINGS_IS_PREMIUM = "SETTINGS_IS_PREMIUM";
        public static final String SETTINGS_METRONOME_BPM_KEY = "SETTINGS_METRONOME_BPM_KEY";
        public static final String SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY = "SETTINGS_METRONOME_FIRST_BEAT_SOUND_KEY";
        public static final String SETTINGS_METRONOME_FLASH_ENABLED_KEY = "SETTINGS_METRONOME_FLASH_ENABLED_KEY";
        public static final String SETTINGS_METRONOME_FLASH_TYPE_KEY = "SETTINGS_METRONOME_FLASH_TYPE_KEY";
        public static final String SETTINGS_METRONOME_LAST_WHEEL_ANGLE_KEY = "SETTINGS_METRONOME_LAST_WHEEL_ANGLE_KEY";
        public static final String SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY = "SETTINGS_METRONOME_MAIN_BEAT_SOUND_KEY";
        public static final String SETTINGS_METRONOME_METER_KEY = "SETTINGS_METRONOME_METER_KEY";
        public static final String SETTINGS_METRONOME_SUB_BEAT_KEY = "SETTINGS_METRONOME_SUB_BEAT_KEY";
        public static final String SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY = "SETTINGS_METRONOME_SUB_BEAT_SOUND_KEY";
        public static final String SETTINGS_METRONOME_SUB_DIVISION_KEY = "SETTINGS_METRONOME_SUB_DIVISION_KEY";
        public static final String SETTINGS_METRONOME_VOLUME_KEY = "SETTINGS_METRONOME_VOLUME_KEY";
        public static final String SETTINGS_PLUMB_BOB_CALIB_ANGLE_OXY = "SETTINGS_PLUMB_BOB_CALIB_ANGLE_OXY";
        public static final String SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZX = "SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZX";
        public static final String SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZY = "SETTINGS_PLUMB_BOB_CALIB_ANGLE_OZY";
        public static final String SETTINGS_PLUMB_BOB_CALIB_MATRIX_KEY = "SETTINGS_PLUMB_BOB_CALIB_MATRIX_%d%d_KEY";
        public static final String SETTINGS_PLUMB_BOB_LOCK_KEY = "SETTINGS_PLUMB_BOB_LOCK_KEY";
        public static final String SETTINGS_PLUMB_BOB_RESET_CALIBRATION = "SETTINGS_PLUMB_BOB_RESET_CALIBRATION";
        public static final String SETTINGS_PLUMB_BOB_SENSITIVITY_KEY = "SETTINGS_PLUMB_BOB_SENSITIVITY_KEY";
        public static final String SETTINGS_PLUMB_BOB_UNIT_KEY = "SETTINGS_PLUMB_BOB_UNIT_KEY";
        public static final String SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY = "SETTINGS_PROTRACTOR_DEGREE_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY = "SETTINGS_PROTRACTOR_GRADIAN_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_LAST_ANGLE_KEY = "SETTINGS_PROTRACTOR_LAST_ANGLE_KEY";
        public static final String SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY = "SETTINGS_PROTRACTOR_PRIMARY_UNIT_KEY";
        public static final String SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY = "SETTINGS_PROTRACTOR_RADIAN_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY = "SETTINGS_PROTRACTOR_REVOLUTION_SNAP_KEY";
        public static final String SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY = "SETTINGS_PROTRACTOR_SECONDARY_UNIT_KEY";
        public static final String SETTINGS_PROTRACTOR_SHOW_HAND_LINE_KEY = "SETTINGS_PROTRACTOR_SHOW_HAND_LINE_KEY";
        public static final String SETTINGS_PROTRACTOR_SNAPPING_KEY = "SETTINGS_PROTRACTOR_SNAPPING_KEY";
        public static final String SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY = "SETTINGS_PROTRACTOR_SNAPPING_SENSITIVITY_KEY";
        public static final String SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY = "SETTINGS_PROTRACTOR_UNIT_TO_SNAP_KEY";
        public static final String SETTINGS_RULER_CENTIMETER_SNAP_KEY = "SETTINGS_RULER_CENTIMETER_SNAP_KEY";
        public static final String SETTINGS_RULER_CURRENT_PAGE_KEY = "SETTINGS_RULER_CURRENT_PAGE_KEY";
        public static final String SETTINGS_RULER_CURRENT_X_KEY = "SETTINGS_RULER_CURRENT_X_KEY";
        public static final String SETTINGS_RULER_DPI_KEY = "SETTINGS_RULER_DPI_KEY";
        public static final String SETTINGS_RULER_HFLIPPED_KEY = "SETTINGS_RULER_HFLIPPED_KEY";
        public static final String SETTINGS_RULER_INCH_SNAP_KEY = "SETTINGS_RULER_INCH_SNAP_KEY";
        public static final String SETTINGS_RULER_PICA_SNAP_KEY = "SETTINGS_RULER_PICA_SNAP_KEY";
        public static final String SETTINGS_RULER_PIXEL_SNAP_KEY = "SETTINGS_RULER_PIXEL_SNAP_KEY";
        public static final String SETTINGS_RULER_PRIMARY_UNIT_KEY = "SETTINGS_RULER_PRIMARY_UNIT_KEY";
        public static final String SETTINGS_RULER_SECONDARY_UNIT_KEY = "SETTINGS_RULER_SECONDARY_UNIT_KEY";
        public static final String SETTINGS_RULER_SHOW_HAND_LINE_KEY = "SETTINGS_RULER_SHOW_HAND_LINE_KEY";
        public static final String SETTINGS_RULER_SNAPPING_KEY = "SETTINGS_RULER_SNAPPING_KEY";
        public static final String SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY = "SETTINGS_RULER_SNAPPING_SENSITIVITY_KEY";
        public static final String SETTINGS_RULER_UNIT_TO_SNAP_KEY = "SETTINGS_RULER_UNIT_TO_SNAP_KEY";
        public static final String SETTINGS_SEISMOMETER_ALARM_ENABLED_KEY = "SETTINGS_SEISMOMETER_ALARM_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY = "SETTINGS_SEISMOMETER_ALARM_LEVEL_KEY";
        public static final String SETTINGS_SEISMOMETER_ALARM_SOUND_KEY = "SETTINGS_SEISMOMETER_ALARM_SOUND_KEY";
        public static final String SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY = "SETTINGS_SEISMOMETER_AXIS_X_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY = "SETTINGS_SEISMOMETER_AXIS_Y_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY = "SETTINGS_SEISMOMETER_AXIS_Z_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_SCALE_TYPE_KEY = "SETTINGS_SEISMOMETER_SCALE_TYPE_KEY";
        public static final String SETTINGS_SEISMOMETER_TIMELINE_ENABLED_KEY = "SETTINGS_SEISMOMETER_TIMELINE_ENABLED_KEY";
        public static final String SETTINGS_SEISMOMETER_UPDATE_FREQUENCY_KEY = "SETTINGS_SEISMOMETER_UPDATE_FREQUENCY_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_CALIB_KEY_TEMPLATE = "SETTINGS_SPIRIT_LEVEL_CALIB_%d_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_LOCK_KEY = "SETTINGS_SPIRIT_LEVEL_LOCK_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_RESET_CALIBRATION_KEY = "SETTINGS_SPIRIT_LEVEL_RESET_CALIBRATION_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_SENSITIVITY_KEY = "SETTINGS_SPIRIT_LEVEL_SENSITIVITY_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_THEME_KEY = "SETTINGS_SPIRIT_LEVEL_THEME_KEY";
        public static final String SETTINGS_SPIRIT_LEVEL_UNIT_KEY = "SETTINGS_SPIRIT_LEVEL_UNIT_KEY";
        public static final String SETTINGS_STOPWATCH_RETAIN_ENABLED_KEY = "SETTINGS_STOPWATCH_RETAIN_ENABLED_KEY";
        public static final String SETTINGS_STOPWATCH_SPLIT_MODE_KEY = "SETTINGS_STOPWATCH_SPLIT_MODE_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_LEFT_X = "SETTINGS_SURFACE_LEVEL_CALIB_LEFT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_LEFT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_LEFT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_X = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Z = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Z";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Z = "SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Z";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_X = "SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_X";
        public static final String SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_Y = "SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_Y";
        public static final String SETTINGS_SURFACE_LEVEL_LOCK_KEY = "SETTINGS_SURFACE_LEVEL_LOCK_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_RESET_CALIBRATION_KEY = "SETTINGS_SURFACE_LEVEL_RESET_CALIBRATION_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_SENSITIVITY_KEY = "SETTINGS_SURFACE_LEVEL_SENSITIVITY_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_THEME_KEY = "SETTINGS_SURFACE_LEVEL_THEME_KEY";
        public static final String SETTINGS_SURFACE_LEVEL_UNIT_KEY = "SETTINGS_SURFACE_LEVEL_UNIT_KEY";
        public static final String SETTINGS_TESLAMETER_ALERT_ENABLED_KEY = "SETTINGS_TESLAMETER_ALERT_ENABLED_KEY";
        public static final String SETTINGS_TESLAMETER_ALERT_SOUND_KEY = "SETTINGS_TESLAMETER_ALERT_SOUND_KEY";
        public static final String SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY = "SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY";
        public static final String SETTINGS_TESLAMETER_RUNNING_KEY = "SETTINGS_TESLAMETER_RUNNING_KEY";
        public static final String SETTINGS_TESLAMETER_SENSITIVITY_KEY = "SETTINGS_TESLAMETER_SENSITIVITY_KEY";
        public static final String SETTINGS_TESLAMETER_UNIT_KEY = "SETTINGS_TESLAMETER_UNIT_KEY";
        public static final String SETTINGS_TIMER_ALARM_SOUND_KEY_TEMPLATE = "SETTINGS_TIMER_ALARM_SOUND_%d_KEY";
        public static final String SETTINGS_TIMER_ALARM_SOUND_LOOP_KEY_TEMPLATE = "SETTINGS_TIMER_ALARM_SOUND_LOOP_%d_KEY";
        public static final String SETTINGS_TIMER_CONFIGURE_KEY_TEMPLATE = "SETTINGS_TIMER_CONFIGURE_%d_KEY";
        public static final String SETTINGS_TIMER_COUNT_DOWN_MODE_KEY = "SETTINGS_TIMER_COUNT_DOWN_MODE_KEY";
        public static final String SETTINGS_TIMER_DEFAULT_DURATION_KEY_TEMPLATE = "SETTINGS_TIMER_DEFAULT_DURATION_%d_KEY";
        public static final String SETTINGS_TIMER_MESSAGE_KEY_TEMPLATE = "SETTINGS_TIMER_MESSAGE_%d_KEY";
        public static final String SETTINGS_TIMER_NAME_KEY_TEMPLATE = "SETTINGS_TIMER_NAME_%d_KEY";
        public static final String SETTINGS_TIMER_REPEAT_ENABLED_KEY_TEMPLATE = "SETTINGS_TIMER_REPEAT_ENABLED_%d_KEY";
        public static final String SETTINGS_TIMER_RETAIN_ENABLED_KEY = "SETTINGS_TIMER_RETAIN_ENABLED_KEY";
    }

    /* renamed from: com.vivekwarde.measure.settings.SettingsActivity.1 */
    class AnonymousClass1 implements OnPreferenceClickListener {
        final /* synthetic */ Preference val$calibrationPref;

        AnonymousClass1(Preference preference) {
            this.val$calibrationPref = preference;
        }

        public boolean onPreferenceClick(Preference preference) {
            CharSequence[] str = new CharSequence[]{SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_1), SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_2), SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION_TYPE_3)};
            Builder b = new Builder(SettingsActivity.this);
            b.setTitle(SettingsActivity.this.getResources().getString(R.string.IDS_CALIBRATION));
            b.setItems(str, new OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    SettingsActivity.onCalibrate(SettingsActivity.this, position, AnonymousClass1.this.val$calibrationPref);
                }
            });
            b.create().show();
            return false;
        }
    }
}
