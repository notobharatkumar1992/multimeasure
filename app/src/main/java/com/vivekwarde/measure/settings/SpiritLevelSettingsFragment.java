package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;
import java.util.Locale;

@SuppressLint({"NewApi"})
public class SpiritLevelSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_spirit_level);
        Preference pref = findPreference(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(SpiritLevelSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        pref = findPreference(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(SpiritLevelSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        findPreference(SettingsKey.SETTINGS_SPIRIT_LEVEL_RESET_CALIBRATION_KEY).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                Builder builder = new Builder(SpiritLevelSettingsFragment.this.getActivity());
                builder.setMessage(SpiritLevelSettingsFragment.this.getString(R.string.IDS_RESET_CALIBRATION_ASKING)).setCancelable(false).setPositiveButton("OK", new OnClickListener() {
                    @SuppressLint({"DefaultLocale"})
                    public void onClick(DialogInterface dialog, int id) {
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(SpiritLevelSettingsFragment.this.getActivity()).edit();
                        for (int calibRotationType = 0; calibRotationType < 8; calibRotationType++) {
                            editor.putFloat(String.format(Locale.getDefault(), SettingsKey.SETTINGS_SPIRIT_LEVEL_CALIB_KEY_TEMPLATE, new Object[]{Integer.valueOf(calibRotationType)}), 0.0f);
                        }
                        editor.apply();
                    }
                }).setNegativeButton(SpiritLevelSettingsFragment.this.getString(R.string.IDS_CANCEL), new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                MiscUtility.setDialogFontSize(SpiritLevelSettingsFragment.this.getActivity(), alert);
                return true;
            }
        });
    }
}
