package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.vivekwarde.measure.R;

@SuppressLint({"NewApi"})
public class StopwatchSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_stopwatch);
    }
}
