package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.MiscUtility;

@SuppressLint({"NewApi"})
public class SurfaceLevelSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_surface_level);
        Preference pref = findPreference(SettingsKey.SETTINGS_SURFACE_LEVEL_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(SurfaceLevelSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        pref = findPreference(SettingsKey.SETTINGS_SURFACE_LEVEL_THEME_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(SurfaceLevelSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        findPreference(SettingsKey.SETTINGS_SURFACE_LEVEL_RESET_CALIBRATION_KEY).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                Builder builder = new Builder(SurfaceLevelSettingsFragment.this.getActivity());
                builder.setMessage(SurfaceLevelSettingsFragment.this.getString(R.string.IDS_RESET_CALIBRATION_ASKING)).setCancelable(false).setPositiveButton("OK", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(SurfaceLevelSettingsFragment.this.getActivity()).edit();
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_X, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Y, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Z, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_LEFT_X, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_LEFT_Y, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Y, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Z, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_X, 0.0f);
                        editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_Y, 0.0f);
                        editor.apply();
                    }
                }).setNegativeButton(SurfaceLevelSettingsFragment.this.getString(R.string.IDS_CANCEL), new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                MiscUtility.setDialogFontSize(SurfaceLevelSettingsFragment.this.getActivity(), alert);
                return true;
            }
        });
    }
}
