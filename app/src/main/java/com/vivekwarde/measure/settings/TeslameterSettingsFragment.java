package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.SoundUtility;
import java.io.IOException;

@SuppressLint({"NewApi"})
public class TeslameterSettingsFragment extends PreferenceFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_teslameter);
        Preference pref = findPreference(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY);
        pref.setSummary(((ListPreference) pref).getEntry());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                preference.setDefaultValue(newValue);
                Editor editor = PreferenceManager.getDefaultSharedPreferences(TeslameterSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY).setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                TeslameterSettingsFragment.this.updateAlertControls();
                return false;
            }
        });
        pref = findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY);
        pref.setSummary(((EditTextPreference) pref).getText());
        pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());
                Editor editor = PreferenceManager.getDefaultSharedPreferences(TeslameterSettingsFragment.this.getActivity()).edit();
                editor.putString(preference.getKey(), newValue.toString());
                editor.apply();
                return false;
            }
        });
        try {
            String[] teslameterSoundFileNameList = getActivity().getAssets().list("sounds/teslameter");
            String[] teslameterSoundNameList = new String[teslameterSoundFileNameList.length];
            for (int i = 0; i < teslameterSoundNameList.length; i++) {
                teslameterSoundNameList[i] = teslameterSoundFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
            }
            String alertSoundSettingsKey = SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY;
            Preference alertSoundPref = findPreference(alertSoundSettingsKey);
            ((ListPreference) alertSoundPref).setEntries(teslameterSoundNameList);
            ((ListPreference) alertSoundPref).setEntryValues(teslameterSoundNameList);
            alertSoundPref.setSummary(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(alertSoundSettingsKey, teslameterSoundNameList[3]));
            alertSoundPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                    preference.setDefaultValue(newValue);
                    Editor editor = PreferenceManager.getDefaultSharedPreferences(TeslameterSettingsFragment.this.getActivity()).edit();
                    editor.putString(preference.getKey(), newValue.toString());
                    editor.apply();
                    SoundUtility.getInstance().previewSound("sounds/teslameter/" + newValue.toString() + ".mp3");
                    return false;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateAlertControls() {
        Boolean enableAlert = Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY, true));
        findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY).setEnabled(enableAlert.booleanValue());
        findPreference(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY).setEnabled(enableAlert.booleanValue());
    }
}
