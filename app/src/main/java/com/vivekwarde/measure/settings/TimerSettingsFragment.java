package com.vivekwarde.measure.settings;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.io.IOException;
import java.util.Locale;

@SuppressLint({"NewApi"})
public class TimerSettingsFragment extends PreferenceFragment {
    @SuppressLint({"DefaultLocale"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_timer);
        for (int timerIndex = 1; timerIndex <= 5; timerIndex++) {
            Preference configureScreenPreference = findPreference(String.format(SettingsKey.SETTINGS_TIMER_CONFIGURE_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)}));
            configureScreenPreference.setTitle(configureScreenPreference.getTitle().toString() + String.format(Locale.US, " %1$d", new Object[]{Integer.valueOf(timerIndex)}));
            EditTextPreference timerNamePreference = (EditTextPreference) findPreference(String.format(SettingsKey.SETTINGS_TIMER_NAME_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)}));
            timerNamePreference.setDefaultValue(String.format(getString(R.string.IDS_TIMER) + " %1$d", new Object[]{Integer.valueOf(timerIndex)}));
            timerNamePreference.setSummary(timerNamePreference.getText());
            timerNamePreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    preference.setSummary(newValue.toString());
                    Editor editor = PreferenceManager.getDefaultSharedPreferences(TimerSettingsFragment.this.getActivity()).edit();
                    editor.putString(preference.getKey(), newValue.toString());
                    editor.apply();
                    return false;
                }
            });
            String[] durationNamesFormat = getResources().getStringArray(R.array.timer_duration_names);
            String[] durationNames = durationNamesFormat;
            int i = 0;
            while (true) {
                int length = durationNamesFormat.length;
                if (i >= length) {
                    break;
                }
                if (i < 6) {
                    durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_SECONDS)});
                } else if (i < 7) {
                    durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_MINUTE)});
                } else if (i < 12) {
                    durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_MINUTES)});
                } else if (i < 13) {
                    durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_HOUR)});
                } else {
                    durationNames[i] = String.format(durationNamesFormat[i], new Object[]{getString(R.string.IDS_HOURS)});
                }
                i++;
            }
            ListPreference timerDefaultDurationPreference = (ListPreference) findPreference(String.format(SettingsKey.SETTINGS_TIMER_DEFAULT_DURATION_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)}));
            timerDefaultDurationPreference.setEntries(durationNames);
            timerDefaultDurationPreference.setSummary(timerDefaultDurationPreference.getEntry());
            timerDefaultDurationPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                    preference.setDefaultValue(newValue);
                    Editor editor = PreferenceManager.getDefaultSharedPreferences(TimerSettingsFragment.this.getActivity()).edit();
                    editor.putString(preference.getKey(), newValue.toString());
                    editor.apply();
                    return false;
                }
            });
            try {
                String[] timerSoundFileNameList = getActivity().getAssets().list("sounds/timer");
                String[] timerSoundNameList = new String[timerSoundFileNameList.length];
                i = 0;
                while (true) {
                    int length = timerSoundNameList.length;
                    if (i >= length) {
                        break;
                    }
                    timerSoundNameList[i] = timerSoundFileNameList[i].replace(".mp3", BuildConfig.FLAVOR);
                    i++;
                }
                String timerNotificationSoundSettingsKey = String.format(SettingsKey.SETTINGS_TIMER_ALARM_SOUND_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)});
                Preference notificationSoundPreference = findPreference(timerNotificationSoundSettingsKey);
                ((ListPreference) notificationSoundPreference).setEntries(timerSoundNameList);
                ((ListPreference) notificationSoundPreference).setEntryValues(timerSoundNameList);
                notificationSoundPreference.setSummary(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(timerNotificationSoundSettingsKey, timerSoundNameList[timerIndex - 1]));
                notificationSoundPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        preference.setSummary(((ListPreference) preference).getEntries()[((ListPreference) preference).findIndexOfValue((String) newValue)].toString());
                        preference.setDefaultValue(newValue);
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(TimerSettingsFragment.this.getActivity()).edit();
                        editor.putString(preference.getKey(), newValue.toString());
                        editor.apply();
                        SoundUtility.getInstance().previewSound("sounds/timer/" + newValue.toString() + ".mp3");
                        return false;
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            String format = String.format(SettingsKey.SETTINGS_TIMER_MESSAGE_KEY_TEMPLATE, new Object[]{Integer.valueOf(timerIndex)});
            try {
                ((EditTextPreference) findPreference("timerMessageSettingsKey")).setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        preference.setSummary(newValue.toString());
                        Editor editor = PreferenceManager.getDefaultSharedPreferences(TimerSettingsFragment.this.getActivity()).edit();
                        editor.putString(preference.getKey(), newValue.toString());
                        editor.apply();
                        return false;
                    }
                });
            } catch (Exception e) {
                Log.d("test", "No find value = " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
