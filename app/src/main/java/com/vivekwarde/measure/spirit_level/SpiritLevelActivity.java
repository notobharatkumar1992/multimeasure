package com.vivekwarde.measure.spirit_level;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.internal.view.SupportMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.AccelerometerUtility;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.Locale;

public class SpiritLevelActivity extends BaseActivity implements OnClickListener, SensorEventListener {
    private double mAngleOxy;
    private double mAngleOzx;
    private double mAngleOzy;
    private ImageView mBaseImage;
    private ImageView mBaseRimImage;
    private ImageView mBkgThemeImage;
    private int mBubbleCurPos;
    private ImageView mBubbleImage;
    private int mBubbleSoundPeriod;
    private Handler mBubbleSoundTimerHandler;
    private Runnable mBubbleSoundTimerTask;
    private float mBubbleSoundVolume;
    private double[] mCalibration;
    private SPImageButton mCalibrationButton;
    private ImageView mDitchImage;
    private double mFilteringFactor;
    private double mGravityX;
    private double mGravityY;
    private double mGravityZ;
    private ImageView mLedScreenImage;
    private SPImageButton mLockButton;
    private ImageView mLowerDotsImage;
    private SPImageButton mMenuButton;
    private double mOutputAngle;
    private TextView mOutputText;
    private TextView mOutputUnitText;
    private ImageView mPipeImage;
    private ImageView mRingImage;
    private SensorManager mSensorManager;
    private SPImageButton mSettingsButton;
    private ImageView mTubeImage;
    private ImageView mUpperDotsImage;

    public SpiritLevelActivity() {
        this.mMenuButton = null;
        this.mSettingsButton = null;
        this.mLockButton = null;
        this.mCalibrationButton = null;
        this.mBaseImage = null;
        this.mBaseRimImage = null;
        this.mUpperDotsImage = null;
        this.mLowerDotsImage = null;
        this.mDitchImage = null;
        this.mPipeImage = null;
        this.mBkgThemeImage = null;
        this.mTubeImage = null;
        this.mBubbleImage = null;
        this.mRingImage = null;
        this.mLedScreenImage = null;
        this.mBubbleCurPos = 0;
        this.mBubbleSoundPeriod = GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE;
        this.mBubbleSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE;
        this.mAngleOxy = 0.0d;
        this.mAngleOzy = 0.0d;
        this.mAngleOzx = 0.0d;
        this.mGravityX = 0.0d;
        this.mGravityY = 0.0d;
        this.mGravityZ = 0.0d;
        this.mOutputAngle = 0.0d;
        this.mFilteringFactor = 0.30000001192092896d;
        this.mCalibration = new double[8];
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(0);
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mBubbleSoundTimerHandler = new Handler();
        this.mBubbleSoundTimerTask = new Runnable() {
            public void run() {
                boolean isLocked = PreferenceManager.getDefaultSharedPreferences(SpiritLevelActivity.this).getBoolean(SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false);
                boolean soundOn = PreferenceManager.getDefaultSharedPreferences(SpiritLevelActivity.this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true);
                if (!isLocked && soundOn) {
                    SoundUtility.getInstance().playSound(1, SpiritLevelActivity.this.mBubbleSoundVolume);
                }
                SpiritLevelActivity.this.mBubbleSoundTimerHandler.postDelayed(this, (long) SpiritLevelActivity.this.mBubbleSoundPeriod);
            }
        };
        createUi();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    @SuppressLint({"NewApi"})
    private void createUi() {
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                SpiritLevelActivity.this.arrangeLayout();
                if (VERSION.SDK_INT >= 16) {
                    SpiritLevelActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    SpiritLevelActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mMainLayout.addView(this.mUiLayout);
        this.mBaseImage = new ImageView(this);
        this.mBaseImage.setId(5);
        tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.bubble_level_base)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        this.mBaseImage.setBackgroundColor(SupportMenu.CATEGORY_MASK);
        if (VERSION.SDK_INT < 16) {
            this.mBaseImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mBaseImage.setBackground(tilebitmap);
        }
        this.mBaseImage.setScaleType(ScaleType.CENTER_CROP);
        this.mUiLayout.addView(this.mBaseImage);
        this.mBaseRimImage = new ImageView(this);
        this.mBaseRimImage.setId(6);
        this.mBaseRimImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.bubble_level_base_rim)).getBitmap());
        this.mBaseRimImage.setScaleType(ScaleType.CENTER_CROP);
        this.mUiLayout.addView(this.mBaseRimImage);
        this.mUpperDotsImage = new ImageView(this);
        this.mUpperDotsImage.setId(7);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_hole)).getBitmap();
        tilebitmap = new BitmapDrawable(getResources(), bitmap);
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mUpperDotsImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mUpperDotsImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mUpperDotsImage);
        this.mLowerDotsImage = new ImageView(this);
        this.mLowerDotsImage.setId(8);
        tilebitmap = new BitmapDrawable(getResources(), bitmap);
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mLowerDotsImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mLowerDotsImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mLowerDotsImage);
        this.mDitchImage = new ImageView(this);
        this.mDitchImage.setId(9);
        tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_ditch_2)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mDitchImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mDitchImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mDitchImage);
        this.mPipeImage = new ImageView(this);
        this.mPipeImage.setId(10);
        tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_pipe)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mPipeImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mPipeImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mPipeImage);
        String theme = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY, "0");
        this.mBkgThemeImage = new ImageView(this);
        this.mBkgThemeImage.setId(11);
        this.mBkgThemeImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals("0") ? R.drawable.bubble_level_background_galaxy : R.drawable.bubble_level_background_hexagon)).getBitmap());
        this.mUiLayout.addView(this.mBkgThemeImage);
        this.mTubeImage = new ImageView(this);
        this.mTubeImage.setId(12);
        this.mTubeImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.spirit_level_tube)).getBitmap());
        this.mUiLayout.addView(this.mTubeImage);
        this.mBubbleImage = new ImageView(this);
        this.mBubbleImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals(getString(R.string.IDS_GALAXY)) ? R.drawable.bubble_level_sun : R.drawable.bubble)).getBitmap());
        this.mUiLayout.addView(this.mBubbleImage);
        this.mRingImage = new ImageView(this);
        this.mRingImage.setId(14);
        this.mRingImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.spirit_level_ring)).getBitmap());
        this.mUiLayout.addView(this.mRingImage);
        this.mLedScreenImage = new ImageView(this);
        this.mLedScreenImage.setId(15);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        this.mLedScreenImage.setImageBitmap(ImageUtility.scale9Bitmap(bitmap, bitmap.getWidth() - 2, bitmap.getHeight() - 2));
        this.mUiLayout.addView(this.mLedScreenImage);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mOutputText = new TextView(this);
        this.mOutputText.setId(16);
        this.mOutputText.setTypeface(face);
        this.mOutputText.setIncludeFontPadding(false);
        this.mOutputText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE));
        this.mOutputText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputText.setPaintFlags(this.mOutputText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputText);
        this.mOutputText.setText("100");
        this.mOutputUnitText = new TextView(this);
        this.mOutputUnitText.setId(17);
        this.mOutputUnitText.setTypeface(face);
        this.mOutputUnitText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE));
        this.mOutputUnitText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputUnitText.setPaintFlags(this.mOutputText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputUnitText);
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.mOutputUnitText.setText("~");
        } else if (unit.equals("1")) {
            this.mOutputUnitText.setText("rad");
        } else if (unit.equals("2")) {
            this.mOutputUnitText.setText("gra");
        } else {
            this.mOutputUnitText.setText("%");
        }
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap());
        this.mMenuButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mMenuButton);
        this.mSettingsButton = new SPImageButton(this);
        this.mSettingsButton.setId(2);
        this.mSettingsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mSettingsButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mSettingsButton);
        this.mCalibrationButton = new SPImageButton(this);
        this.mCalibrationButton.setId(4);
        this.mCalibrationButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_calibration)).getBitmap());
        this.mCalibrationButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mCalibrationButton);
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false);
        this.mLockButton = new SPImageButton(this);
        this.mLockButton.setId(3);
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(isLocked ? R.drawable.button_lock : R.drawable.button_unlock)).getBitmap());
        this.mLockButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mLockButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
    }

    private void arrangeLayout() {
        LayoutParams params = new LayoutParams(mScreenWidth, -2);
        params.addRule(15);
        this.mBaseImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mBaseRimImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mBkgThemeImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mRingImage.setLayoutParams(params);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_hole)).getBitmap();
        params = new LayoutParams(mScreenWidth, bitmap.getHeight() * 4);
        params.addRule(6, 5);
        params.topMargin = (int) getResources().getDimension(R.dimen.SPIRIT_LEVEL_VERT_MARGIN_BTW_TOP_DOTS_AND_MID_BASE);
        this.mUpperDotsImage.setLayoutParams(params);
        params = new LayoutParams(mScreenWidth, bitmap.getHeight() * 4);
        params.addRule(8, 5);
        params.bottomMargin = (int) getResources().getDimension(R.dimen.SPIRIT_LEVEL_VERT_MARGIN_BTW_TOP_DOTS_AND_MID_BASE);
        this.mLowerDotsImage.setLayoutParams(params);
        params = new LayoutParams(mScreenWidth, -2);
        params.addRule(13);
        this.mDitchImage.setLayoutParams(params);
        params = new LayoutParams(mScreenWidth, -2);
        params.addRule(13);
        this.mPipeImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mTubeImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mBubbleImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mBaseImage.getId());
        params.addRule(6, this.mBaseImage.getId());
        params.leftMargin = (int) getResources().getDimension(R.dimen.SPIRIT_LEVEL_HORZ_MARGIN_BTW_LEFT_LEDSCREEN_AND_LEFT_BASE);
        params.topMargin = (int) getResources().getDimension(R.dimen.SPIRIT_LEVEL_VERT_MARGIN_BTW_TOP_LEDSCREEN_AND_TOP_BASE);
        this.mLedScreenImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(0, this.mOutputUnitText.getId());
        params.addRule(6, this.mLedScreenImage.getId());
        params.topMargin = (this.mLedScreenImage.getHeight() - this.mOutputText.getHeight()) / 2;
        params.rightMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_VALUE_LABEL_RIGHT_MARGIN);
        this.mOutputText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(7, this.mLedScreenImage.getId());
        params.addRule(4, this.mOutputText.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.SPIRIT_LEVEL_HORZ_MARGIN_BTW_RIGHT_UNIT_AND_RIGHT_LEDSCREEN);
        this.mOutputUnitText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(11);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), 0);
        this.mMenuButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mMenuButton.getId());
        params.addRule(0, this.mMenuButton.getId());
        this.mSettingsButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(11);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), 0, (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN));
        this.mCalibrationButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mCalibrationButton.getId());
        params.addRule(0, this.mCalibrationButton.getId());
        this.mLockButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(9);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), 0);
        this.mNoAdsButton.setLayoutParams(params);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false)) {
            this.mSensorManager.unregisterListener(this);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
        }
    }

    protected void onResume() {
        super.onResume();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false)) {
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
            this.mBubbleSoundTimerHandler.postDelayed(this.mBubbleSoundTimerTask, (long) this.mBubbleSoundPeriod);
        }
        String theme = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY, "0");
        this.mBkgThemeImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals("0") ? R.drawable.bubble_level_background_galaxy : R.drawable.bubble_level_background_hexagon)).getBitmap());
        this.mBubbleImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals("0") ? R.drawable.bubble_level_sun : R.drawable.bubble)).getBitmap());
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.mOutputUnitText.setText("~");
        } else if (unit.equals("1")) {
            this.mOutputUnitText.setText("rad");
        } else if (unit.equals("2")) {
            this.mOutputUnitText.setText("gra");
        } else {
            this.mOutputUnitText.setText("%");
        }
        for (int i = 0; i < 8; i++) {
            this.mCalibration[i] = (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(String.format(Locale.getDefault(), SettingsKey.SETTINGS_SPIRIT_LEVEL_CALIB_KEY_TEMPLATE, new Object[]{Integer.valueOf(i)}), 0.0f);
        }
    }

    public void onClick(View source) {
        if (source.getId() == 1) {
            onButtonMenu();
        } else if (source.getId() == 2) {
            onButtonSettings();
        } else if (source.getId() == 3) {
            onButtonLock();
        } else if (source.getId() == 4) {
            onButtonCalibration();
        } else if (source.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    private void onButtonLock() {
        boolean z;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(2, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY, false);
        if (isLocked) {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_unlock)).getBitmap());
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBubbleSoundTimerHandler.postDelayed(this.mBubbleSoundTimerTask, (long) this.mBubbleSoundPeriod);
        } else {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_lock)).getBitmap());
            this.mSensorManager.unregisterListener(this);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        String str = SettingsKey.SETTINGS_SPIRIT_LEVEL_LOCK_KEY;
        if (isLocked) {
            z = false;
        } else {
            z = true;
        }
        editor.putBoolean(str, z);
        editor.apply();
    }

    private void onButtonCalibration() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        int calibType = AccelerometerUtility.getCalibTypeWithRotation(((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation(), this.mAngleOzy, this.mAngleOzx, this.mAngleOxy);
        this.mCalibration[calibType] = -this.mOutputAngle;
        String calibKey = String.format(Locale.getDefault(), SettingsKey.SETTINGS_SPIRIT_LEVEL_CALIB_KEY_TEMPLATE, new Object[]{Integer.valueOf(calibType)});
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putFloat(calibKey, (float) this.mCalibration[calibType]);
        editor.apply();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            double y = (double) event.values[1];
            double z = (double) (-event.values[2]);
            this.mGravityX = (this.mFilteringFactor * ((double) event.values[0])) + (this.mGravityX * (1.0d - this.mFilteringFactor));
            this.mGravityY = (this.mFilteringFactor * y) + (this.mGravityY * (1.0d - this.mFilteringFactor));
            this.mGravityZ = (this.mFilteringFactor * z) + (this.mGravityZ * (1.0d - this.mFilteringFactor));
            this.mAngleOxy = Math.atan2(-this.mGravityY, -this.mGravityX);
            this.mAngleOzy = Math.atan2(this.mGravityZ, this.mGravityY);
            this.mAngleOzx = Math.atan2(this.mGravityZ, this.mGravityX);
            this.mAngleOxy = AccelerometerUtility.validateAngle(this.mAngleOxy);
            this.mAngleOzy = AccelerometerUtility.validateAngle(this.mAngleOzy);
            this.mAngleOzx = AccelerometerUtility.validateAngle(this.mAngleOzx);
            this.mAngleOxy = AccelerometerUtility.to180(this.mAngleOxy);
            this.mAngleOzy = AccelerometerUtility.to180(this.mAngleOzy);
            this.mAngleOzx = AccelerometerUtility.to180(this.mAngleOzx);
            updateBubblePosition();
        }
    }

    @SuppressLint({"DefaultLocale"})
    private void updateBubblePosition() {
        int orientation = ((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation();
        double angle = 0.0d;
        boolean fullBubble = true;
        if (orientation == 0) {
            if (Math.abs(this.mAngleOzy) < 0.7853981633974483d) {
                angle = this.mAngleOzx;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 0);
                fullBubble = false;
            }
        } else if (orientation == 2) {
            if (Math.abs(this.mAngleOzy) < 0.7853981633974483d) {
                angle = -this.mAngleOzx;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 2);
                fullBubble = false;
            }
        } else if (orientation == 1) {
            if (Math.abs(this.mAngleOzx) < 0.7853981633974483d) {
                angle = this.mAngleOzy;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 1);
                fullBubble = false;
            }
        } else if (orientation == 3) {
            if (Math.abs(this.mAngleOzx) < 0.7853981633974483d) {
                angle = -this.mAngleOzy;
            } else {
                angle = AccelerometerUtility.rotateAngle(this.mAngleOxy, 3);
                fullBubble = false;
            }
        }
        this.mOutputAngle = angle;
        angle += this.mCalibration[AccelerometerUtility.getCalibTypeWithRotation(orientation, this.mAngleOzy, this.mAngleOzx, this.mAngleOxy)];
        double convertedUnitAngle = angle;
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SPIRIT_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            convertedUnitAngle = (double) MiscUtility.radian2Degree((float) angle);
        } else {
            if (unit.equals("2")) {
                convertedUnitAngle = (double) MiscUtility.radian2Gradian((float) angle);
            } else {
                if (unit.equals("3")) {
                    convertedUnitAngle = (double) MiscUtility.radian2Slope((float) angle);
                }
            }
        }
        this.mOutputText.setText(String.format(Locale.getDefault(), "%.1f", new Object[]{Double.valueOf(convertedUnitAngle)}));
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SPIRIT_LEVEL_THEME_KEY, "0").equals("1")) {
            this.mBubbleImage.setImageResource(fullBubble ? R.drawable.bubble : R.drawable.bubble_half);
        }
        int tubeLengthBound = this.mTubeImage.getWidth() / 2;
        int newBubbleTranslation = (int) ((((double) tubeLengthBound) / Math.sin((double) (((float) PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_SPIRIT_LEVEL_SENSITIVITY_KEY, 80)) / 100.0f))) * Math.sin(angle));
        if (Math.abs(newBubbleTranslation) > tubeLengthBound) {
            newBubbleTranslation = MiscUtility.getSign(newBubbleTranslation) * tubeLengthBound;
        }
        Animation translateAnimation = new TranslateAnimation((float) this.mBubbleCurPos, (float) newBubbleTranslation, 0.0f, 0.0f);
        translateAnimation.setDuration(100);
        translateAnimation.setFillAfter(true);
        this.mBubbleImage.startAnimation(translateAnimation);
        this.mBubbleCurPos = newBubbleTranslation;
        float distance = Math.abs(((float) newBubbleTranslation) / ((float) tubeLengthBound));
        this.mBubbleSoundPeriod = (int) (400.0f + (1600.0f * distance));
        this.mBubbleSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE - distance > 0.5f ? 0.5f : TextTrackStyle.DEFAULT_FONT_SCALE - distance;
    }

    public class ConstantId {
        public static final int TIMER_PERIOD_MAX = 2000;
        public static final int TIMER_PERIOD_MIN = 400;
    }

    public class ControlId {
        public static final int BASE_IMAGE_ID = 5;
        public static final int BASE_RIM_IMAGE_ID = 6;
        public static final int BKG_THEME_IMAGE_ID = 11;
        public static final int BUBBLE_IMAGE_ID = 13;
        public static final int CALIBRATION_BUTTON_ID = 4;
        public static final int DITCH_IMAGE_ID = 9;
        public static final int LED_SCREEN_IMAGE_ID = 15;
        public static final int LOCK_BUTTON_ID = 3;
        public static final int LOWER_DOTS_IMAGE_ID = 8;
        public static final int MENU_BUTTON_ID = 1;
        public static final int OUTPUT_TEXT_ID = 16;
        public static final int OUTPUT_UNIT_TEXT_ID = 17;
        public static final int PIPE_IMAGE_ID = 10;
        public static final int RING_IMAGE_ID = 14;
        public static final int SETTINGS_BUTTON_ID = 2;
        public static final int TUBE_IMAGE_ID = 12;
        public static final int UPGRADE_BUTTON_ID = 0;
        public static final int UPPER_DOTS_IMAGE_ID = 7;
    }
}
