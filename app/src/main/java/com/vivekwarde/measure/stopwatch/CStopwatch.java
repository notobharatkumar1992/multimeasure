package com.vivekwarde.measure.stopwatch;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.text.format.Time;
import android.util.Log;

import com.vivekwarde.measure.utilities.SPTimer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class CStopwatch implements Runnable {
    public static final int STOPWATCH_STATE_PAUSED = 1;
    public static final int STOPWATCH_STATE_RUNNING = 0;
    public static final int STOPWATCH_STATE_STOPPED = 2;
    static final int POLLING_DURATION = 10;
    final String tag;
    Time mEndPoint;
    ArrayList<Long> mLapDataList;
    long mLastLap;
    OnStopwatchEventListener mListener;
    long mPauseTime;
    SPTimer mPollTimer;
    ArrayList<Long> mSplitDataList;
    Time mStartPoint;
    long mStartTime;
    int mState;

    public CStopwatch() {
        this.tag = getClass().getSimpleName();
        this.mListener = null;
        this.mStartPoint = new Time();
        this.mEndPoint = new Time();
        this.mStartTime = 0;
        this.mPauseTime = 0;
        this.mLastLap = 0;
        this.mState = STOPWATCH_STATE_STOPPED;
        this.mPollTimer = null;
        this.mSplitDataList = null;
        this.mLapDataList = null;
    }

    static String doubleToStringWithSep(double seconds, String sep) {
        int hours = (int) (seconds / 3600.0d);
        int secs = (int) ((seconds - ((double) (hours * 3600))) - ((double) (((int) ((seconds / 60.0d) - ((double) (hours * 60)))) * 60)));
        int sec = (int) Math.round((seconds - Math.floor(seconds)) * 100.0d);
        return String.format(Locale.US, "%02d%s%02d%s%02d.%02d", new Object[]{Integer.valueOf(hours), sep, Integer.valueOf((int) ((seconds / 60.0d) - ((double) (hours * 60)))), sep, Integer.valueOf(secs), Integer.valueOf(sec)});
    }

    void init() {
        this.mStartTime = 0;
        this.mLastLap = 0;
        this.mPauseTime = 0;
        this.mState = STOPWATCH_STATE_STOPPED;
        this.mSplitDataList = new ArrayList();
        this.mLapDataList = new ArrayList();
        this.mPollTimer = new SPTimer(new Handler(), this, POLLING_DURATION, false);
    }

    public void run() {
        pollMethod();
    }

    void pollMethod() {
        if (this.mState == 0) {
            double[] elapsedSecs = new double[STOPWATCH_STATE_PAUSED];
            elapsedSecs[STOPWATCH_STATE_RUNNING] = 0.0d;
            double[] lapSecs = new double[STOPWATCH_STATE_PAUSED];
            lapSecs[STOPWATCH_STATE_RUNNING] = 0.0d;
            getElapsedAndLapSeconds(elapsedSecs, lapSecs);
            if (((int) (elapsedSecs[STOPWATCH_STATE_RUNNING] / 3600.0d)) >= 100) {
                start();
                elapsedSecs[STOPWATCH_STATE_RUNNING] = 0.0d;
                lapSecs[STOPWATCH_STATE_RUNNING] = 0.0d;
            }
            if (this.mListener != null) {
                this.mListener.onStopwatchEvent(elapsedSecs[STOPWATCH_STATE_RUNNING], lapSecs[STOPWATCH_STATE_RUNNING]);
            }
        }
    }

    void pausePolling() {
        if (this.mState == 0) {
            this.mPollTimer.stop();
        }
    }

    void resumePolling() {
        if (this.mState == 0) {
            this.mPollTimer.start();
        }
    }

    double truncateDoubleToDecimal(double dblValue, int decimalplace) {
        Object[] objArr = new Object[STOPWATCH_STATE_PAUSED];
        objArr[STOPWATCH_STATE_RUNNING] = Integer.valueOf(decimalplace);
        String sformat = String.format(Locale.US, "%%.%df", objArr);
        Locale locale = Locale.US;
        Object[] objArr2 = new Object[STOPWATCH_STATE_PAUSED];
        objArr2[STOPWATCH_STATE_RUNNING] = Double.valueOf(dblValue);
        return Double.valueOf(String.format(locale, sformat, objArr2)).doubleValue();
    }

    String elapsedTimeByString() {
        double seconds = elapsedSeconds();
        int hours = (int) (seconds / 3600.0d);
        int secs = (int) ((seconds - ((double) (hours * 3600))) - ((double) (((int) ((seconds / 60.0d) - ((double) (hours * 60)))) * 60)));
        int sec = (int) Math.round((seconds - Math.floor(seconds)) * 100.0d);
        return String.format(Locale.US, "%02d : %02d : %02d.%02d", new Object[]{Integer.valueOf(hours), Integer.valueOf((int) ((seconds / 60.0d) - ((double) (hours * 60)))), Integer.valueOf(secs), Integer.valueOf(sec)});
    }

    void start() {
        this.mStartPoint.setToNow();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        this.mLastLap = elapsedRealtime;
        this.mStartTime = elapsedRealtime;
        this.mState = STOPWATCH_STATE_RUNNING;
        this.mPollTimer.start();
    }

    void pause() {
        this.mState = STOPWATCH_STATE_PAUSED;
        this.mPollTimer.stop();
        this.mEndPoint.setToNow();
        this.mPauseTime = SystemClock.elapsedRealtime();
        if (this.mListener != null) {
            this.mListener.onStopwatchEvent(lastElapsedSeconds(), getLastLapSeconds());
        }
    }

    void resume() {
        if (this.mState == STOPWATCH_STATE_PAUSED) {
            long difftime = SystemClock.elapsedRealtime() - this.mPauseTime;
            this.mStartTime += difftime;
            this.mLastLap += difftime;
        }
        this.mState = STOPWATCH_STATE_RUNNING;
        this.mPollTimer.start();
    }

    void stop() {
        this.mStartTime = 0;
        this.mLastLap = 0;
        this.mState = STOPWATCH_STATE_STOPPED;
        this.mPollTimer.stop();
    }

    boolean isRunning() {
        return this.mState == 0;
    }

    boolean isPaused() {
        return this.mState == STOPWATCH_STATE_PAUSED;
    }

    boolean isStopped() {
        return this.mState == STOPWATCH_STATE_STOPPED;
    }

    void getElapsedAndLapSeconds(double[] elapsedSecs, double[] lapSecs) {
        long currentTime = SystemClock.elapsedRealtime();
        elapsedSecs[STOPWATCH_STATE_RUNNING] = ((double) (currentTime - this.mStartTime)) / getTicksPerSecond();
        lapSecs[STOPWATCH_STATE_RUNNING] = ((double) (currentTime - this.mLastLap)) / getTicksPerSecond();
    }

    double elapsedSeconds() {
        return ((double) (SystemClock.elapsedRealtime() - this.mStartTime)) / getTicksPerSecond();
    }

    double lastElapsedSeconds() {
        if (this.mState == STOPWATCH_STATE_PAUSED) {
            return ((double) (this.mPauseTime - this.mStartTime)) / getTicksPerSecond();
        }
        return 0.0d;
    }

    double getLapSeconds() {
        return ((double) (SystemClock.elapsedRealtime() - this.mLastLap)) / getTicksPerSecond();
    }

    double getLastLapSeconds() {
        if (this.mState == STOPWATCH_STATE_PAUSED) {
            return ((double) (this.mPauseTime - this.mLastLap)) / getTicksPerSecond();
        }
        return 0.0d;
    }

    void split() {
        if (this.mState == 0) {
            this.mSplitDataList.add(Long.valueOf(SystemClock.elapsedRealtime() - this.mStartTime));
            return;
        }
        Log.i(this.tag, "split: FAIL. Timer is not running.");
    }

    double getSplitAtIndex(int index) {
        return ((double) ((Long) this.mSplitDataList.get(index)).longValue()) / getTicksPerSecond();
    }

    double getLastSplit() {
        int index = this.mSplitDataList.size() - 1;
        if (index < 0) {
            return 0.0d;
        }
        return ((double) ((Long) this.mSplitDataList.get(index)).longValue()) / getTicksPerSecond();
    }

    int getSplitCount() {
        return this.mSplitDataList.size();
    }

    ArrayList<Long> getSplitData() {
        return this.mSplitDataList;
    }

    void lap() {
        if (this.mState == 0) {
            long currentTime = SystemClock.elapsedRealtime();
            this.mLapDataList.add(Long.valueOf(currentTime - this.mLastLap));
            this.mLastLap = currentTime;
            return;
        }
        Log.i(this.tag, "lap: FAIL. Timer is not running.");
    }

    double getLapAtIndex(int index) {
        return ((double) ((Long) this.mLapDataList.get(index)).longValue()) / getTicksPerSecond();
    }

    double getLastLap() {
        int index = this.mLapDataList.size() - 1;
        if (index < 0) {
            return 0.0d;
        }
        return ((double) ((Long) this.mLapDataList.get(index)).longValue()) / getTicksPerSecond();
    }

    int getLapCount() {
        return this.mLapDataList.size();
    }

    ArrayList<Long> getLapData() {
        return this.mLapDataList;
    }

    void reset() {
        this.mStartTime = 0;
        this.mLastLap = 0;
        this.mPauseTime = 0;
        this.mState = STOPWATCH_STATE_STOPPED;
        this.mPollTimer.stop();
        this.mSplitDataList.clear();
        this.mLapDataList.clear();
    }

    void preRelease() {
        reset();
        this.mPollTimer.stop();
    }

    void saveLastSession(Context context) {
        int i;
        if (this.mState != STOPWATCH_STATE_PAUSED) {
        }
        DataOutputStream file = null;
        try {
            file = new DataOutputStream(context.openFileOutput("stopwatch.sav", STOPWATCH_STATE_RUNNING));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            file.writeInt(this.mState);
            file.writeLong(this.mStartTime);
            file.writeLong(this.mPauseTime);
            file.writeLong(this.mLastLap);
            if (this.mStartPoint != null) {
                file.writeBoolean(true);
                file.writeLong(this.mStartPoint.toMillis(true));
            } else {
                file.writeBoolean(false);
            }
            if (this.mEndPoint != null) {
                file.writeBoolean(true);
                file.writeLong(this.mEndPoint.toMillis(true));
            } else {
                file.writeBoolean(false);
            }
            int size = this.mSplitDataList.size();
            file.writeInt(size);
            for (i = STOPWATCH_STATE_RUNNING; i < size; i += STOPWATCH_STATE_PAUSED) {
                file.writeLong(((Long) this.mSplitDataList.get(i)).longValue());
            }
            size = this.mLapDataList.size();
            file.writeInt(size);
            for (i = STOPWATCH_STATE_RUNNING; i < size; i += STOPWATCH_STATE_PAUSED) {
                file.writeLong(((Long) this.mLapDataList.get(i)).longValue());
            }
            file.close();
            Log.i(this.tag, "Write stopwatch object to file succeeded!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void loadLastSession(Context context) {
        this.mSplitDataList.clear();
        this.mLapDataList.clear();
        try {
            int i;
            DataInputStream file = new DataInputStream(context.openFileInput("stopwatch.sav"));
            this.mState = file.readInt();
            this.mStartTime = file.readLong();
            this.mPauseTime = file.readLong();
            this.mLastLap = file.readLong();
            if (file.readBoolean()) {
                this.mStartPoint = new Time();
                this.mStartPoint.set(file.readLong());
            } else {
                this.mStartPoint = null;
            }
            if (file.readBoolean()) {
                this.mEndPoint = new Time();
                this.mEndPoint.set(file.readLong());
            } else {
                this.mEndPoint = null;
            }
            int size = file.readInt();
            for (i = STOPWATCH_STATE_RUNNING; i < size; i += STOPWATCH_STATE_PAUSED) {
                this.mSplitDataList.add(Long.valueOf(file.readLong()));
            }
            size = file.readInt();
            for (i = STOPWATCH_STATE_RUNNING; i < size; i += STOPWATCH_STATE_PAUSED) {
                this.mLapDataList.add(Long.valueOf(file.readLong()));
            }
            file.close();
            Log.i(this.tag, "Read stopwatch object from file succeeded!");
        } catch (IOException e) {
            Log.i(this.tag, "Read stopwatch object from file failed...\n" + e);
        }
    }

    ArrayList<Integer> lapMinIndexWithValueN(double[] value) {
        if (this.mLapDataList.size() == 0) {
            value[STOPWATCH_STATE_RUNNING] = 0.0d;
            return null;
        }
        int i;
        value[STOPWATCH_STATE_RUNNING] = Double.MAX_VALUE;
        for (i = STOPWATCH_STATE_RUNNING; i < this.mLapDataList.size(); i += STOPWATCH_STATE_PAUSED) {
            double tempVal = truncateDoubleToDecimal(getLapAtIndex(i), STOPWATCH_STATE_STOPPED);
            if (tempVal < value[STOPWATCH_STATE_RUNNING] && tempVal < value[STOPWATCH_STATE_RUNNING]) {
                value[STOPWATCH_STATE_RUNNING] = tempVal;
            }
        }
        ArrayList<Integer> indexes = new ArrayList();
        for (i = STOPWATCH_STATE_RUNNING; i < this.mLapDataList.size(); i += STOPWATCH_STATE_PAUSED) {
            if (truncateDoubleToDecimal(getLapAtIndex(i), STOPWATCH_STATE_STOPPED) == value[STOPWATCH_STATE_RUNNING]) {
                indexes.add(Integer.valueOf(i));
            }
        }
        return indexes;
    }

    ArrayList<Integer> lapMaxIndexWithValueN(double[] value) {
        if (this.mLapDataList.size() == 0) {
            value[STOPWATCH_STATE_RUNNING] = 0.0d;
            return null;
        }
        int i;
        value[STOPWATCH_STATE_RUNNING] = Double.MIN_VALUE;
        for (i = STOPWATCH_STATE_RUNNING; i < this.mLapDataList.size(); i += STOPWATCH_STATE_PAUSED) {
            double tempVal = truncateDoubleToDecimal(getLapAtIndex(i), STOPWATCH_STATE_STOPPED);
            if (tempVal > value[STOPWATCH_STATE_RUNNING] && tempVal > value[STOPWATCH_STATE_RUNNING]) {
                value[STOPWATCH_STATE_RUNNING] = tempVal;
            }
        }
        ArrayList<Integer> indexes = new ArrayList();
        for (i = STOPWATCH_STATE_RUNNING; i < this.mLapDataList.size(); i += STOPWATCH_STATE_PAUSED) {
            if (truncateDoubleToDecimal(getLapAtIndex(i), STOPWATCH_STATE_STOPPED) == value[STOPWATCH_STATE_RUNNING]) {
                indexes.add(Integer.valueOf(i));
            }
        }
        return indexes;
    }

    double lapMin() {
        return ((double) ((Long) Collections.min(this.mLapDataList)).longValue()) / getTicksPerSecond();
    }

    double lapMax() {
        return ((double) ((Long) Collections.max(this.mLapDataList)).longValue()) / getTicksPerSecond();
    }

    double lapAverage() {
        if (this.mLapDataList.size() == 0) {
            return 0.0d;
        }
        double acc = 0.0d;
        for (int i = STOPWATCH_STATE_RUNNING; i < this.mLapDataList.size(); i += STOPWATCH_STATE_PAUSED) {
            acc += truncateDoubleToDecimal(getLapAtIndex(i), STOPWATCH_STATE_STOPPED);
        }
        return acc / ((double) this.mLapDataList.size());
    }

    double lapSpan() {
        return Math.abs(lapMax() - lapMin());
    }

    double getTicksPerSecond() {
        return 1000.0d;
    }

    int getState() {
        return this.mState;
    }

    void setState(int st) {
        this.mState = st;
    }

    long getStartTime() {
        return this.mStartTime;
    }

    long getPauseTime() {
        return this.mPauseTime;
    }

    Time getStartPoint() {
        return this.mStartPoint;
    }

    Time getEndPoint() {
        return this.mEndPoint;
    }

    public void setStopwatchEventListener(OnStopwatchEventListener listener) {
        this.mListener = listener;
    }

    public interface OnStopwatchEventListener {
        void onStopwatchEvent(double d, double d2);
    }
}
