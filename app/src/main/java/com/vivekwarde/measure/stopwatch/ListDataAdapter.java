package com.vivekwarde.measure.stopwatch;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.vision.barcode.Barcode;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;

import java.util.ArrayList;

public class ListDataAdapter extends BaseAdapter {
    private int mColor;
    private Context mContext;
    private ArrayList<String> mData;
    private Typeface mFont;
    private float mFontSize;
    private int mLeftMmargin;
    private int mMaxVisibleItemCount;
    private int mRowHeight;
    private BitmapDrawable mTilebitmap;

    static class ViewHolder {
        public TextView tv;

        ViewHolder() {
        }
    }

    public ListDataAdapter(Context context) {
        this.mData = null;
        this.mRowHeight = 0;
        this.mFont = null;
        this.mFontSize = 0.0f;
        this.mColor = -1;
        this.mLeftMmargin = 0;
        this.mContext = null;
        this.mMaxVisibleItemCount = 0;
        this.mContext = context;
        this.mData = new ArrayList();
        this.mFont = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/Eurostile LT Demi.ttf");
        this.mFontSize = this.mContext.getResources().getDimension(R.dimen.STOPWATCH_TABLELAYOUT_FONT_SIZE);
        this.mColor = ContextCompat.getColor(this.mContext, R.color.LED_BLUE);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this.mContext, R.drawable.tile_caro)).getBitmap();
        this.mRowHeight = bitmap.getHeight() * 2;
        this.mLeftMmargin = (int) this.mContext.getResources().getDimension(R.dimen.STOPWATCH_LIST_VIEW_LEFT_MARGIN);
        this.mTilebitmap = new BitmapDrawable(this.mContext.getResources(), bitmap);
        this.mTilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
    }

    public int getRowHeight() {
        return this.mRowHeight;
    }

    public void setListViewHeight(int height) {
        this.mMaxVisibleItemCount = (int) Math.round(((double) height) / ((double) getRowHeight()));
        updateDummyItems();
        notifyDataSetChanged();
    }

    private void updateDummyItems() {
        int count;
        int i;
        if (this.mData.size() < this.mMaxVisibleItemCount) {
            count = this.mMaxVisibleItemCount - this.mData.size();
            for (i = 0; i < count; i++) {
                this.mData.add(BuildConfig.FLAVOR);
            }
        }
        if (this.mData.size() > this.mMaxVisibleItemCount) {
            count = this.mData.size() - this.mMaxVisibleItemCount;
            for (i = 0; i < count; i++) {
                if (this.mData.get((this.mData.size() - i) - 1) == BuildConfig.FLAVOR) {
                    this.mData.remove(this.mData.size() - 1);
                }
            }
        }
    }

    public void addItem(String sItem, boolean instantNotify) {
        this.mData.add(0, sItem);
        updateDummyItems();
        if (instantNotify) {
            notifyDataSetChanged();
        }
    }

    public void clear(boolean instantNotify) {
        this.mData.clear();
        updateDummyItems();
        if (instantNotify) {
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.mData.size();
    }

    public String getItem(int position) {
        return (String) this.mData.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    @SuppressLint({"NewApi"})
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout listLayout = (LinearLayout) convertView;
        if (listLayout == null) {
            listLayout = new LinearLayout(this.mContext);
            listLayout.setLayoutParams(new LayoutParams(-1, this.mRowHeight));
            if (VERSION.SDK_INT < 16) {
                listLayout.setBackgroundDrawable(this.mTilebitmap);
            } else {
                listLayout.setBackground(this.mTilebitmap);
            }
            TextView tv = new TextView(this.mContext);
            tv.setTypeface(this.mFont);
            tv.setPaintFlags(tv.getPaintFlags() | Barcode.ITF);
            tv.setTextSize(1, this.mFontSize);
            tv.setTextColor(this.mColor);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
            params.leftMargin = this.mLeftMmargin;
            tv.setLayoutParams(params);
            listLayout.addView(tv);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv = tv;
            listLayout.setTag(viewHolder);
        }
        ((ViewHolder) listLayout.getTag()).tv.setText(getItem(position));
        return listLayout;
    }
}
