package com.vivekwarde.measure.stopwatch;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.LEDView;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.stopwatch.CStopwatch.OnStopwatchEventListener;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.ArrayList;
import java.util.Locale;

public class StopwatchActivity extends BaseActivity implements OnClickListener, OnStopwatchEventListener {
    final String tag;
    SPImageButton mInfoButton;
    LEDView mLEDView;
    SPImageButton mLapSplitResetButton;
    ListDataAdapter mListDataAdapter;
    SPImageButton mMailButton;
    SPImageButton mMenuButton;
    LEDView mSmallLEDView;
    SPImageButton mStartStopButton;
    CStopwatch mStopwatch;
    SPImageButton mSwitchModeButton;
    Object[] r20;
    private int mHeaderBarHeight;
    private SPScale9ImageView mLEDScreen;
    private TextView mLabel;
    private ListView mListView;

    public StopwatchActivity() {
        this.tag = getClass().getSimpleName();
        this.mStopwatch = null;
        this.mLEDView = null;
        this.mSmallLEDView = null;
        this.mStartStopButton = null;
        this.mMailButton = null;
        this.mLapSplitResetButton = null;
        this.mSwitchModeButton = null;
        this.mInfoButton = null;
        this.mMenuButton = null;
        this.mLEDScreen = null;
        this.mLabel = null;
        this.mListView = null;
        this.mListDataAdapter = null;
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        loadLastSession();
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (VERSION.SDK_INT >= 16) {
                    StopwatchActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    StopwatchActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                ViewHelper.setPivotX(StopwatchActivity.this.mSmallLEDView, (float) (StopwatchActivity.this.mSmallLEDView.getWidth() / 2));
                ViewHelper.setPivotY(StopwatchActivity.this.mSmallLEDView, (float) StopwatchActivity.this.mSmallLEDView.getHeight());
                ViewHelper.setScaleX(StopwatchActivity.this.mSmallLEDView, 0.4f);
                ViewHelper.setScaleY(StopwatchActivity.this.mSmallLEDView, 0.4f);
                StopwatchActivity.this.mListDataAdapter.setListViewHeight(StopwatchActivity.this.mListView.getHeight());
            }
        });
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        initInfo();
        initSubviews();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    protected void onDestroy() {
        this.mStopwatch.saveLastSession(this);
        this.mStopwatch.pausePolling();
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    void loadLastSession() {
        this.mStopwatch = new CStopwatch();
        this.mStopwatch.setStopwatchEventListener(this);
        this.mStopwatch.init();
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_RETAIN_ENABLED_KEY, true)) {
            this.mStopwatch.loadLastSession(this);
            if (this.mStopwatch.isRunning()) {
                this.mStopwatch.resume();
            }
        }
    }

    void initInfo() {
    }

    @SuppressLint({"NewApi"})
    void initSubviews() {
        this.mUiLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, mScreenHeight);
        layoutParams.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(layoutParams);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        int frameMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_VERT_MARGIN_BTW_DOTLEDSCREEN_AND_HEADER);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap();
        this.mHeaderBarHeight = (bitmap.getHeight() / 2) + (vButtonScreenMargin * 2);
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(bitmap);
        this.mMenuButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        layoutParams.height = bitmap.getHeight() / 2;
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap();
        this.mInfoButton.setBackgroundBitmap(bitmap);
        this.mInfoButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mMenuButton.getId());
        layoutParams.height = bitmap.getHeight() / 2;
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mInfoButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mInfoButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mNoAdsButton.setLayoutParams(layoutParams);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap();
        SPScale9ImageView backgroundView = new SPScale9ImageView(this);
        backgroundView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.topMargin = this.mHeaderBarHeight;
        backgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(backgroundView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();
        int[] capSizes = ImageUtility.getContentCapSizes(bitmap);
        SPScale9ImageView sPScale9ImageView = new SPScale9ImageView(this);
        sPScale9ImageView.setId(1999);
        sPScale9ImageView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, bitmap.getHeight());
        int tempMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_DOTLEDSCREEN_AND_SCREEN);
        layoutParams.setMargins(tempMargin, this.mHeaderBarHeight + frameMargin, tempMargin, 0);
        sPScale9ImageView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(sPScale9ImageView);
        View dotBackgroundView = new View(this);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_dot)).getBitmap());
        bitmapDrawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            dotBackgroundView.setBackgroundDrawable(bitmapDrawable);
        } else {
            dotBackgroundView.setBackground(bitmapDrawable);
        }
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, sPScale9ImageView.getId());
        layoutParams.addRule(6, sPScale9ImageView.getId());
        layoutParams.addRule(7, sPScale9ImageView.getId());
        layoutParams.addRule(8, sPScale9ImageView.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        dotBackgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(dotBackgroundView);
        this.mLEDView = new LEDView(this, 0);
        this.mLEDView.setId(8);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(6, sPScale9ImageView.getId());
        layoutParams.setMargins(0, capSizes[1], 0, 0);
        this.mLEDView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mLEDView);
        this.mSmallLEDView = new LEDView(this, 0);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(8, sPScale9ImageView.getId());
        layoutParams.setMargins(0, 0, ((int) getResources().getDimension(R.dimen.STOPWATCH_LED_OVERLAP_SIZE)) / 2, (int) (((double) capSizes[3]) * 1.3d));
        this.mSmallLEDView.setLayoutParams(layoutParams);
        this.mSmallLEDView.setTime(0.0d);
        this.mUiLayout.addView(this.mSmallLEDView);
        sPScale9ImageView.bringToFront();
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.stopwatch_ditch)).getBitmap();
        ImageView buttonBackgroundLeft = new ImageView(this);
        buttonBackgroundLeft.setId(121);
        buttonBackgroundLeft.setImageBitmap(bitmap);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(12);
        layoutParams.setMargins((int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN), 0, 0, frameMargin);
        buttonBackgroundLeft.setLayoutParams(layoutParams);
        this.mUiLayout.addView(buttonBackgroundLeft);
        ImageView buttonBackgroundRight = new ImageView(this);
        buttonBackgroundRight.setId(122);
        buttonBackgroundRight.setImageBitmap(bitmap);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(11);
        layoutParams.addRule(12);
        layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN), frameMargin);
        buttonBackgroundRight.setLayoutParams(layoutParams);
        this.mUiLayout.addView(buttonBackgroundRight);
        ViewHelper.setRotation(buttonBackgroundRight, BitmapDescriptorFactory.HUE_CYAN);
        this.mStartStopButton = new SPImageButton(this);
        this.mStartStopButton.setId(3);
        this.mStartStopButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.stopwatch_button_start)).getBitmap());
        this.mStartStopButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, buttonBackgroundLeft.getId());
        layoutParams.addRule(5, buttonBackgroundLeft.getId());
        layoutParams.setMargins(0, 0, 0, 0);
        this.mStartStopButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mStartStopButton);
        this.mLapSplitResetButton = new SPImageButton(this);
        this.mLapSplitResetButton.setId(4);
        this.mLapSplitResetButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.stopwatch_button_reset)).getBitmap());
        this.mLapSplitResetButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, buttonBackgroundRight.getId());
        layoutParams.addRule(7, buttonBackgroundRight.getId());
        layoutParams.setMargins(0, 0, 0, 0);
        this.mLapSplitResetButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mLapSplitResetButton);
        this.mMailButton = new SPImageButton(this);
        this.mMailButton.setId(5);
        this.mMailButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.stopwatch_button_email)).getBitmap());
        this.mMailButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, buttonBackgroundLeft.getId());
        layoutParams.addRule(7, buttonBackgroundLeft.getId());
        layoutParams.setMargins(0, 0, 0, 0);
        this.mMailButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mMailButton);
        this.mSwitchModeButton = new SPImageButton(this);
        this.mSwitchModeButton.setId(6);
        this.mSwitchModeButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.stopwatch_button_mode_switch)).getBitmap());
        this.mSwitchModeButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(8, buttonBackgroundRight.getId());
        layoutParams.addRule(5, buttonBackgroundRight.getId());
        layoutParams.setMargins(0, 0, 0, 0);
        this.mSwitchModeButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mSwitchModeButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        int ledmargin = (this.mHeaderBarHeight - (bitmap.getHeight() - 2)) / 2;
        this.mLEDScreen = new SPScale9ImageView(this);
        this.mLEDScreen.setId(7);
        this.mLEDScreen.setBitmap(bitmap);
        layoutParams = new LayoutParams((int) (SeismometerGraphView.MAX_ACCELERATION * ((double) (bitmap.getWidth() - 2))), bitmap.getHeight() - 2);
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        layoutParams.setMargins((int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN), ledmargin, 0, 0);
        this.mLEDScreen.setLayoutParams(layoutParams);
        this.mLEDScreen.setOnClickListener(this);
        this.mUiLayout.addView(this.mLEDScreen);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mLabel = new TextView(this);
        this.mLabel.setPaintFlags(this.mLabel.getPaintFlags() | Barcode.ITF);
        this.mLabel.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabel.setBackgroundColor(0);
        this.mLabel.setTypeface(font);
        this.mLabel.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE));
        this.mLabel.setGravity(17);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, this.mLEDScreen.getId());
        layoutParams.addRule(6, this.mLEDScreen.getId());
        layoutParams.addRule(7, this.mLEDScreen.getId());
        layoutParams.addRule(8, this.mLEDScreen.getId());
        layoutParams.setMargins(0, 0, 0, 0);
        this.mLabel.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mLabel);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();
        capSizes = ImageUtility.getContentCapSizes(bitmap);
        sPScale9ImageView = new SPScale9ImageView(this);
        sPScale9ImageView.setId(123);
        sPScale9ImageView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(3, sPScale9ImageView.getId());
        layoutParams.addRule(2, buttonBackgroundLeft.getId());
        layoutParams.setMargins(hButtonButtonMargin, hButtonButtonMargin, hButtonButtonMargin, hButtonButtonMargin);
        sPScale9ImageView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(sPScale9ImageView);
        this.mListView = new ListView(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, sPScale9ImageView.getId());
        layoutParams.addRule(6, sPScale9ImageView.getId());
        layoutParams.addRule(7, sPScale9ImageView.getId());
        layoutParams.addRule(8, sPScale9ImageView.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        this.mListView.setLayoutParams(layoutParams);
        this.mListView.setDivider(null);
        this.mListView.setDividerHeight(0);
        this.mListView.setVerticalFadingEdgeEnabled(false);
        this.mListDataAdapter = new ListDataAdapter(this);
        this.mListView.setAdapter(this.mListDataAdapter);
        this.mUiLayout.addView(this.mListView);
        sPScale9ImageView.bringToFront();
        this.mLEDView.setTime(this.mStopwatch.lastElapsedSeconds());
        this.mSmallLEDView.setTime(this.mStopwatch.getLastLapSeconds());
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, true)) {
            this.mSmallLEDView.turnOff();
        } else {
            this.mSmallLEDView.turnOn();
        }
        updateButtonsState();
        updateLapSplitData();
    }

    void onStartStop() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mStopwatch.isRunning()) {
            this.mStopwatch.pause();
        } else if (this.mStopwatch.isPaused()) {
            this.mStopwatch.resume();
        } else if (this.mStopwatch.isStopped()) {
            this.mStopwatch.start();
            this.mLapSplitResetButton.setEnabled(true);
        }
        updateButtonsState();
    }

    void updateButtonsState() {
        updateStartStopButtonState();
        updateLapSplitResetButtonState();
        updateSwitchModeButton();
    }

    void updateStartStopButtonState() {
        this.mStartStopButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(this.mStopwatch.isRunning() ? R.drawable.stopwatch_button_pause : R.drawable.stopwatch_button_start)).getBitmap());
    }

    void updateSwitchModeButton() {
        boolean z;
        boolean z2 = true;
        SPImageButton sPImageButton = this.mSwitchModeButton;
        if (this.mStopwatch.isRunning()) {
            z = false;
        } else {
            z = true;
        }
        sPImageButton.setEnabled(z);
        SPImageButton sPImageButton2 = this.mMailButton;
        if (this.mStopwatch.isRunning()) {
            z2 = false;
        }
        sPImageButton2.setEnabled(z2);
    }

    void updateLapSplitResetButtonState() {
        Bitmap bitmap;
        boolean bSplitOn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, true);
        if (this.mStopwatch.isRunning()) {
            bitmap = ((BitmapDrawable) getResources().getDrawable(bSplitOn ? R.drawable.stopwatch_button_split : R.drawable.stopwatch_button_lap)).getBitmap();
        } else {
            bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.stopwatch_button_reset)).getBitmap();
        }
        this.mLapSplitResetButton.setBackgroundBitmap(bitmap);
        this.mLabel.setText(getResources().getString(bSplitOn ? R.string.IDS_SPLIT_MODE : R.string.IDS_LAP_MODE));
    }

    void onLapSplitReset() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mStopwatch.isRunning()) {
            String sText;
            boolean bSplitOn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, true);
            if (bSplitOn) {
                this.mStopwatch.split();
            } else {
                this.mStopwatch.lap();
            }
            int count = bSplitOn ? this.mStopwatch.getSplitCount() : this.mStopwatch.getLapCount();
            String sMode = getResources().getString(bSplitOn ? R.string.IDS_SPLIT : R.string.IDS_LAP);
            if (bSplitOn) {
                sText = String.format(Locale.US, "%s %02d  -  %s", new Object[]{sMode, Integer.valueOf(count), CStopwatch.doubleToStringWithSep(this.mStopwatch.getSplitAtIndex(count - 1), " : ")});
            } else {
                sText = String.format(Locale.US, "%s %02d  -  %s", new Object[]{sMode, Integer.valueOf(count), CStopwatch.doubleToStringWithSep(this.mStopwatch.getLapAtIndex(count - 1), " : ")});
            }
            this.mListDataAdapter.addItem(sText, true);
            this.mListView.setSelectionAfterHeaderView();
            return;
        }
        this.mStopwatch.reset();
        this.mLEDView.setTime(0.0d);
        this.mSmallLEDView.setTime(0.0d);
        this.mLapSplitResetButton.setEnabled(false);
        this.mListDataAdapter.clear(true);
    }

    void onEmail() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        MiscUtility.sendEmail(this, "mailto:", PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, true) ? "Stopwatch - Split data" : "Stopwatch - Lap data", formatMailMessage(), null);
    }

    String formatMailMessage() {
        ArrayList<Long> data;
        boolean bSplitOn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, true);
        if (bSplitOn) {
            data = this.mStopwatch.getSplitData();
        } else {
            data = this.mStopwatch.getLapData();
        }
        int count = data.size();
        if (count <= 0) {
            return BuildConfig.FLAVOR;
        }
        int i;
        String sTemp;
        String formatPattern = "%02d-%02d-%02d %02d:%02d:%02d";
        Time startTime = this.mStopwatch.getStartPoint();
        r20 = new Object[6];
        r20[0] = Integer.valueOf(startTime.year);
        r20[1] = Integer.valueOf(startTime.month + 1);
        r20[2] = Integer.valueOf(startTime.monthDay);
        r20[3] = Integer.valueOf(startTime.hour);
        r20[4] = Integer.valueOf(startTime.minute);
        r20[5] = Integer.valueOf(startTime.second);
        String sMailMessage = "Start: " + String.format(Locale.US, "%02d-%02d-%02d %02d:%02d:%02d", r20) + "\n";
        String sMode = getResources().getString(bSplitOn ? R.string.IDS_SPLIT : R.string.IDS_LAP);
        for (i = 0; i < count; i++) {
            if (bSplitOn) {
                r20 = new Object[3];
                r20[0] = sMode;
                r20[1] = Integer.valueOf(count - i);
                r20[2] = CStopwatch.doubleToStringWithSep(this.mStopwatch.getSplitAtIndex((count - i) - 1), " : ");
                sTemp = String.format(Locale.US, "%s %02d  -  %s\n", r20);
            } else {
                r20 = new Object[3];
                r20[0] = sMode;
                r20[1] = Integer.valueOf(count - i);
                r20[2] = CStopwatch.doubleToStringWithSep(this.mStopwatch.getLapAtIndex((count - i) - 1), " : ");
                sTemp = String.format(Locale.US, "%s %02d  -  %s\n", r20);
            }
            sMailMessage = sMailMessage + sTemp;
        }
        Time stopTime = this.mStopwatch.getEndPoint();
        r20 = new Object[6];
        r20[0] = Integer.valueOf(stopTime.year);
        r20[1] = Integer.valueOf(stopTime.month + 1);
        r20[2] = Integer.valueOf(stopTime.monthDay);
        r20[3] = Integer.valueOf(stopTime.hour);
        r20[4] = Integer.valueOf(stopTime.minute);
        r20[5] = Integer.valueOf(stopTime.second);
        sMailMessage = sMailMessage + "Stop: " + String.format(Locale.US, "%02d-%02d-%02d %02d:%02d:%02d", r20) + "\n";
        if (bSplitOn) {
            return sMailMessage + "\n";
        }
        r20 = new Object[1];
        r20[0] = CStopwatch.doubleToStringWithSep(this.mStopwatch.lapAverage(), ":");
        sMailMessage = sMailMessage + String.format(Locale.US, "\nAverage Lap Time: %s\n", r20);
        double[] temp = new double[]{0.0d};
        ArrayList<Integer> indexes = this.mStopwatch.lapMinIndexWithValueN(temp);
        r20 = new Object[1];
        r20[0] = CStopwatch.doubleToStringWithSep(temp[0], ":");
        sMailMessage = sMailMessage + String.format(Locale.US, "Fastest Lap Time: %s (Lap ", r20);
        for (i = 0; i < indexes.size(); i++) {
            if (i == indexes.size() - 1) {
                sTemp = String.format(Locale.US, "%02d)\n", new Object[]{Integer.valueOf(((Integer) indexes.get(i)).intValue() + 1)});
            } else {
                sTemp = String.format(Locale.US, "%02d, ", new Object[]{Integer.valueOf(((Integer) indexes.get(i)).intValue() + 1)});
            }
            sMailMessage = sMailMessage + sTemp;
        }
        indexes = this.mStopwatch.lapMaxIndexWithValueN(temp);
        r20 = new Object[1];
        r20[0] = CStopwatch.doubleToStringWithSep(temp[0], ":");
        sMailMessage = sMailMessage + String.format(Locale.US, "Slowest Lap Time: %s (Lap ", r20);
        for (i = 0; i < indexes.size(); i++) {
            if (i == indexes.size() - 1) {
                sTemp = String.format(Locale.US, "%02d)\n", new Object[]{Integer.valueOf(((Integer) indexes.get(i)).intValue() + 1)});
            } else {
                sTemp = String.format(Locale.US, "%02d, ", new Object[]{Integer.valueOf(((Integer) indexes.get(i)).intValue() + 1)});
            }
            sMailMessage = sMailMessage + sTemp;
        }
//        return sMailMessage + String.format(Locale.US, "Span Time: %s\n", new Object[]{CStopwatch.doubleToStringWithSep(max - min, ":")});
        return sMailMessage + String.format(Locale.US, "Span Time: %s\n", new Object[]{CStopwatch.doubleToStringWithSep(2 - 1, ":")});
    }

    void onSwitchMode() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean bSplitOn = !PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, true);
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, bSplitOn);
        editor.apply();
        updateLapSplitResetButtonState();
        if (bSplitOn) {
            this.mSmallLEDView.turnOff();
        } else {
            this.mSmallLEDView.turnOn();
        }
        updateLapSplitData();
    }

    void updateLapSplitData() {
        this.mListDataAdapter.clear(false);
        boolean bSplitOn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_STOPWATCH_SPLIT_MODE_KEY, true);
        int count = bSplitOn ? this.mStopwatch.getSplitCount() : this.mStopwatch.getLapCount();
        String sText = BuildConfig.FLAVOR;
        String sMode = getResources().getString(bSplitOn ? R.string.IDS_SPLIT : R.string.IDS_LAP);
        for (int i = 0; i < count; i++) {
            if (bSplitOn) {
                sText = String.format(Locale.US, "%s %02d  -  %s", new Object[]{sMode, Integer.valueOf(i + 1), CStopwatch.doubleToStringWithSep(this.mStopwatch.getSplitAtIndex(i), " : ")});
            } else {
                sText = String.format(Locale.US, "%s %02d  -  %s", new Object[]{sMode, Integer.valueOf(i + 1), CStopwatch.doubleToStringWithSep(this.mStopwatch.getLapAtIndex(i), " : ")});
            }
            this.mListDataAdapter.addItem(sText, false);
        }
        this.mListDataAdapter.notifyDataSetChanged();
        this.mListView.setSelectionAfterHeaderView();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case Barcode.ALL_FORMATS /*0*/:
                onRemoveAdsButton();
            case CompletionEvent.STATUS_FAILURE /*1*/:
                onButtonMenu();
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                onButtonSettings();
            case CompletionEvent.STATUS_CANCELED /*3*/:
                onStartStop();
            case Barcode.PHONE /*4*/:
                onLapSplitReset();
            case Barcode.PRODUCT /*5*/:
                onEmail();
            case Barcode.SMS /*6*/:
                onSwitchMode();
            case Barcode.TEXT /*7*/:
                if (this.mSwitchModeButton.isEnabled()) {
                    onSwitchMode();
                }
            default:
                Log.i(this.tag, "Unknown action id :(");
        }
    }

    public void onStopwatchEvent(double elapsed, double lap) {
        this.mLEDView.setTime(elapsed);
        this.mSmallLEDView.setTime(lap);
    }

    public class Define {
        public static final int BUTTON_EMAIL = 5;
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_LAP_SPLIT_RESET = 4;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_START_STOP = 3;
        public static final int BUTTON_SWITCH_MODE = 6;
        public static final int BUTTON_UPGRADE = 0;
        public static final int LED_PRIMARY = 8;
        public static final int TITLE_SWITCH_MODE = 7;
    }
}
