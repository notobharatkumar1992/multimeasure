package com.vivekwarde.measure.surface_level;

import android.annotation.SuppressLint;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.AccelerometerUtility;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;
import java.util.Locale;

public class SurfaceLevelActivity extends BaseActivity implements OnClickListener, SensorEventListener {
    private double mAngleOxy;
    private double mAngleOzx;
    private double mAngleOzy;
    private ImageView mBaseImage;
    private ImageView mBaseRimImage;
    private ImageView mBkgThemeImage;
    private int mBubbleCurPosX;
    private int mBubbleCurPosY;
    private ImageView mBubbleImage;
    private int mBubbleSoundPeriod;
    private Handler mBubbleSoundTimerHandler;
    private Runnable mBubbleSoundTimerTask;
    private float mBubbleSoundVolume;
    private SPImageButton mCalibrationButton;
    private ImageView mDitchImage;
    private double mFilteringFactor;
    private double mGravityX;
    private double mGravityY;
    private double mGravityZ;
    private ImageView mIndicatorImage;
    private TextView mLabelXText;
    private TextView mLabelYText;
    private ImageView mLedScreenImage;
    private SPImageButton mLockButton;
    private ImageView mLowerDotsImage;
    private SPImageButton mMenuButton;
    private TextView mOutputUnitXText;
    private TextView mOutputUnitYText;
    private TextView mOutputXText;
    private TextView mOutputYText;
    private ImageView mPipeImage;
    private double mPosX;
    private double mPosY;
    private ImageView mRingImage;
    private SensorManager mSensorManager;
    private SPImageButton mSettingsButton;
    private ImageView mUpperDotsImage;

    public class ConstantId {
        public static final int TIMER_PERIOD_MAX = 2000;
        public static final int TIMER_PERIOD_MIN = 400;
    }

    public class ControlId {
        public static final int BASE_IMAGE_ID = 5;
        public static final int BASE_RIM_IMAGE_ID = 6;
        public static final int BKG_THEME_IMAGE_ID = 11;
        public static final int BUBBLE_IMAGE_ID = 12;
        public static final int CALIBRATION_BUTTON_ID = 4;
        public static final int DITCH_IMAGE_ID = 9;
        public static final int INDICATOR_IMAGE_ID = 13;
        public static final int LABEL_X_TEXT_ID = 16;
        public static final int LABEL_Y_TEXT_ID = 19;
        public static final int LED_SCREEN_IMAGE_ID = 15;
        public static final int LOCK_BUTTON_ID = 3;
        public static final int LOWER_DOTS_IMAGE_ID = 8;
        public static final int MENU_BUTTON_ID = 1;
        public static final int OUTPUT_UNIT_X_TEXT_ID = 18;
        public static final int OUTPUT_UNIT_Y_TEXT_ID = 21;
        public static final int OUTPUT_X_TEXT_ID = 17;
        public static final int OUTPUT_Y_TEXT_ID = 20;
        public static final int PIPE_IMAGE_ID = 10;
        public static final int RING_IMAGE_ID = 14;
        public static final int SETTINGS_BUTTON_ID = 2;
        public static final int UPGRADE_BUTTON_ID = 0;
        public static final int UPPER_DOTS_IMAGE_ID = 7;
    }

    public SurfaceLevelActivity() {
        this.mMenuButton = null;
        this.mSettingsButton = null;
        this.mLockButton = null;
        this.mCalibrationButton = null;
        this.mBaseImage = null;
        this.mBaseRimImage = null;
        this.mUpperDotsImage = null;
        this.mLowerDotsImage = null;
        this.mDitchImage = null;
        this.mPipeImage = null;
        this.mBkgThemeImage = null;
        this.mIndicatorImage = null;
        this.mBubbleImage = null;
        this.mRingImage = null;
        this.mLedScreenImage = null;
        this.mBubbleCurPosX = 0;
        this.mBubbleCurPosY = 0;
        this.mBubbleSoundPeriod = GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE;
        this.mBubbleSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE;
        this.mAngleOxy = 0.0d;
        this.mAngleOzy = 0.0d;
        this.mAngleOzx = 0.0d;
        this.mGravityX = 0.0d;
        this.mGravityY = 0.0d;
        this.mGravityZ = 0.0d;
        this.mFilteringFactor = 0.30000001192092896d;
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(0);
        super.onCreate(savedInstanceState);
        mScreenWidth = getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mBubbleSoundTimerHandler = new Handler();
        this.mBubbleSoundTimerTask = new Runnable() {
            public void run() {
                boolean isLocked = PreferenceManager.getDefaultSharedPreferences(SurfaceLevelActivity.this).getBoolean(SettingsKey.SETTINGS_SURFACE_LEVEL_LOCK_KEY, false);
                boolean soundOn = PreferenceManager.getDefaultSharedPreferences(SurfaceLevelActivity.this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true);
                if (!isLocked && soundOn) {
                    SoundUtility.getInstance().playSound(1, SurfaceLevelActivity.this.mBubbleSoundVolume);
                }
                SurfaceLevelActivity.this.mBubbleSoundTimerHandler.postDelayed(this, (long) SurfaceLevelActivity.this.mBubbleSoundPeriod);
            }
        };
        createUi();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    @SuppressLint({"NewApi"})
    private void createUi() {
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        this.mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                SurfaceLevelActivity.this.arrangeLayout();
                if (VERSION.SDK_INT >= 16) {
                    SurfaceLevelActivity.this.mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    SurfaceLevelActivity.this.mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mMainLayout.addView(this.mUiLayout);
        this.mBaseImage = new ImageView(this);
        this.mBaseImage.setId(5);
        tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.bubble_level_base)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mBaseImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mBaseImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mBaseImage);
        this.mBaseRimImage = new ImageView(this);
        this.mBaseRimImage.setId(6);
        this.mBaseRimImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.bubble_level_base_rim)).getBitmap());
        this.mBaseRimImage.setScaleType(ScaleType.CENTER_CROP);
        this.mUiLayout.addView(this.mBaseRimImage);
        this.mUpperDotsImage = new ImageView(this);
        this.mUpperDotsImage.setId(7);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_hole)).getBitmap();
        tilebitmap = new BitmapDrawable(getResources(), bitmap);
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mUpperDotsImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mUpperDotsImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mUpperDotsImage);
        this.mLowerDotsImage = new ImageView(this);
        this.mLowerDotsImage.setId(8);
        tilebitmap = new BitmapDrawable(getResources(), bitmap);
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mLowerDotsImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mLowerDotsImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mLowerDotsImage);
        this.mDitchImage = new ImageView(this);
        this.mDitchImage.setId(9);
        tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_ditch_2)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mDitchImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mDitchImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mDitchImage);
        this.mPipeImage = new ImageView(this);
        this.mPipeImage.setId(10);
        tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_pipe)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mPipeImage.setBackgroundDrawable(tilebitmap);
        } else {
            this.mPipeImage.setBackground(tilebitmap);
        }
        this.mUiLayout.addView(this.mPipeImage);
        String theme = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SURFACE_LEVEL_THEME_KEY, "0");
        this.mBkgThemeImage = new ImageView(this);
        this.mBkgThemeImage.setId(11);
        this.mBkgThemeImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals("0") ? R.drawable.bubble_level_background_galaxy : R.drawable.bubble_level_background_hexagon)).getBitmap());
        this.mUiLayout.addView(this.mBkgThemeImage);
        this.mBubbleImage = new ImageView(this);
        this.mBubbleImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals("0") ? R.drawable.bubble_level_sun : R.drawable.bubble)).getBitmap());
        this.mUiLayout.addView(this.mBubbleImage);
        this.mRingImage = new ImageView(this);
        this.mRingImage.setId(14);
        this.mRingImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.surface_level_ring)).getBitmap());
        this.mUiLayout.addView(this.mRingImage);
        this.mIndicatorImage = new ImageView(this);
        this.mIndicatorImage.setImageBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.surface_level_indicator)).getBitmap());
        this.mUiLayout.addView(this.mIndicatorImage);
        this.mLedScreenImage = new ImageView(this);
        this.mLedScreenImage.setId(15);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        this.mLedScreenImage.setImageBitmap(ImageUtility.scale9Bitmap(bitmap, (int) (((float) bitmap.getWidth()) + (20.0f * getResources().getDisplayMetrics().density)), (bitmap.getHeight() - 2) * 2));
        this.mUiLayout.addView(this.mLedScreenImage);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mLabelXText = new TextView(this);
        this.mLabelXText.setId(16);
        this.mLabelXText.setTypeface(face);
        this.mLabelXText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_LABEL_FONT_SIZE));
        this.mLabelXText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelXText.setPaintFlags(this.mLabelXText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mLabelXText);
        this.mLabelXText.setText("X:");
        this.mOutputXText = new TextView(this);
        this.mOutputXText.setId(17);
        this.mOutputXText.setTypeface(face);
        this.mOutputXText.setMaxLines(1);
        this.mOutputXText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE));
        this.mOutputXText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputXText.setPaintFlags(this.mOutputXText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputXText);
        this.mOutputXText.setText("20");
        this.mOutputUnitXText = new TextView(this);
        this.mOutputUnitXText.setId(18);
        this.mOutputUnitXText.setTypeface(face);
        this.mOutputUnitXText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE));
        this.mOutputUnitXText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputUnitXText.setPaintFlags(this.mOutputXText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputUnitXText);
        this.mLabelYText = new TextView(this);
        this.mLabelYText.setId(19);
        this.mLabelYText.setTypeface(face);
        this.mLabelYText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_LABEL_FONT_SIZE));
        this.mLabelYText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabelYText.setPaintFlags(this.mLabelYText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mLabelYText);
        this.mLabelYText.setText("Y:");
        this.mOutputYText = new TextView(this);
        this.mOutputYText.setId(20);
        this.mOutputYText.setTypeface(face);
        this.mOutputYText.setMaxLines(1);
        this.mOutputYText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE));
        this.mOutputYText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputYText.setPaintFlags(this.mOutputYText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputYText);
        this.mOutputYText.setText("0");
        this.mOutputUnitYText = new TextView(this);
        this.mOutputUnitYText.setId(21);
        this.mOutputUnitYText.setTypeface(face);
        this.mOutputUnitYText.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE));
        this.mOutputUnitYText.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mOutputUnitYText.setPaintFlags(this.mOutputYText.getPaintFlags() | Barcode.ITF);
        this.mUiLayout.addView(this.mOutputUnitYText);
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SURFACE_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.mOutputUnitXText.setText("~");
            this.mOutputUnitYText.setText("~");
        } else if (unit.equals("1")) {
            this.mOutputUnitXText.setText("rad");
            this.mOutputUnitYText.setText("rad");
        } else if (unit.equals("2")) {
            this.mOutputUnitXText.setText("gra");
            this.mOutputUnitYText.setText("gra");
        } else {
            this.mOutputUnitXText.setText("%");
            this.mOutputUnitYText.setText("%");
        }
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap());
        this.mMenuButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mMenuButton);
        this.mSettingsButton = new SPImageButton(this);
        this.mSettingsButton.setId(2);
        this.mSettingsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mSettingsButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mSettingsButton);
        this.mCalibrationButton = new SPImageButton(this);
        this.mCalibrationButton.setId(4);
        this.mCalibrationButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_calibration)).getBitmap());
        this.mCalibrationButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mCalibrationButton);
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SURFACE_LEVEL_LOCK_KEY, false);
        this.mLockButton = new SPImageButton(this);
        this.mLockButton.setId(3);
        this.mLockButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(isLocked ? R.drawable.button_lock : R.drawable.button_unlock)).getBitmap());
        this.mLockButton.setOnClickListener(this);
        this.mUiLayout.addView(this.mLockButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
    }

    private void arrangeLayout() {
        LayoutParams params = new LayoutParams(mScreenWidth, -2);
        params.addRule(13);
        this.mBaseImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mBaseRimImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mBkgThemeImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mRingImage.setLayoutParams(params);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_hole)).getBitmap();
        params = new LayoutParams(mScreenWidth, bitmap.getHeight() * 4);
        params.addRule(6, this.mBaseImage.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_VERT_MARGIN_BTW_TOP_DOTS_AND_MID_BASE);
        this.mUpperDotsImage.setLayoutParams(params);
        params = new LayoutParams(mScreenWidth, bitmap.getHeight() * 4);
        params.addRule(8, this.mBaseImage.getId());
        params.bottomMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_VERT_MARGIN_BTW_TOP_DOTS_AND_MID_BASE);
        this.mLowerDotsImage.setLayoutParams(params);
        params = new LayoutParams(mScreenWidth, -2);
        params.addRule(13);
        this.mDitchImage.setLayoutParams(params);
        params = new LayoutParams(mScreenWidth, -2);
        params.addRule(13);
        this.mPipeImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mIndicatorImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(13);
        this.mBubbleImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mBaseImage.getId());
        params.addRule(6, this.mBaseImage.getId());
        params.leftMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_HORZ_MARGIN_BTW_LEFT_LEDSCREEN_AND_LEFT_BASE);
        params.topMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_VERT_MARGIN_BTW_TOP_LEDSCREEN_AND_TOP_BASE);
        this.mLedScreenImage.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mLedScreenImage.getId());
        params.addRule(4, this.mOutputXText.getId());
        params.leftMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_HORZ_MARGIN_BTW_LEFT_LEDSCREEN_AND_LEFT_OUTPUT_LABEL);
        this.mLabelXText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(0, this.mOutputUnitXText.getId());
        params.addRule(6, this.mLedScreenImage.getId());
        params.topMargin = ((this.mLedScreenImage.getHeight() - (this.mOutputXText.getHeight() * 2)) - ((int) getResources().getDimension(R.dimen.SURFACE_LEVEL_VERT_MARGIN_BTW_BOTTOM_OUTPUT_X_AND_TOP_OUTPUT_Y))) / 2;
        params.rightMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_HORZ_MARGIN_BTW_RIGHT_OUTPUT_AND_LEFT_UNIT);
        this.mOutputXText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(7, this.mLedScreenImage.getId());
        params.addRule(4, this.mOutputXText.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_HORZ_MARGIN_BTW_RIGHT_UNIT_AND_RIGHT_LEDSCREEN);
        this.mOutputUnitXText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mLabelXText.getId());
        params.addRule(4, this.mOutputYText.getId());
        this.mLabelYText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(0, this.mOutputUnitYText.getId());
        params.addRule(3, this.mOutputXText.getId());
        params.topMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_VERT_MARGIN_BTW_BOTTOM_OUTPUT_X_AND_TOP_OUTPUT_Y);
        params.rightMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_HORZ_MARGIN_BTW_RIGHT_OUTPUT_AND_LEFT_UNIT);
        this.mOutputYText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(7, this.mLedScreenImage.getId());
        params.addRule(4, this.mOutputYText.getId());
        params.rightMargin = (int) getResources().getDimension(R.dimen.SURFACE_LEVEL_HORZ_MARGIN_BTW_RIGHT_UNIT_AND_RIGHT_LEDSCREEN);
        this.mOutputUnitYText.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(11);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), 0);
        this.mMenuButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mMenuButton.getId());
        params.addRule(0, this.mMenuButton.getId());
        this.mSettingsButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(12);
        params.addRule(11);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), 0, (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN));
        this.mCalibrationButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(6, this.mCalibrationButton.getId());
        params.addRule(0, this.mCalibrationButton.getId());
        this.mLockButton.setLayoutParams(params);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(9);
        params.setMargins((int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN), (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS), 0);
        this.mNoAdsButton.setLayoutParams(params);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SURFACE_LEVEL_LOCK_KEY, false)) {
            this.mSensorManager.unregisterListener(this);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
        }
    }

    protected void onResume() {
        super.onResume();
        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SURFACE_LEVEL_LOCK_KEY, false)) {
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
            this.mBubbleSoundTimerHandler.postDelayed(this.mBubbleSoundTimerTask, (long) this.mBubbleSoundPeriod);
        }
        String theme = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SURFACE_LEVEL_THEME_KEY, "0");
        this.mBkgThemeImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals("0") ? R.drawable.bubble_level_background_galaxy : R.drawable.bubble_level_background_hexagon)).getBitmap());
        this.mBubbleImage.setImageBitmap(((BitmapDrawable) getResources().getDrawable(theme.equals("0") ? R.drawable.bubble_level_sun : R.drawable.bubble)).getBitmap());
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SURFACE_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            this.mOutputUnitXText.setText("~");
            this.mOutputUnitYText.setText("~");
        } else if (unit.equals("1")) {
            this.mOutputUnitXText.setText("rad");
            this.mOutputUnitYText.setText("rad");
        } else if (unit.equals("2")) {
            this.mOutputUnitXText.setText("gra");
            this.mOutputUnitYText.setText("gra");
        } else {
            this.mOutputUnitXText.setText("%");
            this.mOutputUnitYText.setText("%");
        }
    }

    public void onClick(View source) {
        if (source.getId() == 1) {
            onButtonMenu();
        } else if (source.getId() == 2) {
            onButtonSettings();
        } else if (source.getId() == 3) {
            onButtonLock();
        } else if (source.getId() == 4) {
            onButtonCalibration();
        } else if (source.getId() == 0) {
            onRemoveAdsButton();
        }
    }

    private void onButtonLock() {
        boolean z;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(2, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean isLocked = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_SURFACE_LEVEL_LOCK_KEY, false);
        if (isLocked) {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_unlock)).getBitmap());
            this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 1);
            this.mBubbleSoundTimerHandler.postDelayed(this.mBubbleSoundTimerTask, (long) this.mBubbleSoundPeriod);
        } else {
            this.mLockButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_lock)).getBitmap());
            this.mSensorManager.unregisterListener(this);
            this.mBubbleSoundTimerHandler.removeCallbacks(this.mBubbleSoundTimerTask);
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        String str = SettingsKey.SETTINGS_SURFACE_LEVEL_LOCK_KEY;
        if (isLocked) {
            z = false;
        } else {
            z = true;
        }
        editor.putBoolean(str, z);
        editor.apply();
    }

    private void onButtonCalibration() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, false)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        int rotation = ((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation();
        if (rotation == 1) {
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_X, -((float) this.mAngleOzy));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Y, -((float) this.mAngleOzx));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Z, -((float) this.mAngleOxy));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_LEFT_X, -((float) this.mPosX));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_LEFT_Y, -((float) this.mPosY));
        } else if (rotation == 3) {
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X, -((float) this.mAngleOzy));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Y, -((float) this.mAngleOzx));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_Z, -((float) this.mAngleOxy));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_X, -((float) this.mPosX));
            editor.putFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_Y, -((float) this.mPosY));
        }
        editor.apply();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            double y = (double) event.values[1];
            double z = (double) (-event.values[2]);
            this.mGravityX = (this.mFilteringFactor * ((double) event.values[0])) + (this.mGravityX * (1.0d - this.mFilteringFactor));
            this.mGravityY = (this.mFilteringFactor * y) + (this.mGravityY * (1.0d - this.mFilteringFactor));
            this.mGravityZ = (this.mFilteringFactor * z) + (this.mGravityZ * (1.0d - this.mFilteringFactor));
            this.mAngleOxy = Math.atan2(-this.mGravityY, -this.mGravityX);
            this.mAngleOzy = Math.atan2(this.mGravityZ, this.mGravityY);
            this.mAngleOzx = Math.atan2(this.mGravityZ, this.mGravityX);
            this.mAngleOxy = AccelerometerUtility.validateAngle(this.mAngleOxy);
            this.mAngleOzy = AccelerometerUtility.validateAngle(this.mAngleOzy);
            this.mAngleOzx = AccelerometerUtility.validateAngle(this.mAngleOzx);
            this.mAngleOxy = AccelerometerUtility.to180(this.mAngleOxy);
            this.mAngleOzy = AccelerometerUtility.to180(this.mAngleOzy);
            this.mAngleOzx = AccelerometerUtility.to180(this.mAngleOzx);
            updateBubblePosition();
        }
    }

    @SuppressLint({"DefaultLocale", "NewApi"})
    private void updateBubblePosition() {
        int orientation = ((WindowManager) getSystemService("window")).getDefaultDisplay().getRotation();
        double angleX = this.mAngleOzy;
        double angleY = this.mAngleOzx;
        double angleZ = this.mAngleOxy;
        if (orientation == 3) {
            angleX = AccelerometerUtility.flipAngle(angleX);
            angleY = AccelerometerUtility.flipAngle(angleY);
            angleZ = AccelerometerUtility.rotateAngle(angleZ, orientation);
        } else if (orientation == 1) {
            angleZ = AccelerometerUtility.rotateAngle(angleZ, orientation);
        }
        double r = (double) ((this.mIndicatorImage.getWidth() / 2) - (this.mBubbleImage.getWidth() / 2));
        double Radius = r / Math.sin((double) (((float) PreferenceManager.getDefaultSharedPreferences(this).getInt(SettingsKey.SETTINGS_SURFACE_LEVEL_SENSITIVITY_KEY, 80)) / 100.0f));
        double x = Radius * Math.sin(angleX);
        double y = Radius * Math.sin(angleY);
        double outputAngleX = angleX;
        double outputAngleY = angleY;
        if (orientation == 1) {
            outputAngleX = angleX + ((double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_X, 0.0f));
            outputAngleY = angleY + ((double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_LEFT_Y, 0.0f));
        } else if (orientation == 3) {
            outputAngleX = angleX + ((double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X, 0.0f));
            outputAngleY = angleY + ((double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_OFFSET_RIGHT_X, 0.0f));
        }
        if (Math.abs(angleX) > 0.7853981633974483d) {
            y = (-Radius) * Math.sin(AccelerometerUtility.rotateAngle(angleZ, 3));
            outputAngleY = -AccelerometerUtility.rotateAngle(angleZ, 3);
            if (Math.abs(outputAngleY) > 1.5707963267948966d) {
                outputAngleY = ((double) MiscUtility.getSign(outputAngleY)) * (3.141592653589793d - Math.abs(outputAngleY));
            }
        }
        if (Math.abs(angleY) > 0.7853981633974483d) {
            x = Radius * Math.sin(AccelerometerUtility.rotateAngle(angleZ, 0));
            outputAngleX = AccelerometerUtility.rotateAngle(angleZ, 0);
            if (Math.abs(outputAngleX) > 1.5707963267948966d) {
                outputAngleX = ((double) MiscUtility.getSign(outputAngleX)) * (3.141592653589793d - Math.abs(outputAngleX));
            }
        }
        this.mPosX = x;
        this.mPosY = y;
        if (orientation == 1) {
            x += (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_LEFT_X, 0.0f);
            y += (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_LEFT_Y, 0.0f);
        } else if (orientation == 3) {
            x += (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_X, 0.0f);
            y += (double) PreferenceManager.getDefaultSharedPreferences(this).getFloat(SettingsKey.SETTINGS_SURFACE_LEVEL_CALIB_RIGHT_Y, 0.0f);
        }
        double vectorXYLength = Math.sqrt((x * x) + (y * y));
        if (vectorXYLength > r) {
            double alpha = Math.atan2(x, y);
            x = (((double) MiscUtility.getSign(x)) * r) * Math.abs(Math.sin(alpha));
            y = (((double) MiscUtility.getSign(y)) * r) * Math.abs(Math.cos(alpha));
        }
        double convertedUnitAngleX = outputAngleX;
        double convertedUnitAngleY = outputAngleY;
        String unit = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_SURFACE_LEVEL_UNIT_KEY, "0");
        if (unit.equals("0")) {
            convertedUnitAngleX = (double) MiscUtility.radian2Degree((float) convertedUnitAngleX);
            convertedUnitAngleY = (double) MiscUtility.radian2Degree((float) convertedUnitAngleY);
        } else if (unit.equals("2")) {
            convertedUnitAngleX = (double) MiscUtility.radian2Gradian((float) convertedUnitAngleX);
            convertedUnitAngleY = (double) MiscUtility.radian2Gradian((float) convertedUnitAngleY);
        } else if (unit.equals("3")) {
            convertedUnitAngleX = (double) MiscUtility.radian2Slope((float) convertedUnitAngleX);
            convertedUnitAngleY = (double) MiscUtility.radian2Slope((float) convertedUnitAngleY);
        }
        String outputX = String.format(Locale.getDefault(), "%.1f", new Object[]{Double.valueOf(convertedUnitAngleX)});
        String outputY = String.format(Locale.getDefault(), "%.1f", new Object[]{Double.valueOf(convertedUnitAngleY)});
        this.mOutputXText.setText(outputX);
        this.mOutputYText.setText(outputY);
        Animation translateAnimation = new TranslateAnimation((float) this.mBubbleCurPosX, (float) ((int) x), (float) this.mBubbleCurPosY, (float) ((int) y));
        translateAnimation.setDuration(100);
        translateAnimation.setFillAfter(true);
        this.mBubbleImage.startAnimation(translateAnimation);
        this.mBubbleCurPosX = (int) x;
        this.mBubbleCurPosY = (int) y;
        float distance = vectorXYLength < r ? (float) (vectorXYLength / r) : TextTrackStyle.DEFAULT_FONT_SCALE;
        ViewHelper.setAlpha(this.mIndicatorImage, TextTrackStyle.DEFAULT_FONT_SCALE - distance);
        this.mBubbleSoundPeriod = (int) (400.0f + (1600.0f * distance));
        this.mBubbleSoundVolume = TextTrackStyle.DEFAULT_FONT_SCALE - distance > 0.5f ? 0.5f : TextTrackStyle.DEFAULT_FONT_SCALE - distance;
    }
}
