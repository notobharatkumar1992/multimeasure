package com.vivekwarde.measure.teslameter;

import android.content.Context;
import android.widget.RelativeLayout;

import com.google.android.gms.wearable.WearableStatusCodes;

public class FakeScrollView extends RelativeLayout {
    private static final int MSG_TYPE_ROUTE = 0x0100;
    public static final int MSG_ROUTE_ADDED = MSG_TYPE_ROUTE | 1;
    public static final int MSG_ROUTE_REMOVED = MSG_TYPE_ROUTE | 2;
    public static final int MSG_ROUTE_CHANGED = MSG_TYPE_ROUTE | 3;
    public static final int MSG_ROUTE_VOLUME_CHANGED = MSG_TYPE_ROUTE | 4;
    private static final int MSG_TYPE_PROVIDER = 0x0200;
    MeterView mFirstView;
    int mMaxLevel;
    int mMinLevel;
    MeterView mSecondView;
    MeterView[] mViewsArray;

    public FakeScrollView(Context context) {
        super(context);
        this.mMinLevel = 20;
        this.mMaxLevel = 180;
        this.mViewsArray = new MeterView[]{null, null};
        setBackgroundColor(0);
        initSubviews();
    }

    void initSubviews() {
        this.mFirstView = new MeterView(getContext(), 100);
        LayoutParams params = new LayoutParams(WearableStatusCodes.TARGET_NODE_NOT_CONNECTED, -1);
        params.setMargins(0, 0, 0, 0);
        this.mFirstView.setLayoutParams(params);
        addView(this.mFirstView);
//        this.mSecondView = new MeterView(getContext(), CallbackHandler.MSG_ROUTE_VOLUME_CHANGED);
        this.mSecondView = new MeterView(getContext(), MSG_ROUTE_VOLUME_CHANGED);
        params = new LayoutParams(WearableStatusCodes.TARGET_NODE_NOT_CONNECTED, -1);
        params.setMargins(-4000, 0, 0, 0);
        this.mSecondView.setLayoutParams(params);
        addView(this.mSecondView);
        this.mViewsArray[0] = this.mFirstView;
        this.mViewsArray[1] = this.mSecondView;
    }

    void onUnitChanged() {
        this.mFirstView.setNewIndexWithMiddle(this.mFirstView.getMidValue());
        this.mSecondView.setNewIndexWithMiddle(this.mSecondView.getMidValue());
    }

    void danceWithValue() {
        if (TeslameterGraphView.currValue <= ((float) this.mMaxLevel) && TeslameterGraphView.currValue >= ((float) this.mMinLevel)) {
            ((LayoutParams) this.mViewsArray[0].getLayoutParams()).leftMargin = (int) (((((float) (this.mMinLevel - 20)) - TeslameterGraphView.currValue) * 20.0f) + ((float) (getWidth() / 2)));
        } else if (TeslameterGraphView.currValue > ((float) this.mMaxLevel)) {
            this.mViewsArray[1].setNewIndexWithMiddle((int) TeslameterGraphView.currValue);
            ((LayoutParams) this.mViewsArray[1].getLayoutParams()).leftMargin = (int) (((((float) (this.mMinLevel - 20)) - (TeslameterGraphView.prevValue * 20.0f)) - 4000.0f) + ((float) (getWidth() / 2)));
            ((LayoutParams) this.mViewsArray[0].getLayoutParams()).leftMargin = -4100;
            ((LayoutParams) this.mViewsArray[1].getLayoutParams()).leftMargin = (int) (((-(TeslameterGraphView.currValue - ((float) ((((int) (TeslameterGraphView.currValue / 10.0f)) * 10) - 100)))) * 20.0f) - ((float) (getWidth() / 2)));
            if (this.mViewsArray[0] == this.mFirstView) {
                this.mViewsArray[0] = this.mSecondView;
                this.mViewsArray[1] = this.mFirstView;
            } else {
                this.mViewsArray[0] = this.mFirstView;
                this.mViewsArray[1] = this.mSecondView;
            }
            this.mMinLevel = (((int) (TeslameterGraphView.currValue / 10.0f)) * 10) - 80;
            this.mMaxLevel = (((int) (TeslameterGraphView.currValue / 10.0f)) * 10) + 80;
        } else if (TeslameterGraphView.currValue < ((float) this.mMinLevel)) {
            this.mViewsArray[1].setNewIndexWithMiddle((int) TeslameterGraphView.currValue);
            ((LayoutParams) this.mViewsArray[1].getLayoutParams()).leftMargin = (int) (((((float) (this.mMinLevel - 20)) - (TeslameterGraphView.prevValue * 20.0f)) - 4000.0f) + ((float) (getWidth() / 2)));
            ((LayoutParams) this.mViewsArray[0].getLayoutParams()).leftMargin = -4100;
            ((LayoutParams) this.mViewsArray[1].getLayoutParams()).leftMargin = (int) (((-(TeslameterGraphView.currValue - ((float) ((((int) (TeslameterGraphView.currValue / 10.0f)) * 10) - 100)))) * 20.0f) - ((float) (getWidth() / 2)));
            if (this.mViewsArray[0] == this.mFirstView) {
                this.mViewsArray[0] = this.mSecondView;
                this.mViewsArray[1] = this.mFirstView;
            } else {
                this.mViewsArray[0] = this.mFirstView;
                this.mViewsArray[1] = this.mSecondView;
            }
            this.mMinLevel = (((int) (TeslameterGraphView.currValue / 10.0f)) * 10) - 80;
            this.mMaxLevel = (((int) (TeslameterGraphView.currValue / 10.0f)) * 10) + 80;
        }
    }
}
