package com.vivekwarde.measure.teslameter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;

import java.util.Locale;

public class MeterView extends View {
    int mMiddleValue;
    Paint mPaint;
    Path mPath;
    float x;
    private TextPaint mTextPaint;

    @Deprecated
    public MeterView(Context context) {
        super(context);
        this.mMiddleValue = 0;
        this.mPaint = null;
        this.mTextPaint = null;
        this.mPath = new Path();
    }

    public MeterView(Context context, int midValue) {
        super(context);
        this.mMiddleValue = 0;
        this.mPaint = null;
        this.mTextPaint = null;
        this.mPath = new Path();
        setBackgroundColor(0);
        this.mMiddleValue = midValue;
        this.mPaint = new Paint();
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.TESLAMETER_X_COLOR));
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Eurostile LT Medium.ttf"));
        this.mTextPaint.setFakeBoldText(false);
        this.mTextPaint.setTextAlign(Align.CENTER);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.TESLAMETER_METER_INDEX_FONT_SIZE));
        this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.TESLAMETER_X_COLOR));
    }

    void setNewIndexWithMiddle(int midValue) {
        this.mMiddleValue = midValue;
        invalidate();
    }

    int getMidValue() {
        return this.mMiddleValue;
    }

    protected void onDraw(Canvas canvas) {
        int i;
        canvas.drawARGB(0, 0, 0, 0);
        this.mPaint.setStrokeWidth(TypedValue.applyDimension(1, 4.0f, getResources().getDisplayMetrics()));
        boolean bTesla = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "0").equalsIgnoreCase("0");
        float y1 = (2.0f * ((float) getHeight())) / 5.0f;
        float y2 = (float) getHeight();
        for (i = 0; i < 21; i++) {
            int i2;
            x = (float) (i * SeismometerGraphView.HISTORY_SIZE_PER_PAGE);
            canvas.drawLine(x, y1, x, y2, this.mPaint);
            this.mPath.reset();
            this.mPath.moveTo(x - 50.0f, y1 - 5.0f);
            this.mPath.lineTo(50.0f + x, y1 - 5.0f);
            Locale locale = Locale.US;
            String str = "%d";
            Object[] objArr = new Object[1];
            int i3 = ((this.mMiddleValue / 10) - 10) + i;
            if (bTesla) {
                i2 = 10;
            } else {
                i2 = 100;
            }
            objArr[0] = Integer.valueOf(i2 * i3);
            canvas.drawTextOnPath(String.format(locale, str, objArr), this.mPath, 0.0f, 0.0f, this.mTextPaint);
        }
        y1 += (((float) getHeight()) - y1) * 0.4f;
        for (i = 0; i < 41; i++) {
            float x = (float) (i * 100);
            canvas.drawLine(x, y1, x, y2, this.mPaint);
        }
        float l = ((float) getHeight()) - y1;
        this.mPaint.setStrokeWidth(TypedValue.applyDimension(1, 2.5f, getResources().getDisplayMetrics()));
        y1 += l * 0.4f;
        for (i = 0; i < 201; i++) {
            x = (float) (i * 20);
            canvas.drawLine(x, y1, x, y2, this.mPaint);
        }
        setDrawingCacheEnabled(true);
    }
}
