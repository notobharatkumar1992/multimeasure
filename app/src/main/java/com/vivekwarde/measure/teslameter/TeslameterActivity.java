package com.vivekwarde.measure.teslameter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.ValueAnimator;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.ActionView;
import com.vivekwarde.measure.custom_controls.ActionView.OnActionViewEventListener;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.surface_level.SurfaceLevelActivity.ConstantId;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

public class TeslameterActivity extends BaseActivity implements OnClickListener, OnTouchListener, SensorEventListener, OnActionViewEventListener, DialogInterface.OnClickListener, OnCancelListener {
    final String tag;
    MediaPlayer mAlarmPlayer;
    SPImageButton mInfoButton;
    boolean mIsRunning;
    String mLoadedFile;
    SPImageButton mMenuButton;
    SPImageButton mStartStopButton;
    boolean mUserRunning;
    private ActionView mActionView;
    private View mAlarmView;
    private String mDataString;
    private FakeScrollView mFakeScrollView;
    private TeslameterGraphView mGraphView;
    private int mHeaderBarHeight;
    private SPScale9ImageView mLEDScreen;
    private TextView[] mLabelArray;
    private SensorEvent mLastData;
    private Sensor mMagnetometer;
    private int mSampleCount;
    private SensorManager mSensorManager;
    private TextView[] mValueLabel;

    public TeslameterActivity() {
        this.tag = getClass().getSimpleName();
        this.mIsRunning = true;
        this.mUserRunning = true;
        this.mSensorManager = null;
        this.mMagnetometer = null;
        this.mStartStopButton = null;
        this.mInfoButton = null;
        this.mMenuButton = null;
        this.mLEDScreen = null;
        this.mLabelArray = new TextView[]{null, null};
        this.mValueLabel = new TextView[]{null, null, null};
        this.mLastData = null;
        this.mActionView = null;
        this.mAlarmPlayer = null;
        this.mLoadedFile = BuildConfig.FLAVOR;
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        initInfo();
        initSensor();
        initSubviews();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    void initSensor() {
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mMagnetometer = this.mSensorManager.getDefaultSensor(2);
    }

    protected void onDestroy() {
        if (this.mLoadedFile.length() > 0) {
            stopSoundAlarm();
            this.mAlarmPlayer.release();
        }
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        start();
    }

    protected void onPause() {
        super.onPause();
        pause();
    }

    void start() {
        if (this.mUserRunning) {
            this.mIsRunning = this.mSensorManager.registerListener(this, this.mMagnetometer, 0);
        }
        updateButtonState();
    }

    void pause() {
        this.mIsRunning = false;
        this.mSensorManager.unregisterListener(this);
        updateButtonState();
    }

    void initInfo() {
        this.mAlarmPlayer = new MediaPlayer();
        this.mLoadedFile = BuildConfig.FLAVOR;
        this.mSampleCount = 0;
        this.mDataString = null;
        initDataFiles();
    }

    void initDataFiles() {
        deleteFile("TeslameterData.csv");
        deleteFile("TeslameterData.zip");
        MiscUtility.writeToFile(this, "TeslameterData.csv", "Date,Time,X-Axis,Y-Axis,Z-Axis,EMF,Unit\n", false);
    }

    @SuppressLint({"NewApi"})
    void initSubviews() {
        int i;
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams layoutParams = new LayoutParams(-1, mScreenHeight);
        layoutParams.addRule(3, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(layoutParams);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        int frameMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_VERT_MARGIN_BTW_DOTLEDSCREEN_AND_HEADER);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap();
        this.mHeaderBarHeight = (bitmap.getHeight() / 2) + (vButtonScreenMargin * 2);
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(bitmap);
        this.mMenuButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        layoutParams.height = bitmap.getHeight() / 2;
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        this.mInfoButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mInfoButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mMenuButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mInfoButton);
        this.mNoAdsButton = new SPImageButton(this);
        this.mNoAdsButton.setId(0);
        this.mNoAdsButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_noads)).getBitmap());
        this.mNoAdsButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(0, this.mInfoButton.getId());
        layoutParams.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mNoAdsButton.setLayoutParams(layoutParams);
        this.mNoAdsButton.setVisibility(MainApplication.mIsRemoveAds ? 8 : 0);
        this.mUiLayout.addView(this.mNoAdsButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap();
        SPScale9ImageView backgroundView = new SPScale9ImageView(this);
        backgroundView.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.topMargin = this.mHeaderBarHeight;
        backgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(backgroundView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();
        int[] capSizes = ImageUtility.getContentCapSizes(bitmap);
        View sPScale9ImageView = new SPScale9ImageView(this);
        sPScale9ImageView.setId(1999);
        ((SPScale9ImageView) sPScale9ImageView).setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, bitmap.getHeight());
        int tempMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_DOTLEDSCREEN_AND_SCREEN);
        layoutParams.setMargins(tempMargin, this.mHeaderBarHeight + frameMargin, tempMargin, 0);
        sPScale9ImageView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(sPScale9ImageView);
        View dotBackgroundView = new View(this);
        Drawable bitmapDrawable = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_dot)).getBitmap());
        ((BitmapDrawable) bitmapDrawable).setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            dotBackgroundView.setBackgroundDrawable(bitmapDrawable);
        } else {
            dotBackgroundView.setBackground(bitmapDrawable);
        }
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, sPScale9ImageView.getId());
        layoutParams.addRule(6, sPScale9ImageView.getId());
        layoutParams.addRule(7, sPScale9ImageView.getId());
        layoutParams.addRule(8, sPScale9ImageView.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        dotBackgroundView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(dotBackgroundView);
        this.mFakeScrollView = new FakeScrollView(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, sPScale9ImageView.getId());
        layoutParams.addRule(6, sPScale9ImageView.getId());
        layoutParams.addRule(7, sPScale9ImageView.getId());
        layoutParams.addRule(8, sPScale9ImageView.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        this.mFakeScrollView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mFakeScrollView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.teslameter_hand)).getBitmap();
        ImageView imageView = new ImageView(this);
        imageView.setId(ConstantId.TIMER_PERIOD_MAX);
        imageView.setImageBitmap(bitmap);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, sPScale9ImageView.getId());
        layoutParams.addRule(14);
        layoutParams.setMargins(0, 0, 0, 0);
        imageView.setLayoutParams(layoutParams);
        this.mUiLayout.addView(imageView);
        imageView.bringToFront();
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.teslameter_led_screen_with_button_output_ninepatch)).getBitmap();
        int[] contentCapsizes = ImageUtility.getContentCapSizes(bitmap);
        SPScale9ImageView sPScale_bottom = new SPScale9ImageView(this);
        sPScale_bottom.setId(121);
        ((SPScale9ImageView) sPScale_bottom).setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, bitmap.getHeight() - 2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.height = bitmap.getHeight() - 2;
        layoutParams.setMargins(tempMargin, 0, tempMargin, frameMargin);
        sPScale_bottom.setLayoutParams(layoutParams);
        this.mUiLayout.addView(sPScale_bottom);
        View view = new View(this);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_hole)).getBitmap();
        bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
        ((BitmapDrawable) bitmapDrawable).setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            view.setBackgroundDrawable(bitmapDrawable);
        } else {
            view.setBackground(bitmapDrawable);
        }
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(12);
        layoutParams.height = bitmap.getHeight() * 2;
        layoutParams.setMargins(contentCapsizes[0], 0, contentCapsizes[2], frameMargin);
        view.setLayoutParams(layoutParams);
        this.mUiLayout.addView(view);
        view.bringToFront();
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        TextView[] nameLabel = new TextView[]{null, null, null};
        tempMargin = (int) getResources().getDimension(R.dimen.TESLAMETER_MARGIN_BTW_XYZ_LABEL_AND_LED);
        float fontSize = getResources().getDimension(R.dimen.TESLAMETER_XYZ_FONT_SIZE);
        int i2 = 3;
        int[] colorId = new int[]{R.color.TESLAMETER_X_COLOR, R.color.TESLAMETER_Y_COLOR, R.color.TESLAMETER_Z_COLOR};
        for (i = 0; i < 3; i++) {
            int color = ContextCompat.getColor(this, colorId[i]);
            nameLabel[i] = new TextView(this);
            nameLabel[i].setId(i + SeismometerGraphView.HISTORY_SIZE_PER_PAGE);
            nameLabel[i].setPaintFlags(nameLabel[i].getPaintFlags() | Barcode.ITF);
            layoutParams = new LayoutParams(-2, -2);
            if (i == 0) {
                layoutParams.addRule(6, sPScale_bottom.getId());
                layoutParams.setMargins(contentCapsizes[0], contentCapsizes[1], 0, 0);
            } else if (i == 2) {
                layoutParams.addRule(8, sPScale_bottom.getId());
                layoutParams.setMargins(contentCapsizes[0], 0, 0, contentCapsizes[3]);
            } else {
                layoutParams.addRule(3, SeismometerGraphView.HISTORY_SIZE_PER_PAGE);
                layoutParams.addRule(2, 202);
                layoutParams.setMargins(contentCapsizes[0], 0, 0, 0);
            }
            layoutParams.addRule(5, sPScale_bottom.getId());
            nameLabel[i].setLayoutParams(layoutParams);
            nameLabel[i].setText(Character.toString((char) (i + 88)) + ":");
            nameLabel[i].setGravity(17);
            nameLabel[i].setTypeface(font, 1);
            nameLabel[i].setTextSize(1, fontSize);
            nameLabel[i].setTextColor(color);
            this.mUiLayout.addView(nameLabel[i]);
            this.mValueLabel[i] = new TextView(this);
            this.mValueLabel[i].setPaintFlags(this.mValueLabel[i].getPaintFlags() | Barcode.ITF);
            layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(6, nameLabel[i].getId());
            if (i == 1) {
                layoutParams.addRule(8, nameLabel[i].getId());
            }
            layoutParams.addRule(1, nameLabel[i].getId());
            layoutParams.setMargins(contentCapsizes[0] / 2, 0, 0, 0);
            this.mValueLabel[i].setLayoutParams(layoutParams);
            this.mValueLabel[i].setText("24");
            this.mValueLabel[i].setGravity(17);
            this.mValueLabel[i].setTypeface(font, 1);
            this.mValueLabel[i].setTextSize(1, fontSize);
            this.mValueLabel[i].setTextColor(color);
            this.mUiLayout.addView(this.mValueLabel[i]);
        }
        this.mStartStopButton = new SPImageButton(this);
        this.mStartStopButton.setId(3);
        this.mStartStopButton.setOnClickListener(this);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, sPScale_bottom.getId());
        layoutParams.addRule(7, sPScale_bottom.getId());
        this.mStartStopButton.setLayoutParams(layoutParams);
        this.mUiLayout.addView(this.mStartStopButton);
        updateButtonState();
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        int ledmargin = (this.mHeaderBarHeight - (bitmap.getHeight() - 2)) / 2;
        this.mLEDScreen = new SPScale9ImageView(this);
        this.mLEDScreen.setId(4);
        this.mLEDScreen.setBitmap(bitmap);
        layoutParams = new LayoutParams((int) (1.3d * ((double) (bitmap.getWidth() - 2))), bitmap.getHeight() - 2);
        layoutParams.addRule(9);
        layoutParams.addRule(10);
        layoutParams.setMargins((int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN), ledmargin, 0, 0);
        this.mLEDScreen.setLayoutParams(layoutParams);
        this.mLEDScreen.setOnClickListener(this);
        this.mUiLayout.addView(this.mLEDScreen);
        float valueFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE);
        float unitFontSize = getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_UNIT_FONT_SIZE);
        for (i = 0; i < 2; i++) {
            float f;
            this.mLabelArray[i] = new TextView(this);
            this.mLabelArray[i].setId((i + 0) + 100);
            this.mLabelArray[i].setPaintFlags(this.mLabelArray[i].getPaintFlags() | Barcode.ITF);
            this.mLabelArray[i].setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
            this.mLabelArray[i].setBackgroundColor(0);
            this.mLabelArray[i].setTypeface(font);
            TextView textView = this.mLabelArray[i];
            if (i % 2 != 0) {
                f = unitFontSize;
            } else {
                f = valueFontSize;
            }
            textView.setTextSize(1, f);
            this.mLabelArray[i].setGravity((i % 2 == 0 ? 16 : 48) | 5);
            this.mLabelArray[i].setOnTouchListener(this);
            this.mUiLayout.addView(this.mLabelArray[i]);
        }
        this.mLabelArray[0].setText("24");
        this.mLabelArray[1].setText(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "0").equalsIgnoreCase("0") ? "|T" : "mG");
        new Paint().setTypeface(font);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, this.mLEDScreen.getId());
        layoutParams.addRule(7, this.mLEDScreen.getId());
        layoutParams.rightMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_UNIT_LABEL_RIGHT_MARGIN);
        layoutParams.topMargin = (int) getResources().getDimension(R.dimen.LEDSCREEN_VALUE_LABEL_RIGHT_MARGIN);
        this.mLabelArray[1].setLayoutParams(layoutParams);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(6, this.mLEDScreen.getId());
        layoutParams.addRule(8, this.mLEDScreen.getId());
        layoutParams.addRule(5, this.mLEDScreen.getId());
        layoutParams.addRule(0, this.mLabelArray[1].getId());
        this.mLabelArray[0].setLayoutParams(layoutParams);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen_hole_ninepatch)).getBitmap();
        capSizes = ImageUtility.getContentCapSizes(bitmap);

        SPScale9ImageView spScaleimage_screen_hole = new SPScale9ImageView(this);
        spScaleimage_screen_hole.setId(123);
        spScaleimage_screen_hole.setBitmap(bitmap);
        layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(3, sPScale9ImageView.getId());
        layoutParams.addRule(2, sPScale_bottom.getId());
        layoutParams.setMargins(hButtonButtonMargin, hButtonButtonMargin, hButtonButtonMargin, hButtonButtonMargin);
        spScaleimage_screen_hole.setLayoutParams(layoutParams);
        this.mUiLayout.addView(spScaleimage_screen_hole);
        this.mGraphView = new TeslameterGraphView(this);
        this.mGraphView.setId(6);
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(5, spScaleimage_screen_hole.getId());
        layoutParams.addRule(6, spScaleimage_screen_hole.getId());
        layoutParams.addRule(7, spScaleimage_screen_hole.getId());
        layoutParams.addRule(8, spScaleimage_screen_hole.getId());
        layoutParams.setMargins(capSizes[0], capSizes[1], capSizes[2], capSizes[3]);
        this.mGraphView.setLayoutParams(layoutParams);
        this.mGraphView.setOnClickListener(this);
        this.mUiLayout.addView(this.mGraphView);
        spScaleimage_screen_hole.bringToFront();

        if (this.mMagnetometer == null) {
            TextView textView = new TextView(this);
            textView.setPaintFlags(textView.getPaintFlags() | Barcode.ITF);
            textView.setTextColor(-1);
            textView.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Eurostile LT Medium.ttf"));
            textView.setTextSize(1, (float) ((int) getResources().getDimension(R.dimen.TESLAMETER_NOT_SUPPORT_LABEL_FONT_SIZE)));
            textView.setGravity(17);
            textView.setText(getString(R.string.IDS_TESLAMETER_NOT_AVAIL_INFO));
            layoutParams = new LayoutParams(-2, -2);
            layoutParams.addRule(13);
            layoutParams.setMargins(0, 0, 0, 0);
            textView.setLayoutParams(layoutParams);
            this.mUiLayout.addView(textView);
        }
    }

    void onStartStop() {
        boolean z = true;
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mUserRunning) {
            z = false;
        }
        this.mUserRunning = z;
        if (this.mUserRunning) {
            start();
            return;
        }
        pause();
        turnOnAlarm(false);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case Barcode.ALL_FORMATS /*0*/:
                onRemoveAdsButton();
            case CompletionEvent.STATUS_FAILURE /*1*/:
                onButtonMenu();
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                onButtonSettings();
            case CompletionEvent.STATUS_CANCELED /*3*/:
                onStartStop();
            case Barcode.PHONE /*4*/:
                onUnitChanged();
            case Barcode.SMS /*6*/:
                if (this.mMagnetometer != null) {
                    openActionView(true);
                }
            default:
                Log.i(this.tag, "Unknown action id :(");
        }
    }

    void openActionView(boolean open) {
        if (open) {
            pause();
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            ArrayList<String> titleList = new ArrayList();
            titleList.add(getResources().getString(R.string.IDS_SEND_DATA_TO_EMAIL));
            titleList.add(getResources().getString(R.string.IDS_CLEAR_GRAPH));
            titleList.add(getResources().getString(R.string.IDS_CLOSE));
            Rect r = new Rect();
            this.mGraphView.getGlobalVisibleRect(r);
            int lmargin = ((LayoutParams) this.mGraphView.getLayoutParams()).leftMargin;
            int rmargin = ((LayoutParams) this.mGraphView.getLayoutParams()).rightMargin;
            this.mActionView = new ActionView(this);
            this.mActionView.construct(lmargin, rmargin, new Point(r.centerX(), r.centerY()), titleList, 0);
            this.mActionView.setLayoutParams(new LayoutParams(-1, -1));
            this.mActionView.setOnActionViewEventListener(this);
            this.mUiLayout.addView(this.mActionView);
        } else if (this.mActionView != null) {
            this.mUiLayout.removeView(this.mActionView);
            this.mActionView = null;
        }
    }

    void onUnitChanged() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        if (PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "0").equalsIgnoreCase("0")) {
            editor.putString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "1");
        } else {
            editor.putString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "0");
        }
        editor.apply();
        updateUnitChanged();
    }

    void updateUnitChanged() {
        this.mLabelArray[1].setText(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "0").equalsIgnoreCase("0") ? "|T" : "mG");
        this.mGraphView.invalidate();
        this.mFakeScrollView.onUnitChanged();
        updateValueLabels(this.mLastData);
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.i(this.tag, "New accuracy = " + accuracy);
    }

    public void onSensorChanged(SensorEvent event) {
        if (this.mIsRunning) {
            this.mLastData = event;
            float magnitude = (float) Math.sqrt((double) (((event.values[0] * event.values[0]) + (event.values[1] * event.values[1])) + (event.values[2] * event.values[2])));
            TeslameterGraphView.prevValue = TeslameterGraphView.currValue;
            TeslameterGraphView.currValue = magnitude;
            if (TeslameterGraphView.prevValue > 0.0f) {
                this.mFakeScrollView.danceWithValue();
            }
            if (TeslameterGraphView.count < Strategy.TTL_SECONDS_DEFAULT) {
                TeslameterGraphView.count++;
            }
            TeslameterGraphView.arrayCoordinateX[0] = (float) this.mGraphView.getWidth();
            for (int i = TeslameterGraphView.count - 1; i > 0; i--) {
                TeslameterGraphView.arrayCoordinateY[i] = TeslameterGraphView.arrayCoordinateY[i - 1];
                TeslameterGraphView.arrayCoordinateX[i] = TeslameterGraphView.arrayCoordinateX[i - 1] - TeslameterGraphView.distancePoint;
            }
            TeslameterGraphView.arrayCoordinateY[0] = magnitude;
            TeslameterGraphView.startX = (int) (((float) TeslameterGraphView.startX) - TeslameterGraphView.distancePoint);
            TeslameterGraphView.startX -= TeslameterGraphView.gridLength * ((int) Math.floor((double) (TeslameterGraphView.startX / TeslameterGraphView.gridLength)));
            this.mGraphView.invalidate();
            updateValueLabels(event);
            if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY, true) || magnitude < ((float) Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY, "100")))) {
                turnOnAlarm(false);
            } else {
                turnOnAlarm(true);
            }
            recordValue(event.values, magnitude);
        }
    }

    void recordValue(float[] values, float magnitude) {
        if (this.mSampleCount > GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE) {
            MiscUtility.writeToFileWithLimitedSize(this, "TeslameterData.csv", this.mDataString, true, 3);
            this.mDataString = null;
            this.mSampleCount = 0;
        }
        new Time().setToNow();
        String formatPattern = "%02d-%02d-%02d,%02d:%02d:%02d,%f,%f,%f,%f,\u00b5T\n";
        Calendar calendar = Calendar.getInstance();
        String sTemp = String.format(Locale.US, "%02d-%02d-%02d,%02d:%02d:%02d,%f,%f,%f,%f,\u00b5T\n", new Object[]{Integer.valueOf(calendar.get(Calendar.YEAR)), Integer.valueOf(calendar.get(Calendar.MONTH) + 1), Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH)), Integer.valueOf(calendar.get(Calendar.HOUR)), Integer.valueOf(calendar.get(Calendar.MINUTE)), Integer.valueOf(calendar.get(Calendar.SECOND)), Float.valueOf(values[0]), Float.valueOf(values[1]), Float.valueOf(values[2]), Float.valueOf(magnitude)});
        if (this.mDataString == null) {
            this.mDataString = sTemp;
        } else {
            this.mDataString += sTemp;
        }
        this.mSampleCount++;
    }

    void turnOnAlarm(boolean on) {
        if (on) {
            playSoundAlarm(PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_TESLAMETER_ALERT_SOUND_KEY, "Sound 1.mp3"));
            if (this.mAlarmView == null) {
                this.mAlarmView = new View(this);
                this.mAlarmView.setBackgroundColor(Color.argb(100, MotionEventCompat.ACTION_MASK, 31, 0));
                this.mUiLayout.addView(this.mAlarmView);
                return;
            }
            return;
        }
        stopSoundAlarm();
        if (this.mAlarmView != null) {
            this.mUiLayout.removeView(this.mAlarmView);
            this.mAlarmView = null;
        }
    }

    void playSoundAlarm(String soundFile) {
        try {
            AssetFileDescriptor descriptor = getAssets().openFd("sounds/teslameter/" + soundFile);
            if (this.mLoadedFile.length() == 0) {
                this.mAlarmPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                this.mAlarmPlayer.prepare();
                this.mAlarmPlayer.setLooping(true);
                this.mAlarmPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
            } else if (this.mLoadedFile != soundFile) {
                stopSoundAlarm();
                this.mAlarmPlayer.reset();
                this.mAlarmPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                this.mAlarmPlayer.prepare();
                this.mAlarmPlayer.setLooping(true);
                this.mAlarmPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
            } else if (!this.mAlarmPlayer.isPlaying()) {
                this.mAlarmPlayer.prepare();
            }
            descriptor.close();
            if (!this.mAlarmPlayer.isPlaying()) {
                this.mAlarmPlayer.start();
            }
            this.mLoadedFile = soundFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void stopSoundAlarm() {
        if (this.mLoadedFile.length() != 0 && this.mAlarmPlayer.isPlaying()) {
            this.mAlarmPlayer.stop();
        }
    }

    void updateValueLabels(SensorEvent event) {
        if (event != null) {
            int i;
            boolean bTesla = PreferenceManager.getDefaultSharedPreferences(this).getString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "0").equalsIgnoreCase("0");
            float magnitude = (float) Math.sqrt((double) (((event.values[0] * event.values[0]) + (event.values[1] * event.values[1])) + (event.values[2] * event.values[2])));
            Locale locale = Locale.US;
            String str = "%.2f";
            Object[] objArr = new Object[1];
            if (bTesla) {
                i = 1;
            } else {
                i = 10;
            }
            objArr[0] = Float.valueOf(((float) i) * magnitude);
            this.mLabelArray[0].setText(String.format(locale, str, objArr));
            for (int i2 = 0; i2 < 3; i2++) {
                locale = Locale.US;
                str = "%.1f";
                objArr = new Object[1];
                float f = event.values[i2];
                if (bTesla) {
                    i = 1;
                } else {
                    i = 10;
                }
                objArr[0] = Float.valueOf(((float) i) * f);
                this.mValueLabel[i2].setText(String.format(locale, str, objArr));
            }
        }
    }

    void updateButtonState() {
        this.mStartStopButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(this.mIsRunning ? R.drawable.teslameter_button_stop : R.drawable.teslameter_button_start)).getBitmap());
    }

    public void onActionViewEvent(ActionView actionView, int actionId) {
        if (actionId == 0) {
            emailData();
        } else if (actionId == 1) {
            clearGraph();
        } else if (actionId == 2) {
            start();
        }
        openActionView(false);
    }

    void clearGraph() {
        AlertDialog dialog = new Builder(this).create();
        dialog.setMessage(getResources().getString(R.string.IDS_CLEAR_ALL_HISTORY_ASKING));
        dialog.setButton(-1, getResources().getString(17039379), this);
        dialog.setButton(-2, getResources().getString(17039369), this);
        dialog.setIcon(R.drawable.icon_about);
        dialog.setTitle(getResources().getString(R.string.IDS_CONFIRMATION));
        dialog.setOnCancelListener(this);
        dialog.show();
        MiscUtility.setDialogFontSize(this, dialog);
    }

    void emailData() {
        if (this.mDataString != null) {
            MiscUtility.writeToFile(this, "TeslameterData.csv", this.mDataString, true);
        }
        try {
            MiscUtility.zip(new String[]{getFilesDir() + "/TeslameterData.csv"}, getFilesDir() + "/TeslameterData.zip");
            MiscUtility.sendEmail(this, "mailto:", "Teslameter recorded data", getResources().getString(R.string.IDS_THANKS_FOR_USING), "TeslameterData.zip");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        if (this.mActionView != null) {
            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            openActionView(false);
            start();
            return;
        }
        super.onBackPressed();
    }

    public void onCancel(DialogInterface dialog) {
        start();
    }

    public void onClick(DialogInterface dialog, int which) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        switch (which) {
            case ValueAnimator.INFINITE /*-1*/:
                Arrays.fill(TeslameterGraphView.arrayCoordinateY, 0.0f);
                TeslameterGraphView.count = 0;
                this.mGraphView.invalidate();
                this.mLabelArray[0].setText("NAN");
                for (int i = 0; i < 3; i++) {
                    this.mValueLabel[i].setText("NAN");
                }
                this.mDataString = null;
                this.mSampleCount = 0;
                initDataFiles();
                break;
        }
        start();
    }

    public class Define {
        public static final int ACTION_ID_CLEAR_GRAPH = 1;
        public static final int ACTION_ID_CLOSE = 2;
        public static final int ACTION_ID_SEND_DATA_TO_EMAIL = 0;
        public static final int ACTION_VIEW = 7;
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_START_STOP = 3;
        public static final int BUTTON_UPGRADE = 0;
        public static final int GRAPH_VIEW = 6;
        public static final int LABEL00 = 0;
        public static final int LABEL01 = 1;
        public static final int LED_PRIMARY = 5;
        public static final int TITLE_SWITCH_MODE = 4;
    }
}
