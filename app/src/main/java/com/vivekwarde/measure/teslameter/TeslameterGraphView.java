package com.vivekwarde.measure.teslameter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.nearby.messages.Strategy;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import java.util.Locale;

public class TeslameterGraphView extends View {
    static float[] arrayCoordinateX;
    static float[] arrayCoordinateY;
    static int count;
    static float currValue;
    static float distancePoint;
    static int gridLength;
    static float[] mPointArray;
    static float prevValue;
    static int startX;
    double mLength;
    Paint mPaint;
    Path mPath;
    Rect mTextBounds;
    TextPaint mTextPaint;

    static {
        currValue = 0.0f;
        prevValue = 0.0f;
        arrayCoordinateY = new float[Strategy.TTL_SECONDS_DEFAULT];
        arrayCoordinateX = new float[Strategy.TTL_SECONDS_DEFAULT];
        mPointArray = new float[1200];
        count = 0;
        distancePoint = 0.0f;
        gridLength = 16;
        startX = 16;
    }

    public TeslameterGraphView(Context context) {
        super(context);
        this.mPaint = null;
        this.mTextPaint = null;
        this.mTextBounds = new Rect();
        this.mPath = new Path();
        setBackgroundColor(0);
        this.mPaint = new Paint();
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setAntiAlias(true);
        this.mTextPaint = new TextPaint();
        this.mTextPaint.setSubpixelText(true);
        this.mTextPaint.setTypeface(Typeface.defaultFromStyle(0));
        this.mTextPaint.setFakeBoldText(true);
        this.mTextPaint.setTextAlign(Align.LEFT);
        this.mTextPaint.setAntiAlias(true);
        int applyDimension = (int) TypedValue.applyDimension(1, 10.0f, getResources().getDisplayMetrics());
        startX = applyDimension;
        gridLength = applyDimension;
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != oldw) {
            changeSumPoint();
        }
    }

    void changeSumPoint() {
        distancePoint = (float) ((getWidth() / 15) / (gridLength / 2));
    }

    protected void onDraw(Canvas canvas) {
        int i;
        canvas.drawColor(ContextCompat.getColor(getContext(), R.color.TESLAMETER_GRAPH_BACKGROUND_COLOR));
        int scale = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_TESLAMETER_UNIT_KEY, "0").equalsIgnoreCase("0") ? 10 : 100;
        this.mPaint.setStrokeWidth(TextTrackStyle.DEFAULT_FONT_SCALE);
        this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.TESLAMETER_GRAPH_GRID_COLOR));
        for (float l = (float) startX; l <= ((float) getWidth()); l += (float) gridLength) {
            canvas.drawLine(l, 0.0f, l, (float) getHeight(), this.mPaint);
        }
        this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.TESLAMETER_GRAPH_INDEX_FONT_SIZE));
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf((int) (((double) scale) * Math.ceil((double) (getHeight() / gridLength))));
        String sIndex = String.format(Locale.US, "%d.", objArr);
        this.mTextPaint.getTextBounds(sIndex, 0, sIndex.length(), this.mTextBounds);
        for (i = 0; ((double) i) < Math.ceil((double) (getHeight() / gridLength)); i++) {
            int y = getHeight() - (gridLength * i);
            if (i % 2 == 0) {
                this.mTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.TESLAMETER_GRAPH_INDEX_COLOR));
                sIndex = String.format(Locale.US, "%d", new Object[]{Integer.valueOf(scale * i)});
                this.mPath.reset();
                this.mPath.addRect((float) (getWidth() - this.mTextBounds.width()), (float) y, (float) getWidth(), (float) y, Direction.CW);
                canvas.drawTextOnPath(sIndex, this.mPath, 0.0f, 0.0f, this.mTextPaint);
            }
            this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.TESLAMETER_GRAPH_GRID_COLOR));
            canvas.drawLine(0.0f, (float) y, (float) getWidth(), (float) y, this.mPaint);
        }
        this.mPaint.setStrokeWidth(2.0f);
        this.mPaint.setColor(ContextCompat.getColor(getContext(), R.color.TESLAMETER_GRAPH_LINE_COLOR));
        float prevx = arrayCoordinateX[0] - ((float) this.mTextBounds.width());
        float prevy = ((float) getHeight()) - (arrayCoordinateY[0] * (((float) gridLength) / 10.0f));
        int id = 0;
        for (i = 1; i < count; i++) {
            int id2 = id + 1;
            mPointArray[id] = prevx;
            id = id2 + 1;
            mPointArray[id2] = prevy;
            id2 = id + 1;
            prevx = arrayCoordinateX[i] - ((float) this.mTextBounds.width());
            mPointArray[id] = prevx;
            id = id2 + 1;
            prevy = ((float) getHeight()) - (arrayCoordinateY[i] * (((float) gridLength) / 10.0f));
            mPointArray[id2] = prevy;
        }
        if (id > 0) {
            canvas.drawLines(mPointArray, 0, id, this.mPaint);
        }
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_TESLAMETER_ALERT_ENABLED_KEY, true)) {
            int threshold = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsKey.SETTINGS_TESLAMETER_ALERT_THRESHOLD_KEY, "100"));
            this.mPaint.setStrokeWidth(2.0f);
            int color = ContextCompat.getColor(getContext(), R.color.TESLAMETER_GRAPH_THRESHOLD_COLOR);
            this.mPaint.setColor(color);
            this.mTextPaint.setColor(color);
            this.mTextPaint.setTextSize(getResources().getDimension(R.dimen.TESLAMETER_GRAPH_THRESHOLD_FONT_SIZE));
            int y = (int) (((float) getHeight()) - (((float) threshold) * (((float) gridLength) / 10.0f)));
            canvas.drawLine(0.0f, (float) y, (float) (getWidth() - this.mTextBounds.width()), (float) y, this.mPaint);
            canvas.drawText(getResources().getString(R.string.IDS_ALERT_THRESHOLD), 0.0f, ((float) y) - this.mTextPaint.descent(), this.mTextPaint);
        }
    }
}
