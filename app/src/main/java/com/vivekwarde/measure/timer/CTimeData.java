package com.vivekwarde.measure.timer;

import com.google.android.gms.nearby.messages.Strategy;

public class CTimeData {
    private int day;
    private int hour;
    private int minute;
    private double second;

    private void init(int d, int h, int m, double s) {
        this.day = d;
        this.hour = h;
        this.minute = m;
        this.second = s;
    }

    public CTimeData(int d, int h, int m, double s) {
        this.day = 0;
        this.hour = 0;
        this.minute = 0;
        this.second = 0.0d;
        init(d, h, m, s);
    }

    public static CTimeData createTimeData(int d, int h, int m, double s) {
        return new CTimeData(d, h, m, s);
    }

    double toTimeInterval() {
        return ((double) (((this.day * Strategy.TTL_SECONDS_MAX) + (this.hour * 3600)) + (this.minute * 60))) + this.second;
    }

    static double getTimeInterval(int h, int m, double s) {
        return ((double) ((h * 3600) + (m * 60))) + s;
    }

    static int getSecondsFromTimeInterval(double interval) {
        int hours = (int) (interval / 3600.0d);
        return (int) ((interval - ((double) (hours * 3600))) - ((double) (((int) ((interval / 60.0d) - ((double) (hours * 60)))) * 60)));
    }

    static int getMinutesFromTimeInterval(double interval) {
        return (int) ((interval / 60.0d) - ((double) (((int) (interval / 3600.0d)) * 60)));
    }

    static int getHoursFromTimeInterval(double interval) {
        return (int) (interval / 3600.0d);
    }
}
