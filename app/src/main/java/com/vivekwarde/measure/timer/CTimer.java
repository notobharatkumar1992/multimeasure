package com.vivekwarde.measure.timer;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import com.vivekwarde.measure.utilities.SPTimer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

public class CTimer implements Runnable {
    public static final int TIMER_STATE_PAUSED = 1;
    public static final int TIMER_STATE_RUNNING = 0;
    public static final int TIMER_STATE_STOPPED = 2;
    static final int POLLING_DURATION = 10;
    final String tag;
    boolean mIsCountdown;
    OnTimerEventListener mListener;
    long mPauseTime;
    SPTimer mPollTimer;
    long mStartTime;
    int mState;
    double mTimeInterval;

    public CTimer() {
        this.tag = getClass().getSimpleName();
        this.mListener = null;
        this.mTimeInterval = 0.0d;
        this.mIsCountdown = true;
        this.mStartTime = 0;
        this.mPauseTime = 0;
        this.mState = TIMER_STATE_STOPPED;
        this.mPollTimer = null;
    }

    public void init() {
        this.mIsCountdown = true;
        this.mTimeInterval = 0.0d;
        this.mStartTime = 0;
        this.mPauseTime = 0;
        this.mState = TIMER_STATE_STOPPED;
        this.mPollTimer = new SPTimer(new Handler(), this, POLLING_DURATION, false);
    }

    public void run() {
        pollMethod();
    }

    void pollMethod() {
        if (this.mListener != null && this.mState == 0) {
            double elapsed = elapsedSeconds();
            double remained = this.mTimeInterval - elapsed;
            if (remained < 0.0d) {
                remained = 0.0d;
            }
            if (this.mIsCountdown) {
                this.mListener.onTimerRemained(remained);
            } else {
                this.mListener.onTimerElapsed(elapsed);
            }
            if (remained == 0.0d) {
                this.mState = TIMER_STATE_STOPPED;
                this.mPollTimer.stop();
                this.mListener.onTimerDone(this);
            }
        }
    }

    void start() {
        this.mStartTime = SystemClock.elapsedRealtime();
        this.mState = TIMER_STATE_RUNNING;
        this.mPollTimer.start();
    }

    void pause() {
        this.mState = TIMER_STATE_PAUSED;
        this.mPollTimer.stop();
        this.mPauseTime = SystemClock.elapsedRealtime();
        if (this.mListener != null) {
            double elapsed = lastElapsedSeconds();
            double remained = this.mTimeInterval - elapsed;
            if (remained < 0.0d) {
                remained = 0.0d;
            }
            if (this.mIsCountdown) {
                this.mListener.onTimerRemained(remained);
            } else {
                this.mListener.onTimerElapsed(elapsed);
            }
        }
    }

    void resume() {
        if (this.mState == TIMER_STATE_PAUSED) {
            this.mStartTime += SystemClock.elapsedRealtime() - this.mPauseTime;
        }
        this.mState = TIMER_STATE_RUNNING;
        this.mPollTimer.start();
    }

    void stop() {
        this.mStartTime = 0;
        this.mState = TIMER_STATE_STOPPED;
        this.mPollTimer.stop();
    }

    boolean isRunning() {
        return this.mState == 0;
    }

    boolean isPaused() {
        return this.mState == TIMER_STATE_PAUSED;
    }

    boolean isStopped() {
        return this.mState == TIMER_STATE_STOPPED;
    }

    double elapsedSeconds() {
        return ((double) (SystemClock.elapsedRealtime() - this.mStartTime)) / getTicksPerSecond();
    }

    double lastElapsedSeconds() {
        if (this.mState == TIMER_STATE_PAUSED) {
            return ((double) (this.mPauseTime - this.mStartTime)) / getTicksPerSecond();
        }
        return 0.0d;
    }

    double remainedSeconds() {
        return this.mTimeInterval - (((double) (SystemClock.elapsedRealtime() - this.mStartTime)) / getTicksPerSecond());
    }

    void reset() {
        this.mTimeInterval = 0.0d;
        this.mStartTime = 0;
        this.mPauseTime = 0;
        this.mState = TIMER_STATE_STOPPED;
        this.mPollTimer.stop();
    }

    void preRelease() {
        reset();
        this.mPollTimer.stop();
    }

    double getTicksPerSecond() {
        return 1000.0d;
    }

    void saveLastSessionWithID(Context context, int timerID) {
        String str;
        if (this.mState != TIMER_STATE_PAUSED) {
            try {
            } catch (Exception e) {
                str = this.tag;
                StringBuilder stringBuilder = new StringBuilder();
                Object[] objArr = new Object[TIMER_STATE_PAUSED];
                objArr[TIMER_STATE_RUNNING] = Integer.valueOf(timerID);
                Log.i(str, stringBuilder.append(String.format(Locale.US, "Write timer %d object to file failed!\n", objArr)).append(e).toString());
                return;
            }
        }
        Object[] objArr2 = new Object[TIMER_STATE_PAUSED];
        objArr2[TIMER_STATE_RUNNING] = Integer.valueOf(timerID);
        DataOutputStream file = null;
        try {
            file = new DataOutputStream(context.openFileOutput(String.format(Locale.US, "timer_%d.sav", objArr2), TIMER_STATE_RUNNING));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            file.writeInt(this.mState);
            file.writeLong(this.mStartTime);
            file.writeLong(this.mPauseTime);
            file.writeDouble(this.mTimeInterval);
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        str = this.tag;
        Object[] objArr3 = new Object[TIMER_STATE_PAUSED];
        objArr3[TIMER_STATE_RUNNING] = Integer.valueOf(timerID);
        Log.i(str, String.format(Locale.US, "Write timer %d object to file succeeded!", objArr3));
    }

    boolean loadLastSessionWithID(Context context, int timerID) {
        String str;
        try {
            Object[] objArr = new Object[TIMER_STATE_PAUSED];
            objArr[TIMER_STATE_RUNNING] = Integer.valueOf(timerID);
            DataInputStream file = new DataInputStream(context.openFileInput(String.format(Locale.US, "timer_%d.sav", objArr)));
            this.mState = file.readInt();
            this.mStartTime = file.readLong();
            this.mPauseTime = file.readLong();
            this.mTimeInterval = file.readDouble();
            file.close();
            str = this.tag;
            Object[] objArr2 = new Object[TIMER_STATE_PAUSED];
            objArr2[TIMER_STATE_RUNNING] = Integer.valueOf(timerID);
            Log.i(str, String.format(Locale.US, "Read timer %d object to file succeeded!", objArr2));
            return true;
        } catch (IOException e) {
            str = this.tag;
            StringBuilder stringBuilder = new StringBuilder();
            Object[] objArr3 = new Object[TIMER_STATE_PAUSED];
            objArr3[TIMER_STATE_RUNNING] = Integer.valueOf(timerID);
            Log.i(str, stringBuilder.append(String.format(Locale.US, "Read timer %d object to file failed!\n", objArr3)).append(e).toString());
            return false;
        }
    }

    public void setTimerEventListener(OnTimerEventListener listener) {
        this.mListener = listener;
    }

    int getState() {
        return this.mState;
    }

    void setState(int st) {
        this.mState = st;
    }

    boolean isCountdownMode() {
        return this.mIsCountdown;
    }

    void setCountdownMode(boolean cd) {
        this.mIsCountdown = cd;
    }

    long getStartTime() {
        return this.mStartTime;
    }

    long getPauseTime() {
        return this.mPauseTime;
    }

    void setTimeData(CTimeData timeData) {
        this.mTimeInterval = timeData.toTimeInterval();
    }

    double getTimeInterval() {
        return this.mTimeInterval;
    }

    void setTimeInterval(double interval) {
        this.mTimeInterval = interval;
    }

    public interface OnTimerEventListener {
        void onTimerDone(CTimer cTimer);

        void onTimerElapsed(double d);

        void onTimerRemained(double d);
    }
}
