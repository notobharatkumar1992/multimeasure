package com.vivekwarde.measure.timer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.ValueAnimator;
import com.vivekwarde.measure.MainApplication;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.AutoResizeTextView;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.seismometer.SeismometerGraphView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.MiscUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.Locale;

public class TimerActivity extends BaseActivity implements OnClickListener, DialogInterface.OnClickListener, OnGlobalLayoutListener {
    final String tag;
    SPImageButton mInfoButton;
    SPImageButton mMenuButton;
    SPImageButton mResetButton;
    SPImageButton mStartStopButton;
    TimerCell[] mTimerCells;
    int mTimerCount;
    private SPScale9ImageView mBackgroundView;
    private int mHeaderBarHeight;
    private SPScale9ImageView mLEDScreen;
    private TextView mLabel;
    private AutoResizeTextView[] mTimerNameLabels;

    public TimerActivity() {
        this.tag = getClass().getSimpleName();
        this.mStartStopButton = null;
        this.mResetButton = null;
        this.mInfoButton = null;
        this.mMenuButton = null;
        this.mLEDScreen = null;
        this.mLabel = null;
        this.mTimerCount = 5;
    }

    @SuppressLint({"NewApi"})
    protected void onCreate(Bundle savedInstanceState) {
        super.setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        loadLastSession();
        mScreenHeight = getResources().getDisplayMetrics().heightPixels;
        this.mMainLayout = new RelativeLayout(this);
        this.mMainLayout.setBackgroundColor(-1);
        BitmapDrawable tilebitmap = new BitmapDrawable(getResources(), ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_canvas)).getBitmap());
        tilebitmap.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
        if (VERSION.SDK_INT < 16) {
            this.mMainLayout.setBackgroundDrawable(tilebitmap);
        } else {
            this.mMainLayout.setBackground(tilebitmap);
        }
        setContentView(this.mMainLayout, new LayoutParams(-1, -1));
        initInfo();
        initSubviews();
        MainApplication.getTracker().setScreenName(getClass().getSimpleName());
        MainApplication.getTracker().send(new ScreenViewBuilder().build());
    }

    protected void onDestroy() {
        for (int i = 0; i < this.mTimerCount; i++) {
            this.mTimerCells[i].onDestroy();
        }
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        for (int i = 0; i < this.mTimerCount; i++) {
            this.mTimerCells[i].resumeSoundAlarm();
        }
        displayTimerNames();
    }

    protected void onPause() {
        super.onPause();
        for (int i = 0; i < this.mTimerCount; i++) {
            this.mTimerCells[i].pauseSoundAlarm();
        }
    }

    void loadLastSession() {
    }

    void initInfo() {
    }

    void initSubviews() {
        this.mUiLayout = new RelativeLayout(this);
        LayoutParams uiLayoutParam = new LayoutParams(-1, mScreenHeight);
        uiLayoutParam.addRule(2, BaseActivity.AD_VIEW_ID);
        this.mUiLayout.setLayoutParams(uiLayoutParam);
        this.mMainLayout.addView(this.mUiLayout);
        int hButtonScreenMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTON_AND_SCREEN);
        int vButtonScreenMargin = (int) getResources().getDimension(R.dimen.VERT_MARGIN_BTW_BUTTON_AND_SCREEN);
        int hButtonButtonMargin = (int) getResources().getDimension(R.dimen.HORZ_MARGIN_BTW_BUTTONS);
        int frameMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_VERT_MARGIN_BTW_DOTLEDSCREEN_AND_HEADER);
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_menu)).getBitmap();
        this.mHeaderBarHeight = (bitmap.getHeight() / 2) + (vButtonScreenMargin * 2);
        this.mMenuButton = new SPImageButton(this);
        this.mMenuButton.setId(1);
        this.mMenuButton.setBackgroundBitmap(bitmap);
        this.mMenuButton.setOnClickListener(this);
        LayoutParams params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(11);
        params.height = bitmap.getHeight() / 2;
        params.setMargins(0, vButtonScreenMargin, hButtonScreenMargin, 0);
        this.mMenuButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mMenuButton);
        this.mInfoButton = new SPImageButton(this);
        this.mInfoButton.setId(2);
        this.mInfoButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_info)).getBitmap());
        this.mInfoButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mMenuButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mInfoButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mInfoButton);
        this.mResetButton = new SPImageButton(this);
        this.mResetButton.setId(3);
        this.mResetButton.setBackgroundBitmap(((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.button_timer_reset)).getBitmap());
        this.mResetButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(10);
        params.addRule(0, this.mInfoButton.getId());
        params.setMargins(0, vButtonScreenMargin, hButtonButtonMargin, 0);
        this.mResetButton.setLayoutParams(params);
        this.mUiLayout.addView(this.mResetButton);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.tile_base)).getBitmap();
        this.mBackgroundView = new SPScale9ImageView(this);
        this.mBackgroundView.setId(5);
        this.mBackgroundView.setBitmap(bitmap);
        params = new LayoutParams(-1, -1);
        params.topMargin = this.mHeaderBarHeight;
        this.mBackgroundView.setLayoutParams(params);
        this.mUiLayout.addView(this.mBackgroundView);
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.led_screen)).getBitmap();
        int ledmargin = (this.mHeaderBarHeight - (bitmap.getHeight() - 2)) / 2;
        this.mLEDScreen = new SPScale9ImageView(this);
        this.mLEDScreen.setId(4);
        this.mLEDScreen.setBitmap(bitmap);
        params = new LayoutParams((int) (SeismometerGraphView.MAX_ACCELERATION * ((double) (bitmap.getWidth() - 2))), bitmap.getHeight() - 2);
        params.addRule(9);
        params.addRule(10);
        params.setMargins((int) getResources().getDimension(R.dimen.STOPWATCH_HORZ_MARGIN_BTW_LEDSCREEN_AND_SCREEN), ledmargin, 0, 0);
        this.mLEDScreen.setLayoutParams(params);
        this.mLEDScreen.setOnClickListener(this);
        this.mUiLayout.addView(this.mLEDScreen);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/My-LED-Digital.ttf");
        this.mLabel = new TextView(this);
        this.mLabel.setPaintFlags(this.mLabel.getPaintFlags() | Barcode.ITF);
        boolean bCountdown = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_TIMER_COUNT_DOWN_MODE_KEY, true);
        this.mLabel.setText(getResources().getString(bCountdown ? R.string.IDS_COUNT_DOWN : R.string.IDS_COUNT_UP));
        this.mLabel.setTextColor(ContextCompat.getColor(this, R.color.LED_BLUE));
        this.mLabel.setBackgroundColor(0);
        this.mLabel.setTypeface(font);
        this.mLabel.setTextSize(1, getResources().getDimension(R.dimen.LED_SCREEN_OUTPUT_FONT_SIZE));
        this.mLabel.setGravity(17);
        params = new LayoutParams(-2, -2);
        params.addRule(5, this.mLEDScreen.getId());
        params.addRule(6, this.mLEDScreen.getId());
        params.addRule(7, this.mLEDScreen.getId());
        params.addRule(8, this.mLEDScreen.getId());
        params.setMargins(0, 0, 0, 0);
        this.mLabel.setLayoutParams(params);
        this.mUiLayout.addView(this.mLabel);
        this.mTimerCells = new TimerCell[this.mTimerCount];
        this.mTimerNameLabels = new AutoResizeTextView[this.mTimerCount];
        bitmap = ((BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.timer_cell_background_ninepatch)).getBitmap();
        int[] contentCapSizes = ImageUtility.getContentCapSizes(bitmap);
        int timerNameLabelHeight = (((mScreenHeight - (frameMargin * 2)) - this.mHeaderBarHeight) - (bitmap.getHeight() * 5)) / 6;
        int i = 0;
        while (true) {
            int i2 = this.mTimerCount;
            if (i < i2) {
                this.mTimerCells[i] = new TimerCell(this, i + 1);
                this.mTimerCells[i].setId(i + 100);
                params = new LayoutParams(-2, -2);
                params.addRule(14);
                if (i == 0) {
                    params.addRule(6, this.mBackgroundView.getId());
                    params.setMargins(0, frameMargin, 0, 0);
                } else {
                    params.addRule(3, (i + 100) - 1);
                    params.setMargins(0, frameMargin, 0, 0);
                }
                this.mTimerCells[i].setLayoutParams(params);
                this.mUiLayout.addView(this.mTimerCells[i]);
                this.mTimerNameLabels[i] = new AutoResizeTextView(this);
                this.mTimerNameLabels[i].setId(i + Define.TIMER_NAME_LABEL_BASE_ID);
                params = new LayoutParams(-1, timerNameLabelHeight);
                params.addRule(6, this.mTimerCells[i].getId());
                params.setMargins(contentCapSizes[0], -timerNameLabelHeight, contentCapSizes[2] / 2, 0);
                this.mTimerNameLabels[i].setLayoutParams(params);
                this.mTimerNameLabels[i].setPadding(0, 0, 0, 0);
                this.mTimerNameLabels[i].setTextColor(ContextCompat.getColor(this, R.color.TIMER_NAME_LABEL_COLOR));
                this.mTimerNameLabels[i].setTypeface(Typeface.defaultFromStyle(0), 0);
                this.mTimerNameLabels[i].setTextSize(1, getResources().getDimension(R.dimen.TIMER_NAME_MAX_FONT_SIZE));
                this.mTimerNameLabels[i].setMinTextSize(8.0f);
                this.mTimerNameLabels[i].setGravity(83);
                this.mUiLayout.addView(this.mTimerNameLabels[i]);
                i++;
            } else {
                this.mUiLayout.getViewTreeObserver().addOnGlobalLayoutListener(this);
                return;
            }
        }
    }

    void displayTimerNames() {
        if (this.mTimerNameLabels != null) {
            for (int i = 0; i < this.mTimerCount; i++) {
                this.mTimerNameLabels[i].setText(PreferenceManager.getDefaultSharedPreferences(this).getString(String.format(Locale.US, SettingsKey.SETTINGS_TIMER_NAME_KEY_TEMPLATE, new Object[]{Integer.valueOf(i + 1)}), getResources().getString(R.string.IDS_TIMER) + " " + (i + 1)));
            }
        }
    }

    void onSwitchMode() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        boolean bCountdown = !PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_TIMER_COUNT_DOWN_MODE_KEY, true);
        Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SettingsKey.SETTINGS_TIMER_COUNT_DOWN_MODE_KEY, bCountdown);
        editor.apply();
        this.mLabel.setText(getResources().getString(bCountdown ? R.string.IDS_COUNT_DOWN : R.string.IDS_COUNT_UP));
        for (int i = 0; i < this.mTimerCount; i++) {
            this.mTimerCells[i].switchTimerMode();
        }
    }

    void resetAllTimers() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(4, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        AlertDialog dialog = new Builder(this).create();
        dialog.setMessage(getResources().getString(R.string.IDS_RESET_ALL_TIMERS_ASKING));
        dialog.setButton(-1, getResources().getString(17039379), this);
        dialog.setButton(-2, getResources().getString(17039369), this);
        dialog.setIcon(R.drawable.icon_about);
        dialog.setTitle(getResources().getString(R.string.IDS_CONFIRMATION));
        dialog.show();
        MiscUtility.setDialogFontSize(this, dialog);
    }

    public void onClick(DialogInterface dialog, int which) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        switch (which) {
            case ValueAnimator.INFINITE /*-1*/:
                for (int i = 0; i < this.mTimerCount; i++) {
                    this.mTimerCells[i].reset();
                }
            default:
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case CompletionEvent.STATUS_FAILURE /*1*/:
                onButtonMenu();
            case CompletionEvent.STATUS_CONFLICT /*2*/:
                onButtonSettings();
            case CompletionEvent.STATUS_CANCELED /*3*/:
                resetAllTimers();
            case Barcode.PHONE /*4*/:
                onSwitchMode();
            default:
                Log.i(this.tag, "Unknown action id :(");
        }
    }

    public void onBackPressed() {
        int i = 0;
        while (i < this.mTimerCount) {
            if (!this.mTimerCells[i].closeDurationPicker()) {
                i++;
            } else if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
                SoundUtility.getInstance().playSound(5, TextTrackStyle.DEFAULT_FONT_SCALE);
                return;
            } else {
                return;
            }
        }
        super.onBackPressed();
    }

    @SuppressLint({"NewApi"})
    public void onGlobalLayout() {
        if (VERSION.SDK_INT >= 16) {
            this.mUiLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        } else {
            this.mUiLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        int frameMargin = (int) getResources().getDimension(R.dimen.STOPWATCH_VERT_MARGIN_BTW_DOTLEDSCREEN_AND_HEADER);
        int space = (this.mUiLayout.getHeight() - (frameMargin * 2)) - this.mHeaderBarHeight;
        for (int i = 0; i < this.mTimerCount; i++) {
            RelativeLayout cell = this.mTimerCells[i];
            LayoutParams params = (LayoutParams) cell.getLayoutParams();
            if (i == 0) {
                space = (space - (cell.getHeight() * 5)) / 6;
                params.setMargins(0, frameMargin + space, 0, 0);
            } else {
                params.setMargins(0, space, 0, 0);
            }
            cell.setLayoutParams(params);
            params = (LayoutParams) this.mTimerNameLabels[i].getLayoutParams();
            params.topMargin = -space;
            params.height = space;
            this.mTimerNameLabels[i].setLayoutParams(params);
            this.mTimerCells[i].relayoutDurationPicker();
        }
    }

    public class Define {
        public static final int BUTTON_INFO = 2;
        public static final int BUTTON_MENU = 1;
        public static final int BUTTON_RESET = 3;
        public static final int DURATION_PICKER = 6;
        public static final int TIMER_CELL_BASE_ID = 100;
        public static final int TIMER_NAME_LABEL_BASE_ID = 110;
        public static final int TITLE_SWITCH_MODE = 4;
        public static final int VIEW_BACKGROUND = 5;
    }
}
