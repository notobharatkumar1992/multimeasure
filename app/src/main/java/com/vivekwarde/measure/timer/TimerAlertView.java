package com.vivekwarde.measure.timer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.vision.barcode.Barcode;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.AutoResizeTextView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.utilities.ImageUtility;
import java.util.Locale;

public class TimerAlertView extends RelativeLayout implements AnimatorListener, OnTouchListener {
    int[] m9PatchCapSizes;
    Bitmap m9PatchFrameBitmap;
    int mActiveIndex;
    AnimatorSet mCloseAnimator;
    int[] mContentCapSizes;
    TextView mDismissLabel;
    Bitmap mFrameBitmap;
    boolean mIsOpened;
    OnTimerAlertListener mListener;
    AutoResizeTextView mMessageLabel;
    AnimatorSet mOpenAnimator;

    public class Define {
        public static final int DISMISS_TEXTVIEW_ID = 2;
        public static final int MESSAGE_TEXTVIEW_ID = 1;
    }

    public interface OnTimerAlertListener {
        void onTimerAlertDismiss();
    }

    @Deprecated
    public TimerAlertView(Context context) {
        super(context);
        this.mActiveIndex = 0;
        this.mMessageLabel = null;
        this.mDismissLabel = null;
        this.mIsOpened = false;
        this.mListener = null;
        this.m9PatchFrameBitmap = null;
        this.mFrameBitmap = null;
        this.m9PatchCapSizes = null;
        this.mContentCapSizes = null;
        this.mOpenAnimator = null;
        this.mCloseAnimator = null;
    }

    public TimerAlertView(Context context, int timerID) {
        super(context);
        this.mActiveIndex = 0;
        this.mMessageLabel = null;
        this.mDismissLabel = null;
        this.mIsOpened = false;
        this.mListener = null;
        this.m9PatchFrameBitmap = null;
        this.mFrameBitmap = null;
        this.m9PatchCapSizes = null;
        this.mContentCapSizes = null;
        this.mOpenAnimator = null;
        this.mCloseAnimator = null;
        setBackgroundColor(0);
        this.mActiveIndex = timerID;
        initImages();
        initSubviews();
        setPadding(0, this.mContentCapSizes[1], 0, this.mContentCapSizes[3]);
        setOnTouchListener(this);
        ViewHelper.setScaleX(this, 0.0f);
        ViewHelper.setScaleY(this, 0.0f);
    }

    int getTopCapSize() {
        if (this.mContentCapSizes != null) {
            return this.mContentCapSizes[1];
        }
        return 0;
    }

    int getBottomCapSize() {
        if (this.mContentCapSizes != null) {
            return this.mContentCapSizes[2] / 2;
        }
        return 0;
    }

    void initImages() {
        this.m9PatchFrameBitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.timer_alert_frame_ninepatch)).getBitmap();
        this.m9PatchCapSizes = ImageUtility.getNinePatchCapSizes(this.m9PatchFrameBitmap);
        this.mContentCapSizes = ImageUtility.getContentCapSizes(this.m9PatchFrameBitmap);
        this.m9PatchFrameBitmap = Bitmap.createBitmap(this.m9PatchFrameBitmap, 1, 1, this.m9PatchFrameBitmap.getWidth() - 2, this.m9PatchFrameBitmap.getHeight() - 2);
        if (this.mActiveIndex % 2 == 0) {
            this.m9PatchFrameBitmap = ImageUtility.flipHorizontal(this.m9PatchFrameBitmap);
            int temp = this.mContentCapSizes[0];
            this.mContentCapSizes[0] = this.mContentCapSizes[2];
            this.mContentCapSizes[2] = temp;
            temp = this.m9PatchCapSizes[0];
            this.m9PatchCapSizes[0] = this.m9PatchCapSizes[2];
            this.m9PatchCapSizes[2] = temp;
        }
    }

    void initSubviews() {
        String sDismiss = getResources().getString(R.string.IDS_TAP_TO_DISMISS);
        this.mDismissLabel = new TextView(getContext());
        this.mDismissLabel.setId(2);
        this.mDismissLabel.setPaintFlags(this.mDismissLabel.getPaintFlags() | Barcode.ITF);
        LayoutParams params = new LayoutParams(-1, -2);
        params.addRule(12);
        params.addRule(14);
        params.setMargins(this.mContentCapSizes[0], 0, this.mContentCapSizes[2], 0);
        this.mDismissLabel.setLayoutParams(params);
        this.mDismissLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.TIMER_ALERT_DISMISS_COLOR));
        this.mDismissLabel.setTypeface(Typeface.defaultFromStyle(2), 2);
        this.mDismissLabel.setTextSize(1, getResources().getDimension(R.dimen.TIMER_ALERT_DISMISS_FONT_SIZE));
        this.mDismissLabel.setText(sDismiss);
        this.mDismissLabel.setGravity(81);
        addView(this.mDismissLabel);
        String sMsg = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(String.format(Locale.US, SettingsKey.SETTINGS_TIMER_MESSAGE_KEY_TEMPLATE, new Object[]{Integer.valueOf(this.mActiveIndex)}), getResources().getString(R.string.IDS_TIMER_DONE));
        this.mMessageLabel = new AutoResizeTextView(getContext());
        this.mMessageLabel.setId(1);
        this.mMessageLabel.setPaintFlags(this.mMessageLabel.getPaintFlags() | Barcode.ITF);
        params = new LayoutParams(-1, -2);
        params.addRule(10);
        params.addRule(2, this.mDismissLabel.getId());
        params.addRule(14);
        params.setMargins(this.mContentCapSizes[0], 0, this.mContentCapSizes[2], 0);
        this.mMessageLabel.setLayoutParams(params);
        this.mMessageLabel.setText(sMsg);
        this.mMessageLabel.setTextColor(ContextCompat.getColor(getContext(), R.color.TIMER_ALERT_MESSAGE_COLOR));
        this.mMessageLabel.setTypeface(Typeface.defaultFromStyle(0), 0);
        this.mMessageLabel.setTextSize(1, getResources().getDimension(R.dimen.TIMER_ALERT_MESSAGE_FONT_SIZE));
        this.mMessageLabel.setMinTextSize(TextTrackStyle.DEFAULT_FONT_SCALE);
        this.mMessageLabel.setGravity(17);
        addView(this.mMessageLabel);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != oldw || h != oldh) {
            this.mFrameBitmap = ImageUtility.scale9Bitmap(this.m9PatchFrameBitmap, this.m9PatchCapSizes[0], this.m9PatchCapSizes[2], this.m9PatchCapSizes[1], this.m9PatchCapSizes[3], w, h);
        }
    }

    public void setOnTimerAlertListener(OnTimerAlertListener listener) {
        this.mListener = listener;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mFrameBitmap != null) {
            canvas.drawBitmap(this.mFrameBitmap, 0.0f, 0.0f, null);
        }
    }

    void showMe() {
        ObjectAnimator a1 = ObjectAnimator.ofFloat((Object) this, "scaleX", TextTrackStyle.DEFAULT_FONT_SCALE);
        ObjectAnimator a2 = ObjectAnimator.ofFloat((Object) this, "scaleY", TextTrackStyle.DEFAULT_FONT_SCALE);
        this.mOpenAnimator = new AnimatorSet();
        this.mOpenAnimator.addListener(this);
        this.mOpenAnimator.setDuration(300);
        this.mOpenAnimator.setStartDelay(0);
        this.mOpenAnimator.playTogether(a1, a2);
        this.mOpenAnimator.start();
    }

    void onDismiss() {
        ObjectAnimator a1 = ObjectAnimator.ofFloat((Object) this, "scaleX", 0.0f);
        ObjectAnimator a2 = ObjectAnimator.ofFloat((Object) this, "scaleY", 0.0f);
        this.mCloseAnimator = new AnimatorSet();
        this.mCloseAnimator.addListener(this);
        this.mCloseAnimator.setDuration(300);
        this.mCloseAnimator.setStartDelay(0);
        this.mCloseAnimator.playTogether(a1, a2);
        this.mCloseAnimator.start();
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() != 0 || !this.mIsOpened) {
            return false;
        }
        onDismiss();
        return true;
    }

    public void onAnimationCancel(Animator animator) {
        onAnimationEnd(animator);
    }

    public void onAnimationEnd(Animator animator) {
        if (animator.equals(this.mCloseAnimator)) {
            this.mIsOpened = false;
            if (this.mListener != null) {
                this.mListener.onTimerAlertDismiss();
            }
            this.mCloseAnimator = null;
        } else if (animator.equals(this.mOpenAnimator)) {
            this.mIsOpened = true;
            this.mOpenAnimator = null;
        }
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationStart(Animator animator) {
    }
}
