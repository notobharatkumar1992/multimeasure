package com.vivekwarde.measure.timer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;

import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.drive.events.CompletionEvent;
import com.vivekwarde.measure.BuildConfig;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.SPImageButton;
import com.vivekwarde.measure.custom_controls.SPScale9ImageView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import com.vivekwarde.measure.timer.CTimer.OnTimerEventListener;
import com.vivekwarde.measure.timer.TimerAlertView.OnTimerAlertListener;
import com.vivekwarde.measure.timer.TimerDurationPicker.OnDurationPickerEventListener;
import com.vivekwarde.measure.utilities.ImageUtility;
import com.vivekwarde.measure.utilities.SoundUtility;

import java.util.Locale;
import java.util.Timer;

public class TimerCell extends RelativeLayout implements OnTimerEventListener, OnClickListener, OnTimerAlertListener, OnTouchListener, OnDurationPickerEventListener {
    final String tag;
    MediaPlayer mAlarmPlayer;
    TimerAlertView mAlertView;
    Point mDownPoint;
    TimerDurationPicker mDurationPicker;
    int mEditUnit;
    boolean mIsMoved;
    TimerLEDView mLEDView;
    String mLoadedFile;
    SPImageButton mStartStopButton;
    int mTag;
    CTimer mTimer;
    Timer mTimerSlide;
    private SPScale9ImageView mBackgroundView;
    private double mOriginInterval;
    private boolean mSlideDisabled;

    @Deprecated
    public TimerCell(Context context) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mIsMoved = false;
        this.mLEDView = null;
        this.mStartStopButton = null;
        this.mTimer = null;
        this.mDownPoint = null;
        this.mTimerSlide = null;
        this.mSlideDisabled = false;
        this.mOriginInterval = 0.0d;
        this.mDurationPicker = null;
        this.mEditUnit = -1;
        this.mAlertView = null;
    }

    public TimerCell(Context context, int timerTag) {
        super(context);
        this.tag = getClass().getSimpleName();
        this.mIsMoved = false;
        this.mLEDView = null;
        this.mStartStopButton = null;
        this.mTimer = null;
        this.mDownPoint = null;
        this.mTimerSlide = null;
        this.mSlideDisabled = false;
        this.mOriginInterval = 0.0d;
        this.mDurationPicker = null;
        this.mEditUnit = -1;
        this.mAlertView = null;
        setBackgroundColor(0);
        setOnTouchListener(this);
        this.mTag = timerTag;
        this.mAlarmPlayer = new MediaPlayer();
        this.mLoadedFile = BuildConfig.FLAVOR;
        this.mTimer = new CTimer();
        this.mTimer.init();
        this.mTimer.setTimerEventListener(this);
        boolean loaded = false;
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_TIMER_RETAIN_ENABLED_KEY, true)) {
            loaded = this.mTimer.loadLastSessionWithID(getContext(), this.mTag);
        }
        if (!loaded) {
            this.mTimer.setTimeInterval((double) getDefaultDuration());
        }
        initSubviews();
        updateLED();
        if (this.mTimer.isRunning()) {
            this.mTimer.resume();
        }
    }

    void initSubviews() {
        Bitmap bitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.timer_cell_background_ninepatch)).getBitmap();
        int[] contentCapSizes = ImageUtility.getContentCapSizes(bitmap);
        int cellMargin = (int) getResources().getDimension(R.dimen.TIMER_CELL_HMARGIN);
        this.mBackgroundView = new SPScale9ImageView(getContext());
        this.mBackgroundView.setId(3);
        this.mBackgroundView.setBitmap(bitmap);
        LayoutParams params = new LayoutParams(-1, bitmap.getHeight() - 2);
        params.addRule(15);
        params.setMargins(cellMargin, 0, cellMargin, 0);
        this.mBackgroundView.setLayoutParams(params);
        addView(this.mBackgroundView);
        this.mStartStopButton = new SPImageButton(getContext());
        this.mStartStopButton.setId(1);
        this.mStartStopButton.setOnClickListener(this);
        params = new LayoutParams(-2, -2);
        params.addRule(15);
        params.addRule(7, this.mBackgroundView.getId());
        params.setMargins(0, 0, 0, 0);
        this.mStartStopButton.setLayoutParams(params);
        addView(this.mStartStopButton);
        updateStartStopButtonState();
        this.mLEDView = new TimerLEDView(getContext());
        this.mLEDView.setId(2);
        params = new LayoutParams(-2, -2);
        params.addRule(15);
        params.addRule(9);
        params.addRule(11);
        params.setMargins(contentCapSizes[0], 0, contentCapSizes[2] - cellMargin, 0);
        this.mLEDView.setLayoutParams(params);
        addView(this.mLEDView);
    }

    void updateLED() {
        if (this.mTimer.isCountdownMode()) {
            this.mLEDView.setTime(this.mTimer.getTimeInterval() - this.mTimer.lastElapsedSeconds());
        } else {
            this.mLEDView.setTime(this.mTimer.lastElapsedSeconds());
        }
    }

    void updateStartStopButtonState() {
        int id;
        if (this.mTimer.getState() == 0) {
            id = R.drawable.timer_button_pause;
        } else if (this.mTimer.isCountdownMode()) {
            id = R.drawable.timer_button_countdown;
        } else {
            id = R.drawable.timer_button_countup;
        }
        this.mStartStopButton.setBackgroundBitmap(((BitmapDrawable) getResources().getDrawable(id)).getBitmap());
    }

    void onStartStop() {
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(SettingsKey.SETTINGS_GENERAL_SOUNDFX_KEY, true)) {
            SoundUtility.getInstance().playSound(1, TextTrackStyle.DEFAULT_FONT_SCALE);
        }
        if (this.mTimer.getTimeInterval() > 0.0d) {
            if (this.mTimer.isRunning()) {
                this.mTimer.pause();
            } else if (this.mTimer.isPaused()) {
                this.mTimer.resume();
            } else if (this.mTimer.isStopped()) {
                this.mTimer.start();
            }
            updateStartStopButtonState();
        }
    }

    void reset() {
        this.mTimer.reset();
        this.mTimer.setTimeInterval((double) getDefaultDuration());
        this.mLEDView.setTime(this.mTimer.getTimeInterval());
        updateStartStopButtonState();
    }

    int getDefaultDuration() {
        int duration = this.mTag * 5;
        try {
            duration = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(String.format(Locale.US, SettingsKey.SETTINGS_TIMER_DEFAULT_DURATION_KEY_TEMPLATE, new Object[]{Integer.valueOf(this.mTag)}), String.valueOf(this.mTag * 5)));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return duration;
    }

    void switchTimerMode() {
        this.mTimer.setCountdownMode(!this.mTimer.isCountdownMode());
        updateLED();
        updateStartStopButtonState();
    }

    void showAlertWithIndex(int timerID) {
        if (this.mAlertView == null) {
            this.mAlertView = new TimerAlertView(getContext(), timerID);
            LayoutParams params = new LayoutParams(-1, (getHeight() + this.mAlertView.getTopCapSize()) + this.mAlertView.getBottomCapSize());
            params.addRule(5, getId());
            params.addRule(6, getId());
            params.setMargins(0, -this.mAlertView.getTopCapSize(), 0, 0);
            this.mAlertView.setLayoutParams(params);
            this.mAlertView.setOnTimerAlertListener(this);
            ((RelativeLayout) getParent()).addView(this.mAlertView);
            this.mAlertView.showMe();
        }
    }

    void onDestroy() {
        stopSoundAlarm();
        this.mTimer.saveLastSessionWithID(getContext(), this.mTag);
        if (this.mTimer.isRunning()) {
            this.mTimer.stop();
        }
    }

    public void onTimerRemained(double remained) {
        this.mLEDView.setTime(remained);
    }

    public void onTimerElapsed(double elapsed) {
        this.mLEDView.setTime(elapsed);
    }

    public void onTimerDone(CTimer timer) {
        this.mLEDView.setTime(timer.getTimeInterval());
        String sKeySound = String.format(Locale.US, SettingsKey.SETTINGS_TIMER_ALARM_SOUND_KEY_TEMPLATE, new Object[]{Integer.valueOf(this.mTag)});
        String sKeyLooping = String.format(Locale.US, SettingsKey.SETTINGS_TIMER_ALARM_SOUND_LOOP_KEY_TEMPLATE, new Object[]{Integer.valueOf(this.mTag)});
        playSoundAlarm(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(sKeySound, String.format(Locale.US, "Timer alarm %d", new Object[]{Integer.valueOf(this.mTag)})) + ".mp3", PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(sKeyLooping, true));
        if (PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(String.format(Locale.US, SettingsKey.SETTINGS_TIMER_REPEAT_ENABLED_KEY_TEMPLATE, new Object[]{Integer.valueOf(this.mTag)}), false)) {
            timer.start();
        }
        updateStartStopButtonState();
        showAlertWithIndex(this.mTag);
    }

    void playSoundAlarm(String soundFile, boolean looping) {
        try {
            AssetFileDescriptor descriptor = getContext().getAssets().openFd("sounds/timer/" + soundFile);
            if (this.mLoadedFile.length() == 0) {
                this.mAlarmPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                this.mAlarmPlayer.prepare();
                this.mAlarmPlayer.setLooping(looping);
                this.mAlarmPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
            } else if (this.mLoadedFile != soundFile) {
                stopSoundAlarm();
                this.mAlarmPlayer.reset();
                this.mAlarmPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                this.mAlarmPlayer.prepare();
                this.mAlarmPlayer.setLooping(looping);
                this.mAlarmPlayer.setVolume(TextTrackStyle.DEFAULT_FONT_SCALE, TextTrackStyle.DEFAULT_FONT_SCALE);
            }
            descriptor.close();
            if (!this.mAlarmPlayer.isPlaying()) {
                this.mAlarmPlayer.start();
            }
            this.mLoadedFile = soundFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void pauseSoundAlarm() {
        if (this.mLoadedFile.length() != 0 && this.mAlarmPlayer.isPlaying()) {
            this.mAlarmPlayer.pause();
        }
    }

    void resumeSoundAlarm() {
        if (this.mLoadedFile.length() != 0 && !this.mAlarmPlayer.isPlaying()) {
            this.mAlarmPlayer.start();
        }
    }

    void stopSoundAlarm() {
        if (this.mLoadedFile.length() != 0 && this.mAlarmPlayer.isPlaying()) {
            this.mAlarmPlayer.stop();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case CompletionEvent.STATUS_FAILURE /*1*/:
                onStartStop();
            default:
        }
    }

    public void onTimerAlertDismiss() {
        stopSoundAlarm();
        ((RelativeLayout) getParent()).removeView(this.mAlertView);
        this.mAlertView = null;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            onTouchDown(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 1) {
            onTouchUp(v, new Point((int) event.getX(), (int) event.getY()));
        } else if (event.getAction() == 2) {
            onTouchMove(v, new Point((int) event.getX(), (int) event.getY()));
        }
        return true;
    }

    protected void onTouchDown(View source, Point currentPosition) {
        if (!this.mTimer.isRunning()) {
            this.mIsMoved = false;
            this.mSlideDisabled = false;
            this.mDownPoint = currentPosition;
            this.mOriginInterval = this.mTimer.getTimeInterval();
            this.mEditUnit = -1;
            for (int i = 0; i < 3; i++) {
                if (this.mDownPoint.x < this.mLEDView.getRightOfUnit(i + 0) + this.mLEDView.getLeft()) {
                    this.mEditUnit = i + 0;
                    break;
                }
            }
            postDelayed(new Runnable() {
                public void run() {
                    TimerCell.this.disableSlide();
                }
            }, 100);
        }
    }

    void disableSlide() {
        this.mSlideDisabled = true;
    }

    protected void onTouchUp(View source, Point currentPosition) {
        Point pt = currentPosition;
        if (!this.mTimer.isRunning()) {
            this.mLEDView.stopAnimation(this.mEditUnit);
            if (this.mIsMoved) {
                if (Math.abs(pt.x - this.mDownPoint.x) >= getWidth() / 3 && Math.atan2((double) Math.abs(pt.y - this.mDownPoint.y), (double) Math.abs(pt.x - this.mDownPoint.x)) <= 0.5235987755982988d) {
                    reset();
                } else if (!this.mSlideDisabled) {
                }
            } else if (this.mDurationPicker == null) {
                openDurationPicker();
            }
        }
    }

    void openDurationPicker() {
        int[] location = new int[]{0, 0};
        getLocationInWindow(location);
        this.mDurationPicker = new TimerDurationPicker(getContext(), this.mTag);
        this.mDurationPicker.setId(6);
        this.mDurationPicker.construct(new Point(location[0] + (getWidth() / 3), location[1] + (getHeight() / 2)));
        this.mDurationPicker.setLayoutParams(new LayoutParams(-1, -1));
        this.mDurationPicker.setOnDurationPickerEventListener(this);
        ((RelativeLayout) getParent().getParent()).addView(this.mDurationPicker);
    }

    boolean closeDurationPicker() {
        if (this.mDurationPicker == null) {
            return false;
        }
        ((RelativeLayout) getParent().getParent()).removeView(this.mDurationPicker);
        this.mDurationPicker = null;
        return true;
    }

    protected void onTouchMove(View source, Point currentPosition) {
        Point pt = currentPosition;
        if (!this.mTimer.isRunning()) {
            if (!this.mIsMoved) {
                if (Math.sqrt((double) (((this.mDownPoint.x - pt.x) * (this.mDownPoint.x - pt.x)) + ((this.mDownPoint.y - pt.y) * (this.mDownPoint.y - pt.y)))) > ((double) ViewConfiguration.get(getContext()).getScaledTouchSlop())) {
                    this.mIsMoved = true;
                } else {
                    return;
                }
            }
            this.mIsMoved = true;
            if (this.mEditUnit != -1 && Math.atan2((double) Math.abs(pt.x - this.mDownPoint.x), (double) Math.abs(pt.y - this.mDownPoint.y)) <= 0.7853981633974483d) {
                this.mLEDView.startAnimate(this.mEditUnit);
                onMovePoint(pt);
            }
        }
    }

    void onMovePoint(Point pt) {
        double interval;
        int delta = Math.abs(pt.y - this.mDownPoint.y) / 10;
        if (pt.y < this.mDownPoint.y) {
            interval = addTimeDuration(delta, this.mEditUnit, this.mOriginInterval);
        } else {
            interval = subTimeDuration(delta, this.mEditUnit, this.mOriginInterval);
        }
        this.mTimer.reset();
        this.mTimer.setTimeInterval(interval);
        this.mLEDView.setTime(interval);
    }

    double addTimeDuration(int duration, int unit, double interval) {
        int hours = (int) ((interval / 60.0d) / 60.0d);
        int mins = (int) ((interval / 60.0d) - ((double) (hours * 60)));
        int secs = (int) ((interval - ((double) ((hours * 60) * 60))) - ((double) (mins * 60)));
        if (unit == 0) {
            hours += duration;
            if (hours > 99) {
                hours = (hours - 99) - 1;
            }
        } else if (unit == 1) {
            mins += duration;
            if (mins > 59) {
                mins = (mins - 59) - 1;
            }
        } else if (unit == 2) {
            secs += duration;
            if (secs > 59) {
                secs = (secs - 59) - 1;
            }
        }
        return CTimeData.getTimeInterval(hours, mins, (double) secs);
    }

    double addTimeDuration(int duration, int unit) {
        return addTimeDuration(duration, unit, this.mTimer.getTimeInterval());
    }

    double subTimeDuration(int duration, int unit, double interval) {
        int hours = (int) ((interval / 60.0d) / 60.0d);
        int mins = (int) ((interval / 60.0d) - ((double) (hours * 60)));
        int secs = (int) ((interval - ((double) ((hours * 60) * 60))) - ((double) (mins * 60)));
        if (unit == 0) {
            hours -= duration;
            if (hours < 0) {
                hours = (hours + 99) + 1;
            }
        } else if (unit == 1) {
            mins -= duration;
            if (mins < 0) {
                mins = (mins + 59) + 1;
            }
        } else if (unit == 2) {
            secs -= duration;
            if (secs < 0) {
                secs = (secs + 59) + 1;
            }
        }
        return CTimeData.getTimeInterval(hours, mins, (double) secs);
    }

    double subTimeDuration(int duration, int unit) {
        return subTimeDuration(duration, unit, this.mTimer.getTimeInterval());
    }

    public void onDurationSelected(boolean selected) {
        if (selected) {
            reset();
        }
        closeDurationPicker();
    }

    public void relayoutDurationPicker() {
        getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @SuppressLint({"NewApi"})
            public void onGlobalLayout() {
                if (VERSION.SDK_INT >= 16) {
                    TimerCell.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    TimerCell.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                if (TimerCell.this.mDurationPicker != null) {
                    int[] location = new int[]{0, 0};
                    TimerCell.this.getLocationInWindow(location);
                    TimerCell.this.mDurationPicker.relayoutBorder(new Point(location[0] + (TimerCell.this.getWidth() / 3), location[1] + (TimerCell.this.getHeight() / 2)));
                }
            }
        });
    }

    public class Define {
        public static final int BACKGROUND_VIEW = 3;
        public static final int BUTTON_START_STOP = 1;
        public static final int LED_VIEW = 2;
    }

    public class TimeUnit {
        public static final int TIMER_UNIT_HOUR = 0;
        public static final int TIMER_UNIT_MINUTE = 1;
        public static final int TIMER_UNIT_NONE = -1;
        public static final int TIMER_UNIT_SECOND = 2;
    }
}
