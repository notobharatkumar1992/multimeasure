package com.vivekwarde.measure.timer;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.BaseActivity;
import com.vivekwarde.measure.custom_controls.BorderView;
import com.vivekwarde.measure.settings.SettingsActivity.SettingsKey;
import java.util.ArrayList;
import java.util.Locale;

public class TimerDurationPicker extends RelativeLayout implements OnItemClickListener, OnTouchListener {
    Point mArrowPosition;
    protected BorderView mBorderView;
    private TimerDurationDataAdapter mListDataAdapter;
    private ListView mListView;
    OnDurationPickerEventListener mListener;
    int mSelectedIndex;
    int mTimerId;
    int mWidth;

    public interface OnDurationPickerEventListener {
        void onDurationSelected(boolean z);
    }

    public class TimerDurationDataAdapter extends BaseAdapter {
        private int mColor;
        private Context mContext;
        private ArrayList<String> mData;
        private Typeface mFont;
        private float mFontSize;
        private int mRowHeight;
        private int mSelectionPosition;
        private ArrayList<Integer> mValueArray;

        public TimerDurationDataAdapter(Context context) {
            this.mData = null;
            this.mValueArray = null;
            this.mRowHeight = 0;
            this.mFont = null;
            this.mFontSize = 0.0f;
            this.mColor = -1;
            this.mContext = null;
            this.mSelectionPosition = -1;
            this.mContext = context;
            initData();
            this.mFont = Typeface.defaultFromStyle(0);
            this.mFontSize = this.mContext.getResources().getDimension(R.dimen.TIMER_DURATION_PICKER_FONT_SIZE);
            this.mColor = ContextCompat.getColor(this.mContext, R.color.LED_BLUE);
            this.mRowHeight = (int) this.mContext.getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_ROW_HEIGHT);
        }

        void initData() {
            String[] durationNamesFormat = TimerDurationPicker.this.getResources().getStringArray(R.array.timer_duration_names);
            this.mData = new ArrayList();
            for (int i = 0; i < durationNamesFormat.length; i++) {
                if (i < 6) {
                    this.mData.add(String.format(durationNamesFormat[i], new Object[]{this.mContext.getString(R.string.IDS_SECONDS)}));
                } else if (i < 7) {
                    this.mData.add(String.format(durationNamesFormat[i], new Object[]{this.mContext.getString(R.string.IDS_MINUTE)}));
                } else if (i < 12) {
                    this.mData.add(String.format(durationNamesFormat[i], new Object[]{this.mContext.getString(R.string.IDS_MINUTES)}));
                } else if (i < 13) {
                    this.mData.add(String.format(durationNamesFormat[i], new Object[]{this.mContext.getString(R.string.IDS_HOUR)}));
                } else {
                    this.mData.add(String.format(durationNamesFormat[i], new Object[]{this.mContext.getString(R.string.IDS_HOURS)}));
                }
            }
            String[] stringValueArray = TimerDurationPicker.this.getResources().getStringArray(R.array.timer_duration_values);
            this.mValueArray = new ArrayList();
            for (String valueOf : stringValueArray) {
                this.mValueArray.add(Integer.valueOf(valueOf));
            }
        }

        public int indexOf(int duration) {
            return this.mValueArray.indexOf(Integer.valueOf(duration));
        }

        public int getRowHeight() {
            return this.mRowHeight;
        }

        public int getCount() {
            return this.mData.size();
        }

        public String getItem(int position) {
            return (String) this.mData.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public int setSelectionDuration(int duration) {
            return setSelection(indexOf(duration));
        }

        public int setSelection(int position) {
            if (position == this.mSelectionPosition) {
                this.mSelectionPosition = -1;
            } else {
                this.mSelectionPosition = position;
            }
            notifyDataSetChanged();
            return this.mSelectionPosition;
        }

        public int getSelectionDuration() {
            if (this.mSelectionPosition < 0 || this.mSelectionPosition >= this.mValueArray.size()) {
                return TimerDurationPicker.this.mTimerId * 5;
            }
            return ((Integer) this.mValueArray.get(this.mSelectionPosition)).intValue();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RelativeLayout listLayout = (RelativeLayout) convertView;
            if (listLayout == null) {
                listLayout = new RelativeLayout(this.mContext);
                listLayout.setLayoutParams(new LayoutParams(-1, this.mRowHeight));
                TextView tv = new TextView(this.mContext);
                int id = 1 + 1;
                tv.setId(1);
                tv.setTypeface(this.mFont);
                tv.setTextSize(1, this.mFontSize);
                tv.setTextColor(-1);
                tv.setLayoutParams(new RelativeLayout.LayoutParams(-2, -1));
                tv.setGravity(19);
                listLayout.addView(tv);
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.tv = tv;
                listLayout.setTag(viewHolder);
            }
            listLayout.setBackgroundColor(position == this.mSelectionPosition ? this.mColor : 0);
            ((ViewHolder) listLayout.getTag()).tv.setText(getItem(position));
            return listLayout;
        }
    }

    static class ViewHolder {
        public TextView tv;

        ViewHolder() {
        }
    }

    public void setOnDurationPickerEventListener(OnDurationPickerEventListener listener) {
        this.mListener = listener;
    }

    @Deprecated
    public TimerDurationPicker(Context context) {
        super(context);
        this.mListener = null;
        this.mArrowPosition = null;
        this.mBorderView = null;
        this.mWidth = 0;
        this.mSelectedIndex = 0;
    }

    public TimerDurationPicker(Context context, int timerId) {
        super(context);
        this.mListener = null;
        this.mArrowPosition = null;
        this.mBorderView = null;
        this.mWidth = 0;
        this.mSelectedIndex = 0;
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.DIALOG_BACKGROUND_COLOR));
        setOnTouchListener(this);
        this.mTimerId = timerId;
        this.mWidth = getResources().getDisplayMetrics().widthPixels;
    }

    public void construct(Point point) {
        this.mArrowPosition = point;
        initSubviews();
    }

    protected void initSubviews() {
        int rowHeight = (int) getResources().getDimension(R.dimen.METRONOME_LOAD_PRESET_ROW_HEIGHT);
        int margin = (int) getResources().getDimension(R.dimen.ACTION_VIEW_MARGIN);
        this.mBorderView = new BorderView(getContext());
        this.mBorderView.construct(1, (double) (((float) (this.mTimerId - 1)) * 0.25f));
        this.mBorderView.setId(BaseActivity.AD_VIEW_ID);
        int h = (rowHeight * 8) + (this.mBorderView.getCornerCapSize() * 2);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(this.mWidth / 2, h);
        params.addRule(9);
        params.addRule(10);
        params.setMargins(this.mArrowPosition.x, (this.mArrowPosition.y - this.mBorderView.getCornerCapSize()) - (this.mBorderView.getArrowCapSize() / 2), 0, 0);
        params.topMargin = (int) (((double) ((this.mArrowPosition.y - this.mBorderView.getCornerCapSize()) - (this.mBorderView.getArrowCapSize() / 2))) - (this.mBorderView.getAnchorPos() * ((double) ((h - (this.mBorderView.getCornerCapSize() * 2)) - this.mBorderView.getArrowCapSize()))));
        this.mBorderView.setPadding(0, margin, 0, margin);
        this.mBorderView.setLayoutParams(params);
        this.mBorderView.setClickable(true);
        addView(this.mBorderView);
        this.mListView = new ListView(getContext());
        int id = 10 + 1;
        this.mListView.setId(10);
        params = new RelativeLayout.LayoutParams(-1, rowHeight * 8);
        params.addRule(5, 1);
        params.addRule(7, 1);
        params.addRule(10);
        params.setMargins(this.mBorderView.getCornerCapSize(), 0, this.mBorderView.getCornerCapSize(), 0);
        this.mListView.setLayoutParams(params);
        this.mListView.setDivider(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.LISTVIEW_DIVIDER_COLOR)));
        this.mListView.setDividerHeight(1);
        this.mListView.setVerticalFadingEdgeEnabled(false);
        this.mListView.setVerticalScrollBarEnabled(false);
        this.mListDataAdapter = new TimerDurationDataAdapter(getContext());
        this.mListView.setAdapter(this.mListDataAdapter);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setChoiceMode(1);
        this.mListView.setSelector(new ColorDrawable(0));
        this.mBorderView.addView(this.mListView);
        this.mSelectedIndex = this.mListDataAdapter.setSelectionDuration(getCurrentDuration());
        this.mListView.post(new Runnable() {
            public void run() {
                TimerDurationPicker.this.mListView.setSelection(TimerDurationPicker.this.mSelectedIndex);
                View v = TimerDurationPicker.this.mListView.getChildAt(TimerDurationPicker.this.mSelectedIndex);
                if (v != null) {
                    v.requestFocus();
                }
            }
        });
    }

    int getCurrentDuration() {
        int duration = this.mTimerId * 5;
        try {
            duration = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(String.format(Locale.US, SettingsKey.SETTINGS_TIMER_DEFAULT_DURATION_KEY_TEMPLATE, new Object[]{Integer.valueOf(this.mTimerId)}), String.valueOf(this.mTimerId * 5)));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return duration;
    }

    void updateToPreferences() {
        int duration = this.mListDataAdapter.getSelectionDuration();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        String sKey = String.format(Locale.US, SettingsKey.SETTINGS_TIMER_DEFAULT_DURATION_KEY_TEMPLATE, new Object[]{Integer.valueOf(this.mTimerId)});
        Editor editor = pref.edit();
        editor.putString(sKey, String.format(Locale.US, "%d", new Object[]{Integer.valueOf(duration)}));
        editor.apply();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mListDataAdapter.setSelection(position);
        updateToPreferences();
        if (this.mListener != null) {
            this.mListener.onDurationSelected(true);
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!v.equals(this)) {
            return false;
        }
        if (event.getAction() == 0) {
            this.mListener.onDurationSelected(false);
        }
        return true;
    }

    public void relayoutBorder(Point point) {
        this.mArrowPosition = point;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) this.mBorderView.getLayoutParams();
        params.topMargin = (int) (((double) ((this.mArrowPosition.y - this.mBorderView.getCornerCapSize()) - (this.mBorderView.getArrowCapSize() / 2))) - (this.mBorderView.getAnchorPos() * ((double) ((this.mBorderView.getHeight() - (this.mBorderView.getCornerCapSize() * 2)) - this.mBorderView.getArrowCapSize()))));
        this.mBorderView.setLayoutParams(params);
    }
}
