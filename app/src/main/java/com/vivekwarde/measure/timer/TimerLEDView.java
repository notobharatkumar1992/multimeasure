package com.vivekwarde.measure.timer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vivekwarde.measure.R;
import com.vivekwarde.measure.custom_controls.LEDView;
import com.vivekwarde.measure.custom_controls.MyImageView;
import com.vivekwarde.measure.utilities.MiscUtility;

import java.lang.reflect.Array;

public class TimerLEDView extends LEDView {
    private Bitmap[][] mArrowBitmaps;
    private MyImageView[][] mArrowViews;

    public TimerLEDView(Context context) {
        super(context, 1);
    }

    protected void initSubviews() {
        LayoutParams params;
        super.initSubviews();
        int id = 9981;
        this.mArrowBitmaps = (Bitmap[][]) Array.newInstance(Bitmap.class, new int[]{2, 2});
        this.mArrowBitmaps[0][0] = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.timer_arrow_backlit)).getBitmap();
        this.mArrowBitmaps[0][1] = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), R.drawable.timer_arrow)).getBitmap();
        Matrix matrix = new Matrix();
        matrix.postRotate(BitmapDescriptorFactory.HUE_CYAN);
        this.mArrowBitmaps[1][0] = Bitmap.createBitmap(this.mArrowBitmaps[0][0], 0, 0, this.mArrowBitmaps[0][0].getWidth(), this.mArrowBitmaps[0][0].getHeight(), matrix, true);
        this.mArrowBitmaps[1][1] = Bitmap.createBitmap(this.mArrowBitmaps[0][1], 0, 0, this.mArrowBitmaps[0][1].getWidth(), this.mArrowBitmaps[0][1].getHeight(), matrix, true);
        int arrowOffset = (int) ((((double) ((BitmapDrawable) this.mLEDView[0][0].getDrawable()).getBitmap().getHeight()) * Math.atan((double) MiscUtility.degree2Radian(8.0f))) / 2.0d);
        AnimationDrawable animation = new AnimationDrawable();
        animation.addFrame(ContextCompat.getDrawable(getContext(), R.drawable.timer_arrow_backlit), 100);
        animation.addFrame(ContextCompat.getDrawable(getContext(), R.drawable.timer_arrow), 100);
        animation.setOneShot(false);
        this.mArrowViews = (MyImageView[][]) Array.newInstance(MyImageView.class, new int[]{3, 2});
        int i = 0;
        while (i < 3) {
            int j = 0;
            int id2 = id;
            while (j < 2) {
                this.mArrowViews[i][j] = new MyImageView(getContext());
                id = id2 + 1;
                this.mArrowViews[i][j].setId(id2);
                this.mArrowViews[i][j].setBitmaps(this.mArrowBitmaps[j][0], this.mArrowBitmaps[j][1]);
                params = new LayoutParams(-2, -2);
                params.addRule(5, this.mLEDView[i][0].getId());
                params.addRule(7, this.mLEDView[i][1].getId());
                if (j == 0) {
                    params.addRule(10);
                    this.mArrowViews[i][j].setPadding(arrowOffset, 0, 0, 0);
                } else {
                    params.addRule(3, this.mLEDView[i][0].getId());
                    this.mArrowViews[i][j].setPadding(0, 0, (int) (2.5d * ((double) arrowOffset)), 0);
                }
                this.mArrowViews[i][j].setLayoutParams(params);
                addView(this.mArrowViews[i][j]);
                j++;
                id2 = id;
            }
            i++;
            id = id2;
        }
        params = new LayoutParams(-2, -2);
        params.addRule(3, this.mArrowViews[0][0].getId());
        this.mLEDView[0][0].setLayoutParams(params);
        for (i = 0; i < 4; i++) {
            int j = 0;
            while (j < 2) {
                if (i != 0 || j != 0) {
                    params = (LayoutParams) this.mLEDView[i][j].getLayoutParams();
                    params.addRule(6, this.mLEDView[0][0].getId());
                    params.addRule(15, 0);
                    this.mLEDView[i][j].setLayoutParams(params);
                }
                j++;
            }
        }
    }

    public void startAnimate(int unit) {
        if (unit < 0) {
            unit = 0;
        }
        if (unit > 2) {
            unit = 2;
        }
        this.mArrowViews[unit][0].startAnimation(75);
        this.mArrowViews[unit][1].startAnimation(75);
    }

    public void stopAnimation(int unit) {
        if (unit >= 0 && unit <= 2) {
            this.mArrowViews[unit][0].stopAnimation();
            this.mArrowViews[unit][1].stopAnimation();
        }
    }

    public int getRightOfUnit(int unit) {
        if (unit < 0 || unit > 2) {
            return 0;
        }
        return (this.mColonView[unit].getLeft() + this.mColonView[unit].getRight()) / 2;
    }
}
