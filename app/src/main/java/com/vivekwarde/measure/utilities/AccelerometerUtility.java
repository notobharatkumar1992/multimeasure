package com.vivekwarde.measure.utilities;

public class AccelerometerUtility {
    public static final int CALIB_COUNT = 8;
    public static final int CALIB_LAND_HORZ_LEFT = 2;
    public static final int CALIB_LAND_HORZ_RIGHT = 3;
    public static final int CALIB_LAND_VERT_LEFT = 4;
    public static final int CALIB_LAND_VERT_RIGHT = 5;
    public static final int CALIB_PORT_HORZ_DOWN = 7;
    public static final int CALIB_PORT_HORZ_UP = 6;
    public static final int CALIB_PORT_VERT_DOWN = 1;
    public static final int CALIB_PORT_VERT_UP = 0;

    public static double validateAngle(double angle) {
        if (angle >= 0.0d) {
            return 4.71238898038469d - angle;
        }
        angle = (-angle) - 1.5707963267948966d;
        if (angle < 0.0d) {
            return 6.283185307179586d - (-angle);
        }
        return angle;
    }

    public static double reverseAngle(double angle) {
        return 6.283185307179586d - angle;
    }

    public static double flipAngle(double angle) {
        return -angle;
    }

    public static double to180(double angle) {
        if (angle > 3.141592653589793d) {
            return angle - 6.283185307179586d;
        }
        return angle;
    }

    public static double rotateAngle(double angle, int rotation) {
        if (rotation == 0) {
            return angle;
        }
        if (rotation == CALIB_LAND_HORZ_LEFT) {
            return angle - (((double) MiscUtility.getSign((int) angle)) * 3.141592653589793d);
        }
        if (rotation == CALIB_PORT_VERT_DOWN) {
            if (angle < -3.141592653589793d || angle > -1.5707963267948966d) {
                return angle - 1.5707963267948966d;
            }
            return angle + 4.71238898038469d;
        } else if (rotation != CALIB_LAND_HORZ_RIGHT) {
            return 0.0d;
        } else {
            if (angle < 1.5707963267948966d || angle > 3.141592653589793d) {
                return angle + 1.5707963267948966d;
            }
            return angle - 4.71238898038469d;
        }
    }

    public static double rotateArbitraryAngle(double angle, double deltaAngle) {
        double rotAngle = angle - deltaAngle;
        if (rotAngle < 0.0d) {
            rotAngle += 6.283185307179586d;
        }
        return rotAngle - (((double) (((int) Math.floor(rotAngle / 6.283185307179586d)) * CALIB_LAND_HORZ_LEFT)) * 3.141592653589793d);
    }

    public static int getCalibTypeWithRotation(int rotation, double ozy, double ozx, double oxy) {
        if (rotation == 0) {
            if (Math.abs(ozy) < 0.7853981633974483d) {
                return CALIB_PORT_HORZ_UP;
            }
            return 0;
        } else if (rotation == CALIB_LAND_HORZ_LEFT) {
            if (Math.abs(ozy) < 0.7853981633974483d) {
                return CALIB_PORT_HORZ_DOWN;
            }
            return CALIB_PORT_VERT_DOWN;
        } else if (rotation == CALIB_PORT_VERT_DOWN) {
            if (Math.abs(ozx) < 0.7853981633974483d) {
                return CALIB_LAND_VERT_LEFT;
            }
            return CALIB_LAND_HORZ_LEFT;
        } else if (rotation != CALIB_LAND_HORZ_RIGHT) {
            return 0;
        } else {
            if (Math.abs(ozx) < 0.7853981633974483d) {
                return CALIB_LAND_VERT_RIGHT;
            }
            return CALIB_LAND_HORZ_RIGHT;
        }
    }
}
