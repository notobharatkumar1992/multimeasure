package com.vivekwarde.measure.utilities;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.util.Log;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import java.util.Locale;

public class ImageUtility {
    private static final int FLIP_HORIZONTAL = 2;
    private static final int FLIP_VERTICAL = 1;
    private static final String tag = "ImageUtility";

    public static Bitmap createBitmap(Bitmap bitmap, int alpha) {
        Bitmap alphaBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(alphaBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        Paint paint = new Paint();
        paint.setAlpha(alpha);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return alphaBitmap;
    }

    public static Bitmap flipHorizontal(Bitmap bitmap) {
        return flip(bitmap, FLIP_HORIZONTAL);
    }

    public static Bitmap flipVertical(Bitmap bitmap) {
        return flip(bitmap, FLIP_VERTICAL);
    }

    private static Bitmap flip(Bitmap bitmap, int type) {
        Matrix matrix = new Matrix();
        if (type == FLIP_VERTICAL) {
            matrix.preScale(TextTrackStyle.DEFAULT_FONT_SCALE, GroundOverlayOptions.NO_DIMENSION);
        } else if (type == FLIP_HORIZONTAL) {
            matrix.preScale(GroundOverlayOptions.NO_DIMENSION, TextTrackStyle.DEFAULT_FONT_SCALE);
        } else {
            Log.i(tag, "Invalid flip type");
            return null;
        }
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap scale9Bitmap(Bitmap scale9Bitmap, int leftCapWidth, int rightCapWidth, int topCapHeight, int bottomCapHeight, int newWidth, int newHeight) {
        if (scale9Bitmap == null) {
            Log.i(tag, "Bitmap not set yet");
            return null;
        } else if (leftCapWidth + rightCapWidth >= newWidth || topCapHeight + bottomCapHeight >= newHeight) {
            Log.i(tag, String.format(Locale.US, "Kich co scale [%d, %d] phai lon hon kich co bitmap [%d, %d]!\nHoac kich co CAP qua to so voi bitmap :(", new Object[]{Integer.valueOf(newWidth), Integer.valueOf(newHeight), Integer.valueOf(scale9Bitmap.getWidth()), Integer.valueOf(scale9Bitmap.getHeight())}));
            return null;
        } else {
            Bitmap retBitmap = Bitmap.createBitmap(newWidth, newHeight, Config.ARGB_8888);
            drawScale9Bitmap(new Canvas(retBitmap), scale9Bitmap, leftCapWidth, rightCapWidth, topCapHeight, bottomCapHeight, newWidth, newHeight);
            return retBitmap;
        }
    }

    public static boolean drawScale9Bitmap(Canvas canvas, Bitmap scale9Bitmap, int leftCapWidth, int rightCapWidth, int topCapHeight, int bottomCapHeight, int newWidth, int newHeight) {
        if (scale9Bitmap == null) {
            Log.i(tag, "Bitmap not set yet");
            return false;
        } else if (leftCapWidth + rightCapWidth >= newWidth || topCapHeight + bottomCapHeight >= newHeight) {
            Log.i(tag, String.format(Locale.US, "Kich co scale [%d, %d] phai lon hon kich co bitmap [%d, %d]!\nHoac kich co CAP qua to so voi bitmap :(", new Object[]{Integer.valueOf(newWidth), Integer.valueOf(newHeight), Integer.valueOf(scale9Bitmap.getWidth()), Integer.valueOf(scale9Bitmap.getHeight())}));
            return false;
        } else {
            canvas.drawARGB(0, 0, 0, 0);
            Rect src = new Rect();
            Rect dst = new Rect();
            Paint paint = new Paint();
            int srcW = scale9Bitmap.getWidth();
            int srcH = scale9Bitmap.getHeight();
            int dstW = newWidth;
            int dstH = newHeight;
            src.set(0, 0, leftCapWidth, topCapHeight);
            dst.set(0, 0, leftCapWidth, topCapHeight);
            canvas.drawBitmap(scale9Bitmap, src, dst, null);
            src.set(srcW - rightCapWidth, 0, srcW, topCapHeight);
            dst.set(dstW - rightCapWidth, 0, dstW, topCapHeight);
            canvas.drawBitmap(scale9Bitmap, src, dst, null);
            src.set(0, srcH - bottomCapHeight, leftCapWidth, srcH);
            dst.set(0, dstH - bottomCapHeight, leftCapWidth, dstH);
            canvas.drawBitmap(scale9Bitmap, src, dst, null);
            src.set(srcW - rightCapWidth, srcH - bottomCapHeight, srcW, srcH);
            dst.set(dstW - rightCapWidth, dstH - bottomCapHeight, dstW, dstH);
            canvas.drawBitmap(scale9Bitmap, src, dst, null);
            canvas.save();
            src.set(leftCapWidth, 0, srcW - rightCapWidth, topCapHeight);
            paint.setShader(new BitmapShader(Bitmap.createBitmap(scale9Bitmap, src.left, src.top, src.width(), src.height()), TileMode.REPEAT, TileMode.REPEAT));
            dst.set(0, 0, (dstW - leftCapWidth) - rightCapWidth, topCapHeight);
            canvas.translate((float) leftCapWidth, 0.0f);
            canvas.drawRect(dst, paint);
            canvas.restore();
            canvas.save();
            src.set(leftCapWidth, srcH - bottomCapHeight, srcW - rightCapWidth, srcH);
            paint.setShader(new BitmapShader(Bitmap.createBitmap(scale9Bitmap, src.left, src.top, src.width(), src.height()), TileMode.REPEAT, TileMode.REPEAT));
            dst.set(0, 0, (dstW - leftCapWidth) - rightCapWidth, bottomCapHeight);
            canvas.translate((float) leftCapWidth, (float) (dstH - bottomCapHeight));
            canvas.drawRect(dst, paint);
            canvas.restore();
            canvas.save();
            src.set(0, topCapHeight, leftCapWidth, srcH - bottomCapHeight);
            paint.setShader(new BitmapShader(Bitmap.createBitmap(scale9Bitmap, src.left, src.top, src.width(), src.height()), TileMode.REPEAT, TileMode.REPEAT));
            dst.set(0, 0, leftCapWidth, (dstH - topCapHeight) - bottomCapHeight);
            canvas.translate(0.0f, (float) topCapHeight);
            canvas.drawRect(dst, paint);
            canvas.restore();
            canvas.save();
            src.set(srcW - rightCapWidth, topCapHeight, srcW, srcH - bottomCapHeight);
            paint.setShader(new BitmapShader(Bitmap.createBitmap(scale9Bitmap, src.left, src.top, src.width(), src.height()), TileMode.REPEAT, TileMode.REPEAT));
            dst.set(0, 0, rightCapWidth, (dstH - topCapHeight) - bottomCapHeight);
            Canvas canvas2 = canvas;
            canvas2.translate((float) (dstW - rightCapWidth), (float) topCapHeight);
            canvas.drawRect(dst, paint);
            canvas.restore();
            canvas.save();
            src.set(leftCapWidth, topCapHeight, srcW - rightCapWidth, srcH - bottomCapHeight);
            paint.setShader(new BitmapShader(Bitmap.createBitmap(scale9Bitmap, src.left, src.top, src.width(), src.height()), TileMode.REPEAT, TileMode.REPEAT));
            dst.set(0, 0, (dstW - leftCapWidth) - rightCapWidth, (dstH - topCapHeight) - bottomCapHeight);
            canvas.translate((float) leftCapWidth, (float) topCapHeight);
            canvas.drawRect(dst, paint);
            canvas.restore();
            return true;
        }
    }

    public static int getRightBottomCapSize(Bitmap bitmap) {
        for (int i = bitmap.getHeight() - 1; i >= 0; i--) {
            if (bitmap.getPixel(bitmap.getWidth() - 1, i) != 0) {
                return (bitmap.getHeight() - i) - 2;
            }
        }
        return 0;
    }

    public static int[] getNinePatchCapSizes(Bitmap scale9Bitmap) {
        int i;
        int[] capSizes = new int[]{0, 0, 0, 0};
        for (i = 0; i < scale9Bitmap.getWidth(); i += FLIP_VERTICAL) {
            if (scale9Bitmap.getPixel(i, 0) != 0) {
                capSizes[0] = i - 1;
                break;
            }
        }
        for (i = scale9Bitmap.getWidth() - 1; i >= 0; i--) {
            if (scale9Bitmap.getPixel(i, 0) != 0) {
                capSizes[FLIP_HORIZONTAL] = (scale9Bitmap.getWidth() - i) - 2;
                break;
            }
        }
        for (i = 0; i < scale9Bitmap.getHeight(); i += FLIP_VERTICAL) {
            if (scale9Bitmap.getPixel(0, i) != 0) {
                capSizes[FLIP_VERTICAL] = i - 1;
                break;
            }
        }
        for (i = scale9Bitmap.getHeight() - 1; i >= 0; i--) {
            if (scale9Bitmap.getPixel(0, i) != 0) {
                capSizes[3] = (scale9Bitmap.getHeight() - i) - 2;
                break;
            }
        }
        return capSizes;
    }

    public static int[] getContentCapSizes(Bitmap scale9Bitmap) {
        int i;
        int[] capSizes = new int[]{0, 0, 0, 0};
        for (i = 0; i < scale9Bitmap.getWidth(); i += FLIP_VERTICAL) {
            if (scale9Bitmap.getPixel(i, scale9Bitmap.getHeight() - 1) != 0) {
                capSizes[0] = i - 1;
                break;
            }
        }
        for (i = scale9Bitmap.getWidth() - 1; i >= 0; i--) {
            if (scale9Bitmap.getPixel(i, scale9Bitmap.getHeight() - 1) != 0) {
                capSizes[FLIP_HORIZONTAL] = (scale9Bitmap.getWidth() - i) - 2;
                break;
            }
        }
        for (i = 0; i < scale9Bitmap.getHeight(); i += FLIP_VERTICAL) {
            if (scale9Bitmap.getPixel(scale9Bitmap.getWidth() - 1, i) != 0) {
                capSizes[FLIP_VERTICAL] = i - 1;
                break;
            }
        }
        for (i = scale9Bitmap.getHeight() - 1; i >= 0; i--) {
            if (scale9Bitmap.getPixel(scale9Bitmap.getWidth() - 1, i) != 0) {
                capSizes[3] = (scale9Bitmap.getHeight() - i) - 2;
                break;
            }
        }
        return capSizes;
    }

    public static Bitmap scale9Bitmap(Bitmap scale9Bitmap, int newWidth, int newHeight) {
        int[] capSizes = getNinePatchCapSizes(scale9Bitmap);
        return scale9Bitmap(Bitmap.createBitmap(scale9Bitmap, FLIP_VERTICAL, FLIP_VERTICAL, scale9Bitmap.getWidth() - 2, scale9Bitmap.getHeight() - 2), capSizes[0], capSizes[FLIP_HORIZONTAL], capSizes[FLIP_VERTICAL], capSizes[3], newWidth, newHeight);
    }
}
