package com.vivekwarde.measure.vendors;

import java.lang.reflect.Array;

public final class Matrix {
    private final int M;
    private final int N;
    private final double[][] data;
//    Matrix A;

    public Matrix(int M, int N) {
        this.M = M;
        this.N = N;
        this.data = (double[][]) Array.newInstance(Double.TYPE, new int[]{M, N});
//        A = new Matrix(this.N, this.M);
    }

    public Matrix(double[][] data) {
        this.M = data.length;
        this.N = data[0].length;
        this.data = (double[][]) Array.newInstance(Double.TYPE, new int[]{this.M, this.N});
        for (int i = 0; i < this.M; i++) {
            for (int j = 0; j < this.N; j++) {
                this.data[i][j] = data[i][j];
            }
        }
    }

    private Matrix(Matrix A) {
        this(A.data);
    }

    public static Matrix random(int M, int N) {
        Matrix A = new Matrix(M, N);
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                A.data[i][j] = Math.random();
            }
        }
        return A;
    }

    public static Matrix identity(int N) {
        Matrix I = new Matrix(N, N);
        for (int i = 0; i < N; i++) {
            I.data[i][i] = 1.0d;
        }
        return I;
    }

    private void swap(int i, int j) {
        double[] temp = this.data[i];
        this.data[i] = this.data[j];
        this.data[j] = temp;
    }

    public Matrix transpose() {
        Matrix A = new Matrix(this.N, this.M);
        for (int i = 0; i < this.M; i++) {
            for (int j = 0; j < this.N; j++) {
                A.data[j][i] = this.data[i][j];
            }
        }
        return A;
    }

//    public Matrix plus(Matrix B) {
//        if (B.M == this.M && B.N == A.N) {
//            Matrix C = new Matrix(this.M, this.N);
//            for (int i = 0; i < this.M; i++) {
//                for (int j = 0; j < this.N; j++) {
//                    C.data[i][j] = A.data[i][j] + B.data[i][j];
//                }
//            }
//            return C;
//        }
//        throw new RuntimeException("Illegal matrix dimensions.");
//    }
//
//    public Matrix minus(Matrix B) {
//        if (B.M == this.M && B.N == A.N) {
//            Matrix C = new Matrix(this.M, this.N);
//            for (int i = 0; i < this.M; i++) {
//                for (int j = 0; j < this.N; j++) {
//                    C.data[i][j] = A.data[i][j] - B.data[i][j];
//                }
//            }
//            return C;
//        }
//        throw new RuntimeException("Illegal matrix dimensions.");
//    }

    public boolean eq(Matrix B) {
        Matrix A = new Matrix(this);
        if (B.M == this.M && B.N == A.N) {
            for (int i = 0; i < this.M; i++) {
                for (int j = 0; j < this.N; j++) {
                    if (A.data[i][j] != B.data[i][j]) {
                        return false;
                    }
                }
            }
            return true;
        }
        throw new RuntimeException("Illegal matrix dimensions.");
    }

    public Matrix times(Matrix B) {
        Matrix A = new Matrix(this);
        if (this.N != B.M) {
            throw new RuntimeException("Illegal matrix dimensions.");
        }
        Matrix C = new Matrix(A.M, B.N);
        for (int i = 0; i < C.M; i++) {
            for (int j = 0; j < C.N; j++) {
                for (int k = 0; k < A.N; k++) {
                    double[] dArr = C.data[i];
                    dArr[j] = dArr[j] + (A.data[i][k] * B.data[k][j]);
                }
            }
        }
        return C;
    }

    public Matrix solve(Matrix rhs) {

        if (this.M == this.N && rhs.M == this.N && rhs.N == 1) {
            int j;
            int k;
            Matrix A = new Matrix(this);
            Matrix b = new Matrix(rhs);
            for (int i = 0; i < this.N; i++) {
                int max = i;
                for (j = i + 1; j < this.N; j++) {
                    if (Math.abs(A.data[j][i]) > Math.abs(A.data[max][i])) {
                        max = j;
                    }
                }
                A.swap(i, max);
                b.swap(i, max);
                if (A.data[i][i] == 0.0d) {
                    throw new RuntimeException("Matrix is singular.");
                }
                double[] dArr;
                for (j = i + 1; j < this.N; j++) {
                    dArr = b.data[j];
                    dArr[0] = dArr[0] - ((b.data[i][0] * A.data[j][i]) / A.data[i][i]);
                }
                for (j = i + 1; j < this.N; j++) {
                    double m = A.data[j][i] / A.data[i][i];
                    for (k = i + 1; k < this.N; k++) {
                        dArr = A.data[j];
                        dArr[k] = dArr[k] - (A.data[i][k] * m);
                    }
                    A.data[j][i] = 0.0d;
                }
            }
            Matrix x = new Matrix(this.N, 1);
            for (j = this.N - 1; j >= 0; j--) {
                double t = 0.0d;
                for (k = j + 1; k < this.N; k++) {
                    t += A.data[j][k] * x.data[k][0];
                }
                x.data[j][0] = (b.data[j][0] - t) / A.data[j][j];
            }
            return x;
        }
        throw new RuntimeException("Illegal matrix dimensions.");
    }

    public double getData(int i, int j) {
        return this.data[i][j];
    }

    public void setData(int i, int j, double value) {
        this.data[i][j] = value;
    }
}
